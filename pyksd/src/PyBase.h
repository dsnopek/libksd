//--------------------------------------------------------------------------- 
//	pyksd -- Python KewL STuFf DirectMedium
//	Copyright 2001-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//	
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		_PyBase
//	
//	Purpose:		An interface for class which need to have 
//				virtual functions overloaded in python.
//	
//	Checked-in by:	$Author$
//					$Date$
//--------------------------------------------------------------------------- 
#ifndef _PYKSD_PyBaseH_
#define _PYKSD_PyBaseH_
//---------------------------------------------------------------------------
#include <Python.h>
//--------------------------------------------------------------------------- 
class _PyBase {
public:
	_PyBase() { self = NULL; }
	virtual ~_PyBase() { };

	PyObject* GetPyObject() const { return self; }
	void SetPyObject(PyObject* temp) { self = temp; }
protected:
	PyObject* self;
};
//--------------------------------------------------------------------------- 
#endif


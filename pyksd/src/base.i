
#include <Python.h>

class _PyBase {
public:
	void SetPyObject(PyObject*);
};

%wrapper %{
static PyObject* _PyBase_SetPyObject(PyObject *self, PyObject *args) {
	PyObject *resultobj;
	_PyBase *arg1 ;
	PyObject * obj0  = 0 ;
	PyObject * obj1  = 0 ;
   
	if(!PyArg_ParseTuple(args,(char *)"OO:PyApp_SetPyObject",&obj0,&obj1)) return NULL;
	if ((SWIG_ConvertPtr(obj0,(void **) &arg1, SWIGTYPE_p__PyBase,1)) == -1) return NULL;
    
	arg1->SetPyObject(obj1);
    
	Py_INCREF(Py_None);
	resultobj = Py_None;
	return resultobj;
}
%}

%native(_PyBase_SetPyObject) _PyBase_SetPyObject;


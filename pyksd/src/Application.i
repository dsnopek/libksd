
/* Define a concrete sub-class of TAppliation that calls python functions
 * on in all of the virtual C++ functions.
 */

//%import "Widget.i"
%import "base.i"

%header %{
#include "PyApp.h"
%}

class TApplication /*: public TWidget*/ {
public:
	void Run(int argc, char **argv);
	void CreateScreen(int width, int height, int bpp = 0);

	int GetWidth() const;
	int GetHeight() const;
};

class _PyApp : public TApplication, public _PyBase {
public:
	_PyApp();
	~_PyApp();
	
	//void Run(int argc, char **argv);
	//void CreateScreen(int width, int height, int bpp = 0);
};

%inline %{
_PyApp* GetApplication() {
	return (_PyApp*)ksd::Application;
}
%}

%{
namespace ksd {
%}

void Quit(int i = 0);
void InitLibrary();
void ShutdownLibrary();


%{
}; // namespace ksd
%}



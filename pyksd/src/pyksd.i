%module pyksd
%{
	#include <ksd/ksd.h>
	using namespace ksd;
%}

%include "types.i"
%include "base.i"

%include "Color.i"
%include "Application.i"

# append to the real *.py module
%shadow "extras.py";


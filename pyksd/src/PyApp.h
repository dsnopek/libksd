//--------------------------------------------------------------------------- 
//	pyksd -- Python KewL STuFf DirectMedium
//	Copyright 2001-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//	
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		TPyApp
//	
//	Purpose:		Deals with adapting TApplication to native Python
//	
//	Checked-in by:	$Author$
//					$Date$
//--------------------------------------------------------------------------- 
#ifndef _PYKSD_PyAppH_
#define _PYKSD_PyAppH_
//---------------------------------------------------------------------------
#include <Python.h>
#include <ksd/Application.h>
#include "PyBase.h"
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma interface
#endif
//--------------------------------------------------------------------------- 
class _PyApp : public ksd::TApplication, public _PyBase {
public:
	void Init();
	void Shutdown();
	void Cycle();
};
//--------------------------------------------------------------------------- 
#endif



import sys

# define the application class for use by pythoneers
class PyApp(_PyApp):
	def __init__(self):
		_PyApp.__init__(self)
		_PyBase_SetPyObject(self.this, self)
	def Init(self):
		pass
	def Shutdown(self):
		pass
	def Cycle(self):
		pass

# convenience function that works just like the C++ one
def KSD_MAIN(appclass):
	InitLibrary()
	app = appclass()
	result = app.Run(sys.argv)
	ShutdownLibrary()
	return result
	

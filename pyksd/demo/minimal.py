#!/usr/bin/env python

from pyksd import PyApp
from pyksd import KSD_MAIN

class TestApp(PyApp):
	def __init__(self):
		PyApp.__init__(self)
	def Init(self):
		print "Init'ing ..."
		self.CreateScreen(320, 200, 16)
	def Shutdown(self):
		print "Shutting down ..."

KSD_MAIN(TestApp)


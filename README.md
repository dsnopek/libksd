LibKSD
======

This is a small game engine that I, David Snopek, developed between 2000-2003
with the help of a handful of contributors.

It's not very interesting - **DON'T TRY TO ACTUALLY USE IT!** - I'd just like
to preserve it for my own nostalgia. :-)

At some point, I might replace the build system and modernize the code a little
so it's easier to build and try, but it's never going to have further real
development.

Building
--------

LibKSD depends on ancient versions of various software to build, so setting
those up is the first step.

### System (32-bit) ###

I've found that LibKSD only runs on 32-bit systems. I tested with Xubuntu 18.04
(the 32-bit version) running in a VM.

Download from here:

http://mirror.us.leaseweb.net/ubuntu-cdimage/xubuntu/releases/18.04/release/

Install some system dependencies:

    sudo apt install build-essential git libsdl1.2-dev libsdl-image1.2-dev \
        libsdl-ttf2.0-dev libsdl-mixer1.2-dev m4

### Ancient Dependencies ###

#### Python 2.1 ####

Download the source code:

https://www.python.org/ftp/python/2.1/

Build with:

    ./configure --prefix=$HOME/prj/libksd/build
    make
    make install

Update your PATH in order to use this as Python:

    export PATH="$HOME/prj/libksd/build/bin:$PATH"

#### SCons 0.90 ####

Download the source code:

https://sourceforge.net/projects/scons/files/OldFiles/scons-0.90.zip/download

Install with:

    python setup.py install

#### libsigc++ 1.2 ####

Download the source code:

https://download.gnome.org/sources/libsigc++/1.2/

Build with:

    ./configure --prefix=$HOME/prj/libksd/build
    make
    make install

### Building LibKSD ###

Copy the site.py.example file to site.py and change:

    AC_PREFIX = "/home/dsnopek/prj/libksd/build"

Then build with:

    export PKG_CONFIG_PATH="$HOME/prj/libksd/build/lib/pkgconfig"
    scons
    scons install
    scons demos

    # For some reason install doesn't copy the gui library.
    cp src/gui/libksd_gui* ../build/lib/

Running the demos
-----------------

Assuming you built libksd as above, you need tell the linker where to find the libraries:

    export LD_LIBRARY_PATH="$HOME/prj/libksd/build/lib"

Then go into the 'demo' directory and try running them!

I've been able to see *most* of them working, including:

 - fonttest
 - imgview
 - particle
 - widget

The interesting ones that I haven't seen running are:

 - opengl (it segfaults)
 - joytest (appears to be commented out)

Those would have been very cool to see, but need more "massaging" to work.


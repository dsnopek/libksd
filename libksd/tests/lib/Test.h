//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			Chuck Allison
//	Revised by:		David Snopek
//	Version:		$Revision$
//	
//	Objects:		TTest
//	
//	Purpose:		A unit testing object
//	
//	Checked-in by:	$Author$
//					$Date$
//---------------------------------------------------------------------------
#ifndef _TestH
#define _TestH
//---------------------------------------------------------------------------  
#include <string>
#include <iosfwd>
#include "export_ksd.h"
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma interface
#endif
//---------------------------------------------------------------------------  
// macros used to report tests and failures from within the test class
#define _test(cond) do_test(cond, #cond, __FILE__, __LINE__)
#define _fail(str) do_fail(str, __FILE__, __LINE__)
//---------------------------------------------------------------------------  
KSD_EXPORT_CLASS class TTest {
public:
	TTest(std::ostream* _osptr = NULL) { 
		Stream = _osptr;
		Passed = Failed = 0;
	}
	virtual ~TTest() { }

	// overload to define test functionality
	virtual void Run() = 0;

	long 			GetPassed() const 				{ return Passed; }
	long 			GetFailed() const 				{ return Failed; }
	std::ostream* 	GetStream() const 				{ return Stream; }
	void			SetStream(std::ostream* _osptr)	{ Stream = _osptr; } 

	void _succeed() 		{ Passed++; }
	virtual void Reset() 	{ Passed = Failed = 0; }
	virtual long Report();
protected:
	long Passed, Failed;

	void do_test(bool cond, const std::string& label, const char* filename, long line);
	void do_fail(const std::string& label, const char* filename, long line);
private:
	std::ostream* Stream;
};
//---------------------------------------------------------------------------
#endif

//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			Chuck Allison
//	Revised by:		David Snopek
//	Version:		$Revision$
//	
//	Objects:		TSuite
//	
//	Purpose:		A unit test suite.
//	
//	Checked-in by:	$Author$
//					$Date$
//---------------------------------------------------------------------------
#ifndef _SuiteH
#define _SuiteH
//---------------------------------------------------------------------------  
#include <map>
#include "Test.h"
#include "export_ksd.h"
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma interface
#endif
//---------------------------------------------------------------------------  
KSD_EXPORT_CLASS class TSuite : public TTest {
public:
	TSuite(const std::string& _name, std::ostream* _osptr = NULL) : TTest(_osptr) {
		Name = _name;
	}
	virtual ~TSuite();

	void Run();
	void Run(const std::string& _name);
	
	void Reset();
	long Report();

	std::string GetName() const { return Name; }

	void AddTest(TTest* _t, const std::string& _name); 
private:
	std::string Name;

	typedef std::map<std::string, TTest*> TestListType;
	typedef TestListType::iterator TestListIterator;
	TestListType TestList;

	// disallow these ops
	//TSuite(const TSuite&) { };
	//TSuite& operator=(const TSuite&);
};
//---------------------------------------------------------------------------  
#endif

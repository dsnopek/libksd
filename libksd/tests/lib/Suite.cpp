//--------------------------------------------------------------------------- 
//	$Header$
//	
//	Author:			Chuck Allison
//	Revised by:		David Snopek
//	Version:		$Revision$
//	
//	Checked-in by:	$Author$
//	
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma implementation
#endif
//---------------------------------------------------------------------------
#define KSD_BUILD_LIB
#include <iostream>
#include <stdexcept>
#include <cassert>
#include "Suite.h"

using namespace std;
//---------------------------------------------------------------------------
class TestSuiteError : public logic_error {
public:
	TestSuiteError(const string& s = "") : logic_error(s) { }
};
//---------------------------------------------------------------------------  
TSuite::~TSuite() 
{
	TestListIterator i;
	for(i = TestList.begin(); i != TestList.end(); i++) {
		delete (*i).second;
	}
	TestList.clear();
}
//---------------------------------------------------------------------------  
void TSuite::Run()
{
	TestListIterator i;

	Reset();
	
	for(i = TestList.begin(); i != TestList.end(); i++) {
		(*i).second->Run();
		Passed += (*i).second->GetPassed();
		Failed += (*i).second->GetFailed();
	}
}
//---------------------------------------------------------------------------  
void TSuite::Run(const std::string& _name)
{
	TestListIterator i = TestList.find(_name);
	
	if(i == TestList.end()) {
		cerr << "Cannot run test: " << _name << endl;
		cerr << "No test by that name!" << endl << flush;
		return;	
	}
	
	(*i).second->Run();
	Passed += (*i).second->GetPassed();
	Failed += (*i).second->GetFailed();
}
//---------------------------------------------------------------------------  
void TSuite::Reset()
{
	TestListIterator i;

	TTest::Reset();
	
	for(i = TestList.begin(); i != TestList.end(); i++) {
		(*i).second->Reset();
	}
}
//---------------------------------------------------------------------------  
long TSuite::Report()
{
	if(GetStream()) {
		#define STROUT (*GetStream())
		unsigned int i, TotalFailed = 0;
		TestListIterator e;

		// output suite name
		STROUT << "Suite \"" << Name << "\"\n=======";

		// ouput seperator
		for(i = 0; i < Name.size(); i++)
			STROUT << '=';
		STROUT << "=\n";

		// output test reports
		for(e = TestList.begin(); e != TestList.end(); e++) {
			TotalFailed += (*e).second->Report();
		}

		// ouput seperator
		for(i = 0; i < Name.size(); i++)
			STROUT << '=';
		STROUT << "=\n";

		return TotalFailed;
	}

	return Failed;	
}
//---------------------------------------------------------------------------  
void TSuite::AddTest(TTest* _t, const string& _name)
{
	if(_t == NULL) 
		throw TestSuiteError("Null test Suite::AddTest");
	else if(GetStream() != NULL && _t->GetStream() == NULL)
		_t->SetStream(GetStream());

	TestList.insert(make_pair(_name, _t));
	_t->Reset();
}
//---------------------------------------------------------------------------  


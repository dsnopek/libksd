//--------------------------------------------------------------------------- 
//	$Header$
//	
//	Author:			Chuck Allison
//	Revised by:		David Snopek
//	Version:		$Revision$
//	
//	Checked-in by:	$Author$
//	
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma implementation
#endif
//---------------------------------------------------------------------------
#define KSD_BUILD_LIB
#include <iostream>
#include <typeinfo>
#include "Test.h"

using namespace std;
//---------------------------------------------------------------------------  
void TTest::do_test(bool cond, const string& label, const char* filename, long line)
{
	if(!cond) {
		do_fail(label, filename, line);
	} else {
		_succeed();
	}
}
//---------------------------------------------------------------------------  
void TTest::do_fail(const string& label, const char* filename, long line)
{
	Failed++;
	if(Stream) {
		*Stream << typeid(*this).name() 
				<< "failure: (" << label << "), " 
				<< filename << "(line " << line << ")\n";
	}
}
//---------------------------------------------------------------------------  
long TTest::Report()
{
	if(Stream) {
		*Stream << "Test \"" << typeid(*this).name() << "\":\n"
				<< "\tPassed: " << Passed
				<< "\tFailed: " << Failed
				<< endl;
	}

	return Failed;
}
//---------------------------------------------------------------------------  

//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		TreeTest
//	
//	Purpose:		Tests the ksd::tree template.	
//	
//	Checked-in by:	$Author$
//					$Date$
//---------------------------------------------------------------------------
#ifndef TreeTestH
#define TreeTestH
//--------------------------------------------------------------------------- 
#include <iostream>
#include "Test.h"
#include "tree.h"

using namespace ksd;
using namespace std;
//---------------------------------------------------------------------------
class TreeTest : public TTest {
public:
	void Run() {
		TestBuildTree();
		TestErase();
		TestMove();
		TestIterator();
		TestLayerIterator();
		TestIteratorRange();
		ShowTree();
	}
private:
	// define some types
	typedef tree<std::string> tree_type;
	typedef tree_type::iterator tree_iterator;
	typedef tree_type::layer_iterator tree_layer_iterator;

	// our test tree
	tree_type FamilyTree;

	void ShowTree() {
		tree_iterator i;
		tree_type::reverse_layer_iterator_range children_list;
		tree_type::reverse_layer_iterator child;

		for(i = FamilyTree.begin(); i != FamilyTree.end(); i++) {
			children_list = FamilyTree.get_reverse_child_layer_range(i);
		
			cout << *i << ": ";
			for(child = children_list.begin(); child != children_list.end(); child++) {
				cout << *child << ", ";
			}
			cout << endl;
		}
	}

	void TestBuildTree() {
		FamilyTree.set_root("granddad");
		FamilyTree.insert_last("granddad", "mom");
		FamilyTree.insert_last("mom", "little johnny");
		FamilyTree.insert_last("mom", "me");
		FamilyTree.insert_last("granddad", "aunt jenny");
		FamilyTree.insert_last("aunt jenny", "cousin danny");
		FamilyTree.insert_last("aunt jenny", "cousin mari");
		FamilyTree.insert_last("granddad", "uncle mike");
		FamilyTree.insert_first("granddad", "long lost jack");
		FamilyTree.insert_first("long lost jack", "juley");
		FamilyTree.insert_first("long lost jack", "josh");

		// make sure that the root can't be set
		try {
			FamilyTree.set_root("grandma");
			_fail("FamilyTree.set_root(...)");
		} catch(...) {
			_succeed();
		}

		// test tree itegrity
		tree_iterator i = FamilyTree.begin();
		_test(*i++ == "granddad");
		_test(*i++ == "long lost jack");
		_test(*i++ == "josh");
		_test(*i++ == "juley");
		_test(*i++ == "mom");
		_test(*i++ == "little johnny");
		_test(*i++ == "me");
		_test(*i++ == "aunt jenny");
		_test(*i++ == "cousin danny");
		_test(*i++ == "cousin mari");
		_test(*i++ == "uncle mike");
	}

	void TestErase() {
		// disown jack
		FamilyTree.erase(FamilyTree.find_item("long lost jack"));

		// test tree itegrity
		tree_iterator i = FamilyTree.begin();
		_test(*i++ == "granddad");
		_test(*i++ == "mom");
		_test(*i++ == "little johnny");
		_test(*i++ == "me");
		_test(*i++ == "aunt jenny");
		_test(*i++ == "cousin danny");
		_test(*i++ == "cousin mari");
		_test(*i++ == "uncle mike");
	}

	void TestMove() {
		tree_iterator mom_iter = FamilyTree.find_item("mom"), i;

		if(mom_iter == FamilyTree.end()) {
			_fail("Could not find an element that should be there!");
			return;
		}

		// This function tests a number of things about the API of the
		// move functions:
		// 	- That the elements are move as expected
		// 	- That iterators are not invalidated
		// 	- That you cannot move past the end or beginning

		FamilyTree.move_to_back(mom_iter);

		// test tree itegrity
		i = FamilyTree.begin();
		_test(*i++ == "granddad");
		_test(*i++ == "aunt jenny");
		_test(*i++ == "cousin danny");
		_test(*i++ == "cousin mari");
		_test(*i++ == "uncle mike");
		_test(*i++ == "mom");
		_test(*i++ == "little johnny");
		_test(*i++ == "me");

		FamilyTree.move_to_back(mom_iter);

		// test tree itegrity
		i = FamilyTree.begin();
		_test(*i++ == "granddad");
		_test(*i++ == "aunt jenny");
		_test(*i++ == "cousin danny");
		_test(*i++ == "cousin mari");
		_test(*i++ == "uncle mike");
		_test(*i++ == "mom");
		_test(*i++ == "little johnny");
		_test(*i++ == "me");

		FamilyTree.move_to_front(mom_iter);

		// test tree itegrity
		i = FamilyTree.begin();
		_test(*i++ == "granddad");
		_test(*i++ == "mom");
		_test(*i++ == "little johnny");
		_test(*i++ == "me");
		_test(*i++ == "aunt jenny");
		_test(*i++ == "cousin danny");
		_test(*i++ == "cousin mari");
		_test(*i++ == "uncle mike");

		FamilyTree.move_to_front(mom_iter);

		// test tree itegrity
		i = FamilyTree.begin();
		_test(*i++ == "granddad");
		_test(*i++ == "mom");
		_test(*i++ == "little johnny");
		_test(*i++ == "me");
		_test(*i++ == "aunt jenny");
		_test(*i++ == "cousin danny");
		_test(*i++ == "cousin mari");
		_test(*i++ == "uncle mike");

		FamilyTree.move_backward(mom_iter);

		// test tree itegrity
		i = FamilyTree.begin();
		_test(*i++ == "granddad");
		_test(*i++ == "aunt jenny");
		_test(*i++ == "cousin danny");
		_test(*i++ == "cousin mari");
		_test(*i++ == "mom");
		_test(*i++ == "little johnny");
		_test(*i++ == "me");
		_test(*i++ == "uncle mike");

		FamilyTree.move_backward(mom_iter);

		// test tree itegrity
		i = FamilyTree.begin();
		_test(*i++ == "granddad");
		_test(*i++ == "aunt jenny");
		_test(*i++ == "cousin danny");
		_test(*i++ == "cousin mari");
		_test(*i++ == "uncle mike");
		_test(*i++ == "mom");
		_test(*i++ == "little johnny");
		_test(*i++ == "me");

		FamilyTree.move_backward(mom_iter);

		// test tree itegrity
		i = FamilyTree.begin();
		_test(*i++ == "granddad");
		_test(*i++ == "aunt jenny");
		_test(*i++ == "cousin danny");
		_test(*i++ == "cousin mari");
		_test(*i++ == "uncle mike");
		_test(*i++ == "mom");
		_test(*i++ == "little johnny");
		_test(*i++ == "me");

		FamilyTree.move_forward(mom_iter);

		// test tree itegrity
		i = FamilyTree.begin();
		_test(*i++ == "granddad");
		_test(*i++ == "aunt jenny");
		_test(*i++ == "cousin danny");
		_test(*i++ == "cousin mari");
		_test(*i++ == "mom");
		_test(*i++ == "little johnny");
		_test(*i++ == "me");
		_test(*i++ == "uncle mike");

		FamilyTree.move_forward(mom_iter);

		// test tree itegrity
		i = FamilyTree.begin();
		_test(*i++ == "granddad");
		_test(*i++ == "mom");
		_test(*i++ == "little johnny");
		_test(*i++ == "me");
		_test(*i++ == "aunt jenny");
		_test(*i++ == "cousin danny");
		_test(*i++ == "cousin mari");
		_test(*i++ == "uncle mike");

		FamilyTree.move_forward(mom_iter);

		// test tree itegrity
		i = FamilyTree.begin();
		_test(*i++ == "granddad");
		_test(*i++ == "mom");
		_test(*i++ == "little johnny");
		_test(*i++ == "me");
		_test(*i++ == "aunt jenny");
		_test(*i++ == "cousin danny");
		_test(*i++ == "cousin mari");
		_test(*i++ == "uncle mike");
	}

	// NOTE: although this test uses the iterator classes, the problems found stemming
	// from this test most likely have to do with basic list operations not setting all
	// the node-links properly.
	void TestIterator() {
		tree_iterator i;

		//
		// test next_sibling
		//
		
		i = FamilyTree.begin();
		_test(*i == "granddad");
		i.next_sibling();
		_test(i == FamilyTree.end());

		i = FamilyTree.begin();
		i.first_child();
		_test(*i == "mom");
		i.next_sibling();
		_test(*i == "aunt jenny");
		i.next_sibling();
		_test(*i == "uncle mike");
		i.next_sibling();
		_test(i == FamilyTree.end());

		// NOTE: test an important property of next_sibling - the fact that it 
		// ends on the next sibling of the parent

		// make point at little johnny
		i = FamilyTree.begin();
		i.first_child();
		i.first_child();
		_test(*i == "little johnny"); 
		i.next_sibling();
		_test(*i == "me"); 
		i.next_sibling();
		_test(*i == "aunt jenny"); 
		i.next_sibling();
		_test(*i == "uncle mike"); 
		i.next_sibling();
		_test(i == FamilyTree.end());

		// 
		// test previous_sibling
		//

		i = FamilyTree.begin();
		_test(*i == "granddad");
		i.previous_sibling();
		_test(*i == "granddad");

		i = FamilyTree.begin();
		i.last_child();
		_test(*i == "uncle mike");
		i.previous_sibling();
		_test(*i == "aunt jenny");
		i.previous_sibling();
		_test(*i == "mom");
		i.previous_sibling();
		_test(*i == "granddad");

		i = FamilyTree.find_item("me");
		_test(i != FamilyTree.end());
		_test(*i == "me");
		i.previous_sibling();
		_test(*i == "little johnny");
		i.previous_sibling();
		_test(*i == "mom");
		i.previous_sibling();
		_test(*i == "granddad");
	}

	// NOTE: this tests are identical as TestIterator except that we use layer_iterator
	// instead.  If we pass the former but not the latter, than its an iterator problem.
	void TestLayerIterator() {
		tree_layer_iterator i;

		//
		// test next_sibling
		//
		
		i = FamilyTree.layer_begin();
		_test(*i++ == "granddad");
		_test(i == FamilyTree.layer_end());

		i = FamilyTree.layer_begin();
		i.first_child();
		_test(*i++ == "mom");
		_test(*i++ == "aunt jenny");
		_test(*i++ == "uncle mike");
		_test(i == FamilyTree.layer_end());

		// NOTE: test an important property of next_sibling - the fact that it 
		// ends on the next sibling of the parent

		// make point at little johnny
		i = FamilyTree.layer_begin();
		i.first_child();
		i.first_child();

		_test(*i++ == "little johnny");
		_test(*i++ == "me");
		_test(*i++ == "aunt jenny");
		_test(*i++ == "uncle mike");

		// 
		// test previous_sibling
		//

		i = FamilyTree.layer_begin();
		_test(*i-- == "granddad");
		_test(*i-- == "granddad");

		i = FamilyTree.layer_begin();
		i.last_child();
		_test(*i-- == "uncle mike");
		_test(*i-- == "aunt jenny");
		_test(*i-- == "mom");
		_test(*i-- == "granddad");

		i = FamilyTree.find_item("me");
		_test(i != FamilyTree.layer_end());
		_test(*i-- == "me");
		_test(*i-- == "little johnny");
		_test(*i-- == "mom");
		_test(*i-- == "granddad");
	}

	void TestIteratorRange() {
		tree_type::layer_iterator top;
		tree_type::reverse_layer_iterator_range children;
		tree_type::reverse_layer_iterator i;
		
		int counter = 0;
		static char* result[] = { 
			"uncle mike", 
			"aunt jenny", 
			"cousin mari", 
			"cousin danny" };

		// start at the top of the tree
		top = FamilyTree.layer_begin();
		children = FamilyTree.get_reverse_child_layer_range(top.base());
		i = children.begin();
	
		while(i != children.end()) {
			// do the test: _test(*i == result[counter]);
			if(*i != result[counter]) _fail(std::string("*i == ") + result[counter]); 
			counter++;

			cout << *i << " "; // temp
			
			if(i.has_children()) {
				// noe traverse the children
				top = i.base();
				children = FamilyTree.get_reverse_child_layer_range(top.base());
				i = children.begin();
			} else ++i;
		}

		cout << endl; // temp
	}
};
//--------------------------------------------------------------------------- 
#endif

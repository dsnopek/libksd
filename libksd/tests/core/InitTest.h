//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		InitTest
//	
//	Purpose:		Pushes and pulls on the video system.
//	
//	Checked-in by:	$Author$
//					$Date$
//---------------------------------------------------------------------------
#ifndef InitTestH
#define InitTestH
//--------------------------------------------------------------------------- 
#include <iostream>
#include "Test.h"
#include "Application.h"

using namespace ksd;
using namespace std;
//---------------------------------------------------------------------------
class InitTest : public TTest {
private:
	class ExtraApp : public TApplication {
	public:
		void Init() { 
			CreateScreen(320, 200, 16, false); 
			InitTest::ExtraAppCreated = true;
		}
	};

public:
	static bool ExtraAppCreated;

	void Run() {
		// NOTE: won't pass this test with gcc3 !!
		//TestExtraApp(); // a very severe test!
		TestCreateScreen();
	}
private:
	void TestExtraApp() {
		// creating an extra app should fail!!
		try {
			ExtraApp app;
			app.Run(0, NULL);
		} catch(...) {
			_succeed();
		}

		// double check
		if(ExtraAppCreated == false) { _succeed(); return; }
		
		_fail("ExtraApp was successfully created!!");
	}

	void TestCreateScreen() {
		cout << "Testing the create screen function...\n"
		     << "The screen may flicker and windows may appear and disappear!\n"
		     << flush;

		// Try to crash the program!
		try {
			int Size = 100;
			bool fullscreen = false;
	
			try {
				Application->CreateScreen(0, 0);
				_fail("Applicaton->CreateScreen(0, 0);");
			} catch(...) {
				_succeed();
			}
			
			Application->CreateScreen(1, 1, 8, false);
			Application->CreateScreen(2, 2, 32, false);

			for(unsigned int i = 0; i < 4; i++) {
				Application->CreateScreen(Size, Size, 8, false);
				Application->CreateScreen(Size, Size, 16, false);
				Application->CreateScreen(Size, Size, 24, false);
				Application->CreateScreen(Size, Size, 32, false);

				Application->CreateScreen(Size, Size, 8, true);
				Application->CreateScreen(Size, Size, 16, true);
				Application->CreateScreen(Size, Size, 24, true);
				Application->CreateScreen(Size, Size, 32, true);

				Size += 100;	
			}
		} catch(...) {
			_fail("Failed CreateScreen test");
		}

		_succeed();
	}
};
//---------------------------------------------------------------------------  
bool InitTest::ExtraAppCreated = false;
//--------------------------------------------------------------------------- 
#endif

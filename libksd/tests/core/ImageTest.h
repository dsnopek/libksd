//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		ImageTest
//	
//	Purpose:		Makes sure that TImage operates as expected.
//	
//	Checked-in by:	$Author$
//					$Date$
//---------------------------------------------------------------------------
#ifndef ImageTestH
#define ImageTestH
//--------------------------------------------------------------------------- 
#include <iostream>
#include "Image.h"

using namespace ksd;
using namespace std;
//---------------------------------------------------------------------------
class ImageTest : public TTest {
public:
	void Run() {
		TestRefCounting();
		TestCopyOnWrite();
	}

	void TestRefCounting() {
		TImage* img1, *img2;

		img1 = new TImage;
		img1->Create(100, 100, 16);
		img1->Fill(TColor::red);

		img2 = new TImage(*img1);
		TCanvas canvas2(img2);
		_test(canvas2.GetPixel(50, 50) == TColor::red);
		delete img1;
		_test(canvas2.GetPixel(50, 50) == TColor::red);
		delete img2;
	}

	
	void TestCopyOnWrite() {
		TImage img1, img2;

		img1.Create(100, 100, 16);
		TCanvas canvas1(&img1);
		img1.Fill(TColor::red);

		img2 = img1;
		TCanvas canvas2(&img2);
		img2.Fill(TColor::blue);

		_test(canvas1.GetPixel(50, 50) == TColor::red);
		_test(canvas2.GetPixel(50, 50) == TColor::blue);
	}
};
//--------------------------------------------------------------------------- 
#endif

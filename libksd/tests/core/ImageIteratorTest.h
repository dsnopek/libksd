//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		ImageTest
//	
//	Purpose:		Makes sure that TImage operates as expected.
//	
//	Checked-in by:	$Author$
//					$Date$
//---------------------------------------------------------------------------
#ifndef ImageIteratorTestH
#define ImageIteratorTestH
//--------------------------------------------------------------------------- 
#include <iostream>
#include "ImageIterator.h"

using namespace ksd;
using namespace std;
//---------------------------------------------------------------------------
class ImageIteratorTest : public TTest {
public:
	void Run() {
		// TestSomething();
	}

	void TestSomething() {
		//_test( );	
	}
};
//--------------------------------------------------------------------------- 
#endif

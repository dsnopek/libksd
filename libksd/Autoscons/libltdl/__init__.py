
import SCons.Script.SConscript
import SCons.Node.FS
import Autoscons, os
import Autoscons.Util

def Build(env, build_dir = "#/libltdl", package = None):
	path = Autoscons.libltdl.__path__[0]
	exports = [ 'env', 'build_dir' ]
	if package != None:
		pkg = package
		exports.append('pkg')
	return SCons.Script.SConscript.SConscript([ os.path.join(path, "SConstruct") ], exports)


import Autoscons, os

Import("env", "config_h")

# start configuring for most of the options
sconf = Autoscons.Configure(env)

config_h.DefineQuoted("LTDL_SHLIB_EXT", env.subst("${SHLIBSUFFIX}"), \
	"Define to the extension used for shared libraries, say, \".so\".")
if os.name is 'dos':
	libtool_lib = "_libs"
else:
	libtool_lib = ".libs"
config_h.DefineQuoted("LTDL_OBJDIR", libtool_lib, \
	"Define to the sub-directory in which libtool stores uninstalled libraries.")

have_headers = []
def CheckHeaders(*headers, **kw):
	global have_headers

	try:
		multi = kw['multi']
	except KeyError:
		multi = 1
	try:
		required = kw['required']
	except KeyError:
		required = 0
	
	found = 0
	for h in Autoscons.Util.flatten(headers):
		found_this_one = 0

		if h in have_headers:
			found_this_one = 1
		elif sconf.CheckCHeader(h, "<>"):
			def_name = "HAVE_"
			for c in h.upper():
				if c in [ "/", "." ]:
					c = "_"
				def_name += c
			config_h.Define(def_name, 1, "Define if you have the <%s> header file." % h)
			have_headers.append(h)
			found_this_one = 1

		if multi:
			if required and not found_this_one:
				return 0
			else:
				found |= found_this_one
		else:
			if found_this_one:
				return 1
		
	return found

def CheckFuncs(*funcs, **kw):
	try:
		multi = kw['multi']
	except KeyError:
		multi = 1
	for f in Autoscons.Util.flatten(funcs):
		if sconf.CheckFunc(f):
			def_name = "HAVE_" + f.upper()
			config_h.Define(def_name, 1, "Define if you have the %s function." % f)
			if not multi:
				return

# like AC_CHECK_STDC
if not CheckHeaders("string.h", "stddef.h", "stdarg.h", "string.h", "float.h", required = 1):
	print >> sys.stderr, "Cannot build libltdl without the standard C headers"
	Exit(1)
# like AC_CHECK_DIRENT
if not CheckHeaders("dirent.h", "sys/ndir.h", "sys/dir.h", "ndir.h", multi = 0):
	print >> sys.stderr, "Cannot build libltdl without dirent"
	Exit(1)

CheckHeaders("errno.h", "malloc.h", "memory.h", "stdlib.h", "stdio.h", "ctype.h", "unistd.h")
CheckHeaders("dlfcn.h", "dl.h", "sys/dl.h", "dld.h")
CheckHeaders("string.h", "strings.h", multi = 0)

CheckFuncs("strchr", "index", multi = 0)
CheckFuncs("strrchr", "rindex", multi = 0)
CheckFuncs("memcpy", "bcopy", multi = 0)
CheckFuncs("memmove", "strcmp")

# check for libdl or equivalent
have_libdl = 0
if sconf.CheckLib("dl", "dlopen"):
	env.Append(LIBS = [ "dl" ])
	have_libdl = 1
elif sconf.CheckFunc("dlopen"):
	have_libdl = 1
elif sconf.CheckLib("svld", "dlopen"):
	env.Append(LIBS = [ "svld" ])
	have_libdl = 1
if have_libdl:
	config_h.Define("HAVE_LIBDL", 1, \
		"Define if you have the libdl library or equivalent.")
	config_h.Define("HAVE_DLOPEN", 1, \
		"Define if you have the dlopen function.")

# check for shl_load (HP-UX ?)
have_shl_load = 0
if sconf.CheckFunc("shl_load"):
	have_shl_load = 1
elif sconf.CheckLib("dld", "shl_load"):
	env.Append(LIBS = [ "dld" ])
	have_shl_load = 1
if have_shl_load:
	config_h.Define("HAVE_SHL_LOAD", 1, \
		"Define if you have the shl_load function.")

# check for GNU dld 
have_libdld = sconf.CheckLib("dld", "dld_link")
if have_libdld:
	env.Append(LIBS = [ "dld" ])
	config_h.Define("HAVE_DLD", 1, \
		"Define if you have the GNU dld library.")

# check for dlerror
if have_libdl:
	CheckFuncs("dlerror")

# check for argz
CheckHeaders("argz.h")
have_argz = "argz.h" in have_headers
if have_argz:
	have_error_t = sconf.CheckType("error_t", "#include <argz.h>")
else:
	have_error_t = sconf.CheckType("error_t")
if not have_error_t:
	config_h.Define("error_t", "int", \
		"Define to a type to use for 'error_t' if it is not otherwise available.")

if os.name in ('nt', 'dos'):
	# TODO: shouldn't this check the SYSTEMROOT env variable?
	search_path = r"C:\\WINDOWS\\SYSTEM;C:\\WINDOWS\\SYSTEM32"
else:
	search_path = "/lib:/usr/lib"
config_h.DefineQuoted("LTDL_SYSSEARCHPATH", search_path, \
	"Define this to the system library search path.")

# end configure
sconf.Finish()

# there are a few very important defines we haven't done yet:
#  - NEED_USCORE: if symbols have a leading underscore
#  - LTDL_DLOPEN_DEPLIBS: if dlopen should load the dependancies (ie. OS won't)
# Here we don't specify them, so we can test using this bootstrap library.
bootstrap_env = env.Copy()
bootstrap_env.Append(CPPFLAGS = " " + config_h.GetCPPFlags())
bootstrap_env.StaticLibrary(target = "ltdl_bootstrap", source = [ "ltdl.c" ])


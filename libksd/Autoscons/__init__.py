"""
Autoscons is a GNU AutoTools replacement, for SCons.

TODO:
 * Read and write libtool *.la's, pkg-config *.pc's, and *-config scripts (in python, dos batch, and sh-compatible shell).
 * Built-in tests for basic platform stuff, such as, determining that we are going to do a mingw or an msvc build so we can do platform specific configuration.
 * Functions for comparing versions strings to require a minimum version.
 * WIN32: Ensure that import libraries get installed in "lib/" and dlls in "dll/"
 * Port to python 1.5.2.
"""

__author__ = "David Snopek"

from Autoscons.Template import Template
from Autoscons.Tool import Tool
from Autoscons.Configure import Configure
from Autoscons.AutoBuild import Init, Package, Library, Program, Features, ConfHeader, Options

def _setup():
	import types
	# add StringTypes if this python doesn't have it
	if not hasattr(types, "StringTypes"):
		types.StringTypes = (types.StringType, types.UnicodeType)

_setup()

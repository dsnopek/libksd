
import os.path

import SCons.Builder
import SCons.Action

GzipAction = SCons.Action.Action("$GZIP $GZIPFLAGS - < $SOURCE > ${TARGET.abspath}")
GzipBuilder = SCons.Builder.Builder(action = '$GZIPCOM', suffix = '$GZIPSUFFIX')

def generate(env):
    """Add Builders and construction variables for zip to an Environment."""
    try:
        bld = env['BUILDERS']['Gzip']
    except KeyError:
        bld = GzipBuilder
        env['BUILDERS']['Gzip'] = bld

	env['GZIP']        = 'gzip'
    env['GZIPFLAGS']   = '--best'
    env['GZIPCOM']     = GzipAction
    env['GZIPSUFFIX']  = '.gz'

def exists(env):
    return env.Detect('gzip')


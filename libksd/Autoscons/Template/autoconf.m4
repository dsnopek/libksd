# Configure paths for Autoscons
# stolen from Karsten Laux
# stolen from Sam Lantinga 
# stolen from Manish Singh
# stolen back from Frank Belew
# stolen from Manish Singh
# Shamelessly stolen from Owen Taylor      

dnl AM_PATH_%(LIB_TARGET_UPPER)s([MINIMUM-VERSION, [ACTION-IF-FOUND [, ACTION-IF-NOT-FOUND]]])
dnl Test for lib%(LIB_TARGET)s, and define %(LIB_TARGET_UPPER)s_CFLAGS and %(LIB_TARGET_UPPER)s_LIBS
dnl
AC_DEFUN(AM_PATH_%(LIB_TARGET_UPPER)s,
[dnl
dnl Get the cflags and libraries from the %(LIB_TARGET)s-config script
dnl
AC_ARG_WITH(%(LIB_TARGET)s-prefix,[  --with-%(LIB_TARGET)s-prefix=PFX   Prefix where lib%(LIB_TARGET)s is installed (optional)],
            %(LIB_TARGET)s_prefix="$withval", %(LIB_TARGET)s_prefix="")
AC_ARG_WITH(%(LIB_TARGET)s-exec-prefix,[  --with-%(LIB_TARGET)s-exec-prefix=PFX Exec prefix where lib%(LIB_TARGET)s is installed
 (optional)],
            %(LIB_TARGET)s_exec_prefix="$withval", %(LIB_TARGET)s_exec_prefix="")    
  if test x$%(LIB_TARGET)s_exec_prefix != x ; then
     %(LIB_TARGET)s_args="$%(LIB_TARGET)s_args --exec-prefix=$%(LIB_TARGET)s_exec_prefix"
     if test x${%(LIB_TARGET_UPPER)s_CONFIG+set} != xset ; then
        %(LIB_TARGET_UPPER)s_CONFIG=$%(LIB_TARGET)s_exec_prefix/bin/%(LIB_TARGET)s-config
     fi
  fi
  if test x$%(LIB_TARGET)s_prefix != x ; then
     %(LIB_TARGET)s_args="$%(LIB_TARGET)s_args --prefix=$%(LIB_TARGET)s_prefix"
     if test x${%(LIB_TARGET_UPPER)s_CONFIG+set} != xset ; then
        %(LIB_TARGET_UPPER)s_CONFIG=$%(LIB_TARGET)s_prefix/bin/%(LIB_TARGET)s-config
     fi
  fi
 
  AC_PATH_PROG(%(LIB_TARGET_UPPER)s_CONFIG, %(LIB_TARGET)s-config, no)
  min_%(LIB_TARGET)s_version=ifelse([$1], ,0.3.32,$1)
  AC_MSG_CHECKING(for lib%(LIB_TARGET)s - version >= $min_%(LIB_TARGET)s_version)
  no_%(LIB_TARGET)s=""
  if test "$%(LIB_TARGET_UPPER)s_CONFIG" = "no" ; then
    no_%(LIB_TARGET)s=yes
    AC_MSG_RESULT(no)
    ifelse([$3], , :, [$3])
  else
    %(LIB_TARGET_UPPER)s_CFLAGS=`$%(LIB_TARGET_UPPER)s_CONFIG $%(LIB_TARGET)sconf_args --cflags`
    %(LIB_TARGET_UPPER)s_LIBS=`$%(LIB_TARGET_UPPER)s_CONFIG $%(LIB_TARGET)sconf_args --libs`
 
    %(LIB_TARGET)s_major_version=`$%(LIB_TARGET_UPPER)s_CONFIG $%(LIB_TARGET)s_args --version | \
           sed 's/\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\1/'`
    %(LIB_TARGET)s_minor_version=`$%(LIB_TARGET_UPPER)s_CONFIG $%(LIB_TARGET)s_args --version | \
           sed 's/\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\2/'`
    %(LIB_TARGET)s_micro_version=`$%(LIB_TARGET_UPPER)s_CONFIG $%(LIB_TARGET)s_args --version | \
           sed 's/\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\3/'`

    ac_save_CFLAGS="$CFLAGS"
    ac_save_LIBS="$LIBS"
    CFLAGS="$CFLAGS $%(LIB_TARGET_UPPER)s_CFLAGS"
    LIBS="$LIBS $%(LIB_TARGET_UPPER)s_LIBS"

    rm -f conf.%(LIB_TARGET)stest
    AC_TRY_RUN([
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char*
my_strdup (char *str)
{
  char *new_str;
  
  if (str)
    {
      new_str = malloc ((strlen (str) + 1) * sizeof(char));
      strcpy (new_str, str);
    }
  else
    new_str = NULL;
  
  return new_str;
}

int main ()
{
  int major, minor, micro;
  char *tmp_version;

  system ("touch conf.%(LIB_TARGET)stest");

  /* HP/UX 9 (*@#!) writes to sscanf strings */
  tmp_version = my_strdup("$min_%(LIB_TARGET)s_version");
  if (sscanf(tmp_version, "%%d.%%d.%%d", &major, &minor, &micro) != 3) {
     printf("%%s, bad version string\n", "$min_%(LIB_TARGET)s_version");
     exit(1);
   }

   if (($%(LIB_TARGET)s_major_version > major) ||
      (($%(LIB_TARGET)s_major_version == major) && ($%(LIB_TARGET)s_minor_version > minor)) ||
      (($%(LIB_TARGET)s_major_version == major) && ($%(LIB_TARGET)s_minor_version == minor) && ($%(LIB_TARGET)s_micro_version >= micro)))
    {
      return 0;
    }
  else
    {
      printf("\n*** '%(LIB_TARGET)s-config --version' returned %%d.%%d.%%d, but the minimum version\n", $%(LIB_TARGET)s_major_version, $%(LIB_TARGET)s_minor_version, $%(LIB_TARGET)s_micro_version);
      printf("*** of lib%(LIB_TARGET)s required is %%d.%%d.%%d.\n", major, minor, micro);
      printf("*** If %(LIB_TARGET)s-config was wrong, set the environment variable %(LIB_TARGET_UPPER)s_CONFIG\n");
      printf("*** to point to the correct copy of %(LIB_TARGET)s-config, and remove the file\n");
      printf("*** config.cache before re-running configure\n");
      return 1;
    }
}

],,no_%(LIB_TARGET)s=yes,[echo $ac_n "cross compiling; assumed OK... $ac_c"])
       
    CFLAGS="$ac_save_CFLAGS"
    LIBS="$ac_save_LIBS"
    if test "x$no_%(LIB_TARGET)s" = x ; then
      AC_MSG_RESULT(yes)	
      ifelse([$2], , :, [$2])
    else
      AC_MSG_RESULT(no)
      ifelse([$3], , :, [$3])	
    fi
   fi
  AC_SUBST(%(LIB_TARGET_UPPER)s_CFLAGS)
  AC_SUBST(%(LIB_TARGET_UPPER)s_LIBS)
 
]) 


import SCons, sys, os

def Template(name):
	dirname = sys.modules['Autoscons.Template'].__path__[0]
	path = os.path.join(dirname, name)

	if not os.path.exists(path):
		raise SCons.Errors.UserError, "Unknown template: " + path

	return SCons.Node.FS.default_fs.File(path)


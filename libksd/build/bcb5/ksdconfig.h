
/* ksd version */
#define KSD_VERSION 0.1.0

/* Enable libksd debugging */
/* #undef __DEBUG__ */

/* Enable libksd dangerous debugging */
/* #undef __DEBUG_DANGEROUSLY__ */

/* Enable image lib */
/* #define KSD_ENABLE_IMGLIB 1 */

/* Enable audio */
/* #define KSD_ENABLE_AUDIO 1 */

/* Enable opengl */
/* #undef KSD_ENABLE_OPENGL */

/* Enable fast timers */
/* #undef KSD_ENABLE_FAST_TIMERS */

/* Merge cycle and event thread */
/* #undef KSD_MERGE_CYCLE_AND_EVENT_THREAD */

/* Mark that libksd is NOT broken up into plugins */
#define KSD_NO_PLUGINS 1

/* Use TTF for fonts */
/* #define KSD_USE_TTF 1 */

/* Use SFont for fonts */
/* #define KSD_USE_SFONT 1 */

/* Use bloated draw code */
/* #undef KSD_BLOATED_DRAW_CODE */

/* Use small draw code */
#define KSD_SMALL_DRAW_CODE 1

/* Use debug draw code */
/* #undef KSD_DEBUG_DRAW_CODE */

/* We can't use variable argument macros */
#define KSD_NO_VAARG_MACROS 1

/* We can't use the pretty function macro */
#define KSD_NO_PRETTYFUNCTION_MACRO 1

/* Define the ksd plugin directory */
//#define KSD_PLUGIN_DIR "C:\Program Files\libksd\Plugins"

/* This system supports a native event thread */
/* #undef KSD_SUPPORT_EVENT_THREAD */



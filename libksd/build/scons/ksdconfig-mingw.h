/* This file controls how libksd is built.  Some of the defines are 
 * essential to build libksd with MingW, others can be
 * used to tailor your libksd build.
 */

/* These options are configurable.  Feel free to change them.
 * NOTE: Setting a define to 0 will not disable the option.  You
 * must comment it out!
 */

/* Enable libksd debugging */
/* #define __DEBUG__ */

/* Enable libksd dangerous debugging */
/* #define __DEBUG_DANGEROUSLY__ */

/* Enable image lib */
/* #define KSD_ENABLE_IMGLIB */

/* Use TTF for fonts */
/* #define KSD_USE_TTF */

/* Use SFont for fonts */
/* #define KSD_USE_SFONT */

/* Enable audio */
/* #define KSD_ENABLE_AUDIO */

/* Enable opengl */
/* #define KSD_ENABLE_OPENGL */

/* Use bloated draw code */
/* #define KSD_BLOATED_DRAW_CODE */

/* Use small draw code */
#define KSD_SMALL_DRAW_CODE

/* Use debug draw code */
/* #define KSD_DEBUG_DRAW_CODE */

/* DO NOT EDIT THE DEFINES BELOW THIS LINE!! */

/* ksd version */
#define KSD_VERSION 0.1.0

/* Merge cycle and event thread */
/* #define KSD_MERGE_CYCLE_AND_EVENT_THREAD */

/* Mark that libksd is NOT broken up into plugins */
#define KSD_NO_PLUGINS

/* We can't use variable argument macros */
/* #define KSD_NO_VAARG_MACROS */

/* Use shared library loading code */
/* #define KSD_ENABLE_SHARED_LIBRARY_SUPPORT */

/* We can't use the pretty function macro */
/* #define KSD_NO_PRETTYFUNCTION_MACRO */

/* Define the ksd plugin directory */
/* #define KSD_PLUGIN_DIR "C:\Program Files\libksd\Plugins" */

/* This system supports a native event thread */
/* #define KSD_SUPPORT_EVENT_THREAD */



import sys

if len(sys.argv) < 2:
	print >> sys.stderr, "Not enough arguments"
	sys.exit(1)

if "--cflags" in sys.argv:
	print "-IC:\\libksd-buildenv\\include -IC:\\libksd-buildenv\\include\\sigc++ -IC:\\libksd-buildenv\\lib\\config ",
if "--libs" in sys.argv:
	print "-LC:\\libksd-buildenv\\lib -lsigc++ ",
if "--version" in sys.argv:
	print "1.2.2",
print ""


//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	$Header$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Checked-in by:	$Author$
//
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma implementation
#endif
//--------------------------------------------------------------------------- 
#define KSD_BUILD_LIB
#include <iostream>
#include <iomanip>
#include "TextBox.h"
#include "Canvas.h"
#include "log.h"

using namespace ksd;
using namespace std;
//---------------------------------------------------------------------------  
TTextBox::TTextBox()
{
	Width = 0;
	wrap_type = WordWrap;
	ActivePos = 0;
	UseActiveAttr = false;
	
	LineList.push_back(Line());
}
//---------------------------------------------------------------------------  
unsigned int TTextBox::GetHeight() const
{
	unsigned int Height = 0;

	for(unsigned int i = 0; i < LineList.size(); i++) {
		Height += GetLineHeight(i) + GetLineSkip(i);
	}

	return Height;
}
//---------------------------------------------------------------------------  
void TTextBox::SetText(const std::string& new_text, const TextAttr& attr) 
{
	//  refuse to do without a valid text attr
	if(attr.IsValid()) {
		unsigned int cur_line = 0;

		// clear all old info
		Clear();

		// set text
		Text = new_text;

		// set-up explicit lines
		for(unsigned int i = 0; i < Text.size(); i++) {
			if(Text[i] == '\n') {
				Line temp;
				temp.Pos = i + 1;
				LineList.push_back(temp);
				cur_line++;
			} else {
				LineList[cur_line].Len++;
			}
		}

		// NOTE: add text attr calls recalc and OnChange
		SetTextAttr(attr);
	}
}
//---------------------------------------------------------------------------  
void TTextBox::SetWidth(Uint16 _width)
{
	Width = _width;
	ResetLineData();
}
//---------------------------------------------------------------------------  
void TTextBox::SetWrapType(WrapType _wt)
{
	// TODO: deal with recalcing properly on word wrap type changes
	if(wrap_type != _wt) {
		wrap_type = _wt;
		ResetLineData();
	}
}
//---------------------------------------------------------------------------  
void TTextBox::SetTextAttr(const TTextBox::TextAttr& attr, unsigned int Pos, unsigned int Len)
{
	// cannot add a text attr of length zero
	if(Len == 0) return;

	/*unsigned int End = Pos + Len, line = GetCurrentLine(Pos);
	TextInfoIter info_iter = GetCurrentInfo(Pos);
	TextAttr end_attr = (*info_iter).Attr;
	TextInfo ti;

	// get new attr into list
	if(Pos == (*info_iter).Pos) {
		(*info_iter).Attr = attr;
		info_iter++;
	} else {
		// build text info
		ti.Attr = attr;
		ti.Pos = Pos;

		// add new text info
		info_iter++;
		if(info_iter == TextInfoList.end()) {
			TextInfoList.push_back(ti);
		} else {
			TextInfoList.insert(info_iter, ti);
		}
	}

	// delete the in between text infos
	while(info_iter != TextInfoList.end() && End > (*info_iter).Pos + GetTextInfoLength(info_iter)) {
		end_attr = (*info_iter).Attr;
		TextInfoList.erase(info_iter);
	}

	// push last one back
	//if(info_iter != TextInfoList.end()) {
		//if(End > (*info_iter).Pos) {
		//	(*info_iter).Pos = End;
		//}

		// make and insert end info
		ti.Pos = End - 1;
		ti.Attr = end_attr;
		TextInfoList.insert(info_iter, ti);
	//}

	ResetLineData(line);*/

	// refuse to add an invalid text attr
	if(attr.IsValid()) {
		unsigned int End = Pos + Len;

		if(End < GetLength() && TextInfoList.size() > 0) {
			try {
				SetTextAttr(GetTextAttr(End), End);
			} catch(...) {
				LOG(1, "Unable to add text attr");
			}
		}
		
		if(Len > 1)
			RemoveTextAttrs(Pos, Len - 1);

		SetTextAttr(attr, Pos);
	}
}
//---------------------------------------------------------------------------  
void TTextBox::SetTextAttr(const TTextBox::TextAttr& text_attr, unsigned int Pos)
{
	// refuse to add an invalid text attr
	if(text_attr.IsValid()) {
		TextInfoIter info_iter = TextInfoList.begin(),
					 i = TextInfoList.begin();
		unsigned int line = GetCurrentLine(Pos);

		// build text info object
		TextInfo ti;	
		ti.Pos = Pos;
		ti.Attr = text_attr;

		// find location for new text info object
		while(info_iter != TextInfoList.end()) {
			if((*info_iter).Pos == Pos) { // replace old info
				*info_iter = ti;
				goto clear_line;
			} else if(Pos > (*info_iter).Pos) {
				i = ++info_iter;
			} else break;
		}

		// insert the text info
		if(i == TextInfoList.end()) {
			TextInfoList.push_back(ti);
		} else {
			TextInfoList.insert(i, ti);
		}

		// clear line data
		clear_line:
		// note: this calls recalc and OnChange for us
		ResetLineData(line);
	}
}
//---------------------------------------------------------------------------  
void TTextBox::SetActiveAttr(const TTextBox::TextAttr& text_attr, unsigned int Pos)
{
	UseActiveAttr = true;
	ActiveAttr = text_attr;
	ActivePos = Pos;

	ResetLineData(GetCurrentLine(Pos));
}
//---------------------------------------------------------------------------  
void TTextBox::AbondonActiveAttr()
{
	if(UseActiveAttr) {
		UseActiveAttr = false;
		ResetLineData(GetCurrentLine(ActivePos));
	}
}
//---------------------------------------------------------------------------  
void TTextBox::RemoveTextAttr(unsigned int Pos) 
{
	for(TextInfoIter i = TextInfoList.begin(); i != TextInfoList.end(); i++) {
		if((*i).Pos == Pos) {
			TextInfoList.erase(i);
			break;
		} else if((*i).Pos > Pos) break;
	}

	LineList[GetCurrentLine(Pos)].Clear();
}
//---------------------------------------------------------------------------  
void TTextBox::RemoveTextAttrs(unsigned int Start, unsigned int Len)
{
	unsigned int End = Start + Len, start_line, end_line;
	TextInfoIter i = TextInfoList.begin();

	while(i != TextInfoList.end()) {
		// increment until we passed the start of our removal
		if((*i).Pos == 0 || (*i).Pos < Start) i++;
		else {
			if((*i).Pos + GetTextInfoLength(i) <= End) {
				TextInfoList.erase(i++);
			} else break;
		}
	}

	// clear line heights
	start_line = GetCurrentLine(Start);
	end_line = GetCurrentLine(End);
	for(unsigned int i = start_line; i <= end_line; i++) {
		LineList[i].Clear();
	}
}
//---------------------------------------------------------------------------  
TTextBox::TextAttr& TTextBox::GetTextAttr(unsigned int CharPos)
{
	LineList[GetCurrentLine(CharPos)].Clear();

	if(UseActiveAttr && ActivePos == CharPos) {
		return ActiveAttr;
	} else {
		//return (*GetCurrentInfo(CharPos)).Attr;
		TextInfoIter iter = GetCurrentInfo(CharPos);
		if(iter == TextInfoList.end()) {
			// throw exception
			throw("No text attrs");
		} else {
			return (*iter).Attr;
		}
	}
}
//---------------------------------------------------------------------------  
TTextBox::TextAttr* TTextBox::GetUniqueTextAttr(unsigned int Pos)
{
	// find text attrs
	for(TextInfoIter i = TextInfoList.begin(); i != TextInfoList.end(); i++) {
		if((*i).Pos == Pos) return &(*i).Attr;
		else if((*i).Pos > Pos) break;
	}

	// clear line heights in case the attr is changed
	LineList[GetCurrentLine(Pos)].Clear();

	return NULL;
}
//---------------------------------------------------------------------------  
vector<TTextBox::TextAttr*> TTextBox::GetUniqueTextAttrs(unsigned int Start, unsigned int Len)
{
	unsigned int End = Start + Len, start_line, end_line;
	vector<TextAttr*> List;

	for(TextInfoIter i = TextInfoList.begin(); i != TextInfoList.end(); i++) {
		if((*i).Pos >= Start && (*i).Pos <= End) List.push_back(&(*i).Attr);
		else if((*i).Pos > End) break;
	}

	// clear line heights in case the attr is changed
	start_line = GetCurrentLine(Start);
	end_line = GetCurrentLine(End);
	for(unsigned int i = start_line; i <= end_line; i++) {
		LineList[i].Clear();
	}

	return List;
}
//---------------------------------------------------------------------------  
unsigned int TTextBox::GetCharPos(int X, int Y)
{
	TextChunk chunk(this);
	unsigned int Pos, line, line_end, done = 0;
	int cur_Y = 0, cur_X = 0, chunk_width;

	// find line based on y-pos
	for(line = 0; line < LineList.size(); line++) {
		cur_Y += GetLineHeight(line) + GetLineSkip(line);

		if(Y < cur_Y) break;
	}

	// if beyond end, then return last value
	if(line >= LineList.size()) {
		return Text.size();
	}

	// find x position
	line_end = GetLineEnd(line);
	for(chunk.GetFirst(line); ; chunk++) {
		if(chunk.Start >= line_end) {
			// return pos at the end of the line
			Pos = line_end;
			if(IsExplicitLine(line)) Pos--;
			break;
		}

		// increment done
		done += chunk.GetLength();

		// get current the current chunks width
		chunk_width = chunk.GetWidth();
		
		// deal with over shooting the pos
		if(cur_X + chunk_width > X) {
			Pos = GetLineStart(line) + done;
		
			// cut off characters
			for(unsigned int i = 0; i < chunk.GetLength() && cur_X + chunk_width > X; i++, Pos--) {
				chunk_width = chunk.GetWidth(i + 1);
			}

			// exit loop
			break;
		}

		// increment cur x
		cur_X += chunk.GetWidth();
	}

	return Pos;
}
//---------------------------------------------------------------------------  
unsigned int TTextBox::GetScreenPos(unsigned int Pos, int& X, int& Y)
{
	unsigned int line = GetCurrentLine(Pos), done = 0, end;
	TextChunk chunk(this, line);

	// clear X and Y
	X = Y = 0;

	// find line y pos
	for(unsigned int i = 0; i < line; i++) {
		Y += GetLineHeight(i) + GetLineSkip(i);
	}

	end = Pos - GetLineStart(line);
	while(done < end) {
		// check for empty chunks
		if(chunk.GetLength() == 0) break;

		// increment done
		//done += temp.size();
		done += chunk.GetLength();

		// deal with over shooting the pos
		if(done > end) {
			// increment X and exit the loop
			X += chunk.GetWidth(done - end);
			break;
		}

		// increment X and get next chunk
		X += chunk.GetWidth();
		chunk++;
	}

	return line;
}
//--------------------------------------------------------------------------- 
void TTextBox::Clear()
{
	Text = "";
	LineList.clear();
	TextInfoList.clear();
	UseActiveAttr = false;

	LineList.push_back(Line());
}
//---------------------------------------------------------------------------  
const char* TTextBox::GetText(int start, int len)
{
	return Text.substr(start, len).c_str();
}
//---------------------------------------------------------------------------  
void TTextBox::InsertText(const string& _text, unsigned int Pos)
{
	// refuse to insert text into an invalid text box
	if(IsValid()) {
		unsigned int line = GetCurrentLine(Pos);
		TextInfoIter info_iter;

		// we want to insert before the text info at Pos, because it marks the
		// end of the text info before it, which should be expanded.  The only
		// exception is the first text info.
		if(Pos > 0) {
			info_iter = GetCurrentInfo(Pos - 1);
		} else {
			info_iter = TextInfoList.begin();
		}

		// do actual insertion
		Text.insert(Pos, _text); 

		// expand lines
		LineList[line].Len += _text.size();
		for(unsigned int i = line + 1; i < LineList.size(); i++) {
			LineList[i].Pos += _text.size();
		}

		// move text infos
		for(++info_iter; info_iter != TextInfoList.end(); info_iter++) {
			(*info_iter).Pos += _text.size();
		}

		// deal with active attr
		if(UseActiveAttr && Pos == ActivePos) {
			SetTextAttr(ActiveAttr, Pos, _text.size());
			UseActiveAttr = false;
		}

		Recalc(line);
		OnChange();
	}
}
//---------------------------------------------------------------------------  
void TTextBox::RemoveText(unsigned int start, unsigned int len)
{
	// refuse to deal with an invalid text box
	if(IsValid()) {
		unsigned int start_line = GetCurrentLine(start),
			   		 end_line = GetCurrentLine(start + len);
		TextInfoIter info_iter;
		TextAttr end_attr = GetTextAttr(start + len);

		// check to see if we need to destroy some lines
		if(end_line > start_line) {
			unsigned int loss, gain;

			// remove actual characters
			Text.erase(start, len);

			// get gain and loss
			gain = loss = (LineList[start_line].Pos + LineList[start_line].Len) - start;
			for(unsigned int i = start_line + 1; i < end_line; i++) {
				loss += LineList[i].Len;
			}
			loss += (start + len) - LineList[end_line].Pos;
			gain = ((LineList[end_line].Pos + LineList[end_line].Len) - (start + len)) - gain;

			// remove lines
			for(unsigned int i = start_line + 1; i <= end_line; i++) {
				LineList.erase(LineList.begin() + i);
			}

			// adjust line props
			LineList[start_line].Len += gain;
			for(unsigned int i = start_line + 1; i < LineList.size(); i++) {
				LineList[i].Pos -= loss;
			}
		} else {
			Text.erase(start, len);
			LineList[start_line].Len -= len;
			for(unsigned int i = start_line + 1; i < LineList.size(); i++) {
				LineList[i].Pos -= len;
			}
		}
			
		// delete the in between text attrs
		RemoveTextAttrs(start, len);

		// check to see if first text info after erasure is even needed anymore
		info_iter = GetCurrentInfo(start);
		info_iter++;
		if((*info_iter).Attr == GetTextAttr((*info_iter).Pos - len)) {
			TextInfoList.erase(info_iter++);
		}

		// move the ones after, backward
		for(; info_iter != TextInfoList.end(); info_iter++) {
			(*info_iter).Pos -= len;
		}

		// deal with putting the end at front
		if(start + 1 < GetLength() && end_attr != GetTextAttr(start)) {
			SetTextAttr(end_attr, start);
		}

		// print useful diagnostic info
		//PrintDiag();
		
		// move lingering text up, so it can be passed down properly
		PullLine(start_line);
		
		RecalcAll(start_line);
		OnChange();
	}
}
//---------------------------------------------------------------------------  
void TTextBox::InsertLine(unsigned int Pos)
{
	unsigned int line = GetCurrentLine(Pos);
	Line temp_line;
	TextInfoIter info_iter;
	TextInfo info;

	// insert newline marker
	Text.insert(Pos, 1, '\n');

	// create new line
	temp_line.Pos = Pos + 1; 
	temp_line.Len = (LineList[line].Pos + LineList[line].Len) - Pos;

	// get text info of new line
	info_iter = GetCurrentInfo(temp_line.Pos); // should this be Pos instead ??
	info = *info_iter;

	// adjust the text infos
	for(++info_iter; info_iter != TextInfoList.end(); info_iter++) {
		(*info_iter).Pos++;
	}

	// adjust other lines
	LineList[line].Len = (Pos + 1) - LineList[line].Pos;
	for(unsigned int i = line + 1; i < LineList.size(); i++) {
		LineList[i].Pos++;
	}

	// actually add the line
	LineList.insert(LineList.begin() + line + 1, temp_line);

	// force line height of original line to be recalculated
	LineList[line].Clear();

	// deal with pushing ActiveAttr on to the next line if neccessary
	if(UseActiveAttr && ActivePos == Pos) {
		ActivePos++;
	}

	RecalcAll(line, line + 1);
	OnChange();
}
//---------------------------------------------------------------------------
void TTextBox::Draw(TCanvas* Canvas, int X, int Y, int start_line)
{
	for(unsigned int line = start_line; line < LineList.size(); line++) {
		// set up for this line
		Y += LineList[line].Ascent;

		// draw line
		DrawLine(Canvas, X, Y, line);

		// set up for next line
		Y = (Y - LineList[line].Descent) + LineList[line].LineSkip;
	}
}
//---------------------------------------------------------------------------  
void TTextBox::DrawLine(TCanvas* Canvas, int X, int Y, unsigned int line)
{
	TextChunk current_chunk(this, line);
	unsigned int line_end = LineList[line].Pos + LineList[line].Len;

	// loop through all the chunks on this line
	while(current_chunk.Start < line_end) {
		// draw and increment x
		current_chunk.Draw(Canvas, X, Y);
		X += current_chunk.GetWidth();

		// get the next chunk
		current_chunk++;
	}
}
//---------------------------------------------------------------------------  
void TTextBox::UpdateText(unsigned int Pos, unsigned int Len) 
{
	TextInfoIter end_info;
	unsigned int start_line, end_line;
	
	// find start line
	start_line = GetCurrentLine(Pos);

	// find end line
	end_info = GetCurrentInfo(Pos + Len);
	end_info++;
	end_line = GetCurrentLine((*end_info).Pos);

	// recalc these lines
	//RecalcAll(start_line, end_line);
	// NOTE: this doesn't really work as expected because ResetLineData calls
	// PullLine, which could invalidate end_line.
	for(unsigned int i = start_line; i <= end_line && i < LineList.size(); i++) {
		ResetLineData(i);
	}
}
//---------------------------------------------------------------------------  
void TTextBox::PrintDiag() 
{
	cout << "Text info list: \n";
	cout << "Pos " 
		 << "Size " << "Style " << "Font    "
		 << "Color" << endl;
	for(TextInfoIter i = TextInfoList.begin(); i != TextInfoList.end(); i++) {
		cout << (*i).Pos << "   "
			 << int((*i).Attr.FontAttr.Size) << "   "
			 << int((*i).Attr.FontAttr.Style) << "   "
			 << (*i).Attr.Font << " "
			 /*<< (*i).Attr.Color*/ << endl << flush;
	}
}
//---------------------------------------------------------------------------  
void TTextBox::Recalc(unsigned int line)
{
	do {
		if(Update(line)) line++;
		else return;
	} while(line < LineList.size());
}
//---------------------------------------------------------------------------  
void TTextBox::RecalcAll(unsigned int start_line, unsigned int end_line)
{
	bool Wrapped = false;

	// deal with invalid end line
	if(end_line <= start_line) end_line = LineList.size() - 1;

	// update lines
	for(unsigned int line = start_line; line <= end_line || Wrapped; line++) {
		Wrapped = Update(line);
	}
}
//---------------------------------------------------------------------------  
// TODO: don't take trailing whitespace into account when calculating line_width!!
bool TTextBox::Update(unsigned int line) 
{
	TextChunk current_chunk(this, line);
	int line_width = 0, temp;
	unsigned int diff = 0, line_end = GetLineEnd(line); //, done = 0;
	bool Wrapped = false;

	// loop through all the chunks on this line
	while(current_chunk.Start < line_end) {
		// deal with wrapping
		if(wrap_type != NoWrap) {
			for(;;) {
				temp = current_chunk.GetWidth();
				if(line_width + temp > Width) { // need to wrap more
					diff += DoWrap(current_chunk);

					// deal with a dead chunk
					if(current_chunk.GetLength() == 0) {
						if(current_chunk.End <= GetLineStart(line)) {
							// can't wrap line, give up
							goto end;
						} else {
							// get chunk before
							current_chunk--;
						}
					}
				} else if(diff > 0) { // pass the extra down to the next line
					// if this is the last line then append another
					if(LineList.size() == line + 1) {
						// need to add a new line
						Line temp_line;
						temp_line.Pos = LineList[line].Pos + LineList[line].Len;
						LineList.push_back(temp_line);
					} 
					// if this is an explicit new line, then insert another
					else if(IsExplicitLine(line)) {
						Line temp_line;
						temp_line.Pos = LineList[line].Pos + LineList[line].Len;
						LineList.insert(LineList.begin() + line + 1, temp_line);
					}

					// adjust line data
					LineList[line].Len -= diff;
					LineList[line + 1].Pos -= diff;
					LineList[line + 1].Len += diff;

					Wrapped = true;
					goto end; // return true;
				} else {
					line_width += temp;
					break;
				}
			}
		}

		// get the next chunk
		current_chunk++;
	}

	// check to see if line attrs are up to date
	end: // NOTE: I used a goto so that we can exit both loops in one fell swoop
	if(LineList[line].Ascent == 0) {
		// NOTE: i'm cheating since I know when ascent is zero, then all of the 
		// other line attribute will also be zero
		UpdateLineAttr(line);
	}

	// return false indicating that we didn't wrap anything
	return Wrapped;
}
//---------------------------------------------------------------------------  
void TTextBox::UpdateLineAttr(unsigned int line) 
{	
	unsigned int line_end = LineList[line].Pos + LineList[line].Len;
		
	// clear line attrs
	LineList[line].Clear();

	// don't allow attrs on that start on a new line character to be included 
	// in the line height calculations ...
	if(IsExplicitLine(line)) {
		line_end--;
	}

	// factor the first attr
	LineList[line].FactorAttr((*GetCurrentInfo(LineList[line].Pos)).Attr);

	// factor all the text infos on this line
	for(TextInfoIter i = TextInfoList.begin(); i != TextInfoList.end(); i++) {
		if((*i).Pos >= (LineList[line].Pos + 1) && (*i).Pos <= line_end) {
			LineList[line].FactorAttr((*i).Attr);
		} else if((*i).Pos > line_end) break;
	}

	// factor active attr
	if(UseActiveAttr && ActivePos >= LineList[line].Pos && ActivePos <= line_end) {
		LineList[line].FactorAttr(ActiveAttr);
	}
}
//---------------------------------------------------------------------------  
void TTextBox::PullLine(unsigned int line)
{
	if(!IsExplicitLine(line)) {
		unsigned int end = line;

		// clear line size data
		LineList[line].Clear();

		// find how many lines to erase and expand start line
		for(unsigned int i = line + 1; i < LineList.size(); i++) {
			// set end
			end = i;

			// lengthen start line
			LineList[line].Len += LineList[i].Len;

			// stop when we reach an explicit line
			if(IsExplicitLine(i)) break;
		}

		// erase extra lines
		for(unsigned int i = 0; i < end - line; i++) {
			LineList.erase(LineList.begin() + line + 1);
		}
	}
}
//---------------------------------------------------------------------------  
void TTextBox::ResetLineData(unsigned int start_line)
{
	if(TextInfoList.size() > 0) {
		// TODO: make an end_line!!
		for(unsigned int i = start_line; i < LineList.size(); i++) {
			// un-wrap the lines
			PullLine(i);

			// clear line size data
			LineList[i].Clear();
			
			// for now, manually update line size data
			//UpdateLineAttr(i);
		}

		// re-wrap and calc line size data
		RecalcAll(start_line);
		OnChange();
	}
}
//---------------------------------------------------------------------------  
TTextBox::TextInfoIter TTextBox::GetCurrentInfo(unsigned int Pos)
{
	TextInfoIter i, info = TextInfoList.begin();

	for(i = TextInfoList.begin(); i != TextInfoList.end(); i++) {
		if((*i).Pos == Pos)
			return i;
		else if((*i).Pos < Pos)
			info = i;
		else if((*i).Pos > Pos) 
			break;
	}

	return info;
}
//---------------------------------------------------------------------------  
unsigned int TTextBox::GetCurrentLine(unsigned int Pos)
{
	unsigned int line = 0;
	
	for(unsigned int i = 0; i < LineList.size(); i++) {
		if(Pos == LineList[i].Pos)
			return i;
		else if(Pos > LineList[i].Pos) 
			line = i;
		else if(Pos < LineList[i].Pos) 
			break;
	}

	return line;
}
//---------------------------------------------------------------------------  
unsigned int TTextBox::DoWrap(TextChunk& chunk)
{
	if(wrap_type != NoWrap) {
		if(wrap_type == CharWrap) {
			chunk.Cut(1);
			return 1;	
		} else if(wrap_type == WordWrap) {
			string temp = chunk.GetString();
			unsigned int diff = 1;

			// get passed leading spaces
			while(diff < chunk.GetLength() && Text[chunk.End - diff] == ' ') {
				diff++;
			}
		
			// count backwards to next word
			for(unsigned int i = chunk.End - diff; i > 0; i--) {
				if(Text[i - 1] != ' ') {
					diff++;
				} else break;
			}

			// cut-up chunk
			chunk.Cut(diff);
			
			return diff;
		}
	}

	return 0;
}
//---------------------------------------------------------------------------  
//
// TextChunk functions
// 
//---------------------------------------------------------------------------  
void TTextBox::TextChunk::GetFirst(unsigned int line)
{
	TextInfoIter temp;

	Start = Parent->GetLineStart(line);
	Line = line;
	info_iter = temp = Parent->GetCurrentInfo(Start);

	temp++;
	if(temp == Parent->TextInfoList.end()) {
		End = Start + Parent->GetLineLength(line);
	} else {
		unsigned int line_end = Parent->GetLineEnd(line);

		// clip to line end
		if((*temp).Pos > line_end) {
			End = line_end;
		} else {
			End = (*temp).Pos;
		}
	}
}
//---------------------------------------------------------------------------  
void TTextBox::TextChunk::GetNext()
{
	TextInfoIter old_info, next_info;
	unsigned int next_start, line_end;

	// start where the other chunk left off
	Start = End;

	// get the next text info
	next_info = old_info = info_iter;
	next_info++;

	// find out which info we are going to use and the next start
	if(next_info == Parent->TextInfoList.end()) {
		next_start = Parent->GetLength();
		info_iter = old_info;
	} else {
		// find info
		if(Start >= (*next_info).Pos) {
			info_iter = next_info++;
		} else {
			info_iter = old_info;
		}

		// find next start
		// NOTE: we have to do this check because we increment the next_info above
		if(next_info != Parent->TextInfoList.end()) {
			next_start = (*next_info).Pos;
		} else {
			next_start = Parent->GetLength();
		}
	}

	// find what line this chunk is on
	if(Parent->LineList.size() < Line + 1 && Start >= Parent->GetLineStart(Line + 1)) {
		Line++;
	}

	// get the chunk end
	line_end = Parent->GetLineEnd(Line);
	if(next_start > line_end) {
		End = line_end;
	} else {
		End = next_start;
	}
}
//---------------------------------------------------------------------------  
void TTextBox::TextChunk::GetPrevious()
{
	//
}
//---------------------------------------------------------------------------  
int TTextBox::TextChunk::GetWidth(unsigned int offset)
{
	// don't accept invalid chunks
	if(Start > End) {
		LOG(3, "TTextBox: Generated an invalid chunk");
		return 0;
	}

	// don't except empty chunks
	if(Start == End) {
		return 0;
	}

	int w, h;

	// get width
	(*info_iter).Attr.Font->GetTextSize(GetString(offset), (*info_iter).Attr.FontAttr, w, h);
	
	return w;
}
//---------------------------------------------------------------------------  
void TTextBox::TextChunk::Draw(TCanvas* Canvas, int X, int Y)
{
	// set-up drawing paramerters
	Canvas->Font = (*info_iter).Attr.Font;
	Canvas->FontAttr = (*info_iter).Attr.FontAttr;

	// draw string
	Canvas->DrawString(X, Y - (*info_iter).Attr.Font->GetAscent((*info_iter).Attr.FontAttr), GetString());
}
//---------------------------------------------------------------------------  
void TTextBox::TextChunk::Cut(unsigned int amount)
{
	if(amount >= GetLength()) {
		End = Start;
	} else {
		End -= amount;
	}
}
//---------------------------------------------------------------------------  


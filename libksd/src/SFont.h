/*
    SFONT - SDL Font Library 
    By: Karl Bartel <karlb@gmx.net>

	With special permisson from the author this version of SFont may
	be distributed under the terms of the LGPL.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* Modified by: David Snopek */

#ifndef _KSD_SFontH
#define _KSD_SFontH

#include <SDL/SDL_video.h>
#include "export_ksd.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Font struct.
 * To create a font, load the font file into an SDL_Surface* and then call
 * SFont_CreateFont(surface).
 */
typedef struct {
	/* PRIVATE */
	SDL_Surface *Surface;
	Uint16 CharPos[512];

	/* PUBLIC */
	int h, 				/* The height of the particular font */
		base;			/* The height of the base line */
} SFont_FontInfo;

/* SFont styles */
#define SFONT_NORMAL		0x00
#define SFONT_BOLD			0x01
#define SFONT_ITALIC		0x02
#define SFONT_UNDERLINE		0x04

/* Initializes the font.
 * Surface must contain a valid SFont image.  SFont_CreateFont takes ownership of
 * Surface.  DO NOT FREE IT MANUALLY!!  Use SFont_CloseFont
 */
KSD_EXPORT SFont_FontInfo* SFont_CreateFont(SDL_Surface* Surface);

/* Opens a font file.
 * Reads font from an image file of the specified name.  Usually is a *.PNG file
 * but it does NOT have to be!
 */
KSD_EXPORT SFont_FontInfo* SFont_OpenFont(const char* filename);

/* Resizes the font.
 * Stretches the font from an "origanal" point-size to a new point size.  The origanal
 * must be passed because point-size isn't really a part of SFont.
 */
KSD_EXPORT void SFont_ResizeFont(SFont_FontInfo* Font, Uint8 old_pt, Uint8 new_pt);

/* Colorize the font.
 * Recolorizes the font.  Not just for greyscale anymore!!  THe image is converted
 * to greyscale during the colorize process...
 */
KSD_EXPORT void SFont_ColorizeFont(SFont_FontInfo* Font, SDL_Color Color);

/* Stylize the font.
 * Applies the styles passed to this function to the font.  Best if done AFTER the
 * resize but BEFORE the colorize!!
 */
KSD_EXPORT void SFont_StylizeFont(SFont_FontInfo* Font, Uint8 Style);

/* Blits a string to a surface.
 * Dest = destination surface
 * Font = valid font object
 */
KSD_EXPORT void SFont_DrawString(SDL_Surface* Dest, SFont_FontInfo* Font, int X, int Y,
						  const char* text, SDL_Rect* ClipRect);

/* Returns the width of "text" in pixels */
KSD_EXPORT int SFont_TextWidth(SFont_FontInfo* Font, const char* text);

/* Gets dimensions of a string of text */
KSD_EXPORT void SFont_TextSize(SFont_FontInfo* Font, const char* text, int* X, int* Y);

/* Frees a font created by SFont_CreateFont */
KSD_EXPORT void SFont_CloseFont(SFont_FontInfo* Font);

#ifdef __cplusplus
}; /* extern "C" */
#endif

#endif


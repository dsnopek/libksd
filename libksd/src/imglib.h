/*
    libksd -- KewL STuFf DirectMedium
    Copyright 2000-2002 David Snopek

	With special permisson from the author this version of SFont may
	be distributed under the terms of the LGPL.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef _KSD_imglibH
#define _KSD_imglibH

#include <SDL/SDL_video.h>
#include "export_ksd.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Provides a C interface to IMGLIB that works if it is loaded as a plugin OR
   compiled directly into libksd */
   
KSD_EXPORT int IMGLIB_Load(SDL_Surface** Surface, char* filename, char type[3]);
KSD_EXPORT int IMGLIB_Save(SDL_Surface*  Surface, char* filename, char type[3]);

#ifdef __cplusplus
}; /* extern "C" */
#endif

#endif


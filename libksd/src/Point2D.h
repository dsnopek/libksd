//---------------------------------------------------------------------------
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//
//---------------------------------------------------------------------------
//	File:			$RCSfile$
//
//	Author:			David Snopek
//	Revised by:		Andrew Sterling Hanenkamp
//	Version:		$Revision$
//
//	Objects:		TPoint2D
//
//	Purpose:		Simple point class.
//
//	Checked-in by:	$Author$
//					$Date$
//---------------------------------------------------------------------------
#ifndef _KSD_Point2DH
#define _KSD_Point2DH
//---------------------------------------------------------------------------
#include <SDL/SDL_types.h>
#include "fixed.h"
//---------------------------------------------------------------------------
namespace ksd {
//---------------------------------------------------------------------------
template<class _T>
class Point2 {
public:
    Point2() { X = Y = 0; };
    Point2(_T x, _T y) { X = x; Y = y; };

    _T X, Y;

    inline void Set(_T x, _T y) {
        X = x;
        Y = y;
    }

    inline void Clear() { X = Y = 0; };

	template<class A>
    inline Point2<_T> operator+(const Point2<A>& p) const {
        Point2<_T> V;
        V.X = _T(p.X + X);
        V.Y = _T(p.Y + Y);
        return V;
    }

	template<class A>
    inline Point2<_T> operator-(const Point2<A>& p) const {
        Point2<_T> V;
        V.X = _T(X - p.X);
        V.Y = _T(Y - p.Y);
        return V;
    }

	template<class A>
    inline Point2<_T> operator*(const Point2<A>& p) const {
        Point2<_T> V;
        V.X = _T(X * p.X);
        V.Y = _T(Y * p.Y);
        return V;
    }

	template<class A>
    inline Point2<_T> operator/(const Point2<A>& p) const {
        Point2<_T> V;
        V.X = _T(X / p.X);
        V.Y = _T(Y / p.Y);
        return V;
    }

	template<class A>
    inline Point2<_T> operator+(A s) const {
        Point2<_T> V;
        V.X = _T(X + s);
        V.Y = _T(Y + s);
        return V;
    }

	template<class A>
    inline Point2<_T> operator-(A s) const {
        Point2<_T> V;
        V.X = _T(X - s);
        V.Y = _T(Y - s);
        return V;
    }

	template<class A>
    inline Point2<_T> operator*(A s) const {
        Point2<_T> V;
        V.X = _T(X * s);
        V.Y = _T(Y * s);
        return V;
    }

	template<class A>
    inline Point2<_T> operator/(A s) const {
        Point2<_T> V;
        V.X = _T(X / s);
        V.Y = _T(Y / s);
        return V;
    }

	template<class A>
    inline Point2<_T>& operator+=(const Point2<A>& p) {
        X += _T(p.X);
        Y += _T(p.Y);
        return *this;
    }

	template<class A>
    inline Point2<_T>& operator-=(const Point2<A>& p) {
        X -= _T(p.X);
        Y -= _T(p.Y);
        return *this;
    }

	template<class A>
    inline Point2<_T>& operator*=(const Point2<A>& p) {
        X *= _T(p.X);
        Y *= _T(p.Y);
        return *this;
    }

	template<class A>
    inline Point2<_T>& operator/=(const Point2<A>& p) {
        X /= _T(p.X);
        Y /= _T(p.Y);
        return *this;
    }

/* When trying to do Point2<_T> op Point2<_A>, Borland C favors
 * the scalar operators over the point ones.  Since we don't use the 
 * scalar functions, just remove them until a suitable replacement
 * or solution is found.
 */
#ifndef __BORLANDC__
	template<class A>
    inline Point2<_T>& operator+=(A s) {
        X += _T(s);
        Y += _T(s);
        return *this;
    }

	template<class A>
    inline Point2<_T>& operator-=(A s) {
        X -= _T(s);
        Y -= _T(s);
        return *this;
    }

	template<class A>
    inline Point2<_T>& operator*=(A s) {
        X *= _T(s);
        Y *= _T(s);
        return *this;
    }

	template<class A>
    inline Point2<_T>& operator/=(A s) {
        X /= _T(s);
        Y /= _T(s);
        return *this;
    }
#endif
};
//---------------------------------------------------------------------------
typedef Point2<signed short> sPoint2D;
typedef Point2<signed long> lsPoint2D;
typedef Point2<unsigned short> uPoint2D;
typedef Point2<unsigned long> luPoint2D;
typedef Point2<float>  flPoint2D;
typedef Point2<fixed>  fxPoint2D;
typedef Point2<double> dPoint2D;

// default point type
typedef sPoint2D TPoint2D;
//---------------------------------------------------------------------------
}; // namespace ksd
//---------------------------------------------------------------------------
#endif


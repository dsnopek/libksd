/*
    SFONT - SDL Font Library 
    By: Karl Bartel <karlb@gmx.net>

	With special permisson from the author this version of SFont may
	be distributed under the terms of the LGPL.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* Modified by: David Snopek */

#define KSD_BUILD_LIB
#include <SDL/SDL.h>
#include "SFont.h"
#include "imglib.h"
#include "stdlib.h"
#include "string.h"

#define SFONT_ALLCHARS \
	"!\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~"
#define SFONT_COUNT 96

/* Utility functions */

static Uint32 GetPixel(SDL_Surface *Surface, Uint16 X, Uint16 Y)
{
   Uint8 Bpp = Surface->format->BytesPerPixel;

   // Get the pixel
   switch(Bpp) {
      case 1:
         return *((Uint8 *)Surface->pixels + Y * Surface->pitch + X);
         break;
      case 2:
         return *((Uint16 *)Surface->pixels + Y * Surface->pitch/2 + X);
         break;
      case 3: { // Format/endian independent 
         /*Uint8 r, g, b, *bits;
         bits = ((Uint8 *)Surface->pixels)+Y*Surface->pitch+X*3;
         r = *((bits)+Surface->format->Rshift/8);
         g = *((bits)+Surface->format->Gshift/8);
         b = *((bits)+Surface->format->Bshift/8);
         return SDL_MapRGB(Surface->format, r, g, b);*/
         Uint32 val;
         Uint8* bits = ((Uint8 *)Surface->pixels)+Y*Surface->pitch+X*3;
         memcpy(&val, bits, 3);
         return val;
         break; }
      case 4:
         return *((Uint32 *)Surface->pixels + Y * Surface->pitch/4 + X);
         break;
   }

    return -1;
}

static void SetPixel(SDL_Surface* Surface, Uint16 X, Uint16 Y, Uint32 Color)
{
	Uint8 Bpp = Surface->format->BytesPerPixel;

	// Set the pixel
	switch(Bpp) {
		case 1:
			*((Uint8 *)Surface->pixels + Y * Surface->pitch + X) = (Uint8)Color;
			break;
		case 2:
			*((Uint16 *)Surface->pixels + Y * Surface->pitch/2 + X) = (Uint16)Color;
			break;
		case 3: {
			Uint8* bits = ((Uint8*)Surface->pixels) + Y * Surface->pitch + X * 3;
			memcpy(bits, &Color, 3);
			break; }
		case 4:
			*((Uint32 *)Surface->pixels + Y * Surface->pitch/4 + X) = Color;
			break;
	}
}

static void StretchBlit(SDL_Surface* src, SDL_Rect* src_rect, SDL_Surface* dest, SDL_Rect* dest_rect) 
{
	Uint16 X, Y, SX, SY;
	float RatioW, RatioH;
	Uint32 pixel;

	if(src_rect == NULL) {
		src_rect = malloc(sizeof(SDL_Rect));
		src_rect->x = 0;
		src_rect->y = 0;
		src_rect->w = src->w;
		src_rect->h = src->h;
	}

	if(dest_rect == NULL) {
		dest_rect = malloc(sizeof(SDL_Rect));
		dest_rect->x = 0;
		dest_rect->y = 0;
		dest_rect->w = dest->w;
		dest_rect->h = dest->h;
	}

	RatioW = (float)src_rect->w / dest_rect->w;
	RatioH = (float)src_rect->h / dest_rect->h;

	if(SDL_MUSTLOCK(dest)) SDL_LockSurface(dest);
	if(SDL_MUSTLOCK(src)) SDL_LockSurface(src);
	
	for(Y = 0; Y < dest_rect->h; Y++) {
		for(X = 0; X < dest_rect->w; X++) {
			/* get pixel from the source surface */
			SX = (Uint16)(RatioW * (float)X);
			SY = (Uint16)(RatioH * (float)Y);
			pixel = GetPixel(src, src_rect->x + SX, src_rect->y + SY);
			
			/* plot the pixel on the destination surface */
			SetPixel(dest, dest_rect->x + X, dest_rect->y + Y, pixel);
		}
	}

	if(SDL_MUSTLOCK(src)) SDL_UnlockSurface(src);
	if(SDL_MUSTLOCK(dest)) SDL_UnlockSurface(dest);
}

static void InitFont(SFont_FontInfo* Font, SDL_Surface* Surface)
{
	Uint32 Key;
	int x1, x2 = 0, i = 0;

	/* Initialize font */
	SDL_SetColorKey(Surface, SDL_SRCCOLORKEY, GetPixel(Surface, 0, Surface->h - 1));
	Font->Surface = Surface;
	Font->h = Surface->h;

	if(SDL_MUSTLOCK(Surface)) SDL_LockSurface(Surface);

	/* Get key */
	Key = GetPixel(Surface, 0, 0);

	/* Set up char pos */
	while(x2 < Surface->w) {
		x1 = x2;
		if(GetPixel(Surface, x1, 0) == Key) {
			Font->CharPos[i++] = x1;
			x2 = x1;
			while((x2 < Surface->w - 1) && (GetPixel(Surface, x2, 0) == Key)) x2++;
			Font->CharPos[i++] = x2;
		}
		x2++;
	}
	for(; i < 512; i += 2) {
		Font->CharPos[i] = x1;
		Font->CharPos[i + 1] = x2;
	}

	/* Get base of font --
	 * First check for an sfont extension, if not found a 
	 * perform calculation to find it..
	 */
	Font->base = 0;
	for(i = 1; i < Surface->h; i++) {
		if(GetPixel(Surface, 0, i) == Key) {
			Font->base = i;
			break;
		}
	}
	if(Font->base == 0) { 
		/* find the base by finding the top of the '_' character */
		int ofs;

		/* get char index */
		ofs = ('_' - 33) * 2 + 1;

		/* get center of character */
		x1 = (Font->CharPos[ofs] + Font->CharPos[ofs + 1]) / 2;

		for(i = 1; i < Surface->h; i++) {
			if(GetPixel(Surface, x1, i) != Surface->format->colorkey) {
				Font->base = i;
				break;
			}
		}

		/* if we still have no base then do simple yet inacurate calc */
		if(Font->base == 0) {
			Font->base = (Font->h * .7);
		}
	}
		
	if(SDL_MUSTLOCK(Surface)) SDL_UnlockSurface(Surface);
}

SFont_FontInfo* SFont_CreateFont(SDL_Surface* Surface)
{
	SFont_FontInfo* Font;

	if(Surface == NULL)
		return NULL;
		
	/* Allocate memory for font */
	if((Font = malloc(sizeof(SFont_FontInfo))) == NULL) 
		return NULL;

	/* Clear CharPos */
	/*memset(&Font->CharPos, 0, sizeof(Font->CharPos));*/

	/* Initialize font */
	InitFont(Font, Surface);

	return Font;
}

SFont_FontInfo* SFont_OpenFont(const char* filename)
{
	SDL_Surface* Surface;

	if(IMGLIB_Load(&Surface, (char*)filename, ""))
		return NULL;

	return SFont_CreateFont(Surface);
}

void SFont_ResizeFont(SFont_FontInfo* Font, Uint8 old_pt, Uint8 new_pt)
{
	SDL_Rect dest_rect, src_rect;
	SDL_Surface* Surface;
	SDL_PixelFormat* format;
	float Scale;
	int Width, Height;
	Uint32 Key;
	int ofs, X = 0, i;

	/* calculate scale factor */
	Scale = (float)new_pt / old_pt;

	/* calc new dimensions of new surface */
	Width = (SFont_TextWidth(Font, SFONT_ALLCHARS) * Scale) + 
			(SFONT_COUNT * 13);
	Height = (Font->Surface->h * Scale) 
			 + 2; // (add two for good measure :-)

	/* create new surface */
	format = Font->Surface->format;
	Surface = SDL_CreateRGBSurface(SDL_SWSURFACE, Width, Height, 
		format->BitsPerPixel, format->Rmask, format->Gmask, format->Bmask,
		format->Amask);

	/* Get key and write first marker */
	Key = SDL_MapRGB(Surface->format, 255, 0, 255);
	SetPixel(Surface, 0, 0, Key);

	/* Copy/Stretch */
	for(ofs = 1; ofs < 189; ofs += 2) {
		src_rect.x = Font->CharPos[ofs];
        src_rect.y = 1;
        src_rect.w = Font->CharPos[ofs+1] - Font->CharPos[ofs];
        src_rect.h = Font->Surface->h - 1;
        
    	dest_rect.x = X;
	    dest_rect.y = 1;
	    dest_rect.w = src_rect.w * Scale;
	    dest_rect.h = src_rect.h * Scale;

	    StretchBlit(Font->Surface, &src_rect, Surface, &dest_rect); 

	    X += dest_rect.w;

		/* write seperating key */
	    for(i = 0; i < 13; i++) {
	    	SetPixel(Surface, X++, 0, Key);
	    }
	}

	/* Write base pixel */
	SetPixel(Surface, 0, (Font->base * Scale), Key);

	/* destroy old surfce */
	SDL_FreeSurface(Font->Surface);

	/* initialize with new surface */
	InitFont(Font, Surface);
}

void SFont_ColorizeFont(SFont_FontInfo* Font, SDL_Color Color) 
{
	Uint16 X, Y, factor;
	Uint32 temp;
	SDL_Color pixel;

	/* recolorize */
	for(Y = 1; Y < Font->Surface->h; Y++) {
		for(X = 0; X < Font->Surface->w; X++) {
			/* Get pixel color */
			temp = GetPixel(Font->Surface, X, Y);
			if(temp != Font->Surface->format->colorkey) { /* ignore clear parts */
				SDL_GetRGB(temp, Font->Surface->format, 
					&pixel.r, &pixel.g, &pixel.b);

				/* transform pixel */
				factor = (pixel.r + pixel.g + pixel.b) / 3;
				pixel.r = (Color.r + factor) >> 1;
				pixel.g = (Color.g + factor) >> 1;
				pixel.b = (Color.b + factor) >> 1;

				/* write pixel back */
				temp = SDL_MapRGB(Font->Surface->format, 
					pixel.r, pixel.g, pixel.b);
				SetPixel(Font->Surface, X, Y, temp);
			}
		}
	}		
}

void SFont_StylizeFont(SFont_FontInfo* Font, Uint8 Style) 
{
	/* TODO: Implement!!
	 * 
	 * There are several reasons why this would be near impossible to implement
	 * in the current state of SFont.  I do, however, plan to implement this function
	 * in the future with some considerable changes to the SFont format..
	 * 
	 * BOLD: Very simple and compatible with the current SFont.  Just stretch each
	 * character to be a couple pixels larger BUT maintain the character size.
	 * 
	 * UNDERLINE:  Also very simple but won't work without adding a style state
	 * to the SFont_FontInfo struct.  Yes, a very minimal change but I won't make it
	 * until I do the others.  The underline would be created by stretching the '_'
	 * under all of the words at Draw time.  The reason this can't be cached is that
	 * some '_' have slanted ends which we want to see at the en of the line not on
	 * each character.
	 * 
	 * ITALIC: Modifying the images is pretty easy, but this is where the SFOnt file
	 * format will need to be completely changed.  THe character seperation markers
	 * in the current SFont appear on the top of the image.  If we were to slant each
	 * character, we would create large gaps inbetween them.  I plan to re-write the
	 * SFont code so that it accepts files with markers on the bottom as well as the 
	 * top.
	 */
}

void SFont_DrawString(SDL_Surface* Dest, SFont_FontInfo* Font, int x, int y, 
					  const char* text, SDL_Rect* clip_rect)
{
	Uint16 ofs, i, temp;
    SDL_Rect srcrect, dstrect; 
					
    for(i = 0; text[i] != '\0'; i++) {
        if(text[i] == ' ') {
            x += Font->CharPos[2] - Font->CharPos[1];
		} else {
		    ofs = (text[i] - 33) * 2 + 1;
		    
	        srcrect.w = (Font->CharPos[ofs+2] + Font->CharPos[ofs+1]) / 2 -
	        			(Font->CharPos[ofs] + Font->CharPos[ofs-1]) / 2;
	        srcrect.h = Font->Surface->h-1;
	    	dstrect.x = x - (float)(Font->CharPos[ofs] - Font->CharPos[ofs-1]) / 2;
	    	dstrect.y = y;
	    	srcrect.x = (Font->CharPos[ofs]+Font->CharPos[ofs-1])/2;
	        srcrect.y = 1;

			if(clip_rect != NULL) {
				int char_right, char_bottom, clip_right, clip_bottom;

				clip_right = clip_rect->x + clip_rect->w;
				clip_bottom = clip_rect->y + clip_rect->h;
				char_right = dstrect.x + srcrect.w;
				char_bottom = dstrect.y + srcrect.h;

				/* check to see if it is completely hidden (aka cull) */
				if(dstrect.x > clip_right) break;
				if(dstrect.y > clip_bottom) break;
				if(char_bottom < clip_rect->y) break;
				if(char_right < clip_rect->x) {
					x += Font->CharPos[ofs+1] - Font->CharPos[ofs];
					continue;
				}

				/* perform clipping */
				if(dstrect.x < clip_rect->x) {
					temp = (clip_rect->x - dstrect.x);
					srcrect.x += temp;
					srcrect.w -= temp;
					dstrect.x += temp;
				}
				if(dstrect.y < clip_rect->y) {
					temp = (clip_rect->y - dstrect.y);
					srcrect.y += temp;
					srcrect.h -= temp;
					dstrect.y += temp;
				}
				if(char_right > clip_right) {
					srcrect.w -= (char_right - clip_right);
				}
				if(char_bottom > clip_bottom) {
					srcrect.h -= (char_bottom - clip_bottom);
				}
			}

		    SDL_BlitSurface(Font->Surface, &srcrect, Dest, &dstrect); 

			/* Set-up for next letter */
	        x += Font->CharPos[ofs+1] - Font->CharPos[ofs];
		}
    }
}

void SFont_TextSize(SFont_FontInfo* Font, const char* text, int* X, int* Y)
{
	*X = SFont_TextWidth(Font, text);
	*Y = Font->h;
}

int SFont_TextWidth(SFont_FontInfo *Font, const char *text)
{
    int x=0,i=0;
    unsigned char ofs = 0;

    while (text[i]!='\0') {
        if (text[i]==' ') {
		    x+=Font->CharPos[2]-Font->CharPos[1];
		    i++;
		} else {
		    ofs=(text[i]-33)*2+1;
		    x+=Font->CharPos[ofs+1]-Font->CharPos[ofs];
		    i++;
		}
    }

    /* this is how Karl Bartel wrote it..  I don't know why he does like this since
     * it isn't the true with of the string.  But I'll it here since in case I am
     * missing something.
    return x+Font->CharPos[ofs+2]-Font->CharPos[ofs+1];
     */
    
    return x;
}

void SFont_CloseFont(SFont_FontInfo* Font)
{
	SDL_FreeSurface(Font->Surface);
	free(Font);
}

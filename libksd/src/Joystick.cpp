//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	$Header$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Checked-in by:	$Author$
//
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma implementation
#endif
//--------------------------------------------------------------------------- 
#define KSD_BUILD_LIB
#include <SDL/SDL.h>
#include "Joystick.h"
#include "exception.h"
#include "init.h"
#include "log.h"

using namespace ksd;
//--------------------------------------------------------------------------- 
TJoystickSubsystem ksd::TJoystickSubsystem::JoystickSys;

static void ShutdownJoystickModule();
_MODULE_SHUTDOWN(&ShutdownJoystickModule);
//--------------------------------------------------------------------------- 
TJoystickFilter::TJoystickFilter(TJoystick* joy) : TInputFilter(joy)
{
	//
}
//--------------------------------------------------------------------------- 
TJoystick::TJoystick(SDL_Joystick* _joy) : TInputDevice()
{
	LOG_CONSTRUCTOR("TJoystick");

	if(_joy == NULL) {
		// can't happen.  Throw exception??
		throw(ksd::exception("JoystickSubsystem", 
			"Trying to create a TJoystick object with a NULL handle"));
	}

	Handle = _joy;
}
//--------------------------------------------------------------------------- 
TJoystick::~TJoystick()
{
	LOG_DESTRUCTOR("TJoystick");

	if(SDL_WasInit(SDL_INIT_JOYSTICK)) {
		SDL_JoystickClose(Handle);
	} else {
		// NOTE: We don't consider trying to close a joystick when the joystick 
		// subsystem is un-initialized an error.  This can happen during out
		// of order, system shutdown.  This is due to programmer error, but 
		// we want the system to continue to shutdown (as this is still really
		// possible) and not crash.  Instead just log to tell the programmer.
		LOG(1, "Trying to close a joytick when the joystick subsystem is not initialized.  "
			"Did you forget to call TJoystick::GetInstance()->Close() ?");
	}
}
//--------------------------------------------------------------------------- 
void TJoystickSubsystem::Scan()
{
	LOG_FUNCTION("void TJoystickSubsystem::Scan()");

	// if not initialized, do it
	if(!SDL_WasInit(SDL_INIT_JOYSTICK)) {
		if(SDL_InitSubSystem(SDL_INIT_JOYSTICK) == -1) {
			throw(ksd::exception("JoystickSubsystem", 
				"Unable to initialize the joystick subsystem"));
		}
	}

	unsigned int num = SDL_NumJoysticks();
	SDL_Joystick* sdlj;

	LOG(9, "Number of Joysticks = %d", num);
	
	if(num != GetJoystickCount()) { // number of joysticks has changes
		// delete/remove old joysticks
		while(List.size()) {
			delete List.back();
			List.pop_back();
		}
		
		for(int i = 0; i < num; i++) {
			sdlj = SDL_JoystickOpen(i);
			if(sdlj) { // if successfully opened
				List.push_back(new TJoystick(sdlj));
			}
		}
	}
}
//--------------------------------------------------------------------------- 
void TJoystickSubsystem::Update()
{
	if(SDL_WasInit(SDL_INIT_JOYSTICK)) {
		SDL_JoystickUpdate();
	} else {
		if(List.size() > 0) {
			throw(ksd::exception("JoystickSubsystem", 
				"Library is in corrupt state!  Joystick list contains items but joystick sub-system is not initialized!"));
		} else {
			throw(ENotInitialized("JoystickSubsystem", 
				"Cannot update joysticks without first scanning"));
		}
	}
}
//--------------------------------------------------------------------------- 
void TJoystickSubsystem::Close()
{
	while(List.size()) {
		delete List.back();
		List.pop_back();
	}

	// close the joystick subsystem	
	if(SDL_WasInit(SDL_INIT_JOYSTICK)) {
		SDL_QuitSubSystem(SDL_INIT_JOYSTICK);
	}
}
//---------------------------------------------------------------------------  
void ShutdownJoystickModule()
{
	TJoystickSubsystem::GetInstance()->Close();
}
//--------------------------------------------------------------------------- 



//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
#ifndef _IMG_PPMH
#define _IMG_PPMH
//--------------------------------------------------------------------------- 
#include "Image.h"
#include "Export.h"

using namespace ksd;
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma interface
#endif
//--------------------------------------------------------------------------- 
class PpmLoader : public TFileFormat<TImage> {
public:
	PpmLoader() 
		: TFileFormat<TImage>("PPM", "ppm", "PPM File (*.ppm)", "SDL ppm loader", 1.0)
		{ }

	error Read(const char* filename, TImage* obj);
	error Write(const char* filename, TImage* obj);
	bool AutoDetect(const char* filename);
}; 
//--------------------------------------------------------------------------- 
#endif

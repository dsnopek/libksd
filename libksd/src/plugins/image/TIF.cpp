//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma implementation
#endif
//--------------------------------------------------------------------------- 
#define KSD_BUILD_LIB
#include <SDL/SDL_rwops.h>
#include <SDL/SDL_image.h>
#include "TIF.h"
//--------------------------------------------------------------------------- 
void TifLoader::Read(const char* filename, TImage* obj)
{
	SDL_Surface* surf;
	SDL_RWops* file;

	// open file 
	file = SDL_RWFromFile(filename, "rb");
	if(!file) {
		throw EFileError("TIF Loader", "Cannot open specified image file", filename);
	}

	surf = IMG_LoadTIF_RW(file);
	if(!surf) {
		SDL_RWclose(file);
		throw EFileError("TIF Loader", "Unable to read TIF image from specified file", filename);
	}
	SDL_RWclose(file);
	
	obj->SetSurface(surf);
}
//--------------------------------------------------------------------------- 
void TifLoader::Write(const char* filename, TImage* obj)
{
	// not supported error
	throw EUnsupported("TIF Loader", "Write of TIF files is not currently supported.");
}
//--------------------------------------------------------------------------- 
bool TifLoader::AutoDetect(const char* filename)
{
	SDL_RWops* file;
	bool can_read;

	// open file 
	file = SDL_RWFromFile(filename, "rb");
	if(!file) return false;

	can_read = IMG_isTIF(file);
	SDL_RWclose(file);

	return can_read;
}
//--------------------------------------------------------------------------- 

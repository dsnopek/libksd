//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
#define KSD_BUILD_LIB
#include "ImagePlugin.h"

#include "BMP.h"
#include "GIF.h"
#include "PCX.h"
#include "JPG.h"
#include "TIF.h"
#include "PNG.h"
//--------------------------------------------------------------------------- 
IMPLEMENT_PLUGIN_FUNC void PluginImage(TImageLoader* sys)
{
	// TODO: add the new SDL_image file formats
	sys->Register(new BmpLoader());
	sys->Register(new GifLoader());
	sys->Register(new PcxLoader());
	sys->Register(new JpgLoader());
	sys->Register(new TifLoader());
	sys->Register(new PngLoader());
}
//--------------------------------------------------------------------------- 

//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma implementation
#endif
//--------------------------------------------------------------------------- 
#include <SDL/SDL_rwops.h>
#include <SDL/SDL_image.h>
#include "PPM.h"
//--------------------------------------------------------------------------- 
error PpmLoader::Read(const char* filename, TImage* obj)
{
	SDL_Surface* surf;
	SDL_RWops* file;

	// open file 
	file = SDL_RWFromFile(filename, "rb");
	if(!file) {
		return error(IMGLIB_ERROR, IMGLIB_ECANNOTOPENFILE,
					 "Cannot open specified image file",
					 EA_NOTIFY | EA_KILL);
	}

	surf = IMG_LoadPPM_RW(file);
	if(!surf) {
		SDL_RWclose(file);
		return error(IMGLIB_ERROR, IMGLIB_EUNABLETOREADIMAGE,
					 "Unable to read PPM image from specified file",
					 EA_NOTIFY | EA_KILL);
	}
	SDL_RWclose(file);
	
	obj->SetSurface(surf);

	return success;
}
//--------------------------------------------------------------------------- 
error PpmLoader::Write(const char* filename, TImage* obj)
{
	// not supported error
	return error(IMGLIB_ERROR, IMGLIB_EWRITENOTSUPPORTED,
				 "Write of PPM files is not currently supported",
				 EA_NOTIFY | EA_KILL);
}
//--------------------------------------------------------------------------- 
bool PpmLoader::AutoDetect(const char* filename)
{
	SDL_RWops* file;
	bool can_read;

	// open file 
	file = SDL_RWFromFile(filename, "rb");
	if(!file) return false;

	can_read =  IMG_isPPM(file);
	SDL_RWclose(file);

	return can_read;
}
//--------------------------------------------------------------------------- 

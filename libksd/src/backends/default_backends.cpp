//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	$Header$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Checked-in by:	$Author$
//	
//--------------------------------------------------------------------------- 
#define KSD_BUILD_LIB
#include "default_backends.h"
#include "ksdconfig.h"

// we always have SDL
#include "backends/sdl/BackendSDL.h"

#ifdef KSD_ENABLE_OPENGL
#include "backends/ogl/BackendGL.h"
#endif

using namespace ksd;
//---------------------------------------------------------------------------  
void ksd::LoadDefaultBackends(TBackendSystem* System)
{
	// always have sdl!
	System->AddBackend(new TBackendSDL);

	#ifdef KSD_ENABLE_OPENGL
	System->AddBackend(new TBackendGL);
	#endif
}
//---------------------------------------------------------------------------


//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		TEventDispatcherSDL
//	
//	Purpose:		Performs SDL event dispatching.
//	
//	Checked-in by:	$Author$
//					$Date$
//---------------------------------------------------------------------------
#ifndef _KSD_EventDispatcherSDLH_
#define _KSD_EventDispatcherSDLH_
//---------------------------------------------------------------------------  
#include "EventDispatcher.h"
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma interface
#endif
//---------------------------------------------------------------------------
namespace ksd {
//---------------------------------------------------------------------------
class TEventDispatcherSDL : public TEventDispatcher {
public:
	TEventDispatcherSDL(TWidgetTree* Tree) : TEventDispatcher(Tree) { }

	void PollEvents();
protected:
	void DoSetJoystickEvents(bool enable);

	// sdl doesn't require any of these
	void DoSetJoystickAxisRepeat(bool) { }; 
	void DoSetJoystickCursor(bool) { };
	void DoSetJoystickCursorSensitivity(unsigned int) { };
	void DoSetJoystickCursorNum(unsigned int) { };
};
//---------------------------------------------------------------------------  
}; // namespace ksd
//---------------------------------------------------------------------------  
#endif


//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	$Header$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Checked-in by:	$Author$
//	
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma implementation
#endif
//---------------------------------------------------------------------------
#define KSD_BUILD_LIB
#include <SDL/SDL_events.h>
#include "Application.h"
#include "EventDispatcherSDL.h"

using namespace ksd;
//---------------------------------------------------------------------------
void TEventDispatcherSDL::PollEvents()
{
	SDL_Event event;

	while(SDL_PollEvent(&event)) {
		// quit when close window button is pressed
		switch(event.type) {
			case SDL_QUIT:
				//if(AllowQuit()) // <-- bad name pick another
				Quit(0);
				break;
			case SDL_ACTIVEEVENT:
				switch(event.active.state) {
					case SDL_APPMOUSEFOCUS:
						if(event.active.gain == 1) { // gain
							HandleEnterRoot();
						} else { // loss
							HandleExitRoot();
						}
						break;
					case SDL_APPINPUTFOCUS:
						// on loss = take keyboard focus away from all
						// on gain = re-assign keyboard focus to last with
						break;
					case SDL_APPACTIVE:
						// on loss = suspend all threads (including this one)
						// on gain = restart all threads (including this one)
						// NOTE: in retrospect this is very dumb idea, although 
						// useful in some cases. maybe provide an option
						break; 
				}
				break;
			case SDL_KEYDOWN:
				HandleKeyDown(TKey(event.key.keysym));
				break;
			case SDL_KEYUP:
				HandleKeyUp(TKey(event.key.keysym));
			case SDL_JOYAXISMOTION:
				// only pass the event if axis repeat isn't enabled, so axis move
				// isn;t called twice
				if(!GetJoystickAxisRepeat()) {
					HandleJoyAxisMove(event.jaxis.which, event.jaxis.axis, event.jaxis.value);
				}
				break;
			case SDL_JOYBALLMOTION:
				HandleJoyBallMove(event.jball.which, event.jball.ball,
					event.jball.xrel, event.jball.yrel);
				break;
			case SDL_JOYHATMOTION:
				HandleJoyHatMove(event.jhat.which, event.jhat.hat,
					(TJoyHatPosition)event.jhat.value);
				break;
			case SDL_JOYBUTTONDOWN:
				HandleJoyDown(event.jbutton.which, event.jbutton.button);
				break;
			case SDL_JOYBUTTONUP:
				HandleJoyUp(event.jbutton.which, event.jbutton.button);
				break;		 			
			case SDL_MOUSEMOTION:
				HandleMouseMove(event.motion.x, event.motion.y, event.motion.xrel, event.motion.yrel,
										    (TMouseButton)event.motion.state);
				break;
			case SDL_MOUSEBUTTONDOWN:
				HandleMouseDown(event.button.x, event.button.y, 
					(TMouseButton)event.button.state);
				break;
			case SDL_MOUSEBUTTONUP:
				HandleMouseUp(event.button.x, event.button.y, (TMouseButton)event.button.state);
				break;
		}
	}

	// TODO: One day SDL won't be the only way to access joysticks, so make this use 
	// SDL directly or an SDL specific layer to the joystick code.  
	
	// generate joystick events, if we want axis repeat
	// generate mouse events, if we want joy cursor
	if((GetJoystickAxisRepeat() && GetJoystickEvents()) || GetJoystickCursor()) {
		TJoystickSubsystem* joysys = TJoystickSubsystem::GetInstance();
		Sint16 temp;

		// update the joystick subsystem
		joysys->Update();
		
		if(GetJoystickAxisRepeat()) {
			for(int i = 0; i < joysys->GetJoystickCount(); i++) {
				for(int a = 0; a < joysys->GetJoystick(i)->GetNumAxes(); a++) {
					temp = joysys->GetJoystick(i)->GetAxis(a);
					if(temp != 0) {
						HandleJoyAxisMove(i, a, temp);
					}
				}
			}
		}

		if(GetJoystickCursor()) {
			if(joysys->GetJoystickCount() >= (GetJoystickCursorNum() + 1)) {
				//TMouseButton button = MOUSE_NONE;
				//Uint8 num_buttons;
				int X, Y;

				// get mouse position
				SDL_GetMouseState(&X, &Y);

				// generate mouse move events
				if(joysys->GetJoystick(GetJoystickCursorNum())->GetNumAxes() >= 2) {
					bool changed = false;
					
					if((temp = joysys->GetJoystick(GetJoystickCursorNum())->GetAxis(0))) {
						X += (temp / 32767) * GetJoystickCursorSensitivity();
						changed = true;
					}
					if((temp = joysys->GetJoystick(GetJoystickCursorNum())->GetAxis(1))) {
						Y += (temp / 32767) * GetJoystickCursorSensitivity();
						changed = true;
					}
					if(changed) SDL_WarpMouse(X, Y);
				}

				// generate mouse up/down events
				/*num_buttons = Joystick[Joystick.GetJoyCursorNum()]->GetNumButtons();
				if(num_buttons >= 1) {
					if(Joystick[Joystick.GetJoyCursorNum()]->GetButton(0))
						button |= MOUSE_LEFT;
				}
				if(num_buttons == 2) {
					if(Joystick[Joystick.GetJoyCursorNum()]->GetButton(1)) 
						button |= MOUSE_RIGHT;
				} 
				if(num_buttons >= 3) {
					if(Joystick[Joystick.GetJoyCursorNum()]->GetButton(0))
						button |= MOUSE_LEFT;
					if(Joystick[Joystick.GetJoyCursorNum()]->GetButton(1)) 
						button |= MOUSE_MIDDLE;
					if(Joystick[Joystick.GetJoyCursorNum()]->GetButton(1)) 
						button |= MOUSE_RIGHT;
				}*/
				//DoMouseDown(
			}
		}
	}
}
//---------------------------------------------------------------------------  
void TEventDispatcherSDL::DoSetJoystickEvents(bool enable)
{
	if(enable) {
		SDL_JoystickEventState(SDL_ENABLE);
	} else {
		SDL_JoystickEventState(SDL_IGNORE);
	}
}
//---------------------------------------------------------------------------  



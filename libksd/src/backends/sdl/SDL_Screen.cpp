//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	$Header$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Checked-in by:	$Author$
//
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma implementation
#endif
//--------------------------------------------------------------------------- 
#define KSD_BUILD_LIB
#include <SDL/SDL.h>
#include "SDL_Screen.h"
#include "Canvas.h"
#include "log.h"
//--------------------------------------------------------------------------- 
using namespace ksd;
using namespace std;
//---------------------------------------------------------------------------  
SDL_Screen::SDL_Screen() : TScreen()
{
	Canvas = NULL;
	VideoInfo = NULL;
	Changed = false;
	init_flags = 0;
}
//--------------------------------------------------------------------------- 
void SDL_Screen::Init(int width, int height, int bpp, unsigned int ksd_flags, void*)
	throw(ECannotInitDisplay)
{
	Uint32 sdl_flags = 0;
	SDL_Surface* screen_surface;

	// Get video info
	if(!VideoInfo) GetVideoInfo();

	// build sdl flags
	if(ksd_flags & TScreen::FULLSCREEN) sdl_flags |= SDL_FULLSCREEN;
	if(ksd_flags & TScreen::HARDWARE_BUFFER) sdl_flags |= SDL_HWSURFACE;
	if(ksd_flags & TScreen::DOUBLE_BUFFERED) sdl_flags |= SDL_DOUBLEBUF;
	
	// check that this bit depth is good
	bpp = SDL_VideoModeOK(width, height, bpp, sdl_flags);

	// initialize the screen
	screen_surface = SDL_SetVideoMode(width, height, bpp, sdl_flags);

	if(!screen_surface) {
		throw ECannotInitDisplay("Error initializing the SDL screen", SDL_GetError(),
			width, height, bpp, ksd_flags, NULL);
	}

	// store the init flags for resize and attribute retreiving
	init_flags = screen_surface->flags;

	// store the surface (chaging the reference, thus updating all sub-surfaces)
	GetData()->SetSurface(screen_surface, false);

	// Video information
	LOG(0, "!!! -- Aquired hardware information:");
	LOG(0, "Hardware surfaces = %s", HW_Available() ? "YES" : "NO");
	LOG(0, "Windowing Manager = %s", WM_Available() ? "YES" : "NO");
	LOG(0, "HW to HW Acceleration = %s", Blit_HWtoHW() ? "YES" : "NO");
	LOG(0, "HW to HW (CC) Accel = %s", Blit_HWtoHW_CC() ? "YES" : "NO");
	LOG(0, "HW to HW (ALPHA) Accel = %s", Blit_HWtoHW_Alpha() ? "YES" : "NO");
	LOG(0, "SW to HW Acceleration = %s", Blit_SWtoHW() ? "YES" : "NO");
	LOG(0, "SW to HW (CC) Accel = %s", Blit_SWtoHW_CC() ? "YES" : "NO");
	LOG(0, "SW to HW (ALPHA) Accel = %s", Blit_SWtoHW_Alpha() ? "YES" : "NO");
	LOG(0, "Accelerated fill = %s", Blit_Fill() ? "YES" : "NO");
	LOG(0, "Video Memory = %dk", VideoMem());

	// Screen
	LOG(0, "!!! -- Initalized application screen:");
	LOG(0, "MODE = %s", screen_surface->flags & SDL_FULLSCREEN ? "FULLSCREEN" : "WINDOWED");
	LOG(0, "Width = %d, Height = %d", screen_surface->w, screen_surface->h);
	LOG(0, "Bit Depth = %d", screen_surface->format->BitsPerPixel);
	#ifdef __DEBUG__
	if(screen_surface->flags & SDL_HWSURFACE) {
		LOG(0, "Screen is in HARDWARE memory");
	} else LOG(0, "Screen is in SYSTEM memory");
	if(screen_surface->flags & SDL_DOUBLEBUF) LOG(0, "Screen is double buffering");
	#endif
}
//---------------------------------------------------------------------------  
void SDL_Screen::Resize(int width, int height)
{
	if(IsValid()) {
		int bpp;
		SDL_Surface* screen_surface;

		// check that this bit-depth will work
		bpp = SDL_VideoModeOK(width, height, GetFormat().GetBitsPerPixel(), init_flags);

		// do the resize
		screen_surface = SDL_SetVideoMode(width, height, bpp, init_flags);

		if(screen_surface) {
			// store init flags again (should be un-necessary, but seems like the
			// right thing to do.)
			init_flags = screen_surface->flags;

			// actually set surface
			GetData()->SetSurface(screen_surface, false);
		} else {
			throw ECannotInitDisplay("Error resizing the SDL screen", SDL_GetError(),
				width, height, bpp, 0, NULL);
		}
	} else {
		throw ENotInitialized("Screen", "Trying to resize an uninitialized SDL screen");
	}
}
//--------------------------------------------------------------------------- 
void SDL_Screen::GetVideoInfo()
{
	VideoInfo = SDL_GetVideoInfo();
}
//--------------------------------------------------------------------------- 
void SDL_Screen::Update()
{
	if(IsValid()) {		
		SDL_Flip(GetHandle());
		Changed = false;
	} else {
		throw ENotInitialized("Screen", "Trying to update an uninitialized SDL screen.");
	}
}
//---------------------------------------------------------------------------  
void SDL_Screen::Update(const list<TRect>& UpdateRects)
{
	if(IsValid()) {
		if(GetHandle()->flags & SDL_DOUBLEBUF) {
			Update();
		} else {
			if(UpdateRects.size() > 0) {
				SDL_Rect* rects = new SDL_Rect[UpdateRects.size()];
				unsigned int rects_index = 0;

				// build SDL_rects list
				for(list<TRect>::const_iterator i = UpdateRects.begin(); i != UpdateRects.end(); i++) {
					rects[rects_index].x = (Sint16)(*i).GetLeft();
					rects[rects_index].y = (Sint16)(*i).GetTop();
					rects[rects_index].w = (Uint16)(*i).GetWidth();
					rects[rects_index].h = (Uint16)(*i).GetHeight();

					// move to the next sdl rect
					rects_index++;
				}

				// make SDL call
				SDL_UpdateRects(GetHandle(), (int)UpdateRects.size(), rects);

				// free SDL_rects list
				delete[] rects;
			}
			Changed = false;
		}
	} else {
		throw ENotInitialized("Screen", "Trying to update an uninitialized SDL screen.");
	}
}
//---------------------------------------------------------------------------  
void SDL_Screen::Fill(const TColor& _color) 
{
	if(IsValid()) {
		Changed = true;
		GetData()->Fill(_color);
	} else {
		throw ENotInitialized("Screen", "Trying to fill an uninitialized SDL screen.");
	}
}
//---------------------------------------------------------------------------  
TCanvas* SDL_Screen::GetCanvas()
{
	if(IsValid()) {
		if(!Canvas) {
			Canvas = new TCanvas(this);
		}

		return Canvas;
	} else {
		throw ENotInitialized("Screen", "Trying to get a TCanvas for an uninitialized SDL screen.");
	}
}
//---------------------------------------------------------------------------  
void SDL_Screen::OnWrite()
{
	// makes surface on write functionality consistant with TCanvas on write 
	// functionality.
	Changed = true;
}
//--------------------------------------------------------------------------- 


//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//	
//--------------------------------------------------------------------------- 
#ifndef _KSD_sdlconvH
#define _KSD_sdlconvH
//---------------------------------------------------------------------------  
#include <SDL/SDL_video.h>
#include "Rect.h"
#include "Color.h"
//--------------------------------------------------------------------------- 
namespace ksd {
//--------------------------------------------------------------------------- 
inline SDL_Rect KSDtoSDL(const TRect& rect)
{
	SDL_Rect temp;
	temp.x = rect.GetLeft();
	temp.y = rect.GetTop();
	temp.w = rect.GetWidth();
	temp.h = rect.GetHeight();
	return temp;
}
//---------------------------------------------------------------------------  
inline SDL_Color KSDtoSDL(const TColor& color) 
{
	SDL_Color temp;
	temp.r = color.Red;
	temp.g = color.Green;
	temp.b = color.Blue;
	temp.unused = color.Alpha;
	return temp;
}
//---------------------------------------------------------------------------  
inline TRect SDLtoKSD(const SDL_Rect& rect)
{
	TRect temp;
	temp.SetPosition(TPoint2D(rect.x, rect.y));
	temp.SetWidth(rect.w);
	temp.SetHeight(rect.h);
	return temp;
}
//---------------------------------------------------------------------------  
inline TColor SDLtoKSD(const SDL_Color& color)
{
	TColor temp;
	temp.Red = color.r;
	temp.Green = color.g;
	temp.Blue = color.b;
	temp.Alpha = color.unused;
	return temp;
}
//--------------------------------------------------------------------------- 
}; // namespace KSLib
//--------------------------------------------------------------------------- 
#endif

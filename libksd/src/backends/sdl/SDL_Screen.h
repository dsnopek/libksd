//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//	
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		TScreen
//	
//	Purpose:		A screen surface implemtation
//	
//	Checked-in by:	$Author$
//					$Date$
//--------------------------------------------------------------------------- 
#ifndef _KSD_SDL_ScreenH_
#define _KSD_SDL_ScreenH_
//--------------------------------------------------------------------------- 
#include "SubSurface.h"
#include "Screen.h"
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma interface
#endif
//--------------------------------------------------------------------------- 
namespace ksd {
//--------------------------------------------------------------------------- 
class SDL_Screen : public TScreen {
public:
	SDL_Screen();
	
	void Init(int width, int height, int bpp, unsigned int flags, void*) throw(ECannotInitDisplay);
	bool IsChanged() { return Changed; }
	void Resize(int X, int Y);

	// Video Info
	void 	GetVideoInfo();
	bool 	HW_Available() const 		{ return VideoInfo ? VideoInfo->hw_available : false; }
	bool 	WM_Available() const		{ return VideoInfo ? VideoInfo->wm_available : false; }
	bool 	Blit_HWtoHW() const			{ return VideoInfo ? VideoInfo->blit_hw : false; }
	bool 	Blit_HWtoHW_CC() const		{ return VideoInfo ? VideoInfo->blit_hw_CC : false; }
	bool 	Blit_HWtoHW_Alpha()	const 	{ return VideoInfo ? VideoInfo->blit_hw_A : false; }
	bool 	Blit_SWtoHW() const			{ return VideoInfo ? VideoInfo->blit_sw : false; }
	bool 	Blit_SWtoHW_CC() const		{ return VideoInfo ? VideoInfo->blit_sw_CC : false; }
	bool 	Blit_SWtoHW_Alpha() const	{ return VideoInfo ? VideoInfo->blit_sw_A : false; }
	bool 	Blit_Fill() const			{ return VideoInfo ? VideoInfo->blit_fill : false; }
	Uint32	VideoMem() const			{ return VideoInfo ? VideoInfo->video_mem : 0; }

	void Update();
	void Update(const std::list<TRect>& UpdateRects);

	void Fill(const TColor& Fill);

	TSubSurface* CreateSubSurface(int X, int Y, int Width, int Height, bool Buffered) {
		return new TGenericSubSurface(this, X, Y, Width, Height, Buffered);
	}
	TCanvas* GetCanvas();

	bool GetFullscreen() const 		{ return init_flags & SDL_FULLSCREEN; }
	bool GetHardwareBuffer() const 	{ return init_flags & SDL_HWSURFACE; }
	bool GetDoubleBuffered() const 	{ return init_flags & SDL_DOUBLEBUF; }
protected:
	void OnWrite();
private:
	TCanvas* Canvas;
	const SDL_VideoInfo* VideoInfo;
	bool Changed;
	Uint32 init_flags;
};
//--------------------------------------------------------------------------- 
}; // namespace ksd
//--------------------------------------------------------------------------- 
#endif

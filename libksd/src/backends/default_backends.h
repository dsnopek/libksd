//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		null
//	
//	Purpose:		@DESCRIPTION@
//	
//	Checked-in by:	$Author$
//					$Date$
//---------------------------------------------------------------------------
#ifndef _KSD_default_backendsH_
#define _KSD_default_backendsH_
//---------------------------------------------------------------------------  
#include "export_ksd.h"
//--------------------------------------------------------------------------- 
namespace ksd {

// forward decl
class TBackendSystem;
//---------------------------------------------------------------------------
// Used to load all the backends that are automatically built into the
// main library. 
KSD_EXPORT void LoadDefaultBackends(TBackendSystem*);
//---------------------------------------------------------------------------  
}; // namespace ksd
//---------------------------------------------------------------------------  
#endif


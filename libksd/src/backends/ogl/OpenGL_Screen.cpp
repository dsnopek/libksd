//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	$Header$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Checked-in by:	$Author$
//	
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma implementation
#endif
//---------------------------------------------------------------------------
#define KSD_BUILD_LIB
#include <SDL/SDL.h>
#include "GL.h"
#include "OpenGL_Screen.h"

using namespace ksd;
//---------------------------------------------------------------------------  
OpenGL_Screen::OpenGL_Screen()
{
	init_flags = 0;
}
//---------------------------------------------------------------------------  
/* This code has been borrowed and modified from "testgl.c" that comes
 * with SDL, originally written by Sam Latinga.
 */
void OpenGL_Screen::Init(int width, int height, int bpp, unsigned int ksd_flags, void*)
	throw(ECannotInitDisplay)
{
	SDL_Surface* screen_surface;
	Uint32 sdl_flags = SDL_OPENGL;
	int rgb_size[3];

	// TODO: replace with code that checks the display bit depth
	// default bpp for opengl is 16
	if(bpp == 0) bpp = 16;

	// make video flags
	if(ksd_flags & TScreen::FULLSCREEN) sdl_flags |= SDL_FULLSCREEN;

	// set video mode 
	switch(bpp) {
		case 8:
			rgb_size[0] = 2;
			rgb_size[1] = 3;
			rgb_size[2] = 3;
			break;
		case 15:
		case 16:
			rgb_size[0] = 5;
			rgb_size[1] = 5;
			rgb_size[2] = 5;
			break;
		default:
			rgb_size[0] = 8;
			rgb_size[1] = 8;
			rgb_size[2] = 8;
			break;
	}
	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, rgb_size[0]);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, rgb_size[1]);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, rgb_size[2]);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, bpp);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

	if((screen_surface = SDL_SetVideoMode(width, height, bpp, sdl_flags)) == NULL) {
		throw ECannotInitDisplay("Error initializing OpenGL screen", SDL_GetError(),
			width, height, bpp, ksd_flags, NULL);
	}

	// store init flags for resize and attribute check
	init_flags = screen_surface->flags;

	// store sdl surface
	GetData()->SetSurface(screen_surface, false);

	// OpenGL
	LOG(0, "!!! -- Got OpenGL information:");
	LOG(0, "Vendor     : %s", glGetString(GL_VENDOR));
	LOG(0, "Renderer   : %s", glGetString(GL_RENDERER));
	LOG(0, "Version    : %s", glGetString(GL_VERSION));
	LOG(0, "Extensions : %s", glGetString(GL_EXTENSIONS));

	// Screen
	LOG(0, "!!! -- Initalized application screen:");
	LOG(0, "MODE = %s", screen_surface->flags & SDL_FULLSCREEN ? "FULLSCREEN" : "WINDOWED");
	LOG(0, "Width = %d, Height = %d", screen_surface->w, screen_surface->h);
	LOG(0, "Bit Depth = %d", screen_surface->format->BitsPerPixel);
}
//---------------------------------------------------------------------------  
void OpenGL_Screen::Resize(int width, int height)
{
	// TODO: I think we might have to recreate all OpenGL textures on this 
	// event!!  Not sure though, and current Surface architecture will
	// not support it!  Fix me!!

	if(IsValid()) {
		int bpp;
		SDL_Surface* screen_surface;

		bpp = SDL_VideoModeOK(width, height, GetData()->GetFormat().GetBitsPerPixel(), init_flags);
		screen_surface = SDL_SetVideoMode(width, height, bpp, init_flags);

		if(screen_surface) {
			// store init flags again (should be un-necessary, but seems like the
			// right thing to do.)
			init_flags = screen_surface->flags;

			// actually set surface
			GetData()->SetSurface(screen_surface, false);
		} else {
			throw ECannotInitDisplay("Error resizing OpenGL screen", SDL_GetError(),
				width, height, bpp, 0, NULL);
		}
	} else {
		throw ENotInitialized("Screen", "Trying to resize an uninitialized screen.");
	}
}
//---------------------------------------------------------------------------  
void OpenGL_Screen::Update()
{
	if(IsValid()) {
		// copy sub surfaces to the screen
		bool in_2d_mode = false;
		while(DrawQueue.size() > 0) {
			if(!in_2d_mode) {
				Enter2DMode();
				in_2d_mode = true;
			}
			
			DrawQueue.front()->_DoDraw();
			DrawQueue.pop_front();
		}
		if(in_2d_mode) Leave2DMode(); // exit 2d mode if in it
		
		// display gl to screen
		SDL_GL_SwapBuffers();
	} else {
		throw ENotInitialized("Screen", "Trying to update an uninitialized OpenGL screen.");
	}
}
//---------------------------------------------------------------------------  
void OpenGL_Screen::Enter2DMode()
{
	SDL_Surface* screen = SDL_GetVideoSurface();

	glPushAttrib(GL_ALL_ATTRIB_BITS);
	glPushClientAttrib(GL_CLIENT_PIXEL_STORE_BIT);

	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glDisable(GL_FOG);
	glDisable(GL_ALPHA_TEST);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_SCISSOR_TEST);	
	glDisable(GL_STENCIL_TEST);
	glDisable(GL_CULL_FACE);

	glViewport(0, 0, screen->w, screen->h);

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();

	glOrtho(0.0, (GLdouble)screen->w, (GLdouble)screen->h, 0.0, 0.0, 1.0);

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
}
//---------------------------------------------------------------------------  
void OpenGL_Screen::Leave2DMode()
{
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glPopClientAttrib();
	glPopAttrib();
}
//---------------------------------------------------------------------------  


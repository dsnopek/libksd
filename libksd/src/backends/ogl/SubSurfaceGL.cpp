//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	$Header$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Checked-in by:	$Author$
//
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma implementation
#endif
//--------------------------------------------------------------------------- 
#define KSD_BUILD_LIB
#include "SubSurfaceGL.h"
#include "OpenGL_Screen.h"
#include "glconv.h"

using namespace ksd;
//---------------------------------------------------------------------------  
TSubSurfaceGL::TSubSurfaceGL(OpenGL_Screen* Screen,
	int _X, int _Y, int _Width, int _Height) 
	: TSubSurface()
{
	// functionality of MakeSurface depends on Texture being NULL
	Texture = NULL;

	// set dimensions
	X = _X;
	Y = _Y;
	Width = _Width;
	Height = _Height;
	ScrollX = ScrollY = 0;
	Parent = Screen;

	// make underlying surface object
	MakeSurface();
}
//--------------------------------------------------------------------------- 
void TSubSurfaceGL::Resize(int _Width, int _Height)
{
	// TODO: make thread safe

	Width = _Width;
	Height = _Height;

	// resize underlying surface
	MakeSurface();
}
//--------------------------------------------------------------------------- 
void TSubSurfaceGL::Reposition(int _X, int _Y)
{
	// TODO: make thread safe
	
	X = _X;
	Y = _Y;
}
//---------------------------------------------------------------------------  
void TSubSurfaceGL::SetScroll(unsigned int _sx, unsigned int _sy) 
{
	// TODO: make thread safe

	ScrollX = _sx;
	ScrollY = _sy;
}
//---------------------------------------------------------------------------  
void TSubSurfaceGL::Reset(const TRect& rect)
{
	// TODO: make thread safe

	X = rect.GetLeft();
	Y = rect.GetTop();
	
	Resize(rect.GetWidth(), rect.GetHeight());
}
//---------------------------------------------------------------------------  
void TSubSurfaceGL::Fill(const TColor& Color)
{
	if(IsValid()) { 
		OnWrite();
		GetData()->Fill(Color);
	}
}
//---------------------------------------------------------------------------  
void* TSubSurfaceGL::GetPixels()
{
	return ((char*)GetData()->GetPixels()) + 
			(-ScrollY * GetData()->GetPitch()) +
			(-ScrollX * GetData()->GetFormat().GetBytesPerPixel());
}
//---------------------------------------------------------------------------  
void TSubSurfaceGL::FreeData()
{
	if(Texture) {
		delete Texture;
		Texture = NULL;

		GetData()->SetSurface(NULL, false);
	}
}
//---------------------------------------------------------------------------  
void TSubSurfaceGL::_DoDraw()
{
	if(Texture) {
		if(Changed) Texture->Update();
		Texture->Draw(X, Y);
		Changed = false;
	}
}
//---------------------------------------------------------------------------  
void TSubSurfaceGL::CopyToParent() 
{
	Parent->AddToDrawQueue(this);
}
//---------------------------------------------------------------------------  
void TSubSurfaceGL::MakeSurface()
{
	FreeData();
	
	Texture = new TTexture(Width, Height);
	GetData()->SetSurface(Texture->GetSurface(), false);
}
//---------------------------------------------------------------------------  


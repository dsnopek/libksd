//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	$Header$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Checked-in by:	$Author$
//	
//---------------------------------------------------------------------------
#define KSD_BUILD_LIB
#include <SDL.h>
#include "glconv.h"

using namespace ksd;
/* Most of this code was orginally written by Sam Latinga for the testgl
 * application that comes with SDL.
 */
//---------------------------------------------------------------------------  
static int power_of_two(int input)
{
	int value = 1;

	while ( value < input ) {
		value <<= 1;
	}
	return value;
}
//---------------------------------------------------------------------------  
ksd::TTexture::TTexture(int surface_width, int surface_height)
{
	int w, h;
	SDL_Rect area;

	/* Use the surface width and height expanded to powers of 2 */
	w = power_of_two(surface_width);
	h = power_of_two(surface_height);
	texcoord[0] = 0.0f;			/* Min X */
	texcoord[1] = 0.0f;			/* Min Y */
	texcoord[2] = (GLfloat)surface_width / w;	/* Max X */
	texcoord[3] = (GLfloat)surface_height / h;	/* Max Y */
	width = surface_width;
	height = surface_height;

	surface = SDL_CreateRGBSurface(
			SDL_SWSURFACE,
			w, h,
			32,
#if SDL_BYTEORDER == SDL_LIL_ENDIAN /* OpenGL RGBA masks */
			0x000000FF, 
			0x0000FF00, 
			0x00FF0000, 
			0xFF000000
#else
			0xFF000000,
			0x00FF0000, 
			0x0000FF00, 
			0x000000FF
#endif
		       );
	if ( surface == NULL ) {
		// TODO: raise exception!
		return;
	}

	/* Create an OpenGL texture for the image */
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D,
			     0,
			     GL_RGBA,
			     w, h,
			     0,
			     GL_RGBA,
			     GL_UNSIGNED_BYTE,
			     NULL);
}
//---------------------------------------------------------------------------  
ksd::TTexture::~TTexture()
{
	// NOTE: due to the order with which the application is shutdown, this
	// will be called once after the OpenGL context has been un-initialized.
	glDeleteTextures(1, &texture);

	SDL_FreeSurface(surface);
}
//---------------------------------------------------------------------------  
void ksd::TTexture::Update()
{
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexImage2D(GL_TEXTURE_2D,
			 0,
			 GL_RGBA,
			 surface->w, surface->h,
			 0,
			 GL_RGBA,
			 GL_UNSIGNED_BYTE,
			 surface->pixels);
}
//---------------------------------------------------------------------------  
void ksd::TTexture::Draw(int X, int Y)
{
	glBindTexture(GL_TEXTURE_2D, texture);
	glBegin(GL_TRIANGLE_STRIP);
	glTexCoord2f(texcoord[0], texcoord[1]); glVertex2i(X, Y);
	glTexCoord2f(texcoord[2], texcoord[1]); glVertex2i(X + width, Y);
	glTexCoord2f(texcoord[0], texcoord[3]); glVertex2i(X, Y + height);
	glTexCoord2f(texcoord[2], texcoord[3]); glVertex2i(X + width, Y + height);
	glEnd();

	// Un-binds the textures.  Fixes a bug only seen on some systems, where the
	// texture is applied to user created objects.
	glBindTexture(GL_TEXTURE_2D, 0);
}
//---------------------------------------------------------------------------  


//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//	
//--------------------------------------------------------------------------- 
#ifndef _KSD_glconvH
#define _KSD_glconvH
//---------------------------------------------------------------------------  
#include <SDL/SDL_video.h>
#include "GL.h"
//--------------------------------------------------------------------------- 
namespace ksd {
//--------------------------------------------------------------------------- 
class TTexture {
public:
	TTexture(int width, int height);
	~TTexture();

	/// Returns the underlying surface object
	SDL_Surface* GetSurface() { return surface; }

	/// Copies from the surface to the OpenGL texture
	void Update();

	/** Draws the texture to the screen.  OpenGL must be running and set-up for
	 ** this type of 2D drawing. 
	 */
	void Draw(int X, int Y);
private:
	SDL_Surface* surface;
	GLfloat texcoord[4];
	GLuint texture;
	int width, height;
};
//--------------------------------------------------------------------------- 
}; // namespace ksd
//--------------------------------------------------------------------------- 
#endif

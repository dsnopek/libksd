//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//	
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		TSubSurfaceGL
//	
//	Purpose:		Creates a sub-surface to the OpenGL screen.  Provides a 2D 
//					adaptor	to OpenGL.
//	
//	Checked-in by:	$Author$
//					$Date$
//--------------------------------------------------------------------------- 
#ifndef _KSD_SubSurfaceGLH_
#define _KSD_SubSurfaceGLH_
//--------------------------------------------------------------------------- 
#include "Surface.h"
#include "SubSurface.h"
#include "Canvas.h"
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma interface
#endif
//--------------------------------------------------------------------------- 
namespace ksd {

// forward decl
class TTexture;
class OpenGL_Screen;
//---------------------------------------------------------------------------  
class TSubSurfaceGL : public TSubSurface { friend class OpenGL_Screen;
public:
	/** Creates a sub-surface to the OpenGL screen.  Provides a 2D adaptor
	 ** to OpenGL.
	 */
	TSubSurfaceGL(OpenGL_Screen* Screen, int X, int Y, int width, int height);

	/// Destroyes testure object
	~TSubSurfaceGL() { FreeData(); }

	// modifies the surface for the scroll
	void* GetPixels();
	int GetWidth() const { return Width; }
	int GetHeight() const { return Height; }

	// returns true if this sub-surface was drawn to
	bool GetChanged() const { return Changed; }

	// functions for modifying what the sub-surface includes
	void Resize(int width, int height);
	void Reposition(int _x, int _y);
	void Reset(const TRect& rect);

	// functions for "scrolling" a surface
	void SetScroll(unsigned int _sx, unsigned int _sy);
	TPoint2D GetScroll() const { return TPoint2D(ScrollX, ScrollY); }
	TPoint2D GetPosition() const { return TPoint2D(X, Y); }

	// mark as changed
	void OnWrite() { Changed = true; }
	
	void Fill(const TColor& _color);

	void CopyToParent();
	bool IsBuffered() const { return true; }

	// frees underlying info.  Once glconv.h is replaced with TSurfaceDataGL,
	// reference counting should handle the situation for which this was
	// created.
	void FreeData();

	TSubSurface* CreateSubSurface(int _x, int _y, int _w, int _h, bool _buffered) {
		return new TGenericSubSurface(this, _x, _y, _w, _h, _buffered);
	}
private:
	OpenGL_Screen* Parent;
	TCanvas* Canvas;
	int X, Y, Width, Height;
	unsigned int ScrollX, ScrollY;
	TTexture* Texture;
	bool Changed;

	void MakeSurface();

	// Actually copies to the screen
	void _DoDraw();
};
//--------------------------------------------------------------------------- 
}; // namespace ksd;
//--------------------------------------------------------------------------- 
#endif


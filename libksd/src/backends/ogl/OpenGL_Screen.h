//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		OpenGL_Screen
//	
//	Purpose:		@DESCRIPTION@
//	
//	Checked-in by:	$Author$
//					$Date$
//---------------------------------------------------------------------------
#ifndef _KSD_OpenGL_ScreenH_
#define _KSD_OpenGL_ScreenH_
//---------------------------------------------------------------------------
#include <list>
#include "SubSurfaceGL.h"
#include "Screen.h"
#include "Export.h"
#include "exception.h"
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma interface
#endif
//---------------------------------------------------------------------------
namespace ksd {
//---------------------------------------------------------------------------
class OpenGL_Screen : public TScreen {
public:
	OpenGL_Screen();

	void Init(int width, int height, int bpp, unsigned int screen_flags, void*) throw(ECannotInitDisplay);
	void Resize(int X, int Y);
	// we have no way of know if it was changed so always report true
	bool IsChanged() { return true; }
	
	void Update();
	// we can't update only sections, call the master update
	void Update(const std::list<TRect>& UpdateRects) { Update(); }

	// TODO: implement this function using OpenGL calls.
	void Fill(const TColor& color) { }

	TSubSurface* CreateSubSurface(int X, int Y, int Width, int Height, bool) {
		return new TSubSurfaceGL(this, X, Y, Width, Height);
	}

	// cannot do 2D drawing to a OpenGL_Screen surface
	TCanvas* GetCanvas() { return NULL; }

	bool GetFullscreen() const { return init_flags & SDL_FULLSCREEN; }
	// using OpenGL is a lot like a hardware buffer, so claim that we have it.
	bool GetHardwareBuffer() const { return true; }
	// OpenGL is always double buffered.
	bool GetDoubleBuffered() const { return true; }

	// Use to draw TSubSurfaceGL's to the screen
	void AddToDrawQueue(TSubSurfaceGL* sub) {
		DrawQueue.push_back(sub);
	}
protected:
	// Since we can't write directly to this type, throw exception.
	void OnWrite() { 
		throw EInvalidInput("OpenGL_Screen", 
			"Cannot write directly to an OpenGL_Screen surface!  Use SubSurface's.");
	}
private:
	unsigned int init_flags;
	std::list<TSubSurfaceGL*> DrawQueue;

	void Enter2DMode();
	void Leave2DMode();
};
//---------------------------------------------------------------------------  
}; // namespace ksd
//---------------------------------------------------------------------------  
#endif



/** This code is in the Public Domain.  Use wisely! **/

#ifndef _INIT_H_
#define _INIT_H_

namespace ModuleInit {

/*
This code helps can be used to add startup and shutdown operations to a program
in a modular way.  Instead of collecting all the startup and shutdown code in a
central location that knows about all of the code, you can have the modules
themselves do it.  Thus making it more modular.

First, add the generic calls to your application:

int main()
{
    DoInitFunctions();

    //
    // Run application code.
    //

    DoShutdownFunctions();
    Cleanup();
}

Then, add your startup and shutdown code to your module source:

static void StartModule()
{
    // startme!
}

static void StopModule()
{
    // stopme!
}

_MODULE(&StartModule, &StopModule);

*/

typedef void (*FuncPtr)(void);

// Adds a function to the list of init operations
void AddInitFunction(FuncPtr init);
// Adds a function to the list of shutdown operations
void AddShutdownFunction(FuncPtr shutdown);
// Cleans up the lists
void Cleanup();

// Calls all the init operations
void DoInitFunctions();
// Calls all the shutdown operations
void DoShutdownFunctions();

// internal magic class!
class _init_class {
public:
    _init_class(FuncPtr init, FuncPtr shutdown) {
        if(init) AddInitFunction(init);
        if(shutdown) AddShutdownFunction(shutdown);
    }
};

/*
Use these macros to register init/shutdown functions.  You can only use one
per source file.
*/
#define _MODULE(f1, f2) static ::ModuleInit::_init_class _init_obj(f1, f2);
#define _MODULE_INIT(f) static ::ModuleInit::_init_class _init_obj(f, NULL);
#define _MODULE_SHUTDOWN(f) static ::ModuleInit::_init_class _init_obj(NULL, f);

/**
Q: First of all, why would we want to use your crazy system?  Why can't I just
declare a variable equal to NULL and allocate when the GetFunction is called?

A: You could!  But if your objects are depended upon by global variable constructors
then you are in trouble.  What happen's if your global variable isn't set to NULL
until after the other variable depends on it is constructed?  That's right, you
just used a pointer to an arbitrary value.  These types of errors can go unnoticed
because the object will be created properly on demand once the variable is set to
NULL, silently leaving memory corruption for other parts of your program.

How to use the create on demand system:

******************************************************************************

// First, declare your variable:
_GLOBAL_DECL(MyClass, GlobalObj);

MyClass* GetGlobalObj()
{
    // use create on demand macro
    _GLOBAL_CREATE(GlobalObj, new MyClass);
    return GlobalObj;
}

void Cleanup()
{
    // remember to destroy your object at some point
    _GLOBAL_DESTROY(GlobalObj);
}

******************************************************************************

Some things to note:
    1.  This all must happen in a source file.  The global object is a static
    variable that has to be accessed through a function to the outside.
    2.  You can only use _GLOBAL_CREATE in one function!  This macro works its
    magic by initializing the first time the function is called.
    3.  Do not try to synchronize the clean up on a global variable destructor!
    Do this manually or, if anything, use atexit().

*/

// create on demand global variables
#define _GLOBAL_DECL(type, name) static type* name = NULL;
#define _GLOBAL_CREATE(name, value) \
    static bool _GLOBAL_CREATE_init = false; \
    if(!_GLOBAL_CREATE_init || name == NULL) { \
        name = value; \
        _GLOBAL_CREATE_init = true; \
    }
#define _GLOBAL_EXISTS(name) name != NULL
#define _GLOBAL_DESTROY(name) \
    if(name) { \
        delete name; \
        name = NULL; \
    }

}; // ModuleInit

#endif

//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//	
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		TClassFactory
//	
//	Purpose:		A reusable, plugin capable class factory.
//	
//	Checked-in by:	$Author$
//					$Date$
//---------------------------------------------------------------------------
#ifndef _ksd_ClassFactoryH
#define _ksd_ClassFactoryH
//---------------------------------------------------------------------------
#include "PluginSys.h"
//---------------------------------------------------------------------------
namespace ksd {
//---------------------------------------------------------------------------
template<typename T, typename Key>
class IClassFactory {
public:
	class CreatorBase {
	public:
		virtual T* Create() = 0;
	};

	template<class concrete>
	class Creator : public CreatorBase {
	public:
		T* Create() { return new concrete; }
	};

	virtual Key GetClassID() = 0;
};
//--------------------------------------------------------------------------- 
template<typename T, class id, const char* TypeName>
class TClassFactory
	: public TPluginSystem<typename IClassFactory<T, id>::CreatorBase, TypeName, id> {
public:
	// modified TPluginReg
	typedef void (*TFactoryReg)(TClassFactory<T, id, TypeName>* Sys);

	// capture creator base
	typedef typename IClassFactory<T, id>::CreatorBase CreatorBase;

	TClassFactory() : TPluginSystem<T, TypeName, id>() { }
	TClassFactory(TFactoryReg _init) 
		: TPluginSystem<CreatorBase, TypeName, id>(typename TPluginSystem<CreatorBase, TypeName, id>::TPluginReg(_init)) { }

    // creates an object with an id of Ident
    T* CreateObj(id Ident);
};
//---------------------------------------------------------------------------
template<class T, class id, const char* TypeName>
T* TClassFactory<T, id, TypeName>::CreateObj(id Ident)
{
    CreatorBase* obj = GetPlugin(Ident);
    if(obj != NULL) {
    	return obj->Create();
    }

    // if there is no object of that name 
    return NULL;
}
//---------------------------------------------------------------------------
}; // namespace ksd
//---------------------------------------------------------------------------
#endif
 

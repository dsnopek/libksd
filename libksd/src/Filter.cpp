//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	$Header$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Checked-in by:	$Author$
//
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma implementation
#endif
//--------------------------------------------------------------------------- 
#define KSD_BUILD_LIB
#include <algorithm>
#include "Filter.h"
#include "ImageIterator.h"

using namespace ksd;
using namespace std;
//--------------------------------------------------------------------------- 
TFilterThread::TFilterThread(TImageFilter* _filter, bool _owner)
{
	if(!_filter) {
		// can't happen -- throw exception?
		return;
	}

	Filter = _filter;
	Owner = _owner;
	Surface = NULL;
}
//--------------------------------------------------------------------------- 
TFilterThread::~TFilterThread()
{
	if(Filter && Owner) {
		delete Filter;
	}
}
//--------------------------------------------------------------------------- 
void TFilterThread::Process(TSurface& _surface, bool _once)
{
	if(Surface) {
		Stop();
	}

	// set surface
	Surface = &_surface;
	Once = _once;
	
	Start();
}
//--------------------------------------------------------------------------- 
void TFilterThread::RunThread()
{
	int i = 0;

	Filter->Process(*Surface);
	while(!Once && IsRunning() && i != Filter->GetMax()) {
		i++;
		Filter->Process(*Surface);
	}
}
//--------------------------------------------------------------------------- 
void TPixelFilter::DoProcess(TSurface& surface)
{
	if(surface.IsValid()) {
		Uint32 c;
		TColor temp;
		bool Changed = false;
		int h_inc = surface.GetWidth() - surface.GetRealWidth(); // off by one I think

		surface.Lock();

		ImageIterator image_iter = surface.make_iterator(ImageIterator::READ_WRITE);

		for(int h = 0; h < surface.GetHeight(); h++) {
			for(int w = 0; w < surface.GetWidth(); w++) {
				c = *image_iter;

				// run pixel through the filter
				temp = surface.GetFormat().MapToColor(c);
				Filter(temp, Changed);

				if(Changed) { 
					// write pixel back
					c = surface.GetFormat().MapToPixel(temp);

					// set then inc
					*image_iter = c;
					image_iter++;

					// reset changed flag
					Changed = false;
				} else image_iter++;
			}
			if(h_inc != 0) image_iter += (h_inc - 1); // off by one ??
		}

		surface.Unlock();
	}
}
//--------------------------------------------------------------------------- 
void TGroup4Filter::DoProcess(TSurface& surface)
{
	
}
//--------------------------------------------------------------------------- 
int TFadeFilter::Init(const TSurface& surface)
{
 	Uint8 r, g, b, c;

	if(surface.GetFormat().GetBytesPerPixel() == 2) {
		bound = 8;
	} else bound = 1;

	r = abs(255 - Color.Red);
	g = abs(255 - Color.Green);
	b = abs(255 - Color.Blue);

	c = max(r, max(g, b));

	return (int)c / bound;	
}
//--------------------------------------------------------------------------- 
void TFadeFilter::Filter(TColor& pixel, bool& Changed)
{
	if(Color != pixel) {
		factor = (Color.Red - pixel.Red) * Fade;
		if(factor > 0 && factor < bound) pixel.Red += bound;
		else if(factor < 0 && factor > -bound) pixel.Red -= bound;
		else pixel.Red += (Uint8)factor.ToInt();

		// Calc Green
		factor = (Color.Green - pixel.Green) * Fade;
		if(factor > 0 && factor < bound) pixel.Green += bound;
		else if(factor < 0 && factor > -bound) pixel.Green -= bound;
		else pixel.Green += (Uint8)factor.ToInt();

		// Calc blue
		factor = (Color.Blue - pixel.Blue) * Fade;
		if(factor > 0 && factor < bound) pixel.Blue += bound;
		else if(factor < 0 && factor > -bound) pixel.Blue -= bound;
		else pixel.Blue += (Uint8)factor.ToInt();
		
		Changed = true;
	}
}
//--------------------------------------------------------------------------- 
int TBrightnessFilter::Init(const TSurface& surface)
{
	FadeBlack = Brightness < 0 ? true : false;

	if(surface.GetFormat().GetBytesPerPixel() == 2) {
		bound = 8;
		return 31;
	}

	bound = 1;
	return 255;
}
//--------------------------------------------------------------------------- 
void TBrightnessFilter::Filter(TColor& pixel, bool& Changed)
{
	// skip pixels if we can
	if(FadeBlack) if(pixel == TColor::black) return;
	else if(pixel == TColor::white) return;

	// calc red
	factor = pixel.Red * Brightness;
	if(factor > 0 && factor < bound) pixel.Red += bound;
	else if(factor < 0 && factor > -bound) pixel.Red -= bound;
	else pixel.Red += (Uint8)factor.ToInt();

	// Calc Green
	factor = pixel.Green * Brightness;
	if(factor > 0 && factor < bound) pixel.Green += bound;
	else if(factor < 0 && factor > -bound) pixel.Green -= bound;
	else pixel.Green += (Uint8)factor.ToInt();

	// Calc blue
	factor = pixel.Blue * Brightness;
	if(factor > 0 && factor < bound) pixel.Blue += bound;
	else if(factor < 0 && factor > -bound) pixel.Blue -= bound;
	else pixel.Blue += (Uint8)factor.ToInt();
	
	Changed = true;
}
//--------------------------------------------------------------------------- 


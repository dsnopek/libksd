//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2001 Arthur Peters
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//	
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			Arthur Peters
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		TSample
//	
//	Purpose:	        TSample contains audio that can be played with TMixer
//	
//	Checked-in by:	$Author$
//					$Date$
//--------------------------------------------------------------------------- 
#ifndef _KSD_SampleH_
#define _KSD_SampleH_
//---------------------------------------------------------------------------
#include <SDL/SDL_mixer.h>
#include "exception.h"
#include "export_ksd.h"
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma interface
#endif
//--------------------------------------------------------------------------- 
namespace ksd {
//--------------------------------------------------------------------------- 
class TMixer;
class TSampleInstance;
//---------------------------------------------------------------------------
KSD_EXPORT_CLASS class TSample { friend class TMixer; friend class TSampleInstance;
public:
	/**
	   Initialize the object as a empty sample. If you try to play
	   it the mixer will return an error.
	*/ 
	TSample();
	/**
	   Initialize the object by loading a sample from a file.
	*/
	TSample( const char* filename, const char* type = "AUTODETECT" )
		{
			Load( filename, type );
		}

	~TSample();

	/**
	   Load a sample from a file. \\
	   {\bf Only WAV files are supported right now. I will add more latter. }
	*/
	void Load(const char* Filename, const char* type = "AUTODETECT");

	/**
	   Set the volume of this sample when it is played. This will
	   affect currently playing instances of the sample.
	*/
	void SetVolume( int vol ) throw( ENotInitialized );

	/**
	   Get the volume of this sample.
	*/
	int GetVolume() throw( ENotInitialized );

private:

	void FreeSampleData();

	/**
	   The number of instance of this sample that are currently
	   playing.
	*/
	int ReferenceCount;
            
	/**
	   The SDL_mixer struct the this class wraps.
	*/
	Mix_Chunk* SampleData;
};
//---------------------------------------------------------------------------
}; // namespace ksd
//--------------------------------------------------------------------------- 
#endif

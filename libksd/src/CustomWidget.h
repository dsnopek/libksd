//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		TCustomWidget
//	
//	Purpose:		Descend this class to create a custom widget type.
//	
//	Checked-in by:	$Author$
//					$Date$
//---------------------------------------------------------------------------
#ifndef _KSD_CustomWidgetH_
#define _KSD_CustomWidgetH_
//---------------------------------------------------------------------------
#include "Widget.h"
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma interface
#endif
//---------------------------------------------------------------------------
namespace ksd {
//---------------------------------------------------------------------------
/** A pure-libksd way of implementing a TWidget.  Isn't the only way but this
 ** one does provide a number of good features and it makes creating your 
 ** own widgets very easy.
 */
KSD_EXPORT_CLASS class TCustomWidget : public TWidget {
private:
	// custom widget flags
	enum {
		AUTO_CLEAR_BUFFER = 0x00,
		DOUBLE_BUFFERED,
		ACCEPT_INPUT,
		HOLLOW_CLIENT_AREA,

		AUTO_SAVE_BUFFER,
		AUTO_RESET_CHANGED,
		TRANSPARENT_BACKGROUND,

		FLAGS_MAX
	};
public:
	///
	TCustomWidget(TWidget* _parent);
	///
	~TCustomWidget();

	std::string 	GetCaption() const 		{ return Caption; }
	int 			GetWidth() const		{ return Width; }
	int 			GetHeight() const		{ return Height; }
	TPoint2D 		GetPosition() const		{ return Position; }
	TPoint2D		GetScroll() const		{ return ClientSurface ? ClientSurface->GetScroll() : TPoint2D(); } // temp
	int				GetClientWidth() const	{ return ClientRect.GetWidth(); }
	int				GetClientHeight() const	{ return ClientRect.GetHeight(); }

	void SetPosition(int X, int Y) {
		Position.Set(X, Y);
		GetParent()->NeedsRepaint();

		// try to create on demand
		if(Surface == NULL) RecreateBuffer();
		// check to see if it actually worked, before doing
		if(Surface != NULL) Surface->Reposition(X, Y);
	}
	void SetCaption(const std::string& _caption) {
		Caption = _caption;
		NeedsRepaint();
	}

	/// Returns the value set by \Ref{SetAutoClearBuffer}
	bool GetAutoClearBuffer() const { return flags.test(AUTO_CLEAR_BUFFER); }
	/// Returns the value set by \Ref{SetDoubleBuffered}
	bool GetDoubleBuffered() const { return flags.test(DOUBLE_BUFFERED); }
	/// Returns the value set by \Ref{SetAcceptInput}
	bool GetAcceptInput() const { return flags.test(ACCEPT_INPUT); }
	/// Returns the valeu set by \Ref{SetHollowClientArea}
	bool GetHollowClientArea() const { return flags.test(HOLLOW_CLIENT_AREA); }
	/// Returns the value set by \Ref{SetAutoSaveBuffer}
	bool GetAutoSaveBuffer() const { return flags.test(AUTO_SAVE_BUFFER); }
	/// Returns the value set by \Ref{SetAutoResetChanged}
	bool GetAutoResetChanged() const { return flags.test(AUTO_RESET_CHANGED); }
	/// Returns the value set by \Ref{SetTransparentBackground}
	bool GetTransparentBackground() const { return flags.test(TRANSPARENT_BACKGROUND); }
	
	TSurface* GetSurface() {
		if(!Surface) RecreateBuffer();
		return Surface;
	}
				
	TSurface* GetClientSurface() {
		if(!ClientSurface) RecreateBuffer();
		return ClientSurface;
	}

	virtual void Resize(int _width, int _height);
protected:
	// allows widget to specify where its "decorations" go.  By decorations I
	// don't mean TDecoration's.  A custom widget paints its own decorations, only
	// a TControl uses TDecoration's. 
	TRect GetClientRect() const				{ return ClientRect; }
	void  SetClientRect(const TRect& _rect) { ClientRect = _rect; }

	/** When set to true, the buffer will automatically be cleared on each UPDATE
	 ** as well as each REPAINT.  When set to false, the buffer is automatically cleared
	 ** only on REPAINT.
	 ** 
	 ** \textbf{Default: true}
	 ** 
	 ** @see ClearBuffer, GetAutoClearBuffer
	 */
	void SetAutoClearBuffer(bool _acb) { flags.set(AUTO_CLEAR_BUFFER, _acb);  NeedsUpdate(); }

	/** When set to true, the widget will draw to a software back-buffer before 
	 ** drawing the buffer to the screen.  This can be used to create smoother
	 ** animation, but will take more memory and can be slower.
	 **
	 ** \textbf{Default: false}
	 ** 
	 ** @see SetAutoSaveBuffer, GetDoubleBuffered
	 */
	void SetDoubleBuffered(bool _db) {
		if(_db != GetDoubleBuffered()) {
			flags.set(DOUBLE_BUFFERED, _db);
			RecreateAllBuffers();
		}
	}

	/** When set to false, this widget and its children will be passed over when input
	 ** is being passed down the widget tree.  When set to true, everything will
	 ** operate as expected.
	 ** 
	 ** \textbf{Default: true}
	 ** 
	 ** @see GetAcceptInput
	 */
	void SetAcceptInput(bool _ai) { flags.set(ACCEPT_INPUT, _ai); }

	/** When set to true, causes the widget client area to act like a "hole" through 
	 ** the widget.  The widget will only recieve mouse events if they hit outside
	 ** the client area.
	 ** 
	 ** \textbf{Default: false}
	 ** 
	 ** @see GetHollowClientArea
	 */
	void SetHollowClientArea(bool _hca) { flags.set(HOLLOW_CLIENT_AREA, _hca); }

	/** When set to true along with DoubleBuffered, this widget will automatically
	 ** re-draw the buffer when repainted, but not changed. This should speed up 
	 ** certain operations.
	 ** 
	 ** \textbf{Default: true}
	 **
	 **	@see SetDoubleBuffered, GetAutoSaveBuffer
	 */
	void SetAutoSaveBuffer(bool _asb) { flags.set(AUTO_SAVE_BUFFER, _asb); }

	/** When set to true, the Changed flag will be reset after every draw loop.  Most
	 ** of the time this is the desired behavior.
	 ** 
	 ** @see MarkAsChanged, MarkAllAsChanged
	 */
	void SetAutoResetChanged(bool _arc) { flags.set(AUTO_RESET_CHANGED, _arc); }

	/** When set to true, the widget background will be transparent.  In most cases
	 ** this means that the buffer is never cleared.  But when it is double buffered
	 ** is has to copy part of the parent surface to its buffer first.  Combining
	 ** double buffered and transparent background can be slow.
	 ** 
	 ** \textbf{Default: false}
	 ** 
	 ** @see GetTransparentBackground
	 */
	void SetTransparentBackground(bool _tb)	{ 
		flags.set(TRANSPARENT_BACKGROUND, _tb); 
		GetParent()->NeedsRepaint(); 
	}

	/** Used by children to "scroll" the client area.  This causes all of the children
	 ** widgets and anything else on the client surface (or in the client area) to
	 ** move by (-ScrollX, -ScrollY).  UI might use ScrollBars, etc. in the decoration 
	 ** area of the widget.
	 */
	void SetScroll(unsigned int ScrollX, unsigned int ScrollY) {
		NewScrollX = ScrollX;
		NewScrollY = ScrollY;
		NeedsRepaint();
	}

	/** Override to make a non-client custom widget.  Returns true by default.
	 */
	bool IsClientWidget() const { return true; }
private:
	TSubSurface* Surface, *ClientSurface;
	int Width, Height;
	unsigned int NewScrollX, NewScrollY;
	TPoint2D Position;
	std::string Caption;
	TRect ClientRect;
	std::bitset<FLAGS_MAX> flags;

	// recreates the buffers
	void RecreateBuffer();

	// uses flags to control how widget is drawn and makes sure the client rect
	// and the scroll values are applied.
	bool DrawWidget(DrawType dt);

	// make sure that scroll and client rect are applied to the surface
	TPoint2D GetChildPosition(TWidget* child) const;

	// returns the surface used as the parent surface
	TSurface* GetParentSurface() { 
		return IsClientWidget() ? GetParent()->GetClientSurface() : GetParent()->GetSurface();
	}

	//
	// NOTE: i'm using private so that these can't be overridden again
	//
	
	// use the sub-surface API to deal with buffering
	bool RequiresUpdate() const { 
		return Surface ? Surface->IsBuffered() : false; 
	}

	// use the sub-surface API to deal with buffering
	void UpdateWidget() { 
		if(Surface) Surface->CopyToParent(); 
	}

	// Use Visible, Enabled, and the accept input flag to determine if this 
	// widget can accept input.
	bool CanAcceptInput() const { 
		return IsVisible() && IsEnabled() && GetAcceptInput(); 
	}
	
	// The widget can always accept mouse input unless the hollow client 
	// area flag is used.
	bool CanAcceptMouseInput(int X, int Y) const {
		if(GetHollowClientArea()) {
			return !ClientRect.IsInside(TPoint2D(X, Y));
		}
		return true;
	}
};
//---------------------------------------------------------------------------  
}; // namespace ksd
//---------------------------------------------------------------------------  
#endif



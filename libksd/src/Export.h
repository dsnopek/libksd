/*
    libksd -- KewL STuFf DirectMedium
    Copyright 2000-2002 David Snopek

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

/* This file should allow provide all the macros necessary for dealing with
 * libksd DLLs.  You can use these for creating add-on libraries, build applications,
 * or even writting plugins.  These macros are also used internally by libksd
 * through the macros provided in export_ksd.h.  Use that file as a template
 * for use in your own projects.
 */

#ifndef _KSD_ExportH
#define _KSD_ExportH

/* Only WIN32 and BeOS require symbol exports */
#if !defined(WIN32) && !defined(__BEOS__)
# define NO_EXPORT
# define IMPORT_CMD
# define EXPORT_CMD
#else
# define IMPORT_CMD __declspec(dllimport)
# define EXPORT_CMD __declspec(dllexport)
#endif

/* header meat */
#ifndef NO_EXPORT
// export commands
# define DO_EXPORT_PLUGIN_FUNC extern "C" EXPORT_CMD
# define DO_EXPORT_CLASS EXPORT_CMD
# define DO_EXPORT_TEMPLATE __export
# define DO_EXPORT EXPORT_CMD
// import commands
# define DO_IMPORT_PLUGIN_FUNC extern "C" IMPORT_CMD
# define DO_IMPORT_CLASS
# define DO_IMPORT_TEMPLATE __import
# define DO_IMPORT IMPORT_CMD
#else
// null definitions
# define DO_EXPORT_PLUGIN_FUNC extern "C"
# define DO_EXPORT_CLASS
# define DO_EXPORT_TEMPLATE
# define DO_EXPORT
# define DO_IMPORT_PLUGIN_FUNC extern "C"
# define DO_IMPORT_CLASS
# define DO_IMPORT_TEMPLATE
# define DO_IMPORT
#endif

/* For seperating plugin func declarations and implementations. */
#define IMPLEMENT_PLUGIN_FUNC extern "C"

/* _KSD_EXPORTH */
#endif


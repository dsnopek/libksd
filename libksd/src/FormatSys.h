//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//	
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		TFormatSystem
//	
//	Purpose:		A reusable, plugin capable file loader.
//	
//	Checked-in by:	$Author$
//					$Date$
//---------------------------------------------------------------------------
#ifndef _KSD_FormatSysH
#define _KSD_FormatSysH
//---------------------------------------------------------------------------
#include "PluginSys.h"
#include "exception.h"
#include "log.h"
//---------------------------------------------------------------------------
/** Thrown when you try to load or save a file with an unknown file format.
 */
KSD_EXPORT_CLASS class EUnknownFileFormat : public ::ksd::exception {
public:
	/** Create an unknown file format exception.
	 ** 
	 ** @param message The error message.
	 ** @param format_name The format name that error involves.
	 */
	EUnknownFileFormat(const std::string& message, const std::string& format_name)
		: ::ksd::exception("Format System", message, "format system error")
	{
		FormatName = format_name;
	}

	/// Returns the format name
	std::string GetFormatName() const { return FormatName; }
private:
	std::string FormatName;
};
//---------------------------------------------------------------------------
namespace ksd {
//---------------------------------------------------------------------------
template<typename Type>
class TFileFormat {
public:
	// TODO: Add the possibility for text and binary saves
    virtual void Read(const char* FileName, Type* Object) = 0;
    virtual void Write(const char* FileName, Type* Object) = 0;
    virtual bool AutoDetect(const char* FileName) = 0;

    char* GetSupportedFormat()      { return SupportedFormat; } // identifier for format type (Usually the file signature)
    char* GetExtension()            { return Extension; }		// files default extension (*.extension)
    char* GetDescription()          { return Description; }		// short word description of file (THe one that comes in a file dialog, usually include the extension in parenthesis)
    char* GetModuleName()           { return ModuleName; }		// Name/Version description of this loader (ex. Anim2D1.0 Loader v2.3)
    float GetVersion()              { return Version; }			// Version of the format that this class reads/writes (NOT THE VERSION OF THE LOADER)

    bool Supports(char* FormatID) {
        if(strcmp(FormatID, SupportedFormat) == 0) return true;
        return false;
    }
protected:
    TFileFormat(char* format_id, char* extension, char* description, char* module_name, float version) {
        SupportedFormat = format_id;
        Extension = extension;
        Description = description;
        ModuleName = module_name;
        Version = version;
    }
private:
    char* SupportedFormat;
    char* Extension;
    char* Description;
    char* ModuleName;
    float Version;
};
//---------------------------------------------------------------------------
template<typename Type, const char* TypeName>
class TFormatSystem : public TPluginSystem<TFileFormat<Type>, TypeName, std::string> {
public:
	// modified TPluginReg decl
	typedef void (*TFormatReg)(TFormatSystem<Type, TypeName>* Sys);

    TFormatSystem() 
    	: TPluginSystem<TFileFormat<Type>, TypeName, std::string>() { };

    TFormatSystem(TFormatReg _init) 
    	: TPluginSystem<TFileFormat<Type>, TypeName, std::string>((typename TFormatSystem<Type, TypeName>::TPluginReg)_init) { };

    virtual void Load(char* FormatID, const char* FileName, Type* Object) {
		TFileFormat<Type>* fmt = TPluginSystem<TFileFormat<Type>, TypeName, std::string>::GetPlugin(FormatID);

	    if(!fmt) {
			throw EUnknownFileFormat("Cannot read file with unknown file format", FormatID);
	    }
	    
	    fmt->Read(FileName, Object); 
    }
    
    virtual void Save(char* FormatID, const char* FileName, Type* Object) {
		TFileFormat<Type>* fmt = TPluginSystem<TFileFormat<Type>, TypeName, std::string>::GetPlugin(FormatID);

	    if(!fmt) {
			throw EUnknownFileFormat("Cannot write file with unknown file format", FormatID);
	    }
	    
	    fmt->Write(FileName, Object); 
    }
    
    virtual bool AutoDetect(const char* FileName, Type* Object) {
    	TFileFormat<Type>* fmt;

		for(typename std::map<std::string, TFileFormat<Type>* >::iterator i = this->List.begin(); i != this->List.end(); i++) {
			fmt = (*i).second;
			if(fmt->AutoDetect(FileName)) {
				fmt->Read(FileName, Object);
				return true;
			}
		}

		return false;
	}

    // should hide TPluginSystem<TFileFormat<Type>*, TypeName, std::string>::Register
    void Register(TFileFormat<Type>* Plugin) {
		TPluginSystem<TFileFormat<Type>, TypeName, std::string>::Register(Plugin, 
			std::string(Plugin->GetSupportedFormat()));	
    }

    char* MakeFilter() {
		TFileFormat<Type>* cur;
	    std::string Filter;
	    char* ext;

		// add inital 'any file' line
		Filter = "Any File (*.*)|*.*";

		// FLAWED!!
		// NEEDS TO ADD EXTENSIONS ONLY ONCE!!
	    for(typename std::map<std::string, Type*>::iterator i = this->List.begin(); i != this->List.end(); i++) {
			// store current plugin
			cur = (*i).second;
	    
			// store extension because it is used twice (minimze func calls)
			ext = cur->GetExtension();
		
	        Filter += "|" + cur->GetDescription() + " (" + ext + ")|" + ext;
	    }

	    return Filter;
    }
};
//---------------------------------------------------------------------------
}; // namespace ksd
//---------------------------------------------------------------------------
#endif


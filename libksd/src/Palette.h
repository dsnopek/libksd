//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//	
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		TPalette
//	
//	Purpose:		A palette object
//	
//	Checked-in by:	$Author$
//					$Date$
//--------------------------------------------------------------------------- 
#ifndef _KSD_PaletteH_
#define _KSD_PaletteH_
//--------------------------------------------------------------------------- 
#include <vector>
#include "Color.h"
#include "export_ksd.h"

struct SDL_Surface;
//---------------------------------------------------------------------------
#ifdef __GNUG__
#pragma interface
#endif
//---------------------------------------------------------------------------
namespace ksd {
//---------------------------------------------------------------------------
KSD_EXPORT_CLASS class TPalette { friend class TSurface;
public:
	TPalette();

	// accessors to colors
	TColor			GetColor(int index) const 	{ return Data[index]; };
	unsigned int	GetSize() const				{ return Data.size(); };

	void SetColor(int index, const TColor& V);
	void AddColor(const TColor& V);
	void SetSize(unsigned int size);

	void ApplyPalette(SDL_Surface* Surface);
	void GatherPalette(SDL_Surface* Surface);

	int  FindBestMatch(const TColor& Color) const;
private:
	bool Changed;
	std::vector<TColor> Data;
};
//---------------------------------------------------------------------------
}; // namespace ksd
//---------------------------------------------------------------------------
#endif


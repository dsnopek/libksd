//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//	
//--------------------------------------------------------------------------- 
//	$Header$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Checked-in by:	$Author$
//	
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma implementation
#endif
//--------------------------------------------------------------------------- 
#define KSD_BUILD_LIB
#include <fstream>
#include "PluginSysManager.h"
#include "SharedLib.h"
//--------------------------------------------------------------------------- 
using namespace ksd;
//--------------------------------------------------------------------------- 
void TPluginSystemManager::Clear()
{
	while(PluginSystem.size() > 0) {
		delete PluginSystem.back();
		PluginSystem.pop_back();
	}

	// call parent clear function
	TPluginSystemBase::Clear();
}
//--------------------------------------------------------------------------- 
void TPluginSystemManager::AddPluginSystem(TPluginSystemBase* _psb)
{
	mutex.Lock();
	PluginSystem.push_back(_psb);
	mutex.Unlock();
}
//--------------------------------------------------------------------------- 
void TPluginSystemManager::RemovePluginSystem(TPluginSystemBase* _psb)
{
	int index = -1;

	// find plugin system
	for(unsigned int i = 0; i < PluginSystem.size(); i++) {
		if(PluginSystem[i] == _psb) {
			index = i;
			break;
		}
	}

	// if the plugin is actually found, remove it
	if(index != -1) {
		mutex.Lock();
		PluginSystem.erase(PluginSystem.begin() + index);
		mutex.Unlock();
	}
}
//--------------------------------------------------------------------------- 
bool TPluginSystemManager::LoadPlugin(TSharedLibrary* Lib)
{	
	bool found = false;

	mutex.Lock();
	for(unsigned int i = 0; i < PluginSystem.size(); i++) {
		if(PluginSystem[i]->IsValidPlugin(Lib)) {
			if(PluginSystem[i]->LoadPlugin(Lib)) found = true;
		}
	}
	mutex.Unlock();

	return found;
}
//--------------------------------------------------------------------------- 
bool TPluginSystemManager::IsValidPlugin(TSharedLibrary* Lib)
{	
	bool found = false;

	mutex.Lock();
	for(unsigned int i = 0; i < PluginSystem.size(); i++) {
		if(PluginSystem[i]->IsValidPlugin(Lib)) {
			found = true;
			break;
		}
	}
	mutex.Unlock();

	return found;
}
//--------------------------------------------------------------------------- 


//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	$Header$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Checked-in by:	$Author$
//
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma implementation
#endif
//--------------------------------------------------------------------------- 
#define KSD_BUILD_LIB
#include <fstream>
#include <strstream>
#include <iostream>
#include "Font.h"
#include "exception.h"
#include "log.h"
#include "ksdconfig.h"

#ifdef KSD_USE_SFONT
#include "BitmapFont.h"
#endif 

#ifdef KSD_USE_TTF
#include "TrueTypeFont.h"
#endif

using namespace ksd;
using namespace std;
//---------------------------------------------------------------------------  
const TFontAttr ksd::DefaultFontAttr(TFont::Normal, 12, TColor::black);
//---------------------------------------------------------------------------  
TFont* TFont::LoadFont(const std::string& filename)
{
	#if !defined(KSD_USE_SFONT) && !defined(KSD_USE_TTF)
		throw EUnsupported("Font", 
			"Trying to load a font when no font formats are available.  "
			"Either enable TTF or SFONT");
	#else

	TFont* Font = NULL;
	int len = filename.length();
	string ext;

	// files ending in ".ttf" are True Type all others are sfont
	ext = filename.substr(len - 4, 4);
	if(ext == ".ttf") {
		#ifdef KSD_USE_TTF
		Font = new TTrueTypeFont();
		#endif
	} else {
		#ifdef KSD_USE_SFONT
		Font = new TBitmapFont();
		#endif
	}

	// open file
	if(Font != NULL) {
		try {
			Font->Open(filename);
		} catch(::ksd::exception& e) {
			e.report();
			delete Font;
			Font = NULL;
		}
	}

	// check for failure
	if(!Font) {
		throw EFileError("Font", "Unable to load the specified font", filename);
	}

	// if this font works, then return it
	return Font;
	//List[string(font_name)] = Font;
	
	#endif
}
//---------------------------------------------------------------------------  
void TFontList::LoadFont(const std::string& font_name, const std::string& filename)
{
	TFont* Font = TFont::LoadFont(filename);
	// No need to check Font != NULL.  I am confident that the exception handling
	// system will catch all errors.
	List[string(font_name)] = Font;
}
//--------------------------------------------------------------------------- 
void TFontList::LoadList(const std::string& filename)
{
	#if !defined(KSD_USE_SFONT) && !defined(KSD_USE_TTF)
	throw EUnsupported("Font", 
			"Trying to load a font when no font formats are available.  "
			"Either enable TTF or SFONT");
	#else

	ifstream infile(filename.c_str());
	char line[255];
	std::string name, temp, directory;

	if(!infile) {
		throw EFileError("Font", "Unable to open font list.", filename);
	}

	while(!infile.eof()) {
		// read line
		infile.getline(line, 255);
		istrstream line_stream(line);
		
		// get name of font
		line_stream >> name;
		
		// check for comment symbol
		if(name[0] == '#') continue;

		// check for special commands
		if(name[0] == '/') {
			if(name == "/cd") {
				// change current directory -- makes configuration files
				// easier to read...
			
				line_stream >> temp;
				directory += temp + '/';
			}
			
			// don't try to load it as a font
			continue;
		}

		LOG(8, "Loading font: %s...", name.c_str());

		// read font files, trying each
		while(!line_stream.eof()) {
			line_stream >> temp;
			
			// check for comment symbol
			if(temp[0] == '#') continue;

			// if this font works, then break
			try {
				LoadFont(name.c_str(), (directory + temp).c_str());
			} catch(...) { continue; }
			break;
		}
	}
		
	#endif
}
//--------------------------------------------------------------------------- 


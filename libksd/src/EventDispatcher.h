//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		TEventDispatcher
//	
//	Purpose:		Used to pass events to the widget tree.  Encapsulates
//					the underlying event sub-system.
//	
//	Checked-in by:	$Author$
//					$Date$
//---------------------------------------------------------------------------
#ifndef _KSD_EventDispatcherH_
#define _KSD_EventDispatcherH_
//---------------------------------------------------------------------------
// this screws up gcc-2.95.2 somehow?!?
//#include <bitset>
#include "Widget.h"
#include "Joystick.h"
#include "export_ksd.h"
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma interface
#endif
//---------------------------------------------------------------------------
namespace ksd {
//---------------------------------------------------------------------------
/** Passes events down the widget tree.  Override this class to provide a 
 ** new event backend.
 */
KSD_EXPORT_CLASS class TEventDispatcher {
private:
	// joystick flags
	enum {
		JOY_AXIS_REPEAT,
		JOY_EVENTS,
		JOY_CURSOR,

		JOY_FLAGS_MAX
	};
public:
	/** Contruct an event dispatcher that uses the given widget tree.
	 */
	TEventDispatcher(TWidgetTree* _tree) {
		WidgetTree = _tree;
		JoyCursorNum = 0;
		JoyCursorSens = 2;
		KeyboardFocus = JoystickFocus = WidgetTree->GetRoot();
		MouseFocus = NULL;
	}
	~TEventDispatcher() { }

	/// Returns the widget tree that this object dispatches to.
	TWidgetTree* GetWidgetTree() { return WidgetTree; }
	
	/// Returns true if joystick axis repeat is enabled.
	bool GetJoystickAxisRepeat() const { 
		TJoystickSubsystem::GetInstance()->Scan();
		return joy_flags.test(JOY_AXIS_REPEAT); 
	}
	/// Returns true if joystick events are enabled.
	bool GetJoystickEvents() const { return joy_flags.test(JOY_EVENTS); }
	/// Returns true if joystick cursor is enabled.
	bool GetJoystickCursor() const { return joy_flags.test(JOY_CURSOR); }
	/// Returns the joystick cursor sensitivity.
	unsigned int GetJoystickCursorSensitivity() const { return JoyCursorSens; }
	/// Returns the joystick number that controls the cursor.
	unsigned int GetJoystickCursorNum() const { return JoyCursorNum; }
	
	/// Returns the widget receiving keyboard focus.
	TWidget* GetKeyboardFocus() { return KeyboardFocus; }
	/// Returns the widget receiving mouse input.
	TWidget* GetMouseFocus() { return MouseFocus; }
	/// Returns the widget receiving joystick input.
	TWidget* GetJoystickFocus() { return JoystickFocus; }

	/** Enables/disables joystick axis repeat.  This causes all axis events to
	 ** be repeated until they no longer occur.
	 */
	void SetJoystickAxisRepeat(bool enable) { 
		joy_flags.set(JOY_AXIS_REPEAT, enable);
		DoSetJoystickAxisRepeat(enable);
	}

	/** Enables/disables joystick events.  When disabled the application will not
	 ** receive any joystick events.
	 */
	void SetJoystickEvents(bool enable) {
		joy_flags.set(JOY_EVENTS, enable);
		DoSetJoystickEvents(enable);
	}

	/** Enables/disables the joystick cursor.  This causes a particular joystick
	 ** to control the cursor.  (Note:  As of libksd 0.0.4 this is not implemented.)
	 */
	void SetJoystickCursor(bool enable) { 
		joy_flags.set(JOY_CURSOR, enable); 
		DoSetJoystickCursor(enable);
	}

	/** Sets the joystick cursor sensitivity.
	 */
	void SetJoystickCursorSensitvity(unsigned int _s) {
		JoyCursorSens = _s;
		DoSetJoystickCursorSensitivity(_s);
	}

	/** Sets which joystick controls the cursor.
	 */
	void SetJoystickCursorNum(unsigned int _joy) { 
		JoyCursorNum = _joy; 
		DoSetJoystickCursorNum(_joy);
	}

	/// Sets the widget to recieve keyboard input.
	void SetKeyboardFocus(TWidget* w) { 
		// we cant set keyboard focus to NULL!
		if(w == NULL) {
			KeyboardFocus = WidgetTree->GetRoot();
		} else {
			KeyboardFocus = w; 
		}
	}

	/// Sets the widget to receive joystick input.
	void SetJoystickFocus(TWidget* w) { 
		// we cant set joystick focus to NULL!
		if(w == NULL) {
			JoystickFocus = WidgetTree->GetRoot();
		} else {
			JoystickFocus = w;
		}
	}

	/** Dispatch the mouse down event to the widget tree.  The clicked widget
	 ** receives the mouse focus.
	 */
	void HandleMouseDown(int X, int Y, TMouseButton button);

	/** Dispatch the mouse move event to the widget tree.  This will also deal
	 ** with calling the OnEnter and OnExit handlers when appropriate.
	 */
	void HandleMouseMove(int X, int Y, int RelX, int RelY, TMouseButton button) {
		if(MouseFocus) {
			MouseFocus->MakeLocal(X, Y);
			MouseFocus->DoMouseMove(X, Y, RelX, RelY, button);
		} else {
			_DoHandleMouseMove(X, Y, RelX, RelY, button);
		}
	}

	/** Dispatch the enter root event.  This event occurs when the mouse enters
	 ** the root widget (usually the window in which libksd resides, but this 
	 ** depends on the backend).
	 */
	void HandleEnterRoot() {
		WidgetTree->GetRoot()->DoEnter();
	}
	
	/** Dispatch the enter root event.  This event occurs when the mouse exits
	 ** the root widget (usually the window in which libksd resides, but this 
	 ** depends on the backend).
	 */
	void HandleExitRoot() {
		WidgetTree->GetRoot()->DoExit();
	} 

	/// Dispatch the mouse up event to the widget tree.
	void HandleMouseUp(int X, int Y, TMouseButton button) {
		if(MouseFocus != WidgetTree->GetRoot()) 
			MouseFocus->MakeLocal(X, Y);

		MouseFocus->DoMouseUp(X, Y, button);
		MouseFocus = NULL;
	}
	
	/// Dispatch the key down event to the widget tree.
	void HandleKeyDown(const TKey& key) {
		if(KeyboardFocus) {
			KeyboardFocus->DoKeyDown(key);
		}
	}
	
	/// Dispatch the key up event to the widget tree.
	void HandleKeyUp(const TKey& key) {
		// NOTE: this is useless because keyboard focus should always have 
		// a value but for some reason this just isn't true.  When the 
		// application loses focus, key-ups are called but the focus val
		// seems to be NULL.  I don't know why ...
		if(KeyboardFocus) {
			KeyboardFocus->DoKeyUp(key);
		}
	}

	/// Dispatch the joy axis move event to the widget tree.
	void HandleJoyAxisMove(Uint8 joy, Uint8 axis, Sint16 value) {
		JoystickFocus->DoJoyAxisMove(joy, axis, value);
	}
	
	/// Dispatch the joy ball move event to the widget tree.
	void HandleJoyBallMove(Uint8 joy, Uint8 axis, Sint16 X, Sint16 Y) {
		JoystickFocus->DoJoyBallMove(joy, axis, X, Y);
	}
	
	/// Dispatch the joy hat move event to the widget tree.
	void HandleJoyHatMove(Uint8 joy, Uint8 hat, TJoyHatPosition pos) {
		JoystickFocus->DoJoyHatMove(joy, hat, pos);
	}
	
	/// Dispatch the joy down event to the widget tree.
	void HandleJoyDown(Uint8 joy, Uint8 button) {
		JoystickFocus->DoJoyDown(joy, button);
	}
	
	/// Dispatch the joy up event to the widget tree.
	void HandleJoyUp(Uint8 joy, Uint8 button) {
		JoystickFocus->DoJoyUp(joy, button);
	}

	/** Override to provide the actual event polling and dispatching code based
	 ** on the underlying event backend.
	 */
	virtual void PollEvents() = 0;
protected:
	/// Override to do system specific set-up for joystick axis repeat
	virtual void DoSetJoystickAxisRepeat(bool enable) = 0;
	/// Override to do system specific set-up for joystick events
	virtual void DoSetJoystickEvents(bool enable) = 0;
	/// Override to do system specific set-up for joystick cursor
	virtual void DoSetJoystickCursor(bool enable) = 0;
	/// Override to do system specific set-up for joystick cursor sensitivity
	virtual void DoSetJoystickCursorSensitivity(unsigned int) = 0;
	/// Override to do system specific set-up for joystick cursor number
	virtual void DoSetJoystickCursorNum(unsigned int) = 0;
private:
	void _DoHandleMouseMove(int, int, int, int, TMouseButton);

	TWidgetTree* WidgetTree;
	TWidget* MouseFocus, *KeyboardFocus, *JoystickFocus;
	unsigned int JoyCursorSens, JoyCursorNum;
	std::bitset<JOY_FLAGS_MAX> joy_flags;
};
//---------------------------------------------------------------------------  
}; // namespace ksd
//---------------------------------------------------------------------------  
#endif


//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//	
//---------------------------------------------------------------------------
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		TPluginSystem
//	
//	Purpose:		Loads plugin objects from a shared library.  It is the 
//					base class for a sleu of plugin capable classes, 
//					including: ClassFactory, TFormatSystem, the PIM.
//	
//	Checked-in by:	$Author$
//					$Date$
//---------------------------------------------------------------------------
#ifndef _KSD_PluginSysH
#define _KSD_PluginSysH
//--------------------------------------------------------------------------- 
#include <map>
#include <string>
#include "Thread.h"
#include "PluginSysBase.h"
#include "SharedLib.h"
#include "log.h"
//---------------------------------------------------------------------------
namespace ksd {
//--------------------------------------------------------------------------- 
//	TPluginSystem<class T, const char* TypeName, class Key>
//	
//		T = the plugin class type
//		TypeName = a name describing the type that the plugin loads. MUST BE
//				   UNIQUE TO ALL PLUGINSYSTEMS.
//		Key = the type used to "name" plugins for look up
//		
//	To use, first you need a plugin class, which is basically just an interface
//	that is used by some application.  Then you need to write a shared library
//	containing a function: void Plugin${TypeName}(TPluginSystem<T, TypeName>*),
//	which calls the register function with several child classes of 'T'.  The
//	application using these plugins needs a method of selecting plugins based
//	on there 'Key'.
//---------------------------------------------------------------------------
template<typename T, const char* TypeName, typename Key = std::string>
class TPluginSystem : public TPluginSystemBase {
public:
	// defines the register function that loads all the plugins from a library
    typedef void (*TPluginReg)(TPluginSystem<T, TypeName, Key>* Sys);

    TPluginSystem() {
    	// Note: we cannot do this!  It is unportable!  Memory corruption abounds!
        //RegisterPluginSystem(this);
    }
    TPluginSystem(TPluginReg _initsys) { // used to initialize or to disable plugins
    	// Note: we cannot do this!  It is unportable!  Memory corruption abounds!
        //RegisterPluginSystem(this);
        _initsys(this);
    }
    virtual ~TPluginSystem() {
    	// Note: we cannot do this!  It is unportable!  Memory corruption abounds!
        //UnregisterPluginSystem(this);
    }

    virtual void Register(T* Plugin, const Key& name) {
        mutex.Lock();

   	typename std::map<Key, T*>::iterator i = List.find(name);
   	if(i == List.end()) { // first come first serve
                List[name] = Plugin;
        }

        mutex.Unlock();
    }

    virtual void Remove(const Key& name) {
        mutex.Lock();

        typename std::map<Key, T*>::iterator i = List.find(name);
        if(i != List.end()) { // there is an item of this name
            delete (*i).second;
    	    List.erase(i);
        }

        mutex.Unlock();
    }

    virtual void Clear() {
        mutex.Lock();

	    // free memory
        for(typename std::map<Key, T*>::iterator i = List.begin(); i != List.end(); i++) {
    	    delete (*i).second;
        }
        List.clear();

        mutex.Unlock();
    }

    T*  GetPlugin(const Key& name) {
        TMutexLocker lock(mutex);

        typename std::map<Key, T*>::iterator i = List.find(name);
        if(i != List.end()) { // an item was found
    	    return (*i).second;
        }

        // if there is no element of that name
        return NULL;
    }

    unsigned int GetPluginCount() {
        TMutexLocker lock(mutex);

        return List.size();
    }

	bool IsValidPlugin(TSharedLibrary* LibInstance) {
        std::string RegName;

        // assemble register function from typename
        RegName = std::string("Plugin") + TypeName;

        // check to see if the symbol exists
        LOG(8, "%s%s: Checking for register function", TypeName, "PluginSystem");
        return LibInstance->HasSymbol(RegName.c_str());
    }
protected:
    virtual bool LoadPlugin(TSharedLibrary* LibInstance) {
        std::string RegName;
        TPluginReg Plugin = NULL;

        // assemble register function from typename
        RegName = std::string("Plugin") + TypeName;

	    // check for the symbol first
	    if(!LibInstance->HasSymbol(RegName.c_str())) return false;

        // load plugin registration function
        LOG(8, "%s%s: Loading plugin register function", TypeName, "PluginSystem");
        LibInstance->GetSymbol((void**)&Plugin, RegName.c_str());

        // register plugins
	    LOG(8, "%s%s: Calling plugin register function", TypeName, "PluginSystem");
	    mutex.Lock();
	    Plugin(this);
	    mutex.Unlock();

	    return true;
    }

    std::map<Key, T*> List;
private:
    TMutex mutex;
};
//---------------------------------------------------------------------------
}; // namespace ksd
//---------------------------------------------------------------------------
#endif


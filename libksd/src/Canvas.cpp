//---------------------------------------------------------------------------
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//
//---------------------------------------------------------------------------
//	$Header$
//
//	Author:			David Snopek
//	Revised by:		Andrew Sterling Hanenkamp
//	Version:		$Revision$
//
//	Checked-in by:	$Author$
//
//---------------------------------------------------------------------------
#ifdef __GNUG__
#pragma implementation
#endif
//---------------------------------------------------------------------------
#define KSD_BUILD_LIB
#include "ksdconfig.h"
#include "backends/sdl/sdlconv.h"

#include "Application.h"
#include "Canvas.h"
#include "Surface.h"
#include "Image.h"
#include "ImageIterator.h"
#include "Polygon.h"
#include "Circle.h"
#include "Line.h"
#include "log.h"
#include "clip.h"

// for debugging
#include <iostream>
using namespace std;

using namespace ksd;
//---------------------------------------------------------------------------
//	A function (made macro for speed) that sets up variables for correct
//	clipping proceedure
#if 0
#define CANVAS_PREPARE_CLIPPING(minx, miny, maxx, maxy) \
	{ \
		int temp_w, temp_h; \
		if(DimWidth == 0) { \
			temp_w = Surface->GetWidth() - 1; \
		} else temp_w = DimWidth; \
		if(DimHeight == 0) { \
			temp_h = Surface->GetHeight() - 1; \
		} else temp_h = DimHeight; \
		if(ClipRect.GetWidth() == 0 || ClipRect.GetHeight() == 0) { \
			minx = 0; \
			miny = 0; \
			maxx = temp_w; \
			maxy = temp_h; \
		} else { \
			if(ClipRect.GetLeft() < 0) { \
				minx = 0; \
			} else minx = ClipRect.GetLeft(); \
			if(ClipRect.GetRight() > temp_w) { \
				maxx = temp_w; \
			} else maxx = ClipRect.GetRight(); \
			if(ClipRect.GetTop() < 0) { \
				miny = 0; \
			} else miny = ClipRect.GetTop(); \
			if(ClipRect.GetBottom() > temp_h) { \
				maxy = temp_h; \
			} else maxy = ClipRect.GetBottom(); \
		} \
		if(SystemClipRect) { \
			if(minx < SystemClipRect->GetLeft()) { \
				minx = SystemClipRect->GetLeft(); \
			} \
			if(miny < SystemClipRect->GetTop()) { \
				miny = SystemClipRect->GetTop(); \
			} \
			if(maxx > SystemClipRect->GetRight()) { \
				maxx = SystemClipRect->GetRight(); \
			} \
			if(maxy > SystemClipRect->GetBottom()) { \
				maxy = SystemClipRect->GetBottom(); \
			} \
		} \
		minx -= OffsetX; \
		miny -= OffsetY; \
		maxx -= OffsetX; \
		maxy -= OffsetY; \
		if(minx < -(DimX + OffsetX)) minx = -(DimX + OffsetX); \
		if(miny < -(DimY + OffsetY)) miny = -(DimX + OffsetY); \
	}
#endif

// Used to make sure that BeginDraw() was called
#define CHECK_DRAWBLOCK \
	if(!DrawBlock) throw("Trying to draw to a TCanvas that was never BeginDraw()'n")
//---------------------------------------------------------------------------
template<class Iterator>
static void inline _do_setpixel(Iterator& iter, Uint32 color, TDrawMode mode)
{
	LOG_FUNCTION("_do_setpixel<Iterator>(Iterator&, Uint32, TDrawMode)");

	switch(mode) {
		case OP_REPLACE:
			*iter = color;
			break;
		case OP_AND:
			*iter = (*iter & color);
			break;
		case OP_OR:
			*iter = (*iter | color);
			break;
		case OP_XOR:
			*iter = (*iter ^ color);
			break;
	}
}
//---------------------------------------------------------------------------
class DrawShapeVisitor : public TShape2D::Visitor {
public:
	DrawShapeVisitor(TCanvas* _canvas, bool _fill) { 
		Canvas = _canvas;
		Fill = _fill;
	}
	
	void VisitRect(TRect* rect) { 
		if(Fill) {
			Canvas->FillRect(rect->GetLeft(), rect->GetTop(), 
					rect->GetRight(), rect->GetBottom());
		} else {
			Canvas->FrameRect(rect->GetLeft(), rect->GetTop(),
					rect->GetRight(), rect->GetBottom());
		}
	}
	
	void VisitCircle(TCircle* circle) { 
		TPoint2D Center = circle->GetCenter();
		if(Fill) {
			Canvas->FillCircle(Center.X, Center.Y, circle->GetRadius());
		} else {
			Canvas->Circle(Center.X, Center.Y, circle->GetRadius());
		}
	}
	
	void VisitLine(TLine* line) { 
		if(line->IsVerticle()) {
			Canvas->VertLine(line->GetIntercept().ToInt(), 
					line->GetStart(),
					line->GetStop());
		} else if(line->IsHorizontal()) {
			Canvas->HorzLine(line->GetIntercept().ToInt(),
					line->GetStart(),
					line->GetStop());
		} else {
			TPoint2D start, end;

			start = line->GetStartPoint();
			end = line->GetEndPoint();

			Canvas->Line(start.X, start.Y, end.X, end.Y);
		}
	}
private:
	TCanvas* Canvas;
	bool Fill;
};
//---------------------------------------------------------------------------
TCanvas::TCanvas(TSurface* _surface)
{
	Surface = _surface;
	Mode = OP_REPLACE;
	Font = NULL;
	DrawBlock = 0;
}
//---------------------------------------------------------------------------  
bool TCanvas::PrepareClipping(int& minx, int& miny, int& maxx, int& maxy)
{
	TRect temp;

	if(ClipRect.IsValid()) {
		// clip the TCanvas rectangle against the surface one
		temp = ClipRect;
		if(!Surface->GetClipRect().ClipRect(temp))
			return false;
	} else temp = Surface->GetClipRect();

	minx = temp.GetLeft();
	miny = temp.GetTop();

	if(temp.GetRight() < Surface->GetWidth() - 1) {
		maxx = temp.GetRight();
	} else maxx = Surface->GetWidth() - 1;
	if(temp.GetBottom() < Surface->GetHeight() - 1) {
		maxy = temp.GetBottom();
	} else maxy = Surface->GetHeight() - 1;

	return !(maxx <= minx && maxy <= miny);
}
//---------------------------------------------------------------------------  
void TCanvas::SetPixel(int X, int Y, const TColor& color)
{
	LOG_FUNCTION("TCanvas::SetPixel(int, int, const TColor&)");

	if(IsValid()) {
		Uint32 raw_color = Surface->GetFormat().MapToPixel(color);
		int minx, miny, maxx, maxy;

		if(!PrepareClipping(minx, miny, maxx, maxy)) return;

		if(CLIP_POINT(X, Y, minx, miny, maxx, maxy)) {
			ImageIterator iter = Surface->make_iterator(ImageIterator::READ_WRITE);

			iter.increment(X, Y);
			_do_setpixel(iter, raw_color, Mode);

			LOG(9, "Plot(%d, %d) = <color>", X, Y);
		}
	}
}
//---------------------------------------------------------------------------
void TCanvas::SetPixel(int X, int Y, Uint32 raw_color)
{
	LOG_FUNCTION("TCanvas::SetPixel(int, int, Uint32)");

	if(IsValid()) {
		int minx, miny, maxx, maxy;

		if(!PrepareClipping(minx, miny, maxx, maxy)) return;

		if(CLIP_POINT(X, Y, minx, miny, maxx, maxy)) {
			ImageIterator iter = Surface->make_iterator(ImageIterator::READ_WRITE);

			iter.increment(X, Y);
			_do_setpixel(iter, raw_color, Mode);

			LOG(9, "Plot(%d, %d) = %d", X, Y, raw_color);
		}
	}
}
//---------------------------------------------------------------------------
TColor TCanvas::GetPixel(int X, int Y)
{
	LOG_FUNCTION("TColor TCanvas::GetPixel(int, int)");

	if(IsValid()) {
		int minx, miny, maxx, maxy;
		Uint32 rcolor;

		if(!PrepareClipping(minx, miny, maxx, maxy)) return TColor::black;

		if(!CLIP_POINT(X, Y, minx, miny, maxx, maxy))
			return TColor::black;

		ImageIterator iter = Surface->make_iterator(ImageIterator::READ_ONLY);

		iter.increment(X, Y);
		rcolor = *iter;

		LOG(9, "Pixel(%d, %d) = <color>", X, Y);

		return Surface->GetFormat().MapToColor(rcolor);
	}

	return TColor::black;
}
//---------------------------------------------------------------------------
bool TCanvas::IsValid() const 
{
	//TPoint2D root_pos = Surface->GetRootPosition();
	return Surface && Surface->IsValid();// &&
		   //root_pos.X < Surface->GetWidth() && root_pos.Y < Surface->GetHeight();
		   //DimX < Surface->GetWidth() && DimY < Surface->GetHeight(); &&
		   //DimX + DimWidth > 0 && DimY + DimHeight > 0;
}
//---------------------------------------------------------------------------
// TODO: add some software blitting to support op-blitting.
void TCanvas::Blit(int X, int Y, TImage& src)
{
	LOG_FUNCTION("TCanvas::Blit(int, int, TImage&)");

	if(IsValid() && src.IsValid()) {
		TPoint2D root_pos = Surface->GetRootPosition();
		SDL_Rect src_rect = { 0, 0, src.GetWidth(), src.GetHeight() },
				 dst_rect = { X, Y, 0, 0 };
		int minx, miny, maxx, maxy, temp;

		if(!PrepareClipping(minx, miny, maxx, maxy)) return;

		// if not visible return
		if(dst_rect.x > maxx) return;
		if(dst_rect.y > maxy) return;

		// clip to canvas clip rect
		if(dst_rect.x < minx) {
			temp = minx - X;
			src_rect.x += temp;
			dst_rect.x += temp; // <-- does this make sense??
		}
		if(dst_rect.y < miny) {
			temp = miny - Y;
			src_rect.y += temp;
			dst_rect.y += temp; // <-- does this make sense??
		}
		temp = dst_rect.x + src_rect.w;
		if(temp < minx) return;
		if(temp > maxx) {
			// if whole image is clipped away, don't draw
			if((temp - maxx) >= src_rect.w) return;

			src_rect.w -= temp - maxx - 1;
		}
		temp = dst_rect.y + src_rect.h;
		if(temp < miny) return;
		if(temp > maxy) {
			// if whole image is clipped away, don't draw
			if((temp - maxy) >= src_rect.h) return;

			src_rect.h -= temp - maxy - 1;
		}

		// adjust by the surface offset
		dst_rect.x += root_pos.X;
		dst_rect.y += root_pos.Y;

		// make sure it isn't changed while we blit it
		src.Lock();

		Surface->OnWrite();
		
		// HACK: since an SDL_Surface can't be locked during blit, we first unlock it
		SDL_Surface* src_handle = src.GetHandle();
		if(SDL_MUSTLOCK(src_handle))
			SDL_UnlockSurface(src_handle);
		SDL_Surface* dst_handle = Surface->GetHandle();
		if(SDL_MUSTLOCK(dst_handle))
			SDL_UnlockSurface(dst_handle);
		
		// TODO: Handle return (restoring vid mem -- maybe store source) <-- important for windows
		// TODO: Handle situations where Mode != OP_REPLACE
		SDL_BlitSurface(src_handle, &src_rect, dst_handle, &dst_rect);

		// HACK: make sure to relock so that the lock/unlock protocol is maintained
		if(SDL_MUSTLOCK(src_handle))
			SDL_LockSurface(src_handle);
		if(SDL_MUSTLOCK(dst_handle))
			SDL_LockSurface(dst_handle);

		src.Unlock();
	}
}
//---------------------------------------------------------------------------
// This is a simple derivation of the algorithm given by the following
// pseudo-code for stretching in one-dimension:
//
// sx = 0
// Dx = -.5
// incpos = oldsize/newsize
// incneg = -1
//
// foreach x
//   plot(x, value_at(sx))
//
//   Dx += incpos
//   while Dx > 0
//     Dx += incneg
//     sx++
//   end
// end
//
// I derived this after reviewing the derivation of Bresenhams algorithm. The
// final derivation used in the code below basically performs incremental
// fraction arithmetic without using any fractional numeric representation by
// multiplying everything by 2*newsize. Ultimately, what is going on is a purely
// integer form of division with rounding. This should be just as accurate as
// doing the real number arithmetic directly. -- Sterling Hanenkamp
void TCanvas::StretchBlit(int X, int Y, int Width, int Height, TImage& src)
{
	LOG_FUNCTION("void TCanvas::StretchBlit(int, int, int, int, TSurface&)");

	if(Surface->IsValid() && src.IsValid()) {
		int Dx = -Width, Dy = -Height;
		int incposx = 2*src.GetWidth(), incposy = 2*src.GetHeight();
		int incnegx = -2*Width, incnegy = -2*Height;
		int endx = X+Width, endy = Y+Height;
		int minx, miny, maxx, maxy;
		int clipX = X, clipY = Y;

		// set-up clipping data
		if(!PrepareClipping(minx, miny, maxx, maxy)) return;

		// perform culling
		if(X > maxx) 	return;
		if(Y > maxy) 	return;
		if(endx < minx) return;
		if(endy < miny) return;

		// perform clipping
		CLIP_RECT(clipX, clipY, endx, endy, minx, miny, maxx, maxy);

		// TODO: don't convert surface if the pixel formats are the same
		// convert source image to dest image type
		TImage Temp = src.Convert(Surface->GetFormat());
		if(!Temp.IsValid()) return;

		//Blit(X, Y, Temp);

		ImageIterator dest_iter = Surface->make_iterator(ImageIterator::READ_WRITE);
		dest_iter.increment(minx, miny);
		ImageIterator dest_end  = dest_iter;
		dest_end.increment(0, endy - clipY);
		ImageIterator row_end;
		ImageIterator tdest_iter;
		ImageIterator src_iter  = Temp.make_iterator(ImageIterator::READ_ONLY);
		ImageIterator tsrc_iter;

		// Adjust for clipping
		src_iter.increment(((clipX-X)*Width)/src.GetWidth(),
						   ((clipY-Y)*Height)/src.GetHeight());

		for(; dest_iter != dest_end; dest_iter.increment(0, 1)) {
			row_end = dest_iter + (endx-clipX);
			tsrc_iter = src_iter;
			Dx = -Width;
			for(tdest_iter = dest_iter; tdest_iter != row_end;
					tdest_iter.increment(1)) {
				*tdest_iter = *tsrc_iter;
	
				Dx += incposx;
				while(Dx > 0) {
					Dx += incnegx;
					tsrc_iter.increment(1);
				}
			}

			Dy += incposy;
			while(Dy > 0) {
				Dy += incnegy;
				src_iter.increment(0, 1);
			}
		}

		// Temp freed as it leaves this scope
	}
}
//---------------------------------------------------------------------------
void TCanvas::DrawShape(const TShape2D& shape, bool fill)
{
	DrawShapeVisitor v_obj(this, fill);
	((TShape2D*)&shape)->Visit(&v_obj);
}
//---------------------------------------------------------------------------
void TCanvas::Line(int x1, int y1, int x2, int y2)
{
	// optimize for horizontal and verticle
	if(y1 == y2) { HorzLine(y1, x1, x2); return; }
	if(x1 == x2) { VertLine(x1, y1, y2); return; }

	LOG_FUNCTION("void TCanvas::Line(int, int, int, int)");

	if(IsValid()) {
		int y_unit, x_unit, length;
	    int ydiff, xdiff, error_term = 0;
	    int real_width = Surface->GetRealWidth();
	    int minx, miny, maxx, maxy;
	    Uint32 rcolor;
	    TRect rcliprect;

	    // perform clipping
		if(!PrepareClipping(minx, miny, maxx, maxy)) return;
		rcliprect.Set(minx, miny, maxx, maxy);
		if(!rcliprect.ClipLine(x1, y1, x2, y2)) {
			return;
		}

		// create image iter
		ImageIterator iter = Surface->make_iterator(ImageIterator::READ_WRITE);

		// put into initial position
		iter.increment(x1, y1);

		// get color
		rcolor = Surface->GetFormat().MapToPixel(Pen.Color);

		ydiff = y2 - y1;
		if(ydiff < 0) {
			ydiff = -ydiff;
			y_unit = -real_width;
		}
		else y_unit = real_width;
			xdiff = x2 - x1;
		if(xdiff < 0) {
			xdiff  = -xdiff;
			x_unit = -1;
		}
		else x_unit = 1;
			if(xdiff > ydiff) {
			length = xdiff + 1;
			for(int i = 0; i < length; i++) {
				// set pixel + inc
				_do_setpixel(iter, rcolor, Mode);
				iter += x_unit;
					error_term += ydiff;
				if(error_term > xdiff) {
					error_term -= xdiff;
					iter += y_unit;
				}
			}
		}
		else {
			length = ydiff + 1;
			for(int i = 0; i < length; i++) {
				// set pixel + inc
				_do_setpixel(iter, rcolor, Mode);
				iter += y_unit;
					error_term += xdiff;
				if(error_term > 0) {
					error_term -= ydiff;
					iter += x_unit;
				}
			}
		}

	    LOG(9, "Line (%d, %d, %d, %d) = <color>", x1, y1, x2, y2);
	}
}
//---------------------------------------------------------------------------
void TCanvas::HorzLine(int y, int x1, int x2)
{
	LOG_FUNCTION("void TCanvas::HorzLine(int, int, int)");

	if(IsValid()) {
		int len, x;
		Uint32 rcolor = Surface->GetFormat().MapToPixel(Pen.Color);
		int minx, miny, maxx, maxy;

		// prepare line
		if(x1 > x2) {
			len = x1 - x2;
			x = x2;
		} else {
			len = x2 - x1;
			x = x1;
		}

		// perform clipping
		if(!PrepareClipping(minx, miny, maxx, maxy)) return;
		CLIP_HORZ_LINE(x, len, y, minx, miny, maxx, maxy);

		if(len) { // when length is zero line is not to be drawn
			ImageIterator iter = Surface->make_iterator(ImageIterator::READ_WRITE);
			iter.increment(x, y);
			for(int i = 0; i <= len; i++) {
				_do_setpixel(iter, rcolor, Mode);
				iter++;
			}

			LOG(9, "HorzLine(y = %d from %d to %d) = <color>", y, x, x + len);
		}
	}
}
//---------------------------------------------------------------------------
void TCanvas::VertLine(int x, int y1, int y2)
{
	LOG_FUNCTION("void TCanvas::VertLine(int. int, int)");

	if(IsValid()) {
		int len, y;
		Uint32 rcolor = Surface->GetFormat().MapToPixel(Pen.Color);
		int minx, miny, maxx, maxy;

		if(y1 > y2) {
			len = y1 - y2;
			y = y2;
		} else {
			len = y2 - y1;
			y = y1;
		}

		// perform clipping
		if(!PrepareClipping(minx, miny, maxx, maxy)) return;
		CLIP_VERT_LINE(y, len, x, minx, miny, maxx, maxy);

		if(len) { // when length is zero line is not to be drawn
			ImageIterator iter = Surface->make_iterator(ImageIterator::READ_WRITE);
			iter.increment(x, y);
			for(int i = 0; i <= len; i++) {
				_do_setpixel(iter, rcolor, Mode);
				iter.increment(0, 1);
			}

			LOG(9, "VertLine(x = %d from %d to %d) = <color>", x, y, y + len);
		}
	}
}
//---------------------------------------------------------------------------
void TCanvas::FrameRect(int left, int top, int right, int bottom)
{
	LOG_FUNCTION("void TCanvas::FrameRect(int, int, int, int)");

	if(IsValid()) {
		int len, height;
		int minx, miny, maxx, maxy;
		Uint32 rcolor;
		ImageIterator base_iter = Surface->make_iterator(ImageIterator::READ_WRITE);

		// attr holds rectangle attributes for clipping, so that everything can be
		// pre-clipped -- the disadvantage in most rectangle drawing is that each
		// line is clipped seperately.
		//
		// has flags if drawn:
		// 	left = 		0x20
		// 	top = 		0x80
		// 	right = 	0x02
		// 	bottom = 	0x08
		Uint8 attr = 0;

		// make sure we are drawing a valid/visible rect
		MAKE_RECT_VALID(left, top, right, bottom);
		if(!PrepareClipping(minx, miny, maxx, maxy)) return;
		// TODO: make this into a TRect function!
		if(!IS_RECT_VISIBLE(left, top, right, bottom, minx, miny, maxx, maxy)) return;

		// --
		//  Perform pre-clipping
		// --

		// check for all sides
		if(top >= miny) 	attr |= 0x80;
		if(bottom <= maxy) 	attr |= 0x08;
		if(left >= minx)    attr |= 0x20; else left = minx;
		if(right <= maxx) 	attr |= 0x02; else right = maxx;

		// if draw no side (the frame rect is arround the visible area) then return
		if(attr == 0) return;

		// prepare for drawing
		len = right - left + 1;
		rcolor = Surface->GetFormat().MapToPixel(Pen.Color);

		// --
		//  Draw
		// --

		ImageIterator iter = base_iter;

		// calc height and draw first line
		if(attr & 0x80) { 		// if we have top
			if(attr & 0x08) { 	// and bottom
				height = bottom - top - 1;
			} else { 			// and NOT bottom
				height = maxy - top - 1;
			}

			// position for first line
			iter.increment(left, top);

			// draw top line
			ImageIterator iter_temp = iter;
			for(int i = 0; i < len; i++) {
				_do_setpixel(iter_temp, rcolor, Mode);
				iter_temp++;
			}
			iter.increment(0, 1);
		} else { 				// else, if we DONT have top
			if(attr & 0x08) {	// and bottom
				height = bottom - miny - 1;
			} else { 			// and NOT bottom
				height = maxy - miny;
			}

			// position draw pointer past first line
			iter.increment(left, top + (miny - top));
		}

		// draw sides
		if(attr & 0x20 || attr & 0x02) { // if left/right is visible
			for(int i = 0; i < height; i++) {
				ImageIterator iter_temp = iter;

				// Draw both pixels
				if(attr & 0x20) {
					_do_setpixel(iter_temp, rcolor, Mode);
					iter_temp += len-1;
				} else iter_temp += len-1;
				if(attr & 0x02) _do_setpixel(iter_temp, rcolor, Mode);

				// move to next row
				iter.increment(0, 1);
			}
		}

		// draw bottom line
		if(attr & 0x08) {
			if(len) {
				for(int i = 0; i < len; i++) {
					_do_setpixel(iter, rcolor, Mode);
					iter++;
				}
			}
		}

		LOG(9, "FrameRect(%d, %d, %d, %d) = <color>", left, top, right, bottom);
	}
}
//---------------------------------------------------------------------------
// NOTE: I won't fix this yet but -- when left or right is clipped, a single
// pixel is omitted on the fill line.
void TCanvas::FillRect(int left, int top, int right, int bottom)
{
	LOG_FUNCTION("void TCanvas::FillRect(int, int, int, int)");

	if(IsValid()) {
		int fill_len, fill_start, len, height;
		int minx, miny, maxx, maxy;
		Uint32 rcolor, fill;

		// attr holds rectangle attributes for clipping, so that everything can be
		// pre-clipped -- the disadvantage in most rectangle drawing is that each
		// line is clipped seperately.
		//
		// has flags if drawn:
		// 	left = 		0x20
		// 	top = 		0x80
		// 	right = 	0x02
		// 	bottom = 	0x08
		Uint8 attr = 0;

		// make sure we are drawing a valid/visible rect
		MAKE_RECT_VALID(left, top, right, bottom);
		if(!PrepareClipping(minx, miny, maxx, maxy)) return;
		if(!IS_RECT_VISIBLE(left, top, right, bottom, minx, miny, maxx, maxy)) return;

		// --
		//  Perform pre-clipping
		// --

		// check for all sides
		if(top >= miny) 	attr |= 0x80;
		if(bottom <= maxy) 	attr |= 0x08;
		if(left >= minx)    attr |= 0x20; else left = minx;
		if(right <= maxx) 	attr |= 0x02; else right = maxx;

		// TODO: in frame rect --
		//if(attr == 0) return;

		// prepare for drawing
		len = right - left;
		fill_start = left + 1;
		fill_len = len - 2;
		rcolor = Surface->GetFormat().MapToPixel(Pen.Color);
		fill = Surface->GetFormat().MapToPixel(Brush.Color);

		// pre-clip the fill lines
		CLIP_HORZ_LINE(fill_start, fill_len, top, minx, top, maxx, maxy);
		fill_start = fill_start - left; // make into inc value (don't subtract 1 -- we need to add 1)

		// --
		//  Draw
		// --

		ImageIterator iter = Surface->make_iterator(ImageIterator::READ_WRITE);

		// calc height and draw first line
		if(attr & 0x80) { 		// if we have top
			if(attr & 0x08) { 	// and bottom
				height = bottom - top - 2;
			} else { 			// and NOT bottom
				height = maxy - top - 1;
			}

			// position for first line
			iter.increment(left, top);

			// draw top line
			ImageIterator iter_temp = iter;
			if(len) {
				for(int i = 0; i < len; i++) {
					_do_setpixel(iter_temp, rcolor, Mode);
					iter_temp++;
				}
			}
			iter.increment(0, 1);
		} else { 				// else, if we DONT have top
			if(attr & 0x08) {	// and bottom
				height = bottom - miny - 1;
			} else { 			// and NOT bottom
				height = maxy - miny;
			}

			// position draw pointer past first line
			iter.increment(left, top + (miny - top));
		}

		// draw sides and fill lines
		if(fill_len != 0) { // if fill lines are shown
			for(int i = 0; i < height; i++) {
				ImageIterator iter_temp = iter;

				// Draw both pixels and scan line
				if(attr & 0x20) {
					_do_setpixel(iter_temp, rcolor, Mode);
					iter_temp++;
				} else iter_temp += fill_start;

				for(int i = 0; i < fill_len; i++) {
					_do_setpixel(iter_temp, fill, Mode);
					iter_temp++;
				}

				if(attr & 0x02) _do_setpixel(iter_temp, rcolor, Mode);

				// move to next row
				iter.increment(0, 1);
			}
		}

		// draw bottom line
		if(attr & 0x08) {
			if(len) {
				for(int i = 0; i < len; i++) {
					_do_setpixel(iter, rcolor, Mode);
					iter++;
				}
			}
		}

		LOG(9, "FillRect(%d, %d, %d, %d) = <color>/<fcolor>", left, top, right, bottom);
	}
}
//---------------------------------------------------------------------------
void TCanvas::Polygon(int length, TPoint2D *polygon)
{
	LOG_FUNCTION("void TCanvas::Polygon(int, TPoint2D*)");

	TEdgeTable edge(length, polygon, ClipRect);

	int sx, ex, type;
	bool visible;
	Uint32 rcolor = Surface->GetFormat().MapToPixel(Pen.Color);

	ImageIterator iter = Surface->make_iterator(ImageIterator::READ_WRITE);
	iter.increment(0, edge.GetFirstY());
	ImageIterator temp_iter;
	ImageIterator end;

	int y = edge.GetFirstY();
	while(edge.NextLine()) {
		while(edge.ScanLine(sx, ex, visible, type)) {
			if(visible) {
				LOG(10, "visible = true");
				temp_iter = iter;
				end = iter;
				end.increment(ex);
				LOG(10, "(%d, %d)->(%d, %d) = %d", sx, y, ex, y, rcolor);
				for(temp_iter.increment(sx); temp_iter < end; temp_iter++) {
					_do_setpixel(temp_iter, rcolor, Mode);
				}
			}
		}
		iter.increment(0, 1);
		y++;
	}
}
//---------------------------------------------------------------------------
void TCanvas::FillPolygon(int length, TPoint2D *polygon)
{
	LOG_FUNCTION("void TCanvas::Polygon(int, TPoint2D*)");

	TEdgeTable edge(length, polygon, ClipRect);

	int sx, ex, ox, type;
	bool visible;
	Uint32 rcolor = Surface->GetFormat().MapToPixel(Pen.Color);
	Uint32 fcolor = Surface->GetFormat().MapToPixel(Brush.Color);

	ImageIterator iter = Surface->make_iterator(ImageIterator::READ_WRITE);
	iter.increment(0, edge.GetFirstY());
	ImageIterator temp_iter;
	ImageIterator end;

	int y = edge.GetFirstY();
	while(edge.NextLine()) {
		while(edge.ScanLine(sx, ex, visible, type)) {
			if(visible) {
				LOG(10, "visible = true");
				if(type < 0) {
					ox = ex;
				} else if(type > 0) {
					temp_iter = iter;
					end = iter;
					end.increment(sx);
					for(temp_iter.increment(ox); temp_iter < end; temp_iter++) {
						_do_setpixel(temp_iter, fcolor, Mode);
					}
				}
				LOG(10, "(%d, %d)->(%d, %d) = %d", sx, y, ex, y, rcolor);
				temp_iter = iter;
				end = iter;
				end.increment(ex);
				for(temp_iter.increment(sx); temp_iter < end; temp_iter++) {
					_do_setpixel(temp_iter, rcolor, Mode);
				}
			} else {
				if(type < 0) {
					ox = ex-1;
				} else if(type > 0) {
					temp_iter = iter;
					end = iter;
					end.increment(sx);
					for(temp_iter.increment(ox); temp_iter < end; temp_iter++) {
						_do_setpixel(temp_iter, fcolor, Mode);
					}
				}
			}
		}
		iter.increment(0, 1);
		y++;
	}
}
//---------------------------------------------------------------------------
void TCanvas::Ellipse(int X, int Y, int Width, int Height)
{
	LOG_FUNCTION("void TCanvas::Ellipse(int, int, int, int)");

	int slope_mn, slope_md;							// ellipse calc variables
	int position_mn, position_md;
	int x_pos, y_pos, balance;
	int minx, miny, maxx, maxy;						// clipping vars
	Uint32 rcolor;									// color
	int inc_top = 0, inc_bottom = 0;  				// drawing variables
	Uint16 real_width = Surface->GetRealWidth();
	int temp_x1, temp_x2, temp_y1, temp_y2; 		// used to stop repeated math
	Uint8 clipping = 0; 							// set to 4 if no clipping is needed

	// quick division (x / 2)
    Width = Width >> 1;
    Height = Height >> 1;

	// don;t draw if structure is not visible, and don't clip if its not needed
	if(!PrepareClipping(minx, miny, maxx, maxy)) return;
	temp_x1 = X - Width; 	temp_x2 = X + Width;
	temp_y1 = Y - Height;	temp_y2 = Y + Height;
	if(temp_x2 < minx) return;
    else if(temp_x1 > minx) clipping++;
    if(temp_y2 < miny) return;
   	else if(temp_y1 > miny) clipping++;
    if(temp_x1 > maxx) return;
    else if(temp_x2 < maxx) clipping++;
    if(temp_y1 > maxy) return;
    else if(temp_y2 < maxy) clipping++;

	// get color
    rcolor = Surface->GetFormat().MapToPixel(Pen.Color);

	// Calc slopes
	slope_md = Height * Height; 			// Find md for the slope of the ellipse.
	slope_mn = Width * Width;				// Find mn for the slope of the ellipse.

	// These positions for the ellipse's md and mn start it at the top.
	position_md = 0;						// The minimum value of md for the ellipse.
	position_mn = Width * Width * Height;	// The maximum value of mn for the ellipse.

	x_pos = 0;    		// Start at the middle X of the ellipse.
	y_pos = Height;    	// Start at the top Y of the ellipse.
	balance = 0;      	// Let's start at zero.

	ImageIterator iter_top = Surface->make_iterator(ImageIterator::READ_WRITE);
	ImageIterator iter_bottom = iter_top;

	// put image pointers to start positions
	iter_top.increment(X, Y - Height);
	iter_bottom.increment(X, Y + Height);

	while((y_pos > 0) || (x_pos < Width)) {
		// save on addition/subtraction
		temp_x1 = X - x_pos;
		temp_x2 = X + x_pos;
		temp_y1 = Y - y_pos;
		temp_y2 = Y + y_pos;

		// top left pixel
		if(clipping == 4 || CLIP_POINT(temp_x1, temp_y1, minx, miny, maxx, maxy)) {
			iter_top += inc_top;
			_do_setpixel(iter_top, rcolor, Mode);
		} else iter_top += inc_top;

		// top-right pixel
		ImageIterator iter_temp = iter_top;
		if(clipping == 4 || CLIP_POINT(temp_x2, temp_y1, minx, miny, maxx, maxy)) {
			iter_temp += x_pos << 1;
			_do_setpixel(iter_temp, rcolor, Mode);
		}

		// bottom-left pixel
		if(clipping == 4 || CLIP_POINT(temp_x1, temp_y2, minx, miny, maxx, maxy)) {
			iter_bottom += -inc_bottom;
			_do_setpixel(iter_bottom, rcolor, Mode);
		} else iter_bottom += -inc_bottom;

		// bottom-right pixel
		iter_temp = iter_bottom;
		if(clipping == 4 || CLIP_POINT(temp_x2, temp_y2, minx, miny, maxx, maxy)) {
			iter_temp += x_pos << 1;
			_do_setpixel(iter_temp, rcolor, Mode);
		}

		inc_top = inc_bottom = 0;
		if(balance < 0) {							// If the balance leans toward X...
			balance = balance + position_mn;		// Balance X with Y.
			position_mn = position_mn - slope_mn;	// Move position slope mn down.
			y_pos = y_pos - 1;						// Move Y in the correct direction.
			inc_top = inc_bottom = real_width;		// adjust addrs
		}

		if(balance >= 0) {							// If the balance leans toward Y...
			position_md = position_md + slope_md; 	// Move position slope md up.
			balance = balance - position_md;		// Balance Y with X.
			x_pos = x_pos + 1;						// Move X in the correct direction.
			inc_top--;								// adjust addrs
			inc_bottom++;
		}
	}

	// Draw last two pixels
	// save on addition/subtraction
	temp_x1 = X - x_pos;
	temp_x2 = X + x_pos;
	temp_y1 = Y - y_pos;

	// last left pixel
	if(clipping == 4 || CLIP_POINT(temp_x1, temp_y1, minx, miny, maxx, maxy)) {
		iter_top += inc_top;
		_do_setpixel(iter_top, rcolor, Mode);
	} else iter_top += inc_top;

	// last right pixel
	if(clipping == 4 || CLIP_POINT(temp_x2, temp_y1, minx, miny, maxx, maxy)) {
		iter_top += x_pos << 1;
		_do_setpixel(iter_top, rcolor, Mode);
	}

	LOG(9, "Ellipse(%d, %d, %d, %d) = <color>", X, Y, Width * 2, Height * 2);
}
//---------------------------------------------------------------------------
void TCanvas::FillEllipse(int X, int Y, int Width, int Height)
{
	LOG_FUNCTION("void TCanvas::FillEllipse(int, int, int, int)");

	int slope_mn, slope_md;							// ellipse calc variables
	int position_mn, position_md;
	int x_pos, y_pos, balance;
	int minx, miny, maxx, maxy;						// clipping vars
	Uint32 rcolor, fill;							// colors
	int inc_top = 0, inc_bottom = 0;  				// drawing variables
	Uint16 real_width = Surface->GetRealWidth();
	int temp_x1, temp_x2, temp_y1, temp_y2; 		// used to stop repeated math
	int fill_len, fill_len_temp, temp_fill_start;	// fill vars
	Uint8 clipping = 0; 							// set to 4 if no clipping is needed
	bool draw_scanline = false;						// used to stop repeated scan line drawing

	// quick division (x / 2)
    Width = Width >> 1;
    Height = Height >> 1;

	// don;t draw if structure is not visible, and don't clip if its not needed
	if(!PrepareClipping(minx, miny, maxx, maxy)) return;
	temp_x1 = X - Width; 	temp_x2 = X + Width;
	temp_y1 = Y - Height;	temp_y2 = Y + Height;
	if(temp_x2 < minx) return;
    else if(temp_x1 > minx) clipping++;
    if(temp_y2 < miny) return;
   	else if(temp_y1 > miny) clipping++;
    if(temp_x1 > maxx) return;
    else if(temp_x2 < maxx) clipping++;
    if(temp_y1 > maxy) return;
    else if(temp_y2 < maxy) clipping++;

	// get color
    rcolor = Surface->GetFormat().MapToPixel(Pen.Color);
    fill = Surface->GetFormat().MapToPixel(Brush.Color);

	// Calc slopes
	slope_md = Height * Height; 			// Find md for the slope of the ellipse.
	slope_mn = Width * Width;				// Find mn for the slope of the ellipse.

	// These positions for the ellipse's md and mn start it at the top.
	position_md = 0;						// The minimum value of md for the ellipse.
	position_mn = Width * Width * Height;	// The maximum value of mn for the ellipse.

	x_pos = 0;    		// Start at the middle X of the ellipse.
	y_pos = Height;    	// Start at the top Y of the ellipse.
	balance = 0;      	// Let's start at zero.

	ImageIterator iter_top = Surface->make_iterator(ImageIterator::READ_WRITE);
	ImageIterator iter_bottom = iter_top;

	// put image pointers to start positions
	iter_top.increment(X, Y - Height);
	iter_bottom.increment(X, Y + Height);

	while((y_pos > 0) || (x_pos < Width)) {
		// save on addition/subtraction
		temp_x1 = X - x_pos;
		temp_x2 = X + x_pos;
		temp_y1 = Y - y_pos;
		temp_y2 = Y + y_pos;
		fill_len = ((x_pos << 1) - 2);

		// top left pixel
		if(clipping == 4 || CLIP_POINT(temp_x1, temp_y1, minx, miny, maxx, maxy)) {
			iter_top += inc_top;
			_do_setpixel(iter_top, rcolor, Mode);
		} else iter_top += inc_top;

		// draw top scanline
		ImageIterator iter_temp = iter_top;
		if(draw_scanline) {
			fill_len_temp = fill_len;
			temp_fill_start = temp_x1 + 1;
			if(clipping != 4) CLIP_HORZ_LINE(temp_fill_start, fill_len_temp, temp_y1, minx, miny, maxx, maxy);
			if(fill_len_temp) {
				iter_temp += temp_fill_start - temp_x1;
				for(int i = 0; i < fill_len_temp; i++) {
					_do_setpixel(iter_temp, fill, Mode);
					iter_temp++;
				}
			} else {
				iter_temp += x_pos << 1;
			}
		} else iter_temp += x_pos << 1;

		// top-right pixel
		if(clipping == 4 || CLIP_POINT(temp_x2, temp_y1, minx, miny, maxx, maxy))
			_do_setpixel(iter_temp, rcolor, Mode);

		// bottom-left pixel
		if(clipping == 4 || CLIP_POINT(temp_x1, temp_y2, minx, miny, maxx, maxy)) {
			iter_bottom += -inc_bottom;
			_do_setpixel(iter_bottom, rcolor, Mode);
		} else iter_bottom += -inc_bottom;

		// draw bottom scanline
		iter_temp = iter_bottom;
		if(draw_scanline) {
			fill_len_temp = fill_len;
			temp_fill_start = temp_x1 + 1;
			if(clipping != 4) CLIP_HORZ_LINE(temp_fill_start, fill_len_temp, temp_y2, minx, miny, maxx, maxy);
			if(fill_len_temp) {
				iter_temp += temp_fill_start - temp_x1;
				for(int i = 0; i < fill_len_temp; i++) {
					_do_setpixel(iter_temp, fill, Mode);
					iter_temp++;
				}
			} else {
				iter_temp += x_pos << 1;
			}
		} else iter_temp += x_pos << 1;

		// bottom-right pixel
		if(clipping == 4 || CLIP_POINT(temp_x2, temp_y2, minx, miny, maxx, maxy))
			_do_setpixel(iter_temp, rcolor, Mode);

		inc_top = inc_bottom = 0;
		draw_scanline = false;
		if(balance < 0) {							// If the balance leans toward X...
			balance = balance + position_mn;		// Balance X with Y.
			position_mn = position_mn - slope_mn;	// Move position slope mn down.
			y_pos = y_pos - 1;						// Move Y in the correct direction.
			inc_top = inc_bottom = real_width;		// adjust addrs
			draw_scanline = true;
		}

		if(balance >= 0) {							// If the balance leans toward Y...
			position_md = position_md + slope_md; 	// Move position slope md up.
			balance = balance - position_md;		// Balance Y with X.
			x_pos = x_pos + 1;						// Move X in the correct direction.
			inc_top--;								// adjust addrs
			inc_bottom++;
		}
	}

	// Draw last two pixels
	// save on addition/subtraction
	temp_x1 = X - x_pos;
	temp_x2 = X + x_pos;
	temp_y1 = Y - y_pos;
	fill_len = ((x_pos << 1) - 2);

	// last left pixel
	if(clipping == 4 || CLIP_POINT(temp_x1, temp_y1, minx, miny, maxx, maxy)) {
		iter_top += inc_top;
		_do_setpixel(iter_top, rcolor, Mode);
	} else iter_top += inc_top;

	// draw final scan line
	fill_len_temp = (x_pos << 1) - 2;
	temp_fill_start = temp_x1 + 1;
	if(clipping != 4) CLIP_HORZ_LINE(temp_fill_start, fill_len_temp, temp_y1, minx, miny, maxx, maxy);
	if(fill_len_temp) {
		iter_top += temp_fill_start - temp_x1;
		for(int i = 0; i < fill_len_temp; i++) {
			_do_setpixel(iter_top, fill, Mode);
			iter_top++;
		}
	} else {
		iter_top += x_pos << 1;
	}

	// last right pixel
	if(clipping == 4 || CLIP_POINT(temp_x2, temp_y1, minx, miny, maxx, maxy))
		_do_setpixel(iter_top, rcolor, Mode);

	LOG(9, "FillEllipse(%d, %d, %d, %d) = <color>", X, Y, Width * 2, Height * 2);
}
//---------------------------------------------------------------------------
// most of the circle code is based on the SGE circle drawing code written
// by: Anders Lindstrom.
void TCanvas::Circle(int X, int Y, int Radius)
{
	LOG_FUNCTION("void TCanvas::Circle(int, int, int)");

	Sint16 cx = 0, cy = Radius, df = 1 - Radius, d_e = 3, d_se = -(Radius << 1) + 5;
	Uint16 real_width = Surface->GetRealWidth();
	Sint16 top_inc_outter, top_inc_inner, bottom_inc_outter, bottom_inc_inner;
	int minx, miny, maxx, maxy;
	Uint32 rcolor;
	Uint8 attr;

	/* This is a fairly confusing function and I will try to explain generally
	 * how it works (mostly for myself when it needs maintanance)
	 *
	 * There are eight separate partial arches, and four starting points.
	 * The starting points are at the N, E, S, and W most points on the circle
	 * and are numbered starting at N clockwise from 1 to 4.  The arches are then
	 * drawn moving away from each of these points and are distinguished by
	 * right (R) and left (L) when their starting is placed at the top.
	 *
	 * The arches on the top half of the circle are: 1L, 1R, 2L, 4R
	 * The arches on the top half of the circle are: 2R, 3L, 3R, 4L
	 *
	 * The increment values are used to deal with "cy" changes.  Their names show
	 * which arhes they affect.  For example:
	 *   "top_inc_outter" is applied to the arches on the outside of the top half,
	 *   in other words 4R and 2L.
	 *
	 * See SGE's sge_DoCircle for a clear implementation of the same method..
	 *
	 */

	// get clipping data
	if(!PrepareClipping(minx, miny, maxx, maxy)) return;

	// since this is a fairly quick function we won't do as much with culling as
	// in Ellipse and FillEllipse.
	if(X + Radius < minx) return;
	if(X - Radius > maxx) return;
	if(Y + Radius < miny) return;
	if(Y - Radius > maxy) return;

	// perform pre-clipping, very similar to FrameRect.  We remove whole halves..
	// 		left-half = 	0x20
	// 		top-half = 		0x80
	// 		right-half = 	0x02
	// 		bottom-half = 	0x08
	if(X > minx)
		attr |= 0x20;
	if(Y > miny)
		attr |= 0x80;
	if(X < maxx)
		attr |= 0x02;
	if(Y < maxy)
		attr |= 0x08;

	// get color
	rcolor = Surface->GetFormat().MapToPixel(Pen.Color);

	// TODO: don't assign pixel pointers at all when their half is not used
	// assign initial pixel pointer to each unique addr
	ImageIterator iter_1R = Surface->make_iterator(ImageIterator::READ_WRITE);
	ImageIterator iter_3L = iter_1R;
	ImageIterator iter_2R = iter_1R;
	ImageIterator iter_4L = iter_1R;

	// put all pixel pointers to there specific start position
	iter_3L.increment(X, Y + cy);
	ImageIterator iter_3R = iter_3L;
	iter_1R.increment(X, Y - cy);
	ImageIterator iter_1L = iter_1R;
	iter_2R.increment(X + cy, Y);
	ImageIterator iter_2L = iter_2R;
	iter_4L.increment(X - cy, Y);
	ImageIterator iter_4R = iter_4L;

	do {
		top_inc_outter = bottom_inc_outter = top_inc_inner = bottom_inc_inner = 0;

		// plot/clip all eight points
		// TODO: fix clipping... This is SO ugly... maybe pre-calculate y-vars
		if(attr & 0x02) {
			int temp_x;

			temp_x = X + cx;
			if(temp_x > minx && temp_x < maxx) {
				if(attr & 0x80 && Y - cy > miny && Y - cy < maxy)
					_do_setpixel(iter_1R, rcolor, Mode);
				if(attr & 0x08 && Y + cy > miny && Y + cy < maxy)
					_do_setpixel(iter_3L, rcolor, Mode);
			}
			temp_x = X + cy;
			if(temp_x > minx && temp_x < maxx) {
				if(attr & 0x80 && Y - cx > miny && Y - cx < maxy)
					_do_setpixel(iter_2L, rcolor, Mode);
				if(attr & 0x08 && Y + cx > miny && Y + cx < maxy)
					_do_setpixel(iter_2R, rcolor, Mode);
			}
		}
		if(attr & 0x20) {
			int temp_x;

			temp_x = X - cx;
			if(temp_x > minx && temp_x < maxx) {
				if(attr & 0x80 && Y - cx > miny && Y - cx < maxy)
					_do_setpixel(iter_1L, rcolor, Mode);
				if(attr & 0x08 && Y + cx > miny && Y + cx < maxy)
					_do_setpixel(iter_3R, rcolor, Mode);
			}
			temp_x = X - cy;
			if(temp_x > minx && temp_x < maxx) {
				if(attr & 0x80 && Y - cx > miny && Y - cx < maxy)
					_do_setpixel(iter_4R, rcolor, Mode);
				if(attr & 0x08 && Y + cx > miny && Y + cx < maxy)
					_do_setpixel(iter_4L, rcolor, Mode);
			}
		}

		if(df < 0) {
			df += d_e;
			d_e += 2;
			d_se += 2;
		} else {
			df += d_se;
			d_e += 2;
			d_se += 4;
			cy--;

			// move to next line
			bottom_inc_inner -= real_width;
			bottom_inc_outter--;
			top_inc_inner -= (-real_width);
			top_inc_outter--;
		}

		cx++;

		// increment pixel addrs
		if(attr & 0x80) {
			if(attr & 0x02) {
				iter_1R += top_inc_inner + 1;
				iter_2L += top_inc_outter - real_width;
			}
			if(attr & 0x20) {
				iter_1L += top_inc_inner - 1;
				iter_4R += -top_inc_outter - real_width;
			}
		}
		if(attr & 0x08) {
			if(attr & 0x02) {
				iter_2R += bottom_inc_outter + real_width;
				iter_3L += bottom_inc_inner + 1;
			}
			if(attr & 0x20) {
				iter_3R += bottom_inc_inner - 1;
				iter_4L += -bottom_inc_outter + real_width;
			}
		}
	} while(cx <= cy);

	LOG(9, "Circle(%d, %d, %d) = <color>", X, Y, Radius);
}
//---------------------------------------------------------------------------
void TCanvas::FillCircle(int X, int Y, int Radius)
{
	LOG_FUNCTION("void TCanvas::FillCircle(int, int, int)");

	Sint16 cx = 0, cy = Radius, df = 1 - Radius, d_e = 3, d_se = -(Radius << 1) + 5;
	Uint16 real_width = Surface->GetRealWidth();
	Sint16 top_inc_outter, top_inc_inner, bottom_inc_outter, bottom_inc_inner;
	int minx, miny, maxx, maxy;
	Uint32 rcolor, fcolor;
	Uint8 attr;
	bool fill;

	/* This is a fairly confusing function and I will try to explain generally
	 * how it works (mostly for myself when it needs maintanance)
	 *
	 * There are eight separate partial arches, and four starting points.
	 * The starting points are at the N, E, S, and W most points on the circle
	 * and are numbered starting at N clockwise from 1 to 4.  The arches are then
	 * drawn moving away from each of these points and are distinguished by
	 * right (R) and left (L) when their starting is placed at the top.
	 *
	 * The arches on the top half of the circle are: 1L, 1R, 2L, 4R
	 * The arches on the top half of the circle are: 2R, 3L, 3R, 4L
	 *
	 * The increment values are used to deal with "cy" changes.  Their names show
	 * which arhes they affect.  For example:
	 *   "top_inc_outter" is applied to the arches on the outside of the top half,
	 *   in other words 4R and 2L.
	 *
	 * See SGE's sge_DoCircle for a clear implementation of the same method..
	 *
	 */

	// get clipping data
	if(!PrepareClipping(minx, miny, maxx, maxy)) return;

	// since this is a fairly quick function we won't do as much with culling as
	// in Ellipse and FillEllipse.
	if(X + Radius < minx) return;
	if(X - Radius > maxx) return;
	if(Y + Radius < miny) return;
	if(Y - Radius > maxy) return;

	// perform pre-clipping, very similar to FrameRect.  We remove whole halves..
	// 		left-half = 	0x20
	// 		top-half = 		0x80
	// 		right-half = 	0x02
	// 		bottom-half = 	0x08
	if(X > minx)
		attr |= 0x20;
	if(Y > miny)
		attr |= 0x80;
	if(X < maxx)
		attr |= 0x02;
	if(Y < maxy)
		attr |= 0x08;

	// get color
	rcolor = Surface->GetFormat().MapToPixel(Pen.Color);
	fcolor = Surface->GetFormat().MapToPixel(Brush.Color);

	// TODO: don't assign pixel pointers at all when their half is not used
	// assign initial pixel pointer to each unique addr
	ImageIterator iter_1R = Surface->make_iterator(ImageIterator::READ_WRITE);
	ImageIterator iter_3L = iter_1R;
	ImageIterator iter_2R = iter_1R;
	ImageIterator iter_4L = iter_1R;

	// put all pixel pointers to there specific start position
	iter_3L.increment(X, Y + cy);
	ImageIterator iter_3R = iter_3L;
	iter_1R.increment(X, Y - cy);
	ImageIterator iter_1L = iter_1R;
	iter_2R.increment(X + cy, Y);
	ImageIterator iter_2L = iter_2R;
	iter_4L.increment(X - cy, Y);
	ImageIterator iter_4R = iter_4L;
	ImageIterator temp_iter;

	do {
		top_inc_outter = bottom_inc_outter = top_inc_inner = bottom_inc_inner = 0;

		// plot/clip all eight points and fill each scan line
		// TODO: fix clipping... This is SO ugly... maybe pre-calculate y-vars
		if(attr & 0x02) {
			int temp_x;

			temp_x = X + cx;
			if(temp_x > minx && temp_x < maxx) {
				if(attr & 0x80 && Y - cy > miny && Y - cy < maxy)
					_do_setpixel(iter_1R, rcolor, Mode);
				if(attr & 0x08 && Y + cy > miny && Y + cy < maxy)
					_do_setpixel(iter_3L, rcolor, Mode);
			}
			temp_x = X + cy;
			if(temp_x > minx && temp_x < maxx) {
				if(attr & 0x80 && Y - cx > miny && Y - cx < maxy)
					_do_setpixel(iter_2L, rcolor, Mode);
				if(attr & 0x08 && Y + cx > miny && Y + cx < maxy)
					_do_setpixel(iter_2R, rcolor, Mode);
			}
		}
		if(attr & 0x20) {
			int temp_x;

			temp_x = X - cx;
			if(temp_x > minx && temp_x < maxx) {
				if(attr & 0x80 && Y - cx > miny && Y - cx < maxy) {
					_do_setpixel(iter_1L, rcolor, Mode);
					if(fill) {
						for(temp_iter = iter_1L+1; temp_iter < iter_1R; temp_iter.increment(1)) {
							_do_setpixel(temp_iter, fcolor, Mode);
						}
					}
				}
				if(attr & 0x08 && Y + cx > miny && Y + cx < maxy) {
					_do_setpixel(iter_3R, rcolor, Mode);
					if(fill) {
						for(temp_iter = iter_3R+1; temp_iter < iter_3L; temp_iter.increment(1)) {
							_do_setpixel(temp_iter, fcolor, Mode);
						}
					}
				}
			}
			temp_x = X - cy;
			if(temp_x > minx && temp_x < maxx) {
				if(attr & 0x80 && Y - cx > miny && Y - cx < maxy) {
					_do_setpixel(iter_4R, rcolor, Mode);
					for(temp_iter = iter_4R+1; temp_iter < iter_2L; temp_iter.increment(1)) {
						_do_setpixel(temp_iter, fcolor, Mode);
					}
				}
				if(attr & 0x08 && Y + cx > miny && Y + cx < maxy) {
					_do_setpixel(iter_4L, rcolor, Mode);
					for(temp_iter = iter_4L+1; temp_iter < iter_2R; temp_iter.increment(1)) {
						_do_setpixel(temp_iter, fcolor, Mode);
					}
				}
			}
		}

		if(df < 0) {
			df += d_e;
			d_e += 2;
			d_se += 2;
			fill = false;
		} else {
			df += d_se;
			d_e += 2;
			d_se += 4;
			cy--;

			// move to next line
			bottom_inc_inner -= real_width;
			bottom_inc_outter--;
			top_inc_inner -= (-real_width);
			top_inc_outter--;
			fill = true;
		}

		cx++;

		// increment pixel addrs
		if(attr & 0x80) {
			if(attr & 0x02) {
				iter_1R += top_inc_inner + 1;
				iter_2L += top_inc_outter - real_width;
			}
			if(attr & 0x20) {
				iter_1L += top_inc_inner - 1;
				iter_4R += -top_inc_outter - real_width;
			}
		}
		if(attr & 0x08) {
			if(attr & 0x02) {
				iter_2R += bottom_inc_outter + real_width;
				iter_3L += bottom_inc_inner + 1;
			}
			if(attr & 0x20) {
				iter_3R += bottom_inc_inner - 1;
				iter_4L += -bottom_inc_outter + real_width;
			}
		}
	} while(cx <= cy);

	LOG(9, "Circle(%d, %d, %d) = <color>", X, Y, Radius);
}
//---------------------------------------------------------------------------
// TODO: remove all used of SDL_Rect in favor of TRect
void TCanvas::DrawString(int X, int Y, const std::string& text,
						 const TFontAttr& attr)
{
	LOG_FUNCTION("void TCanvas::DrawString(int, int, const std::string&, TFontAttr)");

	// TODO: this won't work without access to the underlying SDL_Surface*
	if(IsValid() && Font) {
		TPoint2D root_pos = Surface->GetRootPosition();
		int minx, miny, maxx, maxy;
		TRect temp;
		SDL_Rect sdl_rect;

		if(!PrepareClipping(minx, miny, maxx, maxy)) return;

		sdl_rect.x = minx;
		sdl_rect.y = miny;
		sdl_rect.w = maxx - minx;
		sdl_rect.h = maxy - miny;

		// adjust by surface position
		X += root_pos.X;
		Y += root_pos.Y;
		sdl_rect.x += root_pos.X;
		sdl_rect.y += root_pos.Y;

		// make into ksd rect
		temp = SDLtoKSD(sdl_rect);

		Font->DrawString(Surface->GetHandle(), X, Y, text, &temp, FontAttr);

		LOG(9, "DrawString(%p, %d, %d, %s, { %d, %d, <color> })", Surface,
			X, Y, text.c_str(), attr.Style, attr.Size);
	}
}
//---------------------------------------------------------------------------
// TODO: remove all used of SDL_Rect in favor of TRect
void TCanvas::DrawString(int X, int Y, const std::string& text)
{
	LOG_FUNCTION("void TCanvas::DrawString(int, int, const std::string&)");

	// draw string
	DrawString(X, Y, text, FontAttr);
}
//---------------------------------------------------------------------------
// TODO: remove all used of SDL_Rect in favor of TRect
void TCanvas::DrawString(int X, int Y, const std::string& text,
						 Uint8 Style, Uint8 Size, const TColor& Color)
{
	LOG_FUNCTION("void TCanvas::DrawString(int, int, Uint8, Uint8, const TColor&)");

	// draw string
	TFontAttr attr(Style, Size, Color);
	DrawString(X, Y, text, attr);
}
//---------------------------------------------------------------------------



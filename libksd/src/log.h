//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//	
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		<none>
//	
//	Purpose:		A very simple logging tool.
//	
//	Checked-in by:	$Author$
//					$Date$
//--------------------------------------------------------------------------- 
#ifndef _KSD_logH_
#define _KSD_logH_
//---------------------------------------------------------------------------  
// get libksd configuration
#include "ksdconfig.h"
#include "export_ksd.h"
//--------------------------------------------------------------------------- 
//	LOG(level, args)
//	
//	- level = the log level of this message
//	- args = used just life printf
//	
//	What is log level?  
//		Essentially, the higher the log level the more detailed the log is, (
//	the default is 3). Each log message is assigned a log level, if this is smaller 
//	or equal to THE log level, then the message is logged.  Here is a simple 
//	description of what types of messages are sent on each log level:
//		0: general program/logger messages.  Anything sent on level zero, is 
//		   broadcast to all open logs. (ie. program start/finish and logger mode 
//		   changes)
//		1: fatal errors (includes logger errors)
//		2 - 5: reservered for use by the error class
//		6: Object creation and desctruction
//		7: Marking the beginning/ending of functions
//		8: Marking particular sections of functions
//		9: Outputing the values of specific variables
//		10: Things that loop incessently - can be anything (ex. TWidget::Update
//			logs its enter/exit on log level ten because it will make thousands of
//			log entries in a few seconds.)
//		
//	Example:
//	
//	#include <ksd/log.h>
//	
//	using namespace ksd;
//	int main(int argc, char** argv)
//	{
//		SetLogOptions(log::LOG_TO_EXPLICIT_PATH | log::SHOW_ALL);
//		SetLogFilename("log.txt");
//		
//		LOG(0, "Starting application...");
//		
//		LOG(9, "Have %d arguments", argc);
//		
//		LOG(0, "End application...");
//		
//		return 0;
//	}
//		
//---------------------------------------------------------------------------
// if debugging dangrously, it would seem repetitive to put debug in too
#ifdef 	__DEBUG_DANGEROUSLY__
#define __DEBUG__
#endif
//---------------------------------------------------------------------------
#ifdef KSD_NO_PRETTYFUNCTION_MACRO
#define __PRETTY_FUNCTION__ "PRETTY_FUNCTION"
#define __FUNCTION__ "FUNCTION"
#endif
//--------------------------------------------------------------------------- 
#ifdef KSD_NO_VAARG_MACROS
# ifdef __DEBUG__
#  define LOG ::ksd::LogSimpleMessage
# else
#  define LOG //
# endif
#else
# ifdef __DEBUG__
#  define LOG(level, ARGS...) ::ksd::LogMessage(level, __FILE__, __LINE__, ARGS);
# else
#  define LOG(level, ARGS...) ;
# endif
#endif
//---------------------------------------------------------------------------
// Ease of use, loggin macros
#define LOG_CREATE_OBJ(addr, name) 	LOG(6, "(+) CREATED: %p of type %s", addr, name);
#define LOG_DELETE_OBJ(addr, name) 	LOG(6, "(-) DELETED: %p of type %s", addr, name);
// Due to global object creation order, logging constructors and destructors can be
// dangerous.  NOTE: this won't cause a program crach until main is actually exited,
// so it is still useful in extreme situations. (ACTUALLY, It won't crash at all if
// not writting to a file, at least that is my experience with g++). USE WITH
// DISGRETION!!!  (If anyone has a solution to this problem I would appreciate one!)
#ifdef  __DEBUG_DANGEROUSLY__
#define LOG_CONSTRUCTOR(name)		LOG_CREATE_OBJ(this, name);
#define LOG_DESTRUCTOR(name)		LOG_DELETE_OBJ(this, name);
#else
#define LOG_CONSTRUCTOR(name)		
#define LOG_DESTRUCTOR(name)
#endif
#define LOG_ENTER_FUNC(name)		LOG(7, "* ENTERING function %s", name);
#define LOG_EXIT_FUNC(name)			LOG(7, "* EXITING function %s", name);
#define LOG_FUNCTION(name)			LOG(7, "* FUNCTION %s", name);
#define LOG_SECTION(section)		LOG(8, "* * SECTION: %s", section);
#define LOG_ENTER_FUNC_LOOP(name)	LOG_LOOP("* ENTERING function %s", name);
#define LOG_EXIT_FUNC_LOOP(name)	LOG_LOOP("* EXITING function %s", name);
#define LOG_FUNCTION_LOOP(name)		LOG_LOOP("* FUNCTION %s", name);
#define LOG_SECTION_LOOP(section)	LOG_LOOP("* * SECTION: %s", section);

#ifdef KSD_NO_VAARG_MACROS
// we can't do any better!
# define LOG_LOOP //
#else
# define LOG_LOOP(ARGS...)			LOG(10, ARGS);
#endif
//---------------------------------------------------------------------------
namespace ksd {
//--------------------------------------------------------------------------- 
namespace logger {
enum options {
	// Log destination options
	LOG_TO_DATE_TIME_FILENAME 	= 0x0001, // to file with a name like "10-Oct-2000-18-07-12.log"
	LOG_TO_EXPLICIT_PATH 		= 0x0002, // to file specified with SetLogFilename, (default="runlog");
	LOG_TO_MODULE_NAME			= 0x0004, // to file that is the source file with the extension switched to *.log
	LOG_TO_STANDARD_OUTPUT		= 0x0008, // to screen
	LOG_TO_ALL					= 0x00FF,

	// Log appearance options
	SHOW_NONE 					= 0x0000,
	SHOW_TIMESTAMP 				= 0x0100, // show date/time
	SHOW_ELAPSED_TIME 			= 0x0200, // so time since program started
	SHOW_LOG_LEVEL 				= 0x0400, // so log level
	SHOW_MODULE_NAME 			= 0x0800, // show name of source file with extension removed
	SHOW_LINE_NUMBER 			= 0x1000, // show file name and line number
	SHOW_ALL 					= 0xFF00, 

	// predefine mode types (in order from lightest to heaviest)
	MODE_A = // Text output logging
		LOG_TO_STANDARD_OUTPUT | 
		SHOW_LOG_LEVEL |
		SHOW_MODULE_NAME,
	MODE_B = // Single file logging
		LOG_TO_EXPLICIT_PATH |
		SHOW_TIMESTAMP | 
		SHOW_MODULE_NAME,
	MODE_C = // Intense logging (comparision of runs)
		LOG_TO_DATE_TIME_FILENAME |
		SHOW_ELAPSED_TIME |
		SHOW_LINE_NUMBER,
	MODE_D = // Intense logging (seperate modules)
		LOG_TO_MODULE_NAME |
		SHOW_ELAPSED_TIME |
		SHOW_LINE_NUMBER
};
}; // namespace logger
//--------------------------------------------------------------------------- 
KSD_EXPORT void SetLogOptions(long options);		// default = MODE_A
KSD_EXPORT void SetLogLevel(int level);			// default = 3
KSD_EXPORT void SetLogFilename(char* filename);	// default = "runlog"
KSD_EXPORT void LogMessage(int level, char* file, int line, char* message, ...);

// use if you have a simple log message that you want to be logged weather
// __DEBUG__ is defined or not.  I suggest this is avoided when possible,
// logs are a debugging tool NOT a feature.
KSD_EXPORT void SimpleLogMessage(int level, char* message, ...);
//--------------------------------------------------------------------------- 
}; // namespace ksd
//--------------------------------------------------------------------------- 
#endif


//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//	
//--------------------------------------------------------------------------- 
//	$Header$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Checked-in by:	$Author$
//	
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma implementation
#endif
//--------------------------------------------------------------------------- 
#define KSD_BUILD_LIB
#include <fstream>
#include <iostream>
#include <strstream>
#include <memory>
#include <stdio.h>
#include "PluginSysBase.h"
#include "SharedLib.h"
#include "Plugin.h"

using namespace ksd;
using namespace std;
//--------------------------------------------------------------------------- 
void TPluginSystemBase::Clear()
{
	lib_cs.Lock();

    while(FLibrary.size() > 0) {
    	delete FLibrary.back();
        FLibrary.pop_back();
    }

    lib_cs.Unlock();	
}
//--------------------------------------------------------------------------- 
void TPluginSystemBase::LoadPluginLib(const char* LibName)
{
	// use an auto_ptr so that we don't have to worry about memory leaks on 
	// exceptions.
    std::auto_ptr<TSharedLibrary> LibInstance(new TSharedLibrary);
    
    // load library
    LibInstance->Open(LibName);
	
    // register all the plugins based on the loaded library
    if(LoadPlugin(LibInstance.get()) == false) {
		throw EFileError("Plugin System", "Shared library doesn't follow the proper plugin format", LibName);
	}

    // add EXPORT to list of dlls
    lib_cs.Lock();
    FLibrary.push_back(LibInstance.release());
    lib_cs.Unlock();
}
//--------------------------------------------------------------------------- 
void TPluginSystemBase::LoadPluginList(const char* ListName)
{
    std::ifstream file(ListName);
	std::ostrstream error_stream;
    std::string temp;

	// return error if list cannot be opened
    if(!file) {
		throw EFileError("Plugin System", "Unable to open specified plugin list", ListName);
    }

    while(!file.eof()) {
		// read word
        file >> temp;

		// return error if there are problems with the stream (hopefully we can make a 
		// graceful recovery!)
		if(file.bad()) { 
			throw EFileError("Plugin System", "Error reading plugin list", ListName);
		}

		if(temp == "") continue;
		
        // load plugin library
		try {
        	LoadPluginLib(temp.c_str());
        } catch(::ksd::exception& e) {
        	e.report(error_stream);
        }
    }

    file.close();

	// output the errors found while loading
    cerr << error_stream.str() << flush;
}
//--------------------------------------------------------------------------- 


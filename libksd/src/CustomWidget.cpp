//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2001 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	$Header$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Checked-in by:	$Author$
//
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma implementation
#endif
//--------------------------------------------------------------------------- 
#define KSD_BUILD_LIB
#include "CustomWidget.h"
#include "Surface.h"
#include "Image.h"
#include "log.h"

using namespace ksd;
using namespace SigC;
using namespace std;
//---------------------------------------------------------------------------  
TCustomWidget::TCustomWidget(TWidget* _parent) : TWidget(_parent), flags(string("0100100"))
{
	if(_parent == NULL) {
		// TODO: can't happen, throw exception
	}

	Width = Height = 0;
	NewScrollX = NewScrollY = 0;
	Surface = ClientSurface = NULL;
}
//---------------------------------------------------------------------------  
TCustomWidget::~TCustomWidget()
{
	if(Surface) {
		delete Surface;
	}

	if(ClientSurface) {
		delete ClientSurface;
	}
}
//---------------------------------------------------------------------------  
void TCustomWidget::Resize(int _w, int _h)
{
	Width = _w;
	Height = _h;

	GetSurface()->Resize(Width, Height);
}
//---------------------------------------------------------------------------  
void TCustomWidget::RecreateBuffer()
{
	// psurf = parent surface, nsurf = new urface
	TSurface* psurf = GetParentSurface();
	TSubSurface* nsurf;

	if(!psurf) {
		// TODO: throw exception
		// Can't create a buffer for this widget if it has no parent buffer!!
		LOG(1, "Trying to create a buffer for a widget with no parent buffer!");
		return;
	}

	// refuse to create buffers for a widget with no dimensions
	if(GetWidth() == 0 && GetHeight() == 0) return;

	// sub-surface handles all of our buffering stuff
	TPoint2D pos = GetPosition();
	nsurf = psurf->CreateSubSurface(pos.X, pos.Y, GetWidth(), GetHeight(), GetDoubleBuffered());
	
	// we can only destroy the old buffer if it is not owned by the parent
	if(Surface) {
		delete Surface;
	}

	// set surface to nsurf
	Surface = nsurf;

	// destroy client surface
	if(ClientSurface) delete ClientSurface;

	// create new client surface
	ClientSurface = Surface->CreateSubSurface(ClientRect.GetLeft(), ClientRect.GetTop(),
			ClientRect.GetWidth(), ClientRect.GetHeight());
}
//---------------------------------------------------------------------------  
bool TCustomWidget::DrawWidget(DrawType dt)
{
	bool save_buffer, drawn = false;

	// deal with applying the scrolling
	if(GetClientSurface()) {
		TPoint2D OldScroll = ClientSurface->GetScroll();
		if(OldScroll.X != NewScrollX || OldScroll.Y != NewScrollY) {
			ClientSurface->SetScroll(NewScrollX, NewScrollY);
		}
	}

	// check to see if we need to save the buffer
	save_buffer = !GetChanged() && Surface->IsBuffered() && GetAutoSaveBuffer();

	if(!save_buffer) {	
		// clear buffer
		if((dt == REPAINT || GetAutoClearBuffer()) && !GetTransparentBackground()) {
			ClearBuffer();
			drawn = true;
		}

		// draw self and update drawn flag
		drawn |= Draw(GetCanvas(), dt);

		// reset changed
		if(GetAutoResetChanged()) ResetChanged();

		// adjust client area on repaint
		if(dt == REPAINT) {
			ClientSurface->Reset(ClientRect);
		}
	}

	return drawn;
}
//---------------------------------------------------------------------------  
TPoint2D TCustomWidget::GetChildPosition(TWidget* Child) const
{
	TPoint2D pos = Child->GetPosition();

	if(Child->IsClientWidget()) {
		pos += GetClientRect().GetPosition();

		// adjust for scroll
		if(ClientSurface) {
			// NOTE: the reason we don't try to create client surface in this function
			// is that if it doesn't exist, then there is definitely no scroll value
			pos -= ClientSurface->GetScroll();
		}
	}
	
	return pos;
}
//---------------------------------------------------------------------------  

//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//	
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		fixed
//	
//	Purpose:		a class that encapsulates fixed-point math
//	
//	Checked-in by:	$Author$
//					$Date$
//---------------------------------------------------------------------------
#ifndef _KSD_fixedH
#define _KSD_fixedH
//---------------------------------------------------------------------------
#include <iosfwd>
#include "export_ksd.h"
//---------------------------------------------------------------------------
#ifdef __GNUG__
#pragma interface
#endif
//---------------------------------------------------------------------------
#define STD_FIXED_PREC                  8
#define STD_FIXED_HALF                  128
//---------------------------------------------------------------------------
namespace ksd {
// TODO: inline the math
//---------------------------------------------------------------------------
KSD_EXPORT_CLASS class fixed {
	friend fixed operator+(const fixed&, const fixed&);
	friend fixed operator-(const fixed&, const fixed&);
	friend fixed operator*(const fixed&, const fixed&);
	friend fixed operator/(const fixed&, const fixed&);
public:
    fixed() 				{ Value = 0; }; // initializes to zero
    fixed(float _float) 	{ Value = (long)(_float * float(1 << STD_FIXED_PREC)); }
    fixed(double _double)	{ Value = (long)(_double * double(1 << STD_FIXED_PREC)); }
    fixed(int _int)			{ Value = long(_int) << STD_FIXED_PREC; }

    float   ToFloat() const   		{ return float(float(Value) / float(1 << STD_FIXED_PREC)); }
    double  ToDouble() const  		{ return double(double(Value) / double(1 << STD_FIXED_PREC)); }
    long    ToLong() const    		{ return Value >> STD_FIXED_PREC; }
    int     ToInt() const     		{ return Value >> STD_FIXED_PREC; }
    int     RoundInt() const  		{ return (Value + STD_FIXED_HALF) >> STD_FIXED_PREC; }
    long    GetValue() const  		{ return Value; }
    void    SetValue(long _value) 	{ Value = _value; }

    inline fixed operator += (fixed _fixed);
    inline fixed operator -= (fixed _fixed);
    inline fixed operator *= (fixed _fixed);
    inline fixed operator /= (fixed _fixed);
    inline fixed operator ++ (); 	// weird operator, has select uses
    inline fixed operator -- ();	// weird operator, has select uses
    inline fixed operator - ();
    inline bool operator == (fixed _fixed) const;
    inline bool operator != (fixed _fixed) const;
    inline bool operator > (fixed _fixed) const;
    inline bool operator < (fixed _fixed) const;
    inline bool operator >= (fixed _fixed) const;
    inline bool operator <= (fixed _fixed) const;
private:
    long Value;
};
//--------------------------------------------------------------------------- 
// Functions definitions
//---------------------------------------------------------------------------
fixed fixed::operator += (fixed _fixed)
{
    Value += _fixed.Value;
    return *this;
}
//---------------------------------------------------------------------------
fixed fixed::operator -= (fixed _fixed)
{
    Value -= _fixed.Value;
    return *this;
}
//---------------------------------------------------------------------------
fixed fixed::operator *= (fixed _fixed)
{
	#if !defined(__i386__) || !defined(__ASM__)
    Value = (Value * _fixed.Value) >> STD_FIXED_PREC;
    #else
    { 
    asm { mov  eax, Value }
    asm { imul _fixed.Value }
    asm { shrd eax, edx, 32 - STD_FIXED_PREC }
    asm { mov  Value, eax } 
    }
    #endif
    
    return *this;
}
//---------------------------------------------------------------------------
fixed fixed::operator /= (fixed _fixed)
{
	#if !defined(__i386__) || !defined(__ASM__)
    Value = (Value << STD_FIXED_PREC) / _fixed.Value;
    #else
    asm { mov  eax, Value }
    asm { mov  edx, eax }
    asm { sar  edx, STD_FIXED_PREC }
    asm { shl  eax, 32 - STD_FIXED_PREC }
    asm { idiv _fixed.Value }
    asm { mov  Value, eax }
	#endif
	
    return *this;
}
//---------------------------------------------------------------------------
fixed fixed::operator ++ ()
{
    Value++;
    return *this;
}
//---------------------------------------------------------------------------
fixed fixed::operator -- ()
{
    Value--;
    return *this;
}
//---------------------------------------------------------------------------
fixed fixed::operator - ()
{
    fixed _fixed;

    _fixed.SetValue(-Value);

    return _fixed;
}
//---------------------------------------------------------------------------
bool fixed::operator == (fixed _fixed) const
{
    return Value == _fixed.Value;
}
//---------------------------------------------------------------------------
bool fixed::operator != (fixed _fixed) const
{
    return Value != _fixed.Value;
}
//---------------------------------------------------------------------------
bool fixed::operator > (fixed _fixed) const
{
    return Value > _fixed.Value;
}
//---------------------------------------------------------------------------
bool fixed::operator < (fixed _fixed) const
{
    return Value < _fixed.Value;
}
//---------------------------------------------------------------------------
bool fixed::operator >= (fixed _fixed) const
{
    return Value >= _fixed.Value;
}
//---------------------------------------------------------------------------
bool fixed::operator <= (fixed _fixed) const
{
    return Value <= _fixed.Value;
}
//---------------------------------------------------------------------------
inline fixed operator + (const fixed& a, const fixed& b)
{
	fixed temp;
	temp.Value = a.Value + b.Value;
	return temp;
}
//--------------------------------------------------------------------------- 
inline fixed operator - (const fixed& a, const fixed& b)
{
	fixed temp;
	temp.Value = a.Value - b.Value;
	return temp;
}
//--------------------------------------------------------------------------- 
// TODO change to asm
inline fixed operator * (const fixed& a, const fixed& b)
{
	fixed temp;
	temp.Value = (a.Value * b.Value) >> STD_FIXED_PREC;
	return temp;
}
//--------------------------------------------------------------------------- 
// TODO: change to asm
inline fixed operator / (const fixed& a, const fixed& b)
{
	fixed temp;
	temp.Value = (a.Value << STD_FIXED_PREC) / b.Value;
	return temp;
}
//---------------------------------------------------------------------------
}; // namespace ksd;
//--------------------------------------------------------------------------- 
KSD_EXPORT std::ostream& operator << (std::ostream& Stream, ksd::fixed& f);
KSD_EXPORT std::istream& operator >> (std::istream& Stream, ksd::fixed& f);
//---------------------------------------------------------------------------
#endif


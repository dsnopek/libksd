//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//	
//---------------------------------------------------------------------------
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		TSharedLibrary
//	
//	Purpose:		A class that encapsulates loading a shared library on 
//					many platforms.
//	
//	Checked-in by:	$Author$
//					$Date$
//--------------------------------------------------------------------------- 
#ifndef _KSD_SharedLibH
#define _KSD_SharedLibH
//--------------------------------------------------------------------------- 
#include <SDL/SDL_types.h>
#include "exception.h"
#include "export_ksd.h"
//---------------------------------------------------------------------------
#ifdef __GNUG__
#pragma interface
#endif
//---------------------------------------------------------------------------
namespace ksd {
//---------------------------------------------------------------------------
/** Thrown when the shared library is unable to load the desired symbol.
 */
KSD_EXPORT_CLASS class ECannotLoadSymbol : public ::ksd::exception {
public:
	/** Create a cannot load symbol exception.
	 **
	 ** @param message The error description.
	 ** @param filename The shared library involved.
	 ** @param sym The symbol name involved.
	 */
	ECannotLoadSymbol(const std::string& message,
		const std::string& filename = "none", const std::string& sym = "none")
		: ::ksd::exception("Shared Library", message, "shared library error")
	{
		Filename = filename;
		Symbol = sym;
	}

	/// Get filename of shared object which the error involves.
	std::string GetFilename() const { return Filename; }
	/// Get the symbol involved in the error.
	std::string GetSymbol() const { return Symbol; }

	void report(std::ostream& output) const;
private:
	std::string Filename, Symbol;
};
//---------------------------------------------------------------------------
/** A class for using shared libraries.  Most modern systems support some
 ** form of shared library: Windows has DLL's and GNU/Linux has *.so's.  This
 ** class can be used to deal with shared libraries in a platform independant
 ** way.
 */
KSD_EXPORT_CLASS class TSharedLibrary {
public:
	/** Initialize the shared library object with the library "name".
	 **
	 ** @see Open
	 */
    TSharedLibrary(const char* name);

    /** Create an un-initialized shared library object.
     */
    TSharedLibrary();

	/** Destructor
	 */
    ~TSharedLibrary();

	/** Open a shared library.  If an absolute path isn't given, Open will
	 ** look in the current directory and the libksd plugins directory as well
	 ** as the normal system locations (for example, under GNU/linux, using
	 ** the LD_LIBRARY_PATH variable).  If no library by the given name is found,
	 ** Open will try the name with different extensions.  This allows you to specify
	 ** a platform specific extension or no extension at all (for example, "plugin.so"
	 ** will find "plugin.dll").  Open can also load using libtool objects (*.la's).
	 */
	void Open(const char* name);

	/** Load symbol "name" and store its address in "sym"
	 */
	void GetSymbol(void** sym, const char* name);

	/** Checks to see if this library contains the desired symbol.
	 */
	bool HasSymbol(const char* name);

	/** Unload a shared library
	 */
	void Close();

	/** Returns the name used to load the shared library
	 **
	 */
	const char* GetName() { return Name.c_str(); }
private:
    std::string Name;
    void*  Instance;
};
//---------------------------------------------------------------------------
}; // namespace ksd
//---------------------------------------------------------------------------
#endif


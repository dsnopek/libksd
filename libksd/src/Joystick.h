//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//	
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		TJoystick, TJoystickList, TJoystickFilter
//	
//	Purpose:		defines joysticks
//	
//	Checked-in by:	$Author$
//					$Date$
//---------------------------------------------------------------------------
#ifndef _KSD_JoystickH_
#define _KSD_JoystickH_
//--------------------------------------------------------------------------- 
#include <SDL/SDL_joystick.h>
#include <vector>
#include "Input.h"
#include "export_ksd.h"

//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma interface
#endif
//--------------------------------------------------------------------------- 
namespace ksd {

// forward decl
class TJoystick;
//--------------------------------------------------------------------------- 
// TODO: implement!
KSD_EXPORT_CLASS class TJoystickFilter : public TInputFilter {
public:
	TJoystickFilter(TJoystick* joy);

	void Filter(TInputEvent& evt) { }
	void Detect(int id, int time_out = 1000) { }
	void Load(char*) { }
	void Save(char*) { }
};
//---------------------------------------------------------------------------
// NOTE: I don't usually like to copy values from SDL because it makes mantainance
// more difficult than it needs to be, but for consistancy I need an enumeration
// for joy hat position.
/** Describes joystick hat position.
 */
enum TJoyHatPosition {
	///
	JOY_HAT_CENTERED 	= SDL_HAT_CENTERED,
	///
	JOY_HAT_UP			= SDL_HAT_UP,
	///
	JOY_HAT_RIGHT		= SDL_HAT_RIGHT,
	///
	JOY_HAT_DOWN		= SDL_HAT_DOWN,
	///
	JOY_HAT_LEFT		= SDL_HAT_LEFT,
	///
	JOY_HAT_RIGHT_UP	= SDL_HAT_RIGHTUP,
	///
	JOY_HAT_RIGHT_DOWN	= SDL_HAT_RIGHTDOWN,
	///
	JOY_HAT_LEFT_UP		= SDL_HAT_LEFTUP,
	///
	JOY_HAT_LEFT_DOWN	= SDL_HAT_LEFTDOWN
};
//---------------------------------------------------------------------------
/** A joystick object.  Provides a snapshot of joystick state.
 */
KSD_EXPORT_CLASS class TJoystick : public TInputDevice { friend class TJoystickSubsystem;
public:
	///
	~TJoystick();

	/// Creates a joystick input filter
	TInputFilter* CreateFilter() { return new TJoystickFilter(this); }

	/** Does nothing.  The joysticks cannot be updated individually.  You must
	 ** call TJoystickSubsystem's Update() to update all the joysticks state.
	 */
	void Update() { }; // is updated in the event loop

	/// Returns the number of axes on this joystick.
	int GetNumAxes() const { return SDL_JoystickNumAxes(Handle); }
	/// Returns the number of balls on this joystick.
	int GetNumBalls() const { return SDL_JoystickNumBalls(Handle); }
	/// Returns the number of buttons on this joystick.
	int GetNumButtons() const { return SDL_JoystickNumButtons(Handle); }
	/// Returns the number of hats on this joystick.
	int GetNumHats() const { return SDL_JoystickNumHats(Handle); }

	/// Returns the position of the specified axis.
	Sint16 GetAxis(int i) const { return SDL_JoystickGetAxis(Handle, i); }
	// TODO: implement!
	/// Returns the position of the specified ball.
	//Vector2D GetBall(int i) const { return SDL_JoystickGetBall(Handle, i, X, Y); }
	/// Returns the position of the specified hat.
	TJoyHatPosition GetHat(int i) const { return (TJoyHatPosition)SDL_JoystickGetHat(Handle, i); }
	/// Returns true if the specified button is pressed.
	bool Pressed(int btn) const { return SDL_JoystickGetButton(Handle, btn); }
private:
	SDL_Joystick* Handle;

	TJoystick(SDL_Joystick* _joy);
};
//--------------------------------------------------------------------------- 
/** An encapsulation of the joystick sub-system.  Sub-system is inactive until
 ** Scan()'d and returns to that state after Close().
 */
KSD_EXPORT_CLASS class TJoystickSubsystem {
public:
	~TJoystickSubsystem() {
		Close();
	}

	/** Scans the system for joysticks and creates joystick entries for them.
	 ** Use \ref{GetJoystick} and \ref{GetJoystickCount} to retreive state 
	 ** information from an individual joystick.  This function must be called
	 ** when new joysticks are added to the system.
	 */
	void Scan();

	/** Updates joystick state information for all scanned joysticks.  Call
	 ** this before reading state information (usually every frame).
	 */
	void Update();

	/** Closes the joystick subsystem.  Be sure to call this function before
	 ** your program closes.  Currently, libksd doesn't automatically close
	 ** it and forgetting to do so sould cause a memory leak.
	 */
	void Close();  // close all joysticks

	/** Return the specified joystick objects.
	 */
	TJoystick* GetJoystick (int index) { return List[index]; }

	/** Returns the number of joysticks attached to the system since the last
	 ** Scan.
	 */
	unsigned int GetJoystickCount() { return List.size(); }

	/** Returns the joystick subsystem object.  This follows the Singleton pattern.
	 */
	static TJoystickSubsystem* GetInstance() {
		return &JoystickSys;
	}
private:
	std::vector<TJoystick*> List;

	TJoystickSubsystem() { }
	static TJoystickSubsystem JoystickSys;
};
//--------------------------------------------------------------------------- 
}; // namespace ksd
//--------------------------------------------------------------------------- 
#endif


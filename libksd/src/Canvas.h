//---------------------------------------------------------------------------
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//
//---------------------------------------------------------------------------
//	File:			$RCSfile$
//
//	Author:			David Snopek
//	Revised by:		Andrew Sterling Hanenkamp
//	Version:		$Revision$
//
//	Objects:		TCanvas
//
//	Purpose:		A draw interface to an SDL_Surface.
//
//	Checked-in by:	$Author$
//					$Date$
//---------------------------------------------------------------------------
#ifndef _KSD_CanvasH_
#define _KSD_CanvasH_
//---------------------------------------------------------------------------
#include "Surface.h"
#include "Thread.h"
#include "Color.h"
#include "Rect.h"
#include "Font.h"
#include "export_ksd.h"
//---------------------------------------------------------------------------
#ifdef __GNUG__
#pragma interface
#endif
//---------------------------------------------------------------------------
namespace ksd {

// forward decl
//class TSurface;
class TImage;
//---------------------------------------------------------------------------
/** A pen draw object.  Defines the color of the drawing pen used by TCanvas.
 ** The pen is usually used to draw the border of a draw primitive.
 */
struct TPen {
	/// The pen's color.
	TColor Color;
	//Mode mode
	//Style style
	/// The width of the pen.  Not currenly used.
	int Width;
};
//---------------------------------------------------------------------------
/** A brush draw object.  Defines the color of the drawing brush used by the
 ** TCanvas.  The brush is usually used to fill a draw primitive.
 */
struct TBrush {
	/// The brush's color.
	TColor Color;
	//TImage Bitmap;
};
//---------------------------------------------------------------------------
/** The draw operation mode.
 */
enum TDrawMode {
	/// Source replaces destination.
	OP_REPLACE,
	/// Source is binary AND'ed with the destination.
	OP_AND,
	/// Source is binary OR'ed with the destination.
	OP_OR,
	/// Source is binary XOR'ed with the destination.
	OP_XOR
};
//---------------------------------------------------------------------------
KSD_EXPORT_CLASS class TCanvas {
public:
	/** Creates a Canvas arround this TSurface object.
	 */
	TCanvas(TSurface* _surface = NULL);

	///@name Drawing Parameters
	//{
	/// The draw pen. 
	TPen 		Pen;
	/// The draw brush.
	TBrush 		Brush;
	/// The draw mode. Controls how color is combined with the existing data on the Canvas.
	TDrawMode 	Mode;
	/// The clip rect.
	TRect		ClipRect;
	/// The current font.
	TFont*		Font;
	/// The current font attrs.
	TFontAttr	FontAttr;
	//}

	/// Copy all the drawing parameters from another canvas.
	inline void CopyParam(TCanvas* src) {
		Pen = src->Pen;
		Brush = src->Brush;
		Mode = src->Mode;
		ClipRect = src->ClipRect;
		Font = src->Font;
	}

	/// Returns the surface object that we are drawing to.
	TSurface* GetSurface() { return Surface; }
	/// Changes the surface object that we are drawing to.
	void SetSurface(TSurface* _surface) { Surface = _surface; }

	/// Sets the pixel at the given position to the given color.
	void SetPixel(int X, int Y, const TColor&);
	/// Sets the pixel at the given position to the given color.
	void SetPixel(int X, int Y, Uint32 raw_color);
	/// The color value of the pixel at the given position.
	TColor GetPixel(int X, int Y);

	/// Locks the underlying surface so that we can draw to it.
	inline void BeginDraw() { 
		Surface->Lock();
		DrawBlock++;
	}
	/// Unlocks the surface after drawing.
	inline void EndDraw() {
		if(DrawBlock) {
			Surface->Unlock();
			DrawBlock--;
		} else {
			throw("Trying to EndDraw() a TCanvas that was never BeginDraw()'n");
		}
	}
	
	/** Returns false if this is not a valid drawing area.  An invalid canvas either
	 ** means that the canvas points to an ear off the viewable screen or the 
	 ** underlying surface object is invalid.  While all the drawing functions will
	 ** first check for validity, it could possible improve speed by not drawing
	 ** to can invalid surface.
	 */
	bool IsValid() const;

	/** Draws the spcified image to the given position on the canvas.  
	 ** (\textsl{Note: Doesn't heed the Mode!})
	 */
    void Blit(int X, int Y, TImage& src);

	/** Draws the specified image to the given position, and stretches it to
	 ** the given dimensions. (\textsl{Note: Doesn't heed the Mode!})
	 */
	void StretchBlit(int X, int Y, int Width, int Height, TImage& src);

	/** Draws a 2D shape without knowing its underlying type.
	 */
	void DrawShape(const TShape2D& shape, bool fill);
	
	/** Draws a line using the current pen, from (x1, y1) to (x2, y2).
	 */
    void Line(int x1, int y1, int x2, int y2);

	/** Draws a horzontal line using the current pen, from (x1, y) to (x2, y).
	 */
    void HorzLine(int y, int x1, int x2);

    /** Draws a vertical line using the current pen, from (x, y1) to (x, y2).
     **/
    void VertLine(int x, int y1, int y2);

	/** Draws a rectangle to the specified dimenstions using the current pen.
	 */
    void FrameRect(int left, int top, int right, int bottom);

    /** Draws a rectangle to the specified dimenstions using the current pen.
	 */
    void FrameRect(const TRect& r) { FrameRect(r.GetLeft(), r.GetTop(), r.GetRight(), r.GetBottom()); }

	/** Draws a rectangle to the specified dimesntions, using the current pen for
     ** the border and filling the rectangle with the current brush.
     */
    void FillRect(int left, int top, int right, int bottom);

	/** Draws a polygon using the current pen.
	 */
	void Polygon(int length, TPoint2D *polygon);

	/** Draws a filled polygon, using the current pen for the border and filling it
	 ** with the current brush.
	 */
	void FillPolygon(int length, TPoint2D *polygon);

	// TODO: implement
    //void RoundRect(int left, int top, int right, int bottom);
    //void FillRoundRect(int left, int top, int right, int bottom);

	/** Draws an ellipse using the current pen.
	 */
    void Ellipse(int X, int Y, int Width, int Height);

	/** Draws a filled ellipse, using the current pen for the border and filling it
	 ** with the current brush.
	 */
    void FillEllipse(int X, int Y, int Width, int Height);

    /** Draws an ellipse using the current pen.
	 */
    void Circle(int X, int Y, int Radius);

	/** Draws a filled circle, using the current pen for the border and filling it
	 ** with the current brush.
	 */
    void FillCircle(int X, int Y, int Radius);

	/** Draws the string, \textit{text}, at the given position using the current font
	 ** and given font attr.
	 */
    void DrawString(int X, int Y, const std::string& text,
    				const TFontAttr& attr);

    /** Draws the string, \textit{text}, at the given position using the current font
     ** and font attr.
     */
    void DrawString(int X, int Y, const std::string& text);

	/** Draws the string, \textit{text}, at the given position using the current font
	 ** and the given attributes.
	 */
    void DrawString(int X, int Y, const std::string& text,
    				Uint8 Style, Uint8 Size, const TColor& Color);
private:
	TSurface* Surface;
	unsigned int DrawBlock;

	bool PrepareClipping(int& minx, int& miny, int& maxx, int& maxy);
};
//---------------------------------------------------------------------------
}; // namespace ksd
//---------------------------------------------------------------------------
#endif


//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2001 Arthur Peters
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	$Header$
//	
//	Author:			Arthur Peters
//	Revised by:		
//	Version:		$Revision$
//	
//	Checked-in by:	$Author$
//                            $Date$    
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma implementation
#endif
//--------------------------------------------------------------------------- 
#define KSD_BUILD_LIB
#include "Song.h"
#include "exception.h"
#include "log.h"

using namespace ksd;
//--------------------------------------------------------------------------- 
/**
   Initialize the object as a empty song. If you try to play
   it the mixer will return an error.
*/ 
TSong::TSong()
{
	SongData = NULL;
	InUse = false;
}
//--------------------------------------------------------------------------- 
TSong::~TSong()
{
	FreeSongData();
}
//--------------------------------------------------------------------------- 
/**
   Load a song from a file. The supported formats include: MIDI
   (using timidity), MOD, XM, IT, S3M (all module format use
   MikMod), MP3 (using SMPEG) and OGG (using the vorbis
   library). All formats are auto-detected.
*/
void TSong::Load( const char* filename )
{
	if( SongData ) {
		FreeSongData();
	}

	/// SDL_RWops are not supported by SDL_mixer for music
   
	SongData = Mix_LoadMUS( filename );
	if(SongData == NULL) {
		throw EFileError("Song", "Unable to load song from specified file", filename);
	}
}
//--------------------------------------------------------------------------- 
void TSong::FreeSongData()
{
	if( InUse ) {
		LogMessage( 0, __FILE__, __LINE__, "A TSong object that is in use is begin freed."
					" The underlying memory will not be freed. This will cause a memory leak.");
	} else {
		Mix_FreeMusic( SongData );
		SongData = NULL;
	}
}
//---------------------------------------------------------------------------  




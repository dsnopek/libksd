//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//	
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		TFont, TFontList, TFontAttr
//	
//	Purpose:		TFont = An abstract font class.  Children include TTrueTypeFont
//						and TBitmapFont.  Currently doesn't support unicode.
//					TFontList = Used to load fonts.conf files which allow you to 
//						give names to the fonts that are available.
//					TFontAttr = holds the attributes that a font can take on
//	
//	Checked-in by:	$Author$
//					$Date$
//--------------------------------------------------------------------------- 
#ifndef _KSD_FontH_
#define _KSD_FontH_
//--------------------------------------------------------------------------- 
#include <string>
#include <map>
#include "Color.h"
#include "Rect.h"
#include "export_ksd.h"

// forward decls
struct SDL_Surface;
//---------------------------------------------------------------------------
#ifdef __GNUG__
#pragma interface
#endif
//---------------------------------------------------------------------------
namespace ksd {
//---------------------------------------------------------------------------
/** A structure that describes font attributes, including: Style, Size, and
 ** Color.
 */
KSD_EXPORT_CLASS struct TFontAttr {
	///
	TFontAttr() {
		Style = 0;
		Size = 12;
	};

	///
	TFontAttr(const TFontAttr& fa) {
		Style = fa.Style;
		Size = fa.Size;
		Color = fa.Color;
	}

	///
	TFontAttr(Uint8 _style, Uint8 _size = 12, const TColor& _color = TColor::black) {
		Style = _style;
		Size = _size;
		Color = _color;
	};

	/// Style flags
	Uint8 	Style, 
	/// Font size in points
			Size;
	/// Color
	TColor Color;

	///
	inline bool operator == (const TFontAttr& V) const {
		if(Style == V.Style) {
			if(Size == V.Size) {
				if(Color == V.Color) {
					return true;
				}
			}
		}
		return false;
	}

	///
	inline bool operator != (const TFontAttr& V) const {
		if(Style != V.Style) return true;
		if(Size != V.Size) return true;
		if(Color != V.Color) return true;
		return false;
	}
};
//---------------------------------------------------------------------------  
/** The default font attr that is usually used when a font attr is not 
 ** provided.  This font attr is defined as
 ** TFontAttr(TFont::Normal, 12, TColor::black).
 */
KSD_EXPORT extern const TFontAttr DefaultFontAttr; // = { TFont::Normal, 12, TColor::black };
//---------------------------------------------------------------------------
/** Abstract font base class.  All 2D font types descend from this class.
 */
KSD_EXPORT_CLASS class TFont { friend class TCanvas;
public:
	virtual ~TFont() { }

	/// Font style flags
	enum /*Style*/ { 
		///
		Normal = 	0x00, 
		///
		Bold = 		0x01, 
		///
		Italic =	0x02, 
		///
		Underline = 0x04
	};

	/// Load the font data from a file
	virtual void Open(const std::string& filename) = 0;

	/** Optimize a font for use with a particular set of font attr's.
	 ** Some font implementations maintain a copy of the font in each form used 
	 ** so far.  Optimize destroys all but the list of fonts types supplied and creates
	 ** those that did not exist to begin with.  If NULL is supplied as the list then
	 ** the font is optimize to the default.
	 */
	virtual void Optimize(TFontAttr* List = NULL, int n = 0) { } 

	/** Return the height of the font (in pixels) with a particular attr.
	 */
	virtual int GetHeight(const TFontAttr&) = 0;

	/** Return the ascent of the font (in pixels) with a particular attr.  The 
	 ** acsent is the distance from the baseline to the top of the font.  This
	 ** is a positive value.
	 */
	virtual int GetAscent(const TFontAttr&) = 0;

	/** Return the descent of the font (in pixels) with a particular attr.  The 
	 ** descent is the distance from the the baseline to the bottom of the font.  
	 ** This is a negative value.
	 */
	virtual int GetDescent(const TFontAttr&) = 0;

	/** Returns the suggested space between lines (in pixels) for this font with a 
	 ** particular font attr.
	 */
	virtual int GetLineSkip(const TFontAttr&) = 0;

	/** Gets the size of the given text with a particular font attr.  The size
	 ** (in pixels) is placed in the width and height variables.
	 */
	virtual void GetTextSize(const std::string& text, const TFontAttr&, 
							 int& width, int& height) = 0;

	/** Loads a concrete font object.  Use this function to generically
	 ** load a font file.
	 */
	static TFont* LoadFont(const std::string& filename);
protected:
	/** Does the actual string drawing.  Pass NULL as a clip rect to disable
	 ** clipping.
	 */
	// TODO: Why SDL_Surface?
	// NOTE: This function is hidden ("protected") so that the only way to draw is
	// through TCanvas which supports proper drawing protocols. (Although, you can't 
	// pass it a TSurface can you.  I may rethink its hidden nature.)
	virtual void DrawString(SDL_Surface*, int X, int Y, const std::string& text,
							TRect* ClipRect, 
							const TFontAttr& attr = DefaultFontAttr) = 0;
};
//--------------------------------------------------------------------------- 
/** A class for storing font objects by name.  It allows you to make a text file 
 ** containing mappings from font filenames to font names.  It is possible
 ** to specify multiple font files to each font name.  When loading the text 
 ** file, TFontList will try each font file until one works.  This is meant 
 ** to allow you to provide fonts for both font systems in case a user doesn't
 ** use one of them.  Here is example font conf file:

\begin{verbatim}
# You can make comments using the pound-sign

# the "/cd" command allows you to change the current directory for
# loading font files
/cd fontdir

arial	arial.ttf arial.png
fontname fontfile.ttf fontfile.png

\end{verbatim}

 ** 
 */
KSD_EXPORT_CLASS class TFontList {
public:
	~TFontList() { Clear(); }

	/** Clears the font list, deleting all of the font objects.
	 ** \textit{Be sure to clear the font list before the application
	 ** ends!}  If you don't, the font underlying font sub-systems
	 ** could be shutdown before the font objects are deleted.  A
	 ** good place to clear the font list is in your application's
	 ** Shutdown() function.
	 */
	void Clear() {
		for(std::map<std::string, TFont*>::iterator i = List.begin();
			i != List.end(); i++) {
			delete (*i).second;
		}
		List.clear();
	}

	/** Add a font object with the given name.
	 */
	void Add(const std::string& Name, TFont* Font) {
		std::map<std::string, TFont*>::iterator i;
		i = List.find(Name);
		if(i == List.end()) List[Name] = Font;
		else {
			// TODO: Deal with multipe fonts of the same name
		}
	}

	/** Returns a pointer to the font object associated with the given 
	 ** name.
	 */
	TFont* GetFont(const std::string& Name) {
		std::map<std::string, TFont*>::iterator i;
		i = List.find(Name);
		if(i == List.end()) return NULL;

		return (*i).second;
	}

	/** Returns the name associated with the given font object.  Returns an empty
	 ** string if the font object isn't in the list.
	 */
	std::string GetFontName(TFont* Font) {
		std::map<std::string, TFont*>::iterator i = List.begin();
		for(; i != List.end(); i++) {
			if((*i).second == Font) {
				return (*i).first;
			}
		}
		return "";
	}

	/** Loads a font file into the list with the given name.
	 */
	void LoadFont(const std::string& font_name, const std::string& filename);

	/** Loads a font list config file
	 */
	void LoadList(const std::string& filename);

	// TODO: make list saving 
	//error SaveList(char* filename);
private:
	std::map<std::string, TFont*> List;
};
//--------------------------------------------------------------------------- 
}; // namespace ksd
//--------------------------------------------------------------------------- 
#endif


//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//	
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		TSurface
//	
//	Purpose:		A surface abstraction.  Implements reference counting.
//	
//	Checked-in by:	$Author$
//					$Date$
//--------------------------------------------------------------------------- 
#ifndef _KSD_SurfaceH_
#define _KSD_SurfaceH_
//---------------------------------------------------------------------------
#include <SDL/SDL_video.h>
#include "PixelFormat.h"
#include "Thread.h"
#include "ImageIterator.h"
#include "Point2D.h"
#include "Rect.h"
#include "ref.h"
#include "export_ksd.h"
//---------------------------------------------------------------------------
#ifdef __GNUG__
#pragma interface
#endif
//---------------------------------------------------------------------------
namespace ksd {
//---------------------------------------------------------------------------
// forward decl
class TSubSurface;
class TCanvas;
//---------------------------------------------------------------------------
// TODO: instead of having canvas do all the locking/unlocking, make it check
// some flag inside of TSurface and refuse to draw if its not locked.  This
// should simplify our code and possible improve speed?
// NOTE: This thought has been invlidated by the architecture, although it is
// still a good idea.  Find a way to do it directly to SDL and not through
// TSurface voodoo.
//---------------------------------------------------------------------------
/** A raw surface object with no voodoo.  It is a one-to-one representation of 
 ** an SDL_Surface* object.  It is the underlying data used in the entire 
 ** TSurface heirarchy.
 */
KSD_EXPORT_CLASS class TSurfaceData {
public:
	TSurfaceData(SDL_Surface* _surface, bool _owned = true) {
		// set surface depends on Surface being NULL
		Surface = NULL;
		SetSurface(_surface, _owned);
	}

	TSurfaceData() {
		Surface = NULL;
		Owned = true;
	}
	
	~TSurfaceData() {
		DestroySurface();
	}

	/// Flags describing the type of the surface
	enum Type {
		/// Surface resides in system memory.
		SYSTEM_MEMORY 	= 0x00,
		/// Surface resides in video memory.
		VIDEO_MEMORY	= 0x01,
		/// Surface has a color key.
		SRC_COLORKEY	= 0x10,
		/// Surface has an alpha value.
		SRC_ALPHA		= 0x20
	};

	
	// return SDL specific attributes
	bool 			IsValid() const 	{ return Surface != 0; }
	int 			GetWidth() const 	{ return Surface ? Surface->w : 0; }
	int 			GetHeight() const 	{ return Surface ? Surface->h : 0; }
	int 			GetPitch() const 	{ return Surface ? Surface->pitch : 0; }
	void* 			GetPixels() 		{ return Surface ? Surface->pixels : NULL; }
	TPixelFormat 	GetFormat() const 	{ return Format; }
	unsigned int	GetFlags() const	{ return Flags; }

	ImageIterator make_iterator(ImageIterator::Type type) {
		if(IsValid()) {
			return ImageIterator(Surface->pixels, Surface->format->BytesPerPixel, 
				Surface->w, Surface->pitch, Surface->h, type);
		} else {
			return ImageIterator();
		}
	}
	
	// used to custom create an SDL surface
	void SetSurface(SDL_Surface* _surface, bool _owned) {
		if(Surface) DestroySurface();
		Surface = _surface;
		Owned = _owned;

		if(Surface)
			Format.Set(Surface->format);
		else
			Format.Set(TPixelFormat::UNKNOWN);
	}

	SDL_Surface* GetHandle() { return Surface; }

	// convert surface type
	TSurfaceData* Convert(const TPixelFormat& _format, int image_type);

	// functions for creating images
	void CreateImage(int width, int height, 
		TPixelFormatType pixel_format = TPixelFormat::DISPLAY,
		unsigned int flags = SYSTEM_MEMORY);
	void CreateImage(int width, int height, unsigned int bpp,
		unsigned int flags = SYSTEM_MEMORY);

	void Fill(int X, int Y, int Width, int Height, const TColor& _color);
	void Fill(const TColor& _color) {
		if(IsValid()) Fill(0, 0, GetWidth() + 1, GetHeight() + 1, _color);
	}
	void Resize(unsigned int width, unsigned int height);

	// locking/unlocking
	void Lock();
	void Unlock();

	// for dealing with palette
	void ApplyPalette();
	void GatherPalette();
protected:
	void SetFlags(unsigned int _flags) { Flags = _flags; }
private:
	SDL_Surface* Surface;
	TMutex Mutex;
	bool Owned;
	TPixelFormat Format;
	unsigned int Flags;

	void DestroySurface() {
		if(Owned && Surface) {
			SDL_FreeSurface(Surface);
			Surface = NULL;
		}
	}
};
//---------------------------------------------------------------------------
KSD_EXPORT_CLASS class TSurface {
public:
	TSurface();
	virtual ~TSurface();

	/** Returns true if we have a valid, usable surface.
	 */
	bool IsValid() const {
		if(!ref.GetData()->IsValid()) return false;
		else if(GetWidth() == 0 || GetHeight() == 0) return false;

		return true;
	}

	/** Manually, add a reference to this surface.  This is necessary if 
	 ** you must pass surfaces as pointers.  In most cases, references  
	 ** are added and removed automatically by the constructor and 
	 ** destructor.
	 */
	void AddReference() { ref.AddRef(); }

	/** Manually removes a reference to this surface.  This is necessary if 
	 ** you must pass surfaces as pointers.  In most cases, references  
	 ** are added and removed automatically by the constructor and 
	 ** destructor.
	 */
	void RemoveReference() { ref.RemoveRef(); }

	/** Returns the pixel format of this surface
	 */
	TPixelFormat GetFormat() const { return ref.GetData()->GetFormat(); }

	/** Returns the surface flags.
	 */
	unsigned int GetFlags() const { return ref.GetData()->GetFlags(); }

	/** Returns the real width of this surface.  The real width is the distance
	 ** from the start of one line to the start of the next line in pixels.  This
	 ** is necessary because Width doesn't always equal RealWidth.
	 */
	int GetRealWidth() const { return GetPitch() / GetFormat().GetBytesPerPixel(); }

	/** Returns the pitch of this surface.  The pitch is the distance from the start
	 ** of one line to the start of the next line in bytes.  Similar to GetRealWidth
	 ** except that its units are bytes.
	 */
	virtual Uint16 GetPitch() const { return ref.GetData()->GetPitch(); }

	/** Returns the width of this surface.
	 */
	virtual int GetWidth() const { return ref.GetData()->GetWidth(); }

	/** Returns the height of this surface.
	 */
	virtual int GetHeight() const { return ref.GetData()->GetHeight(); }

	/** Returns an ImageIterator that wraps access to this surfaces pixels.  Use this
	 ** to read/write to the surfaces pixel data.  When this function is used to create
	 ** a READ_WRITE iterator, the OnWrite() function is called.  For some surface types,
	 ** this implements the "copy on write" functionality.
	 */
	virtual ImageIterator make_iterator(ImageIterator::Type type) { 
		if(type == ImageIterator::READ_WRITE) {
			// call the on write function
			OnWrite();
		}
		return ref.GetData()->make_iterator(type); 
	}
	
	/** Captures this surface for this thread.  This stops write access to the 
	 ** underlying surface data type for all threads but this one.  Note: Not
	 ** all surfaces need to be "captured".  In which case this function will 
	 ** do nothing.
	 */
	virtual void Lock() { ref.GetData()->Lock(); }

	/** Release this surface so other threads get an oportunity to "capture" this
	 ** surface. Note: Not all surfaces need to be "captured".  In which case this 
	 ** function will do nothing.
	 */
	virtual void Unlock() { ref.GetData()->Unlock(); }

	/** Returns any offset (positive or negitive) that this surface may have.  This 
	 ** function serves no purpose if you use ImageIterator or TCanvas to perform
	 ** your drawings.  It serves only to allow access from outside C libraries.
	 **/
	virtual TPoint2D GetRootPosition() { return TPoint2D(0, 0); }

	/** Returns the surface clip rect.  Where the clip rect actually comes from is up to
	 ** the type of surface.  A TScreen will return its own dimensions, a TSubSurface will
	 ** return its dimensions scrolled, and TImage will return the low-level SDL cliprect.
	 **/
	virtual TRect GetClipRect() { return TRect(0, 0, GetWidth(), GetHeight()); }

	/** Returns the underlying SDL_Surface* object.  FOR NORMAL OPERATION, DO NOT
	 ** USE THIS!  This function exists only to allow a TSurface object to interface
	 ** with C code that was originally targeted at SDL.  There are a number of 
	 ** rules you have to follow when using this value to ensure that the surface 
	 ** operates as expected:
	 **  1. First, when you take the value to be held somewhere (even for just the
	 **     duration of a function), call AddReference().
	 **  2. Call Lock(). Note this calls SDL_LockSurface(), so if your code expects
	 **     the surface to be unlocked you must unlock after this call and then 
	 **     re-lock it before calling Unlock().
	 **  3. Call OnWrite() if you plan on doing any writting.  
	 **  4. Now you can call GetHandle() and use the SDL_Surface* object.  All of your 
	 **     access to the pixel data must be relative to the point returned by
	 **     GetRootPosition().  If the root position is in negative X or Y the first
	 **     X or Y pixels DO NOT EXIST!  You must "scroll" all of your drawing that
	 **     many pixels to the left.  Also, do not follow the height, width, and
	 **     pitch reported by the SDL_Surface*.  You must rely on GetHeight(), 
	 **     GetWidth(), and GetPitch().  All of these values are relative to the
	 **     point returned by GetRootPosition().
	 **  5. Call Unlock().
	 **  6. When you are done holding a reference to the surface, be sure to call
	 **     RemoveReference().
	 ** Changing this recipe can have disaterous consequences, especially from the 
	 ** OnWrite() call which may even change the SDL_Surface* object between calls.
	 **/
	SDL_Surface* GetHandle() {
		return ref.GetData()->GetHandle();
	}
	
	/** Creates a surface the covers an portion of this surface.  Any change 
	 ** made to the subsurface will result in a corresponding change to 
	 ** the parent surface.  You can request to create a \textit{buffered} or
	 ** \textit{unbuffered} sub-surface.  A buffered subsurface creates a buffer
	 ** that the data is first drawn to, then must be explicitly copied onto
	 ** the parent surface.  An unbuffered subsurface draws directly to the 
	 ** parent surface.  While you can request a specific type of sub-surface,
	 ** the type which is actually returned is up to the disgretion of the 
	 ** parent surface.  Make sure to check its type after creating a 
	 ** subsurface.
	 */
	virtual TSubSurface* CreateSubSurface(int X, int Y, int Width, int Height, bool _buffered = false) = 0;

	/** Clears the surface to this color.  This is usually faster than 
	 ** TCanvas::FillRect.
	 */
	virtual void Fill(const TColor& Color) { ref.GetData()->Fill(Color); }

	/** Resize surface to the given dimensions.
	 */
	virtual void Resize(int width, int height) { ref.GetData()->Resize(width, height); }

	/** Copies the actual palette data from the underlying surface object into
	 ** the TPalette stored in this surfaces TPixelFormat.
	 */
	void GatherPalette() { ref.GetData()->GatherPalette(); }

	/** Copies the information stored in the TPalette object in this surface's
	 ** TPixelFormat to the underlying surface object.
	 */
	void ApplyPalette() { ref.GetData()->ApplyPalette(); }

	/** Override this function to provide some sort of "on write" functionality.
	 ** Use of this function is purely optional, and up to the Surface implementation.
	 ** But since this function \textit{is} called by other parts of the API, make
	 ** sure that its affect is consistant with the rest of the surface's operation.
	 */
	virtual void OnWrite() = 0;
protected:
	/** Unreferences current surface data, and creates a new one.
	 */
	void Clear() { ref.Clear(); }

	/** Returns the underlying data object that this object currently refers.
	 */
	TSurfaceData* GetData() { return ref.GetData().Data; }

	/** Sets a new data object.  Make sure this object is of the proper 
	 ** SurfaceData type.  You can use CreateData() if necessary.
	 */
	void SetData(TSurfaceData* _data) { ref.SetData(DataWrapper(_data)); }

	/** Makes this surface in a reference to the data pointed to by "_surf".  This
	 ** should be used by sub-classes in there copy functions to make references.
	 */
	void MakeReferer(const TSurface& _surf) { ref = _surf.ref; }

	/** Returns true if the object referred to by this surface is not refered to by
	 ** any other surface objects.
	 */
	bool IsUnique() const { return ref.GetRefCount() == 1; }

	/** Provides a way to take the SDL_Surface* from GetHandle() and discard the 
	 ** TSurface without the SDL_Surface* getting destroyed.  This is only safe 
	 ** todo if IsUnique() returns true.
	 */
	void Unhook() { ref.Unhook(); }
private:
	// TODO: make into generic template
	class DataWrapper {
	public:
		DataWrapper() { Data = new TSurfaceData; }
		DataWrapper(TSurfaceData* _data) { Data = _data; }
		DataWrapper(const DataWrapper& _copy) { Data = _copy.Data; }
		~DataWrapper() { delete Data; }

		TSurfaceData* operator->() {
			return Data;
		}
		const TSurfaceData* operator->() const {
			return Data;
		}
	
		TSurfaceData* Data;
	};
	
	TRefCounter<DataWrapper> ref;
};
//---------------------------------------------------------------------------  
}; // namespace ksd
//--------------------------------------------------------------------------- 
#endif


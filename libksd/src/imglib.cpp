//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	$Header$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Checked-in by:	$Author$
//
//--------------------------------------------------------------------------- 
#define KSD_BUILD_LIB
#include <SDL/SDL.h>
#include "imglib.h"
#include "Image.h"

using namespace ksd;
using namespace std;
//---------------------------------------------------------------------------  
extern "C" int IMGLIB_Load(SDL_Surface** Surface, char* filename, char _ctype[3])
{
	TImage img;
	string type;

	// TODO: make deal with types better.. What about "   " as a type??
	type = string(*_ctype, 3);
	if(type == "") type = "AUTODETECT";

	// load image
	try {
		img.Load(filename, (char*)type.c_str());
	} catch(::ksd::exception& e) {
		// deal with an error
		SDL_SetError(e.what().c_str());
		return 1;
	}

	// unreference SUrface from img
	*Surface = img.GetHandle();
	img.Unhook();

	return 0;
}
//---------------------------------------------------------------------------  
extern "C" int IMGLIB_Save(SDL_Surface* Surface, char* filename, char _ctype[3])
{
	TImage img;
	string type;

	// create image
	img.SetSurface(Surface, false);

	// TODO: make deal with types better.. What about "   " as a type??
	type = string(*_ctype, 3);
	if(type == "") type = "AUTODETECT";

	// load image
	try {
		img.Save(filename, (char*)type.c_str());
	} catch(::ksd::exception& e) {
		// deal with an error
		SDL_SetError(e.what().c_str());
		return 1;
	}
	
	return 0;
}
//---------------------------------------------------------------------------  


//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		Circle
//	
//	Purpose:		Defines an object representing a circle.
//	
//	Checked-in by:	$Author$
//					$Date$
//---------------------------------------------------------------------------
#ifndef _KSD_CircleH
#define _KSD_CircleH
//---------------------------------------------------------------------------
#include "Shape2D.h"
#include "export_ksd.h"
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma interface
#endif
//---------------------------------------------------------------------------
namespace ksd {
//---------------------------------------------------------------------------
/** A circle shape object.  Used to describe circular regions in screen 
 ** space.  It is not advised that you use it for anything more complex because
 ** TCircle is optimized for speed and size, not precision.
 **
 */
KSD_EXPORT_CLASS class TCircle : public TShape2D {
public:
	///
    TCircle() {
		Radius = 0;	
	}
	
	/// Create a circle centered at (x, y) with the given radius
    TCircle(int x, int y, unsigned int radius) {
		Center.X = x;
    	Center.Y = y;
	    Radius = radius;
	}
	
	/// Create a circle centered at point with the given radius
    TCircle(const TPoint2D& point, unsigned int radius) {
		Set(point, radius);	
	}
	
    /// Create a circle centered at center, with the given point on its edge
	TCircle(const TPoint2D& center, const TPoint2D& point) {
		CreateCircle(center, point);
	}

	/// Set the center of the circle
	void SetCenter(const TPoint2D& V) { Center = V; }
	/// Set the center of the cirlce
	void SetCenter(int X, int Y) { Center.Set(X, Y); }
	/// Set the radius of the circle
	void SetRadius(int r) { Radius = r; }
	/// Returns the center of the circle
    TPoint2D GetCenter() const { return Center; }
	/// Returns the radius of the circle
    unsigned int GetRadius() const { return Radius; }

	/// Creates a circle described by its center and a point on its edge
   	void CreateCircle(const TPoint2D& center, const TPoint2D& point);
   	
   	/// Sets circle attributes
   	void Set(const TPoint2D& _center, unsigned int _radius) {
   		Center = _center;
   		Radius = _radius;
   	}

	// shape functions
    bool IsInside(const TPoint2D& V) const;
    void Move(int X, int Y);
    void MoveTo(int X, int Y);
	void Visit(Visitor* V) { V->VisitCircle(this); }   
 
protected:
	TPoint2D Center;
    unsigned int Radius;
};
//---------------------------------------------------------------------------
}; // namespace ksd
//---------------------------------------------------------------------------
#endif
 

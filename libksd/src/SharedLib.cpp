//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//	
//--------------------------------------------------------------------------- 
//	$Header$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Checked-in by:	$Author$
//	
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma implementation
#endif
//--------------------------------------------------------------------------- 
#define KSD_BUILD_LIB
#include <string>
#include <iostream>
#include "SharedLib.h"
#include "ksdconfig.h"

// multiple platform includes
#ifdef KSD_ENABLE_SHARED_LIBRARY_SUPPORT
	#if defined(USE_LTDL)
		#include <ltdl.h>
		typedef lt_dlhandle ModuleHandle;
	#elif defined(macintosh)
		#error Macintosh shared library code is UNFINISHED!! 
		#error If you wish to help, see libksd/src/SharedLib.cpp

		#include <CodeFragments.h>
		#include <TextUtils.h>
		#include <Types.h>
		#include <Strings.h>
		#include <Aliases.h>

		typedef CFragConnectionID ModuleHandle;
	#endif
#endif
//--------------------------------------------------------------------------- 
using namespace ksd;
using namespace std;
//---------------------------------------------------------------------------  
void ECannotLoadSymbol::report(ostream& output) const
{
	// call parent output
	::ksd::exception::report(output);

	// show extra stuff
	output << "Unable to load symbol \"" << GetSymbol() << "\"";
	output << "from shared library \"" << GetFilename() << "\"";

	// flush output
	output << flush;
}
//---------------------------------------------------------------------------  
TSharedLibrary::TSharedLibrary(const char* name)
{
	// MAC: how do we tell it that an sl is not opened??
	Instance = NULL;
	Open(name);
}
//---------------------------------------------------------------------------  
TSharedLibrary::TSharedLibrary()
{	
	// MAC: how do we tell it that an sl is not opened??
	Instance = NULL;
}
//---------------------------------------------------------------------------  
TSharedLibrary::~TSharedLibrary()
{
	if(Instance) Close();
}
//--------------------------------------------------------------------------- 
void TSharedLibrary::Open(const char* _name)
{
#ifdef KSD_ENABLE_SHARED_LIBRARY_SUPPORT

	// MAC: How do you check that it is open??
	// if it is already open to another library, close first
	if(Instance != NULL) {
		Close();
	}

	// load library
#if defined(USE_LTDL)
	if((Instance = (void*)lt_dlopenext(_name)) == NULL) {
		// TODO: do something with lt_dlerror()
		throw EFileError("Shared Library", "Cannot open library", _name);
	}
#elif defined(macintosh)
    // MAC: Open shared library...
    throw EUnsupported("Shared Library", "No macintosh support for Open() !");
#else
    #error Cannot open a library on an unsupported platform.
#endif	

	// successful load - so set name
    Name = _name;

#else
	throw EUnsupported("SharedLibrary", "LibKSD was compiled without shared library support");
#endif
}
//---------------------------------------------------------------------------  
bool TSharedLibrary::HasSymbol(const char* _name)
{
	void* temp;

	// This is quite the little hack but it works without a lot of code 
	// duplication.  I am not very attached to it though, and if someone
	// can come up with a better idea, I'd be happy to replace it!
	try {
		GetSymbol(&temp, _name);
	} catch(ECannotLoadSymbol& e) {
		return false;
	}

	return true;
}
//--------------------------------------------------------------------------- 
void TSharedLibrary::GetSymbol(void** sym, const char* _name)
{
#ifdef KSD_ENABLE_SHARED_LIBRARY_SUPPORT

	// MAC: how do you check that it is opened??
	if(!Instance) { // if no library is opened!
		throw ENotInitialized("Shared Library", "Trying to load a symbol from an unopen shared library");
	}

	// get addr and deal with errors
#if defined(USE_LTDL)
	*sym = (void*)(lt_dlsym((ModuleHandle)Instance, _name));
	if(*sym == NULL) {
		// do something with lt_dlerror()
		throw ECannotLoadSymbol("Unable to retreive symbol", Name, _name);
	}
#elif defined(macintosh)
	// MAC: Load symbol ...
	throw EUnsupported("Shared Library", "No macintosh support for GetSymbol() !");
#else
	#error Cannot load a symbol on an unsupported platform.
#endif

#else	
	throw EUnsupported("SharedLibrary", "LibKSD was compiled without shared library support");
#endif
}
//--------------------------------------------------------------------------- 
void TSharedLibrary::Close()
{
#ifdef KSD_ENABLE_SHARED_LIBRARY_SUPPORT

	// MAC: this can't be done this way on MacOs
	if(!Instance) {
		// if not open can't close!
		throw ENotInitialized("Shared Library", "Trying to close an un-opened library");
	}

#if defined(USE_LTDL) 
	lt_dlclose((ModuleHandle)Instance);
	Instance = NULL;
#elif defined(macintosh)
	CloseConnection(&Instance);
	// MAC: set Instance to NULL somehow?? Mark as unloaded!
#else
	#error Cannot close a library on an unsupported platform.
#endif

#else	
	throw EUnsupported("SharedLibrary", "LibKSD was compiled without shared library support");
#endif
}
//--------------------------------------------------------------------------- 


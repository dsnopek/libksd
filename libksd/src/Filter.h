//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//	
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		TFilter, TPixelFilter, TGroupFilter --
//					TFadeFilter, TBrightnessFilter, TBlurFilter, TWaterFilter
//	
//	Purpose:		An abstraction of an image filter.
//	
//	Checked-in by:	$Author$
//					$Date$
//--------------------------------------------------------------------------- 
#ifndef _KSD_FilterH_
#define _KSD_FilterH_
//--------------------------------------------------------------------------- 
#include "Surface.h"
#include "fixed.h"
#include "Thread.h"
#include "export_ksd.h"
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma interface
#endif
//--------------------------------------------------------------------------- 
namespace ksd {
//--------------------------------------------------------------------------- 
// Filter heirarchy
//--------------------------------------------------------------------------- 
KSD_EXPORT_CLASS class TImageFilter {
public:
	TImageFilter() { max_c = 0; }

	void Process(TSurface& surface) { 
		max_c = Init(surface); 
		DoProcess(surface);
	}

	// a filter maximum is the min number of times the filter needs to be run,
	// on the same image, until the filter no longer changes it, assuming the 
	// image is in a state diametrically opposed to the desired image state
	// (-- NOTE --) Unless you are working on the framework, you will NEVER
	// have to use this...
	int GetMax() { return max_c; }
protected:
	virtual void DoProcess(TSurface& surface) = 0;
	virtual int  Init(const TSurface&) { return -1; };
private:
	int max_c;
};
//--------------------------------------------------------------------------- 
KSD_EXPORT_CLASS class TFilterThread : public TThread {
public:
	TFilterThread(TImageFilter* _filter, bool _owner = true);
	~TFilterThread();

	void Process(TSurface& _surface, bool _once = true);
protected:
	TImageFilter* Filter;
	TSurface* Surface;
	bool Owner, Once;

	void RunThread();
};
//--------------------------------------------------------------------------- 
KSD_EXPORT_CLASS class TPixelFilter : public TImageFilter {
protected:
	void 	DoProcess(TSurface& surface);
	virtual void Filter(TColor& pixel, bool& Changed) = 0;
};
//--------------------------------------------------------------------------- 
KSD_EXPORT_CLASS class TGroup4Filter {
public:
	void DoProcess(TSurface& surface);
protected:
	virtual void Filter(TColor& pixel, TColor& TopLeft, TColor& TopRight, 
						TColor& BottomLeft, TColor& BottomRight, bool& Changed) = 0;
};
//--------------------------------------------------------------------------- 
// Some common filters
//--------------------------------------------------------------------------- 
KSD_EXPORT_CLASS class TFadeFilter : public TPixelFilter {
public:
	fixed Fade;
	TColor Color;
protected:
	int  Init(const TSurface&);
	void Filter(TColor& pixel, bool& Changed);

	fixed factor;
	Uint8 bound;
};
//---------------------------------------------------------------------------
KSD_EXPORT_CLASS class TBrightnessFilter : public TPixelFilter {
public:
	fixed Brightness;
protected:
	int  Init(const TSurface&);
	void Filter(TColor& pixel, bool& Changed);

	bool FadeBlack;
	fixed factor;
	Uint8 bound;
};
//---------------------------------------------------------------------------
}; // namespace ksd
//---------------------------------------------------------------------------
#endif


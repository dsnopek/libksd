/*
  libuta - a C++ widget library based on SDL (Simple Direct Layer)
  Copyright (C) 1999  Karsten Laux

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Library General Public
  License as published by the Free Software Foundation; either
  version 2 of the License, or (at your option) any later version.
  
  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Library General Public License for more details.
  
  You should have received a copy of the GNU Library General Public
  License along with this library; if not, write to the
  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
  Boston, MA  02111-1307, SA.
*/
//--------------------------------------------------------------------------- 
//	$Header$
//	
//	Author:			Karsten Laux
//	Revised by:		David Snopek
//	Version:		$Revision$
//	
//	Checked-in by:	$Author$
//
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma implementation
#endif
//--------------------------------------------------------------------------- 
#define KSD_BUILD_LIB
#include <SDL/SDL_video.h>
#include "Color.h"

using namespace ksd;
//--------------------------------------------------------------------------- 
TColor::TColor()
{	
	Red = Green = Blue = Alpha = 0;
}
//--------------------------------------------------------------------------- 
/*TColor::TColor(SDL_Color* _color)
{
	Red = _color->r;
	Green = _color->g;
	Blue = _color->b;
	Alpha = _color->unused;
}*/
//--------------------------------------------------------------------------- 
TColor::TColor(Uint8 _red, Uint8 _green, Uint8 _blue, Uint8 _alpha)
{
	Red = _red;
	Green = _green;
	Blue = _blue;
	Alpha = _alpha;
}
//--------------------------------------------------------------------------- 
/*void TColor::CopyTo(SDL_Color* _color) const
{
	_color->r = Red;
	_color->g = Green;
	_color->b = Blue;
	_color->unused = Alpha;
}*/
//--------------------------------------------------------------------------- 
TColor ksd::TColor::transparent = TColor(0,0,0,255);
TColor ksd::TColor::black = TColor(0,0,0);
TColor ksd::TColor::darkgrey = TColor(8,8,8);
TColor ksd::TColor::darkgreen = TColor(32,64,32);
TColor ksd::TColor::darkslateblue = TColor(32,0,64);
TColor ksd::TColor::lightblue = TColor(128,0,255);
TColor ksd::TColor::grey = TColor(64,64,64);
TColor ksd::TColor::lightgrey = TColor(128,128,128);
TColor ksd::TColor::red = TColor(128,32,32);
TColor ksd::TColor::blue = TColor(32,32,128);
TColor ksd::TColor::green = TColor(32,128,32);
TColor ksd::TColor::seagreen = TColor(16,64,64);
TColor ksd::TColor::white = TColor(254,254,254);
TColor ksd::TColor::snow = TColor(255,250,250);
TColor ksd::TColor::GhostWhite = TColor(248,248,255);
TColor ksd::TColor::WhiteSmoke = TColor(245,245,245);
TColor ksd::TColor::gainsboro = TColor(220,220,220);
TColor ksd::TColor::FloralWhite = TColor(255,250,240);
TColor ksd::TColor::OldLace = TColor(253,245,230);
TColor ksd::TColor::linen = TColor(250,240,230);
TColor ksd::TColor::AntiqueWhite = TColor(250,235,215);
TColor ksd::TColor::PapayaWhip = TColor(255,239,213);
TColor ksd::TColor::BlanchedAlmond = TColor(255,235,205);
TColor ksd::TColor::bisque = TColor(255,228,196);
TColor ksd::TColor::PeachPuff = TColor(255,218,185);
TColor ksd::TColor::NavajoWhite = TColor(255,222,173);
TColor ksd::TColor::moccasin = TColor(255,228,181);
TColor ksd::TColor::cornsilk = TColor(255,248,220);
TColor ksd::TColor::ivory = TColor(255,255,240);
TColor ksd::TColor::LemonChiffon = TColor(255,250,205);
TColor ksd::TColor::seashell = TColor(255,245,238);
TColor ksd::TColor::honeydew = TColor(240,255,240);
TColor ksd::TColor::MintCream = TColor(245,255,250);
TColor ksd::TColor::azure = TColor(240,255,255);
TColor ksd::TColor::AliceBlue = TColor(240,248,255);
TColor ksd::TColor::lavender = TColor(230,230,250);
TColor ksd::TColor::LavenderBlush = TColor(255,240,245);
TColor ksd::TColor::MistyRose = TColor(255,228,225);
TColor ksd::TColor::DarkSlateGray = TColor(47,79,79);
TColor ksd::TColor::DarkSlateGrey = TColor(47,79,79);
TColor ksd::TColor::DimGray = TColor(105,105,105);
TColor ksd::TColor::DimGrey = TColor(105,105,105);
TColor ksd::TColor::SlateGray = TColor(112,128,144);
TColor ksd::TColor::SlateGrey = TColor(112,128,144);
TColor ksd::TColor::LightSlateGray = TColor(119,136,153);
TColor ksd::TColor::LightSlateGrey = TColor(119,136,153);
TColor ksd::TColor::gray = TColor(190,190,190);
TColor ksd::TColor::LightGrey = TColor(211,211,211);
TColor ksd::TColor::LightGray = TColor(211,211,211);
TColor ksd::TColor::MidnightBlue = TColor(25,25,112);
TColor ksd::TColor::navy = TColor(0,0,128);
TColor ksd::TColor::NavyBlue = TColor(0,0,128);
TColor ksd::TColor::CornflowerBlue = TColor(100,149,237);
TColor ksd::TColor::DarkSlateBlue = TColor(72,61,139);
TColor ksd::TColor::SlateBlue = TColor(106,90,205);
TColor ksd::TColor::MediumSlateBlue = TColor(123,104,238);
TColor ksd::TColor::LightSlateBlue = TColor(132,112,255);
TColor ksd::TColor::MediumBlue = TColor(0,0,205);
TColor ksd::TColor::RoyalBlue = TColor(65,105,225);
TColor ksd::TColor::DodgerBlue = TColor(30,144,255);
TColor ksd::TColor::DeepSkyBlue = TColor(0,191,255);
TColor ksd::TColor::SkyBlue = TColor(135,206,235);
TColor ksd::TColor::LightSkyBlue = TColor(135,206,250);
TColor ksd::TColor::SteelBlue = TColor(70,130,180);
TColor ksd::TColor::LightSteelBlue = TColor(176,196,222);
TColor ksd::TColor::LightBlue = TColor(173,216,230);
TColor ksd::TColor::PowderBlue = TColor(176,224,230);
TColor ksd::TColor::PaleTurquoise = TColor(175,238,238);
TColor ksd::TColor::DarkTurquoise = TColor(0,206,209);
TColor ksd::TColor::MediumTurquoise = TColor(72,209,204);
TColor ksd::TColor::turquoise = TColor(64,224,208);
TColor ksd::TColor::cyan = TColor(0,255,255);
TColor ksd::TColor::LightCyan = TColor(224,255,255);
TColor ksd::TColor::CadetBlue = TColor(95,158,160);
TColor ksd::TColor::MediumAquamarine = TColor(102,205,170);
TColor ksd::TColor::aquamarine = TColor(127,255,212);
TColor ksd::TColor::DarkGreen = TColor(0,100,0);
TColor ksd::TColor::DarkOliveGreen = TColor(85,107,47);
TColor ksd::TColor::DarkSeaGreen = TColor(143,188,143);
TColor ksd::TColor::SeaGreen = TColor(46,139,87);
TColor ksd::TColor::MediumSeaGreen = TColor(60,179,113);
TColor ksd::TColor::LightSeaGreen = TColor(32,178,170);
TColor ksd::TColor::PaleGreen = TColor(152,251,152);
TColor ksd::TColor::SpringGreen = TColor(0,255,127);
TColor ksd::TColor::LawnGreen = TColor(124,252,0);
TColor ksd::TColor::chartreuse = TColor(127,255,0);
TColor ksd::TColor::MediumSpringGreen = TColor(0,250,154);
TColor ksd::TColor::GreenYellow = TColor(173,255,47);
TColor ksd::TColor::LimeGreen = TColor(50,205,50);
TColor ksd::TColor::YellowGreen = TColor(154,205,50);
TColor ksd::TColor::ForestGreen = TColor(34,139,34);
TColor ksd::TColor::OliveDrab = TColor(107,142,35);
TColor ksd::TColor::DarkKhaki = TColor(189,183,107);
TColor ksd::TColor::khaki = TColor(240,230,140);
TColor ksd::TColor::PaleGoldenrod = TColor(238,232,170);
TColor ksd::TColor::LightGoldenrodYellow = TColor(250,250,210);
TColor ksd::TColor::LightYellow = TColor(255,255,224);
TColor ksd::TColor::yellow = TColor(255,255,0);
TColor ksd::TColor::gold = TColor(255,215,0);
TColor ksd::TColor::LightGoldenrod = TColor(238,221,130);
TColor ksd::TColor::goldenrod = TColor(218,165,32);
TColor ksd::TColor::DarkGoldenrod = TColor(184,134,11);
TColor ksd::TColor::RosyBrown = TColor(188,143,143);
TColor ksd::TColor::IndianRed = TColor(205,92,92);
TColor ksd::TColor::SaddleBrown = TColor(139,69,19);
TColor ksd::TColor::sienna = TColor(160,82,45);
TColor ksd::TColor::peru = TColor(205,133,63);
TColor ksd::TColor::burlywood = TColor(222,184,135);
TColor ksd::TColor::beige = TColor(245,245,220);
TColor ksd::TColor::wheat = TColor(245,222,179);
TColor ksd::TColor::SandyBrown = TColor(244,164,96);
TColor ksd::TColor::tan = TColor(210,180,140);
TColor ksd::TColor::chocolate = TColor(210,105,30);
TColor ksd::TColor::firebrick = TColor(178,34,34);
TColor ksd::TColor::brown = TColor(165,42,42);
TColor ksd::TColor::DarkSalmon = TColor(233,150,122);
TColor ksd::TColor::salmon = TColor(250,128,114);
TColor ksd::TColor::LightSalmon = TColor(255,160,122);
TColor ksd::TColor::orange = TColor(255,165,0);
TColor ksd::TColor::DarkOrange = TColor(255,140,0);
TColor ksd::TColor::coral = TColor(255,127,80);
TColor ksd::TColor::LightCoral = TColor(240,128,128);
TColor ksd::TColor::tomato = TColor(255,99,71);
TColor ksd::TColor::OrangeRed = TColor(255,69,0);
TColor ksd::TColor::HotPink = TColor(255,105,180);
TColor ksd::TColor::DeepPink = TColor(255,20,147);
TColor ksd::TColor::pink = TColor(255,192,203);
TColor ksd::TColor::LightPink = TColor(255,182,193);
TColor ksd::TColor::PaleVioletRed = TColor(219,112,147);
TColor ksd::TColor::maroon = TColor(176,48,96);
TColor ksd::TColor::MediumVioletRed = TColor(199,21,133);
TColor ksd::TColor::VioletRed = TColor(208,32,144);
TColor ksd::TColor::magenta = TColor(255,0,255);
TColor ksd::TColor::violet = TColor(238,130,238);
TColor ksd::TColor::plum = TColor(221,160,221);
TColor ksd::TColor::orchid = TColor(218,112,214);
TColor ksd::TColor::MediumOrchid = TColor(186,85,211);
TColor ksd::TColor::DarkOrchid = TColor(153,50,204);
TColor ksd::TColor::DarkViolet = TColor(148,0,211);
TColor ksd::TColor::BlueViolet = TColor(138,43,226);
TColor ksd::TColor::purple = TColor(160,32,240);
TColor ksd::TColor::MediumPurple = TColor(147,112,219);
TColor ksd::TColor::thistle = TColor(216,191,216);
TColor ksd::TColor::snow1 = TColor(255,250,250);
TColor ksd::TColor::snow2 = TColor(238,233,233);
TColor ksd::TColor::snow3 = TColor(205,201,201);
TColor ksd::TColor::snow4 = TColor(139,137,137);
TColor ksd::TColor::seashell1 = TColor(255,245,238);
TColor ksd::TColor::seashell2 = TColor(238,229,222);
TColor ksd::TColor::seashell3 = TColor(205,197,191);
TColor ksd::TColor::seashell4 = TColor(139,134,130);
TColor ksd::TColor::AntiqueWhite1 = TColor(255,239,219);
TColor ksd::TColor::AntiqueWhite2 = TColor(238,223,204);
TColor ksd::TColor::AntiqueWhite3 = TColor(205,192,176);
TColor ksd::TColor::AntiqueWhite4 = TColor(139,131,120);
TColor ksd::TColor::bisque1 = TColor(255,228,196);
TColor ksd::TColor::bisque2 = TColor(238,213,183);
TColor ksd::TColor::bisque3 = TColor(205,183,158);
TColor ksd::TColor::bisque4 = TColor(139,125,107);
TColor ksd::TColor::PeachPuff1 = TColor(255,218,185);
TColor ksd::TColor::PeachPuff2 = TColor(238,203,173);
TColor ksd::TColor::PeachPuff3 = TColor(205,175,149);
TColor ksd::TColor::PeachPuff4 = TColor(139,119,101);
TColor ksd::TColor::NavajoWhite1 = TColor(255,222,173);
TColor ksd::TColor::NavajoWhite2 = TColor(238,207,161);
TColor ksd::TColor::NavajoWhite3 = TColor(205,179,139);
TColor ksd::TColor::NavajoWhite4 = TColor(139,121,94);
TColor ksd::TColor::LemonChiffon1 = TColor(255,250,205);
TColor ksd::TColor::LemonChiffon2 = TColor(238,233,191);
TColor ksd::TColor::LemonChiffon3 = TColor(205,201,165);
TColor ksd::TColor::LemonChiffon4 = TColor(139,137,112);
TColor ksd::TColor::cornsilk1 = TColor(255,248,220);
TColor ksd::TColor::cornsilk2 = TColor(238,232,205);
TColor ksd::TColor::cornsilk3 = TColor(205,200,177);
TColor ksd::TColor::cornsilk4 = TColor(139,136,120);
TColor ksd::TColor::ivory1 = TColor(255,255,240);
TColor ksd::TColor::ivory2 = TColor(238,238,224);
TColor ksd::TColor::ivory3 = TColor(205,205,193);
TColor ksd::TColor::ivory4 = TColor(139,139,131);
TColor ksd::TColor::honeydew1 = TColor(240,255,240);
TColor ksd::TColor::honeydew2 = TColor(224,238,224);
TColor ksd::TColor::honeydew3 = TColor(193,205,193);
TColor ksd::TColor::honeydew4 = TColor(131,139,131);
TColor ksd::TColor::LavenderBlush1 = TColor(255,240,245);
TColor ksd::TColor::LavenderBlush2 = TColor(238,224,229);
TColor ksd::TColor::LavenderBlush3 = TColor(205,193,197);
TColor ksd::TColor::LavenderBlush4 = TColor(139,131,134);
TColor ksd::TColor::MistyRose1 = TColor(255,228,225);
TColor ksd::TColor::MistyRose2 = TColor(238,213,210);
TColor ksd::TColor::MistyRose3 = TColor(205,183,181);
TColor ksd::TColor::MistyRose4 = TColor(139,125,123);
TColor ksd::TColor::azure1 = TColor(240,255,255);
TColor ksd::TColor::azure2 = TColor(224,238,238);
TColor ksd::TColor::azure3 = TColor(193,205,205);
TColor ksd::TColor::azure4 = TColor(131,139,139);
TColor ksd::TColor::SlateBlue1 = TColor(131,111,255);
TColor ksd::TColor::SlateBlue2 = TColor(122,103,238);
TColor ksd::TColor::SlateBlue3 = TColor(105,89,205);
TColor ksd::TColor::SlateBlue4 = TColor(71,60,139);
TColor ksd::TColor::RoyalBlue1 = TColor(72,118,255);
TColor ksd::TColor::RoyalBlue2 = TColor(67,110,238);
TColor ksd::TColor::RoyalBlue3 = TColor(58,95,205);
TColor ksd::TColor::RoyalBlue4 = TColor(39,64,139);
TColor ksd::TColor::blue1 = TColor(0,0,255);
TColor ksd::TColor::blue2 = TColor(0,0,238);
TColor ksd::TColor::blue3 = TColor(0,0,205);
TColor ksd::TColor::blue4 = TColor(0,0,139);
TColor ksd::TColor::DodgerBlue1 = TColor(30,144,255);
TColor ksd::TColor::DodgerBlue2 = TColor(28,134,238);
TColor ksd::TColor::DodgerBlue3 = TColor(24,116,205);
TColor ksd::TColor::DodgerBlue4 = TColor(16,78,139);
TColor ksd::TColor::SteelBlue1 = TColor(99,184,255);
TColor ksd::TColor::SteelBlue2 = TColor(92,172,238);
TColor ksd::TColor::SteelBlue3 = TColor(79,148,205);
TColor ksd::TColor::SteelBlue4 = TColor(54,100,139);
TColor ksd::TColor::DeepSkyBlue1 = TColor(0,191,255);
TColor ksd::TColor::DeepSkyBlue2 = TColor(0,178,238);
TColor ksd::TColor::DeepSkyBlue3 = TColor(0,154,205);
TColor ksd::TColor::DeepSkyBlue4 = TColor(0,104,139);
TColor ksd::TColor::SkyBlue1 = TColor(135,206,255);
TColor ksd::TColor::SkyBlue2 = TColor(126,192,238);
TColor ksd::TColor::SkyBlue3 = TColor(108,166,205);
TColor ksd::TColor::SkyBlue4 = TColor(74,112,139);
TColor ksd::TColor::LightSkyBlue1 = TColor(176,226,255);
TColor ksd::TColor::LightSkyBlue2 = TColor(164,211,238);
TColor ksd::TColor::LightSkyBlue3 = TColor(141,182,205);
TColor ksd::TColor::LightSkyBlue4 = TColor(96,123,139);
TColor ksd::TColor::SlateGray1 = TColor(198,226,255);
TColor ksd::TColor::SlateGray2 = TColor(185,211,238);
TColor ksd::TColor::SlateGray3 = TColor(159,182,205);
TColor ksd::TColor::SlateGray4 = TColor(108,123,139);
TColor ksd::TColor::LightSteelBlue1 = TColor(202,225,255);
TColor ksd::TColor::LightSteelBlue2 = TColor(188,210,238);
TColor ksd::TColor::LightSteelBlue3 = TColor(162,181,205);
TColor ksd::TColor::LightSteelBlue4 = TColor(110,123,139);
TColor ksd::TColor::LightBlue1 = TColor(191,239,255);
TColor ksd::TColor::LightBlue2 = TColor(178,223,238);
TColor ksd::TColor::LightBlue3 = TColor(154,192,205);
TColor ksd::TColor::LightBlue4 = TColor(104,131,139);
TColor ksd::TColor::LightCyan1 = TColor(224,255,255);
TColor ksd::TColor::LightCyan2 = TColor(209,238,238);
TColor ksd::TColor::LightCyan3 = TColor(180,205,205);
TColor ksd::TColor::LightCyan4 = TColor(122,139,139);
TColor ksd::TColor::PaleTurquoise1 = TColor(187,255,255);
TColor ksd::TColor::PaleTurquoise2 = TColor(174,238,238);
TColor ksd::TColor::PaleTurquoise3 = TColor(150,205,205);
TColor ksd::TColor::PaleTurquoise4 = TColor(102,139,139);
TColor ksd::TColor::CadetBlue1 = TColor(152,245,255);
TColor ksd::TColor::CadetBlue2 = TColor(142,229,238);
TColor ksd::TColor::CadetBlue3 = TColor(122,197,205);
TColor ksd::TColor::CadetBlue4 = TColor(83,134,139);
TColor ksd::TColor::turquoise1 = TColor(0,245,255);
TColor ksd::TColor::turquoise2 = TColor(0,229,238);
TColor ksd::TColor::turquoise3 = TColor(0,197,205);
TColor ksd::TColor::turquoise4 = TColor(0,134,139);
TColor ksd::TColor::cyan1 = TColor(0,255,255);
TColor ksd::TColor::cyan2 = TColor(0,238,238);
TColor ksd::TColor::cyan3 = TColor(0,205,205);
TColor ksd::TColor::cyan4 = TColor(0,139,139);
TColor ksd::TColor::DarkSlateGray1 = TColor(151,255,255);
TColor ksd::TColor::DarkSlateGray2 = TColor(141,238,238);
TColor ksd::TColor::DarkSlateGray3 = TColor(121,205,205);
TColor ksd::TColor::DarkSlateGray4 = TColor(82,139,139);
TColor ksd::TColor::aquamarine1 = TColor(127,255,212);
TColor ksd::TColor::aquamarine2 = TColor(118,238,198);
TColor ksd::TColor::aquamarine3 = TColor(102,205,170);
TColor ksd::TColor::aquamarine4 = TColor(69,139,116);
TColor ksd::TColor::DarkSeaGreen1 = TColor(193,255,193);
TColor ksd::TColor::DarkSeaGreen2 = TColor(180,238,180);
TColor ksd::TColor::DarkSeaGreen3 = TColor(155,205,155);
TColor ksd::TColor::DarkSeaGreen4 = TColor(105,139,105);
TColor ksd::TColor::SeaGreen1 = TColor(84,255,159);
TColor ksd::TColor::SeaGreen2 = TColor(78,238,148);
TColor ksd::TColor::SeaGreen3 = TColor(67,205,128);
TColor ksd::TColor::SeaGreen4 = TColor(46,139,87);
TColor ksd::TColor::PaleGreen1 = TColor(154,255,154);
TColor ksd::TColor::PaleGreen2 = TColor(144,238,144);
TColor ksd::TColor::PaleGreen3 = TColor(124,205,124);
TColor ksd::TColor::PaleGreen4 = TColor(84,139,84);
TColor ksd::TColor::SpringGreen1 = TColor(0,255,127);
TColor ksd::TColor::SpringGreen2 = TColor(0,238,118);
TColor ksd::TColor::SpringGreen3 = TColor(0,205,102);
TColor ksd::TColor::SpringGreen4 = TColor(0,139,69);
TColor ksd::TColor::green1 = TColor(0,255,0);
TColor ksd::TColor::green2 = TColor(0,238,0);
TColor ksd::TColor::green3 = TColor(0,205,0);
TColor ksd::TColor::green4 = TColor(0,139,0);
TColor ksd::TColor::chartreuse1 = TColor(127,255,0);
TColor ksd::TColor::chartreuse2 = TColor(118,238,0);
TColor ksd::TColor::chartreuse3 = TColor(102,205,0);
TColor ksd::TColor::chartreuse4 = TColor(69,139,0);
TColor ksd::TColor::OliveDrab1 = TColor(192,255,62);
TColor ksd::TColor::OliveDrab2 = TColor(179,238,58);
TColor ksd::TColor::OliveDrab3 = TColor(154,205,50);
TColor ksd::TColor::OliveDrab4 = TColor(105,139,34);
TColor ksd::TColor::DarkOliveGreen1 = TColor(202,255,112);
TColor ksd::TColor::DarkOliveGreen2 = TColor(188,238,104);
TColor ksd::TColor::DarkOliveGreen3 = TColor(162,205,90);
TColor ksd::TColor::DarkOliveGreen4 = TColor(110,139,61);
TColor ksd::TColor::khaki1 = TColor(255,246,143);
TColor ksd::TColor::khaki2 = TColor(238,230,133);
TColor ksd::TColor::khaki3 = TColor(205,198,115);
TColor ksd::TColor::khaki4 = TColor(139,134,78);
TColor ksd::TColor::LightGoldenrod1 = TColor(255,236,139);
TColor ksd::TColor::LightGoldenrod2 = TColor(238,220,130);
TColor ksd::TColor::LightGoldenrod3 = TColor(205,190,112);
TColor ksd::TColor::LightGoldenrod4 = TColor(139,129,76);
TColor ksd::TColor::LightYellow1 = TColor(255,255,224);
TColor ksd::TColor::LightYellow2 = TColor(238,238,209);
TColor ksd::TColor::LightYellow3 = TColor(205,205,180);
TColor ksd::TColor::LightYellow4 = TColor(139,139,122);
TColor ksd::TColor::yellow1 = TColor(255,255,0);
TColor ksd::TColor::yellow2 = TColor(238,238,0);
TColor ksd::TColor::yellow3 = TColor(205,205,0);
TColor ksd::TColor::yellow4 = TColor(139,139,0);
TColor ksd::TColor::gold1 = TColor(255,215,0);
TColor ksd::TColor::gold2 = TColor(238,201,0);
TColor ksd::TColor::gold3 = TColor(205,173,0);
TColor ksd::TColor::gold4 = TColor(139,117,0);
TColor ksd::TColor::goldenrod1 = TColor(255,193,37);
TColor ksd::TColor::goldenrod2 = TColor(238,180,34);
TColor ksd::TColor::goldenrod3 = TColor(205,155,29);
TColor ksd::TColor::goldenrod4 = TColor(139,105,20);
TColor ksd::TColor::DarkGoldenrod1 = TColor(255,185,15);
TColor ksd::TColor::DarkGoldenrod2 = TColor(238,173,14);
TColor ksd::TColor::DarkGoldenrod3 = TColor(205,149,12);
TColor ksd::TColor::DarkGoldenrod4 = TColor(139,101,8);
TColor ksd::TColor::RosyBrown1 = TColor(255,193,193);
TColor ksd::TColor::RosyBrown2 = TColor(238,180,180);
TColor ksd::TColor::RosyBrown3 = TColor(205,155,155);
TColor ksd::TColor::RosyBrown4 = TColor(139,105,105);
TColor ksd::TColor::IndianRed1 = TColor(255,106,106);
TColor ksd::TColor::IndianRed2 = TColor(238,99,99);
TColor ksd::TColor::IndianRed3 = TColor(205,85,85);
TColor ksd::TColor::IndianRed4 = TColor(139,58,58);
TColor ksd::TColor::sienna1 = TColor(255,130,71);
TColor ksd::TColor::sienna2 = TColor(238,121,66);
TColor ksd::TColor::sienna3 = TColor(205,104,57);
TColor ksd::TColor::sienna4 = TColor(139,71,38);
TColor ksd::TColor::burlywood1 = TColor(255,211,155);
TColor ksd::TColor::burlywood2 = TColor(238,197,145);
TColor ksd::TColor::burlywood3 = TColor(205,170,125);
TColor ksd::TColor::burlywood4 = TColor(139,115,85);
TColor ksd::TColor::wheat1 = TColor(255,231,186);
TColor ksd::TColor::wheat2 = TColor(238,216,174);
TColor ksd::TColor::wheat3 = TColor(205,186,150);
TColor ksd::TColor::wheat4 = TColor(139,126,102);
TColor ksd::TColor::tan1 = TColor(255,165,79);
TColor ksd::TColor::tan2 = TColor(238,154,73);
TColor ksd::TColor::tan3 = TColor(205,133,63);
TColor ksd::TColor::tan4 = TColor(139,90,43);
TColor ksd::TColor::chocolate1 = TColor(255,127,36);
TColor ksd::TColor::chocolate2 = TColor(238,118,33);
TColor ksd::TColor::chocolate3 = TColor(205,102,29);
TColor ksd::TColor::chocolate4 = TColor(139,69,19);
TColor ksd::TColor::firebrick1 = TColor(255,48,48);
TColor ksd::TColor::firebrick2 = TColor(238,44,44);
TColor ksd::TColor::firebrick3 = TColor(205,38,38);
TColor ksd::TColor::firebrick4 = TColor(139,26,26);
TColor ksd::TColor::brown1 = TColor(255,64,64);
TColor ksd::TColor::brown2 = TColor(238,59,59);
TColor ksd::TColor::brown3 = TColor(205,51,51);
TColor ksd::TColor::brown4 = TColor(139,35,35);
TColor ksd::TColor::salmon1 = TColor(255,140,105);
TColor ksd::TColor::salmon2 = TColor(238,130,98);
TColor ksd::TColor::salmon3 = TColor(205,112,84);
TColor ksd::TColor::salmon4 = TColor(139,76,57);
TColor ksd::TColor::LightSalmon1 = TColor(255,160,122);
TColor ksd::TColor::LightSalmon2 = TColor(238,149,114);
TColor ksd::TColor::LightSalmon3 = TColor(205,129,98);
TColor ksd::TColor::LightSalmon4 = TColor(139,87,66);
TColor ksd::TColor::orange1 = TColor(255,165,0);
TColor ksd::TColor::orange2 = TColor(238,154,0);
TColor ksd::TColor::orange3 = TColor(205,133,0);
TColor ksd::TColor::orange4 = TColor(139,90,0);
TColor ksd::TColor::DarkOrange1 = TColor(255,127,0);
TColor ksd::TColor::DarkOrange2 = TColor(238,118,0);
TColor ksd::TColor::DarkOrange3 = TColor(205,102,0);
TColor ksd::TColor::DarkOrange4 = TColor(139,69,0);
TColor ksd::TColor::coral1 = TColor(255,114,86);
TColor ksd::TColor::coral2 = TColor(238,106,80);
TColor ksd::TColor::coral3 = TColor(205,91,69);
TColor ksd::TColor::coral4 = TColor(139,62,47);
TColor ksd::TColor::tomato1 = TColor(255,99,71);
TColor ksd::TColor::tomato2 = TColor(238,92,66);
TColor ksd::TColor::tomato3 = TColor(205,79,57);
TColor ksd::TColor::tomato4 = TColor(139,54,38);
TColor ksd::TColor::OrangeRed1 = TColor(255,69,0);
TColor ksd::TColor::OrangeRed2 = TColor(238,64,0);
TColor ksd::TColor::OrangeRed3 = TColor(205,55,0);
TColor ksd::TColor::OrangeRed4 = TColor(139,37,0);
TColor ksd::TColor::red1 = TColor(255,0,0);
TColor ksd::TColor::red2 = TColor(238,0,0);
TColor ksd::TColor::red3 = TColor(205,0,0);
TColor ksd::TColor::red4 = TColor(139,0,0);
TColor ksd::TColor::DeepPink1 = TColor(255,20,147);
TColor ksd::TColor::DeepPink2 = TColor(238,18,137);
TColor ksd::TColor::DeepPink3 = TColor(205,16,118);
TColor ksd::TColor::DeepPink4 = TColor(139,10,80);
TColor ksd::TColor::HotPink1 = TColor(255,110,180);
TColor ksd::TColor::HotPink2 = TColor(238,106,167);
TColor ksd::TColor::HotPink3 = TColor(205,96,144);
TColor ksd::TColor::HotPink4 = TColor(139,58,98);
TColor ksd::TColor::pink1 = TColor(255,181,197);
TColor ksd::TColor::pink2 = TColor(238,169,184);
TColor ksd::TColor::pink3 = TColor(205,145,158);
TColor ksd::TColor::pink4 = TColor(139,99,108);
TColor ksd::TColor::LightPink1 = TColor(255,174,185);
TColor ksd::TColor::LightPink2 = TColor(238,162,173);
TColor ksd::TColor::LightPink3 = TColor(205,140,149);
TColor ksd::TColor::LightPink4 = TColor(139,95,101);
TColor ksd::TColor::PaleVioletRed1 = TColor(255,130,171);
TColor ksd::TColor::PaleVioletRed2 = TColor(238,121,159);
TColor ksd::TColor::PaleVioletRed3 = TColor(205,104,137);
TColor ksd::TColor::PaleVioletRed4 = TColor(139,71,93);
TColor ksd::TColor::maroon1 = TColor(255,52,179);
TColor ksd::TColor::maroon2 = TColor(238,48,167);
TColor ksd::TColor::maroon3 = TColor(205,41,144);
TColor ksd::TColor::maroon4 = TColor(139,28,98);
TColor ksd::TColor::VioletRed1 = TColor(255,62,150);
TColor ksd::TColor::VioletRed2 = TColor(238,58,140);
TColor ksd::TColor::VioletRed3 = TColor(205,50,120);
TColor ksd::TColor::VioletRed4 = TColor(139,34,82);
TColor ksd::TColor::magenta1 = TColor(255,0,255);
TColor ksd::TColor::magenta2 = TColor(238,0,238);
TColor ksd::TColor::magenta3 = TColor(205,0,205);
TColor ksd::TColor::magenta4 = TColor(139,0,139);
TColor ksd::TColor::orchid1 = TColor(255,131,250);
TColor ksd::TColor::orchid2 = TColor(238,122,233);
TColor ksd::TColor::orchid3 = TColor(205,105,201);
TColor ksd::TColor::orchid4 = TColor(139,71,137);
TColor ksd::TColor::plum1 = TColor(255,187,255);
TColor ksd::TColor::plum2 = TColor(238,174,238);
TColor ksd::TColor::plum3 = TColor(205,150,205);
TColor ksd::TColor::plum4 = TColor(139,102,139);
TColor ksd::TColor::MediumOrchid1 = TColor(224,102,255);
TColor ksd::TColor::MediumOrchid2 = TColor(209,95,238);
TColor ksd::TColor::MediumOrchid3 = TColor(180,82,205);
TColor ksd::TColor::MediumOrchid4 = TColor(122,55,139);
TColor ksd::TColor::DarkOrchid1 = TColor(191,62,255);
TColor ksd::TColor::DarkOrchid2 = TColor(178,58,238);
TColor ksd::TColor::DarkOrchid3 = TColor(154,50,205);
TColor ksd::TColor::DarkOrchid4 = TColor(104,34,139);
TColor ksd::TColor::purple1 = TColor(155,48,255);
TColor ksd::TColor::purple2 = TColor(145,44,238);
TColor ksd::TColor::purple3 = TColor(125,38,205);
TColor ksd::TColor::purple4 = TColor(85,26,139);
TColor ksd::TColor::MediumPurple1 = TColor(171,130,255);
TColor ksd::TColor::MediumPurple2 = TColor(159,121,238);
TColor ksd::TColor::MediumPurple3 = TColor(137,104,205);
TColor ksd::TColor::MediumPurple4 = TColor(93,71,139);
TColor ksd::TColor::thistle1 = TColor(255,225,255);
TColor ksd::TColor::thistle2 = TColor(238,210,238);
TColor ksd::TColor::thistle3 = TColor(205,181,205);
TColor ksd::TColor::thistle4 = TColor(139,123,139);
TColor ksd::TColor::gray0 = TColor(0,0,0);
TColor ksd::TColor::grey0 = TColor(0,0,0);
TColor ksd::TColor::gray1 = TColor(3,3,3);
TColor ksd::TColor::grey1 = TColor(3,3,3);
TColor ksd::TColor::gray2 = TColor(5,5,5);
TColor ksd::TColor::grey2 = TColor(5,5,5);
TColor ksd::TColor::gray3 = TColor(8,8,8);
TColor ksd::TColor::grey3 = TColor(8,8,8);
TColor ksd::TColor::	gray4 = TColor(10,10,10);
TColor ksd::TColor::	grey4 = TColor(10,10,10);
TColor ksd::TColor::	gray5 = TColor(13,13,13);
TColor ksd::TColor::	grey5 = TColor(13,13,13);
TColor ksd::TColor::	gray6 = TColor(15,15,15);
TColor ksd::TColor::	grey6 = TColor(15,15,15);
TColor ksd::TColor::	gray7 = TColor(18,18,18);
TColor ksd::TColor::	grey7 = TColor(18,18,18);
TColor ksd::TColor::	gray8 = TColor(20,20,20);
TColor ksd::TColor::	grey8 = TColor(20,20,20);
TColor ksd::TColor::	gray9 = TColor(23,23,23);
TColor ksd::TColor::	grey9 = TColor(23,23,23);
TColor ksd::TColor::	gray10 = TColor(26,26,26);
TColor ksd::TColor::	grey10 = TColor(26,26,26);
TColor ksd::TColor::	gray11 = TColor(28,28,28);
TColor ksd::TColor::	grey11 = TColor(28,28,28);
TColor ksd::TColor::	gray12 = TColor(31,31,31);
TColor ksd::TColor::	grey12 = TColor(31,31,31);
TColor ksd::TColor::	gray13 = TColor(33,33,33);
TColor ksd::TColor::	grey13 = TColor(33,33,33);
TColor ksd::TColor::	gray14 = TColor(36,36,36);
TColor ksd::TColor::	grey14 = TColor(36,36,36);
TColor ksd::TColor::	gray15 = TColor(38,38,38);
TColor ksd::TColor::	grey15 = TColor(38,38,38);
TColor ksd::TColor::	gray16 = TColor(41,41,41);
TColor ksd::TColor::	grey16 = TColor(41,41,41);
TColor ksd::TColor::	gray17 = TColor(43,43,43);
TColor ksd::TColor::	grey17 = TColor(43,43,43);
TColor ksd::TColor::	gray18 = TColor(46,46,46);
TColor ksd::TColor::	grey18 = TColor(46,46,46);
TColor ksd::TColor::	gray19 = TColor(48,48,48);
TColor ksd::TColor::	grey19 = TColor(48,48,48);
TColor ksd::TColor::	gray20 = TColor(51,51,51);
TColor ksd::TColor::	grey20 = TColor(51,51,51);
TColor ksd::TColor::	gray21 = TColor(54,54,54);
TColor ksd::TColor::	grey21 = TColor(54,54,54);
TColor ksd::TColor::	gray22 = TColor(56,56,56);
TColor ksd::TColor::	grey22 = TColor(56,56,56);
TColor ksd::TColor::	gray23 = TColor(59,59,59);
TColor ksd::TColor::	grey23 = TColor(59,59,59);
TColor ksd::TColor::	gray24 = TColor(61,61,61);
TColor ksd::TColor::	grey24 = TColor(61,61,61);
TColor ksd::TColor::	gray25 = TColor(64,64,64);
TColor ksd::TColor::	grey25 = TColor(64,64,64);
TColor ksd::TColor::	gray26 = TColor(66,66,66);
TColor ksd::TColor::	grey26 = TColor(66,66,66);
TColor ksd::TColor::	gray27 = TColor(69,69,69);
TColor ksd::TColor::	grey27 = TColor(69,69,69);
TColor ksd::TColor::	gray28 = TColor(71,71,71);
TColor ksd::TColor::	grey28 = TColor(71,71,71);
TColor ksd::TColor::	gray29 = TColor(74,74,74);
TColor ksd::TColor::	grey29 = TColor(74,74,74);
TColor ksd::TColor::	gray30 = TColor(77,77,77);
TColor ksd::TColor::	grey30 = TColor(77,77,77);
TColor ksd::TColor::	gray31 = TColor(79,79,79);
TColor ksd::TColor::	grey31 = TColor(79,79,79);
TColor ksd::TColor::	gray32 = TColor(82,82,82);
TColor ksd::TColor::	grey32 = TColor(82,82,82);
TColor ksd::TColor::	gray33 = TColor(84,84,84);
TColor ksd::TColor::	grey33 = TColor(84,84,84);
TColor ksd::TColor::	gray34 = TColor(87,87,87);
TColor ksd::TColor::	grey34 = TColor(87,87,87);
TColor ksd::TColor::	gray35 = TColor(89,89,89);
TColor ksd::TColor::	grey35 = TColor(89,89,89);
TColor ksd::TColor::	gray36 = TColor(92,92,92);
TColor ksd::TColor::	grey36 = TColor(92,92,92);
TColor ksd::TColor::	gray37 = TColor(94,94,94);
TColor ksd::TColor::	grey37 = TColor(94,94,94);
TColor ksd::TColor::	gray38 = TColor(97,97,97);
TColor ksd::TColor::	grey38 = TColor(97,97,97);
TColor ksd::TColor::	gray39 = TColor(99,99,99);
TColor ksd::TColor::	grey39 = TColor(99,99,99);
TColor ksd::TColor::	gray40 = TColor(102,102,102);
TColor ksd::TColor::	grey40 = TColor(102,102,102);
TColor ksd::TColor::	gray41 = TColor(105,105,105);
TColor ksd::TColor::	grey41 = TColor(105,105,105);
TColor ksd::TColor::	gray42 = TColor(107,107,107);
TColor ksd::TColor::	grey42 = TColor(107,107,107);
TColor ksd::TColor::	gray43 = TColor(110,110,110);
TColor ksd::TColor::	grey43 = TColor(110,110,110);
TColor ksd::TColor::	gray44 = TColor(112,112,112);
TColor ksd::TColor::	grey44 = TColor(112,112,112);
TColor ksd::TColor::	gray45 = TColor(115,115,115);
TColor ksd::TColor::	grey45 = TColor(115,115,115);
TColor ksd::TColor::	gray46 = TColor(117,117,117);
TColor ksd::TColor::	grey46 = TColor(117,117,117);
TColor ksd::TColor::	gray47 = TColor(120,120,120);
TColor ksd::TColor::	grey47 = TColor(120,120,120);
TColor ksd::TColor::	gray48 = TColor(122,122,122);
TColor ksd::TColor::	grey48 = TColor(122,122,122);
TColor ksd::TColor::	gray49 = TColor(125,125,125);
TColor ksd::TColor::	grey49 = TColor(125,125,125);
TColor ksd::TColor::	gray50 = TColor(127,127,127);
TColor ksd::TColor::	grey50 = TColor(127,127,127);
TColor ksd::TColor::	gray51 = TColor(130,130,130);
TColor ksd::TColor::	grey51 = TColor(130,130,130);
TColor ksd::TColor::	gray52 = TColor(133,133,133);
TColor ksd::TColor::	grey52 = TColor(133,133,133);
TColor ksd::TColor::	gray53 = TColor(135,135,135);
TColor ksd::TColor::	grey53 = TColor(135,135,135);
TColor ksd::TColor::	gray54 = TColor(138,138,138);
TColor ksd::TColor::	grey54 = TColor(138,138,138);
TColor ksd::TColor::	gray55 = TColor(140,140,140);
TColor ksd::TColor::	grey55 = TColor(140,140,140);
TColor ksd::TColor::	gray56 = TColor(143,143,143);
TColor ksd::TColor::	grey56 = TColor(143,143,143);
TColor ksd::TColor::	gray57 = TColor(145,145,145);
TColor ksd::TColor::	grey57 = TColor(145,145,145);
TColor ksd::TColor::	gray58 = TColor(148,148,148);
TColor ksd::TColor::	grey58 = TColor(148,148,148);
TColor ksd::TColor::	gray59 = TColor(150,150,150);
TColor ksd::TColor::	grey59 = TColor(150,150,150);
TColor ksd::TColor::	gray60 = TColor(153,153,153);
TColor ksd::TColor::	grey60 = TColor(153,153,153);
TColor ksd::TColor::	gray61 = TColor(156,156,156);
TColor ksd::TColor::	grey61 = TColor(156,156,156);
TColor ksd::TColor::	gray62 = TColor(158,158,158);
TColor ksd::TColor::	grey62 = TColor(158,158,158);
TColor ksd::TColor::	gray63 = TColor(161,161,161);
TColor ksd::TColor::	grey63 = TColor(161,161,161);
TColor ksd::TColor::	gray64 = TColor(163,163,163);
TColor ksd::TColor::	grey64 = TColor(163,163,163);
TColor ksd::TColor::	gray65 = TColor(166,166,166);
TColor ksd::TColor::	grey65 = TColor(166,166,166);
TColor ksd::TColor::	gray66 = TColor(168,168,168);
TColor ksd::TColor::	grey66 = TColor(168,168,168);
TColor ksd::TColor::	gray67 = TColor(171,171,171);
TColor ksd::TColor::	grey67 = TColor(171,171,171);
TColor ksd::TColor::	gray68 = TColor(173,173,173);
TColor ksd::TColor::	grey68 = TColor(173,173,173);
TColor ksd::TColor::	gray69 = TColor(176,176,176);
TColor ksd::TColor::	grey69 = TColor(176,176,176);
TColor ksd::TColor::	gray70 = TColor(179,179,179);
TColor ksd::TColor::	grey70 = TColor(179,179,179);
TColor ksd::TColor::	gray71 = TColor(181,181,181);
TColor ksd::TColor::	grey71 = TColor(181,181,181);
TColor ksd::TColor::	gray72 = TColor(184,184,184);
TColor ksd::TColor::	grey72 = TColor(184,184,184);
TColor ksd::TColor::	gray73 = TColor(186,186,186);
TColor ksd::TColor::	grey73 = TColor(186,186,186);
TColor ksd::TColor::	gray74 = TColor(189,189,189);
TColor ksd::TColor::	grey74 = TColor(189,189,189);
TColor ksd::TColor::	gray75 = TColor(191,191,191);
TColor ksd::TColor::	grey75 = TColor(191,191,191);
TColor ksd::TColor::	gray76 = TColor(194,194,194);
TColor ksd::TColor::	grey76 = TColor(194,194,194);
TColor ksd::TColor::	gray77 = TColor(196,196,196);
TColor ksd::TColor::	grey77 = TColor(196,196,196);
TColor ksd::TColor::	gray78 = TColor(199,199,199);
TColor ksd::TColor::	grey78 = TColor(199,199,199);
TColor ksd::TColor::	gray79 = TColor(201,201,201);
TColor ksd::TColor::	grey79 = TColor(201,201,201);
TColor ksd::TColor::	gray80 = TColor(204,204,204);
TColor ksd::TColor::	grey80 = TColor(204,204,204);
TColor ksd::TColor::	gray81 = TColor(207,207,207);
TColor ksd::TColor::	grey81 = TColor(207,207,207);
TColor ksd::TColor::	gray82 = TColor(209,209,209);
TColor ksd::TColor::	grey82 = TColor(209,209,209);
TColor ksd::TColor::	gray83 = TColor(212,212,212);
TColor ksd::TColor::	grey83 = TColor(212,212,212);
TColor ksd::TColor::	gray84 = TColor(214,214,214);
TColor ksd::TColor::	grey84 = TColor(214,214,214);
TColor ksd::TColor::	gray85 = TColor(217,217,217);
TColor ksd::TColor::	grey85 = TColor(217,217,217);
TColor ksd::TColor::	gray86 = TColor(219,219,219);
TColor ksd::TColor::	grey86 = TColor(219,219,219);
TColor ksd::TColor::	gray87 = TColor(222,222,222);
TColor ksd::TColor::	grey87 = TColor(222,222,222);
TColor ksd::TColor::	gray88 = TColor(224,224,224);
TColor ksd::TColor::	grey88 = TColor(224,224,224);
TColor ksd::TColor::	gray89 = TColor(227,227,227);
TColor ksd::TColor::	grey89 = TColor(227,227,227);
TColor ksd::TColor::	gray90 = TColor(229,229,229);
TColor ksd::TColor::	grey90 = TColor(229,229,229);
TColor ksd::TColor::	gray91 = TColor(232,232,232);
TColor ksd::TColor::	grey91 = TColor(232,232,232);
TColor ksd::TColor::	gray92 = TColor(235,235,235);
TColor ksd::TColor::	grey92 = TColor(235,235,235);
TColor ksd::TColor::	gray93 = TColor(237,237,237);
TColor ksd::TColor::	grey93 = TColor(237,237,237);
TColor ksd::TColor::	gray94 = TColor(240,240,240);
TColor ksd::TColor::	grey94 = TColor(240,240,240);
TColor ksd::TColor::	gray95 = TColor(242,242,242);
TColor ksd::TColor::	grey95 = TColor(242,242,242);
TColor ksd::TColor::	gray96 = TColor(245,245,245);
TColor ksd::TColor::	grey96 = TColor(245,245,245);
TColor ksd::TColor::	gray97 = TColor(247,247,247);
TColor ksd::TColor::	grey97 = TColor(247,247,247);
TColor ksd::TColor::	gray98 = TColor(250,250,250);
TColor ksd::TColor::	grey98 = TColor(250,250,250);
TColor ksd::TColor::	gray99 = TColor(252,252,252);
TColor ksd::TColor::	grey99 = TColor(252,252,252);
TColor ksd::TColor::	gray100 = TColor(255,255,255);
TColor ksd::TColor::	grey100 = TColor(255,255,255);
TColor ksd::TColor::DarkGrey = TColor(169,169,169);
TColor ksd::TColor::DarkGray = TColor(169,169,169);
TColor ksd::TColor::DarkBlue = TColor(0,0,139);
TColor ksd::TColor::DarkCyan = TColor(0,139,139);
TColor ksd::TColor::DarkMagenta = TColor(139,0,139);
TColor ksd::TColor::DarkRed = TColor(139,0,0);
TColor ksd::TColor::LightGreen = TColor(144,238,144);
//--------------------------------------------------------------------------- 


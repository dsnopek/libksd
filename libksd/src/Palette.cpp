//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	$Header$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Checked-in by:	$Author$
//
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma implementation
#endif
//--------------------------------------------------------------------------- 
#define KSD_BUILD_LIB
#include <SDL/SDL_video.h>
#include "Palette.h"
#include "backends/sdl/sdlconv.h"

using namespace ksd;
//--------------------------------------------------------------------------- 
TPalette::TPalette()
{
	Changed = false;
}
//--------------------------------------------------------------------------- 
void TPalette::SetColor(int index, const TColor& V)
{
	if(index < 0) return; // avoids problems that can occur width signed to unsigned
						  // conversion
	if((unsigned int)index >= Data.size()) {
		Data[index] = V;
		Changed = true;
	}
}
//--------------------------------------------------------------------------- 
void TPalette::AddColor(const TColor& V)
{
	if(Data.size() < 255) { // only allow 255 colors
		Data.push_back(V);
		Changed = true;
	}
}
//--------------------------------------------------------------------------- 
void TPalette::SetSize(unsigned int size) 
{
	if(size == Data.size()) return; // don't change size if it is already there

	if(size >= 0 && size <= 255) {
		//if(size > Data.size()) {
			while(size > Data.size()) {
				Data.push_back(TColor());
			}
		//} else {
			while(size < Data.size()) {
				Data.pop_back();
			}
		//}

		Changed = true;
	}
}
//--------------------------------------------------------------------------- 
void TPalette::ApplyPalette(SDL_Surface* Surface)
{
	if(Surface && Changed) { // only do if changes were made
		unsigned int Count = Data.size();
		SDL_Color* Pal;

		Pal = new SDL_Color[Count];
		for(unsigned int i = 0; i < Count; i++) {
			Pal[i] = KSDtoSDL(Data[i]);
			//Data[i].CopyTo(Pal[i]);
		}

		SDL_SetColors(Surface, Pal, 0, Count);
		Changed = false;

		delete[] Pal;
	}
}
//--------------------------------------------------------------------------- 
void TPalette::GatherPalette(SDL_Surface* Surface)
{
	if(Surface) {
		SDL_Palette* Pal = Surface->format->palette;

		if(Pal) {
			Data.clear();
			for(int i = 0; i < Pal->ncolors; i++) {
				Data.push_back(SDLtoKSD(Pal->colors[i]));
			}
		}

		// maybe changes were made but never applied, then an image was loaded.
		// Changes need to be set to false so same data isn't uselessly written back
		Changed = false;
	}
}
//--------------------------------------------------------------------------- 
int TPalette::FindBestMatch(const TColor& Color) const
{
	unsigned int bestindex = 0, i;
	long int match, bestmatch = 
		(Color.Red - Data[bestindex].Red)*(Color.Red - Data[bestindex].Red) +
		(Color.Green - Data[bestindex].Green)*(Color.Green - Data[bestindex].Green) +
		(Color.Blue - Data[bestindex].Blue)*(Color.Blue - Data[bestindex].Blue);

	//serach for best matching Data entry
	for(i=0; i < Data.size(); i++) {
		match = 
			(Color.Red - Data[i].Red)*(Color.Red - Data[i].Red) +
			(Color.Green - Data[i].Green)*(Color.Green - Data[i].Green) +
			(Color.Blue - Data[i].Blue)*(Color.Blue - Data[i].Blue);

		if(match < bestmatch) {
			bestmatch = match;
			bestindex = i;
		}

		if(match == 0)
			break;
	}

	return i;
}
//--------------------------------------------------------------------------- 


//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	$Header$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Checked-in by:	$Author$
//
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma implementation
#endif
//--------------------------------------------------------------------------- 
#define KSD_BUILD_LIB
#include "TrueTypeFont.h"
#include "exception.h"
#include "backends/sdl/sdlconv.h"

using namespace ksd;
using namespace std;
//---------------------------------------------------------------------------  
TTF_Font* ksd::CreateTTF(const TFontAttr& attr, const char* filename)
{
	TTF_Font* Font = TTF_OpenFont(filename, attr.Size);

	if(Font) {
		TTF_SetFontStyle(Font, attr.Style);
	}

	return Font;
}
//--------------------------------------------------------------------------- 
void TTrueTypeFont::Open(const std::string& filename)
{
	DB.SetFilename(filename.c_str());
	TTF_Font* Font = DB.GetFont(DefaultFontAttr);

	// check for ttf errors
	if(!Font) { 
		throw EFileError("Font", "Unable to open TTF file", filename);
	}
}
//---------------------------------------------------------------------------  
int TTrueTypeFont::GetHeight(const TFontAttr& attr)
{
	TTF_Font* Font = DB.GetFont(attr);

	return Font ? TTF_FontHeight(Font) : 0;
}
//---------------------------------------------------------------------------  
int TTrueTypeFont::GetAscent(const TFontAttr& attr)
{
	TTF_Font* Font = DB.GetFont(attr);

	return Font ? TTF_FontAscent(Font) : 0;
}
//---------------------------------------------------------------------------  
int TTrueTypeFont::GetDescent(const TFontAttr& attr)
{
	TTF_Font* Font = DB.GetFont(attr);

	return Font ? TTF_FontDescent(Font) : 0;
}
//---------------------------------------------------------------------------  
int TTrueTypeFont::GetLineSkip(const TFontAttr& attr)
{
	TTF_Font* Font = DB.GetFont(attr);

	return Font ? TTF_FontLineSkip(Font) : 0;
}
//---------------------------------------------------------------------------  
void TTrueTypeFont::GetTextSize(const std::string& text, const TFontAttr& attr, 
								int& width, int& height)
{
	TTF_Font* Font = DB.GetFont(attr);

	if(Font) {
		TTF_SizeText(Font, text.c_str(), &width, &height);
	} else {
		width = 0;
		height = 0;
	}
}
//---------------------------------------------------------------------------  
void TTrueTypeFont::DrawString(SDL_Surface* Surface, int X, int Y, 
							   const std::string& text, TRect* ClipRect,
							   const TFontAttr& attr)
{
	TTF_Font* Font;
	TFontAttr key;

	// color doesn't matter to TTF fonts so all are stored in the default
	// color of black
	key = attr;
	key.Color = TColor::black;
	
	Font = DB.GetFont(key);
	
	if(Font) {
		SDL_Surface* Buffer;
		SDL_Rect dest_rect, src_rect;
		
		// draw text to buffer
		if((Buffer = TTF_RenderText_Solid(Font, text.c_str(), KSDtoSDL(attr.Color))) == NULL)
			return;

		// set up rects
		src_rect.x = 0;
		src_rect.y = 0;
		src_rect.w = Buffer->w;
		src_rect.h = Buffer->h;
		dest_rect.x = X;
		dest_rect.y = Y;

		if(ClipRect) {
			Uint16 surf_right, surf_bottom, clip_right, clip_bottom, temp;
			
			clip_right = ClipRect->GetRight();
			clip_bottom = ClipRect->GetBottom();
			surf_right = dest_rect.x + src_rect.w;
			surf_bottom = dest_rect.y + src_rect.h;

			// perform culling (this isn't very effecient culling since the font
			// is already drawn to the buffer -- ttf font clipping won't be very fast)
			if(dest_rect.x > clip_right) return;
			if(dest_rect.y > clip_bottom) return;
			if(surf_bottom < ClipRect->GetTop()) return;
			if(surf_right < ClipRect->GetLeft()) return;

			if(dest_rect.x < ClipRect->GetLeft()) {
				temp = (ClipRect->GetLeft() - dest_rect.x);
				src_rect.x += temp;
				src_rect.w -= temp;
				dest_rect.x += temp;
			}
			if(dest_rect.y < ClipRect->GetTop()) {
				temp = (ClipRect->GetTop() - dest_rect.y);
				src_rect.y += temp;
				src_rect.h -= temp;
				dest_rect.y += temp;
			}
			if(surf_right > clip_right) {
				src_rect.w -= (surf_right - clip_right);
			}
			if(surf_bottom > clip_bottom) {
				src_rect.h -= (surf_bottom - clip_bottom);
			}
		}
		
		// draw buffer to surface
		SDL_BlitSurface(Buffer, &src_rect, Surface, &dest_rect);
		SDL_FreeSurface(Buffer);
	}
}
//---------------------------------------------------------------------------  


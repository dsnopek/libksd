//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//	
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		TWidget
//	
//	Purpose:		A widget.
//	
//	Checked-in by:	$Author$
//					$Date$
//--------------------------------------------------------------------------- 
#ifndef _KSD_WidgetH_
#define _KSD_WidgetH_
//--------------------------------------------------------------------------- 
#include <sigc++/sigc++.h>
#include <list>
#include <string>
#include <bitset>
#include "WidgetTree.h"
#include "Surface.h"
#include "SubSurface.h"
#include "Keyboard.h"
#include "Joystick.h"
#include "Mouse.h"
#include "Canvas.h"
#include "export_ksd.h"
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma interface
#endif
//--------------------------------------------------------------------------- 
namespace ksd {

class TImage;
class TWidget;
//---------------------------------------------------------------------------  
// TODO: need a way to notify the widget that its font has changed since that 
// affects the way the caption is drawn and a few special case controls as 
// well as updating the child whose fonts mirror their parents...
//--------------------------------------------------------------------------- 
/** The base class for all widgets.
 ** @see Widgets
 */
KSD_EXPORT_CLASS class TWidget : public SigC::Object { 
	friend class TApplication;
	friend class TWidgetTree;
private:
	// widget flags
	enum {
		ALLOW_CHILDREN = 0x00,
		USE_PARENT_BACKGROUND,
		USE_PARENT_COLOR,
		USE_PARENT_FONT,
		
		DRAW_BACKGROUND_COLOR,
		DRAW_BACKGROUND_IMAGE,
		STRETCH_BACKGROUND,
		TILE_BACKGROUND,

		KEY_PREVIEW,
		DRAW_ON_DEMAND,

		FLAGS_MAX
	};

	// draw flags
	enum DrawFlag {
		dfNO_DRAW = 0,  
		dfUPDATE = 1,
		dfREPAINT = 2
	};
public:
	/** Create a widget with the parent passed.  A widgets parent is the widget 
	 ** that it resides on.  Parent-less widgets CAN exist.  But here's the confusing 
	 ** part: the really distinction is not the presense of a parent widget but 
	 ** of a parent surface.
	 ** 
	 ** @see GetParentSurface
	 */
	TWidget(TWidget* _parent);
	virtual ~TWidget();

	/** @name Widget event signals
	 ** @memo Signals called to annouce specific widget events.
	 */
	//@{
	/// Called when the mouse enters this widget.
	SigC::Signal1<void, TWidget*> 									OnEnter;
	/// Called when the mouse exits this widget.
	SigC::Signal1<void, TWidget*> 									OnExit;
	/// Called when a key down event is given to this widget.
	SigC::Signal2<void, TWidget*, const TKey&> 						OnKeyDown;
	/// Called when a key up event is given to this widget.
	SigC::Signal2<void, TWidget*, const TKey&> 						OnKeyUp;
	/// Called when the mouse is moved inside of this widget.
	SigC::Signal5<void, TWidget*, TPoint2D, int, int, TMouseButton>	OnMouseMove;
	/// Called when a mouse button is pressed inside of this widget.
	SigC::Signal4<void, TWidget*, int, int, TMouseButton> 			OnMouseDown;
	/// Called when a mouse button is released inside of this widget.
	SigC::Signal4<void, TWidget*, int, int, TMouseButton>			OnMouseUp;
	/// Called when this widget receives a joystick axis move event.
	SigC::Signal4<void, TWidget*, Uint8, Uint8, Sint16>				OnJoyAxisMove;
	/// Called when this widget receives a jotstick ball move event.
	SigC::Signal5<void, TWidget*, Uint8, Uint8, Sint16, Sint16>		OnJoyBallMove;
	/// Called when this widget receives a joytsick hat move event.
	SigC::Signal4<void, TWidget*, Uint8, Uint8, TJoyHatPosition>	OnJoyHatMove;
	/// Called when this widget receives a joystick button press.
	SigC::Signal3<void, TWidget*, Uint8, Uint8>						OnJoyDown;
	/// Called when this widget receives a joystick button release.
	SigC::Signal3<void, TWidget*, Uint8, Uint8>						OnJoyUp;
	/// Called when this widget is resized.
	SigC::Signal3<bool, TWidget*, int, int>							OnResize;
	/// Called when this widget is shown (made visible).
	SigC::Signal1<void, TWidget*>									OnShow;
	/// Called when this widget is hidden (made invisible).
	SigC::Signal1<void, TWidget*>									OnHide;
	//@}

	/** @name Do-Event Functions
	 ** @memo Emits signal, and calls built handler.
	 */
	//@{
	///
	inline void DoEnter();
	///
	inline void DoExit();
	///
	inline void DoKeyDown(const TKey& key);
	///
	inline void DoKeyUp(const TKey& key);
	///
	inline void DoMouseMove(int X, int Y, int RelX, int RelY, TMouseButton button);
	///
	inline void DoMouseDown(int X, int Y, TMouseButton button);
	///
	inline void DoMouseUp(int X, int Y, TMouseButton button);
	///
	inline void DoJoyAxisMove(Uint8 joy, Uint8 axis, Sint16 value);
	///
	inline void DoJoyBallMove(Uint8 joy, Uint8 ball, Sint16 X, Sint16 Y);
	///
	inline void DoJoyHatMove(Uint8 joy, Uint8 hat, TJoyHatPosition pos);
	///
	inline void DoJoyDown(Uint8 joy, Uint8 button);
	///
	inline void DoJoyUp(Uint8 joy, Uint8 button);
	//@}
	
	/// Returns the parent of this widget.
	TWidget* GetParent() { return Parent; }

	// widget location/dimensions
	/** Returns the widget caption.  A widget caption is used to apply a label
	 ** to the widget.  How this label is actually used is up to the widget
	 ** author.
	 */
	virtual std::string	GetCaption() const = 0;
	
	/** Returns the \textit{actual} widget width, including all of its decorations.
	 ** @see GetClientWidth
	 */
	virtual int GetWidth() const = 0;

	/** Returns the \textit{actual} widget height, including all of its decorations.
	 ** @see GetClientHeight
	 */
	virtual int GetHeight() const = 0;

	/** Returns the position of this widget, relative to its parent.
	 */
	virtual TPoint2D GetPosition() const = 0;

	/** Assigns the widget caption.  A widget caption is used to apply a label
	 ** to the widget.  How this label is actually used is up to the widget
	 ** author.
	 */
	virtual void SetCaption(const std::string& _caption) = 0;

	/** @name void SetPosition(...)
	 ** @memo Sets the widgets position, relative to its parent.
	 */
	//@{
	/// Set using two integers
	virtual void SetPosition(int X, int Y) = 0;
	/// Set using a TPoint2D object.
	virtual void SetPosition(const TPoint2D& p) { SetPosition(p.X, p.Y); }
	//@}

	/** Takes a global point, and returns true of that point is inside of the widget.
	 ** @see Widget Positions
	 */
	bool IsInside(int X, int Y);

	/** Takes a ``local" point, and returns true if that point is inside of the
	 ** widget.  IsInsideLocal has somewhat cyptic behavior.  Its checks the point 
	 ** relative to the widget direct parent NOT a \textit{true} local point like 
	 ** the name might suggest.
	 ** @see Widget Positions
	 */
	bool IsInsideLocal(int X, int Y);

	/** Takes a global position and transforms it into a local position.
	 ** @see Widget Positions
	 */
	void MakeLocal(int& X, int& Y);

	/** Takes a local position and transforms it into a global position.
	 ** @see Widget Positions
	 */
	void MakeGlobal(int& X, int& Y);

	/** Returns the actual child position relative to this widgets origin.  This
	 ** is necessary when the widget uses a seperate client surface because the
	 ** child widget may actually get drawn offset from the position it has stored.
	 ** When overriding this function, you can assume that it is only called with
	 ** valid children of this widget.
	 */
	virtual TPoint2D GetChildPosition(TWidget* child) const = 0;

	/** Resizes the widget to the specified size.
	 */
	virtual void Resize(int w, int h) = 0;
	
	// widget flags -- these should always be configurable
	/** @name Public widget flag "sets"
	 ** @see Widget Flags
	 ** @memo These are widget flags that should always be configurable.
	 */
	//@{
	/** When set to true this widget will use its parent's background parameters
	 ** instead of its custom settings.  A widgets background parameters include:
	 ** background color, background image, stretch background, and tile 
	 ** background.  
	 ** 
	 ** \textbf{Default: true} \textit{(except for TApplication)}
	 ** 
	 ** @see Inheriting Parent Attributes
	 */
	void SetUseParentBackground(bool _upb) { 
		flags.set(USE_PARENT_BACKGROUND, _upb); 
		NeedsRepaint(); 
		MarkAllAsChanged();
	}
	
	/** When set to true, the color set by \Ref{SetBackgroundColor} is used to 
	 ** clear the buffer.  The buffer is cleared on Repaint, when ClearBuffer is 
	 ** called or on Update if AutoClearBuffer is set to true.
	 ** 
	 ** \textbf{Default: true}
	 ** 
	 ** @see GetDrawBackgroundColor, SetAutoClearBuffer, GetAutoClearBuffer
	 */
	void SetDrawBackgroundColor(bool _dbc) { 
		flags.set(DRAW_BACKGROUND_COLOR, _dbc); 
		NeedsRepaint(); 
		MarkAllAsChanged();
	}
	
	/** When set to true, the image set by \Ref{SetBackgroundImage} is used to
	 ** clear the buffer.  The buffer is cleared on Repaint, when ClearBuffer is 
	 ** called or on Update if AutoClearBuffer is set to true.  The way the 
	 ** image is drawn (tiled or stretched) is controlled by the functions
	 ** \Ref{SetStretchBackground} and \Ref{SetTileBackground}.  If 
	 ** \Ref{SetDrawBackgroundColor} is set to true, the the background color
	 ** is drawn first.
	 ** 
	 ** \textbf{Default: false}
	 ** 
	 ** @see GetDrawBackgroundImage
	 */
	void SetDrawBackgroundImage(bool _dbi) { 
		flags.set(DRAW_BACKGROUND_IMAGE, _dbi); 
		NeedsRepaint();
		MarkAllAsChanged(); 
	}
	
	/** When set to true and a background image is used, it will stretch the image
	 ** to fill the widget background.  Incompatable with SetTileBackground.
	 ** 
	 ** \textbf{Default: false}
	 ** 
	 ** @see GetStretchBackground, SetDrawBackgroundImage
	 */
	void SetStretchBackground(bool _sb) { 
		flags.set(STRETCH_BACKGROUND, _sb); 
		NeedsRepaint(); 
		MarkAllAsChanged();
	}
	
	/** When set to true and a background image is used, it will tile the image
	 ** to fill the widget background.  Incompatable with SetStretchBackground.
	 ** 
	 ** \textbf{Default: false}
	 ** 
	 ** @see GetTileBackground, SetDrawBackgroundImage
	 */
	void SetTileBackground(bool _tb) { 
		flags.set(TILE_BACKGROUND, _tb); 
		NeedsRepaint(); 
		MarkAllAsChanged();
	}
	
	/** When set to true, this widget will inherit the color of its parent.  Otherwise it
	 ** will use the color set by SetColor.
	 ** 
	 ** \textbf{Default: true}
	 ** 
	 ** @see Inheriting Parent Attributes
	 */
	void SetUseParentColor(bool _upc) { 
		flags.set(USE_PARENT_COLOR, _upc); 
		NeedsRepaint(); 
		MarkAllAsChanged();
	}
	
	/** When set to true, this widget will inherit the font of its parent.  Otherwise it
	 ** will use the font set by SetFont.
	 ** 
	 ** \textbf{Default: true}
	 ** 
	 ** @see Inheriting Parent Attributes
	 */
	void SetUseParentFont(bool _upf) { 
		flags.set(USE_PARENT_FONT, _upf); 
		NeedsRepaint(); 
		MarkAllAsChanged();
	}
	
	/** When set to true, this widget will recieve he keyboard input of its children
	 ** before it is set to them.
	 ** 
	 ** \textbf{Default: false}
	 ** 
	 ** @see GetKeyPreview
	 */
	void SetKeyPreview(bool _kp) { flags.set(KEY_PREVIEW, _kp); }
	//@}

	/** @name Widget flags "gets"
	 ** @memo Return the state of any widget flag.
	 */
	//@{
	/// Returns the value set by \Ref{SetAllowChildren}.
	bool GetAllowChildren()	const			{ return flags.test(ALLOW_CHILDREN); }
	/// Returns the value set by \Ref{SetUseParentBackground}.
	bool GetUseParentBackground() const		{ return flags.test(USE_PARENT_BACKGROUND); }
	/// Returns the value set by \Ref{SetDrawBackgroundColor}.
	bool GetDrawBackgroundColor() const		{ return flags.test(DRAW_BACKGROUND_COLOR); }
	/// Returns the value set by \Ref{SetDrawBackgroundImage}.
	bool GetDrawBackgroundImage() const		{ return flags.test(DRAW_BACKGROUND_IMAGE); }
	/// Returns the value set by \Ref{SetStretchBackground}.
	bool GetStretchBackground()	const		{ return flags.test(STRETCH_BACKGROUND); }
	/// Returns the value set by \Ref{SetTileBackground}.
	bool GetTileBackground() const			{ return flags.test(TILE_BACKGROUND); }
	/// Returns the value set by \Ref{SetUseParentColor}.
	bool GetUseParentColor() const			{ return flags.test(USE_PARENT_COLOR); }
	/// Returns the value set by \Ref{SetUseParentFont}.
	bool GetUseParentFont() const			{ return flags.test(USE_PARENT_FONT); }
	/// Returns the value set by \Ref{SetKeyPreview}.
	bool GetKeyPreview() const 				{ return flags.test(KEY_PREVIEW); }
	/// Returns the value set by \Ref{SetDrawOnDemand}.
	bool GetDrawOnDemand() const			{ return flags.test(DRAW_ON_DEMAND); }
	//@}

	/** Returns the surface that this widget outputs to.  All widgets must return
	 ** their valid main surface.
	 */
	virtual TSurface* GetSurface() = 0;

	/** The client surface that is used for the widget client area, or where the
	 ** children of this widget draw to by default.  A unique client surface is
	 ** not required, and the main surface can be used for both.
	 */
	virtual TSurface* GetClientSurface() = 0;

	/** Returns true if this widget draws to its parent's client surface
	 ** as a opposed to its main surface.  This value must be returned honestly
	 ** or a few widget operations will not work properly.
	 */
	virtual bool IsClientWidget() const = 0;
	
	/** @name Widget Attribute "Sets"
	 ** @memo Functions to set widget appearance attributes
	 */
	//@{
	/** Sets the widget color.  What the widget color is actually used for is up to
	 ** the widget writter.  Its use is not mandatory.  But calling SetColor, you
	 ** implicitly set UseParentColor to false.
	 ** 
	 ** \textbf{default: black}
	 */
	void SetColor(const TColor& _c) { Color = _c; SetUseParentColor(false); } 

	/** Sets the widget color.  What the widget color is actually used for is up to
	 ** the widget writter.  Its use is not mandatory.  But calling SetFont, you
	 ** implicitly set UseParentFont to false
	 */
	void SetFont(TFont* _f) { Font = _f; SetUseParentFont(false); }

	/** Sets the background color.  Used to fill the background of the widget.
	 ** @see SetDrawBackgroundColor, SetAutoClearBuffer
	 ** 
	 ** \textbf{default: black}
	 */
	void SetBackgroundColor(const TColor& _c) { BackgroundColor = _c; SetUseParentBackground(false); }

	/** Sets the background image.  Used to fill the background of the widget.
	 ** @see SetDrawBackgroundImage, SetStretchBackground, SetTileBackground, SetAutoClearBuffer
	 */
	void SetBackgroundImage(TImage* img) { BackgroundImage = img; SetUseParentBackground(false); }
	//@}

	/** @name Widget Attribute "Gets"
	 ** @memo Functions for getting widget appearance attributes.
	 */
	//@{
	/// Returns the widget color.
	TColor GetColor() const { 
		return Parent && GetUseParentColor() ? Parent->GetColor() : Color; 
	}
	/// Resturns the widget font.
	TFont* GetFont() const {
		return Parent && GetUseParentFont() ? Parent->GetFont() : Font;
	}
	/// Returns the widget background color.
	TColor GetBackgroundColor() const { 
		return Parent && GetUseParentBackground() ? Parent->GetBackgroundColor() : BackgroundColor; 
	}
	/// Returns the widget image.
	TImage* GetBackgroundImage() const { 
		return Parent && GetUseParentBackground() ? Parent->GetBackgroundImage() : BackgroundImage; 
	}
	//@}

	// functions for showing/hiding a widget (please note: only widgets with parents can hide!!)
	/// Makes the widget visible.
	void Show() { if(Parent) { Visible = true; VisibilityEvent(true); OnShow(this); Parent->NeedsRepaint(); } }
	/// Make the widget invisible.  Only widgets with parent's can use.
	void Hide() { if(Parent) { Visible = false; VisibilityEvent(false); OnHide(this); Parent->NeedsRepaint(); } }
	/// When true, show the widget; when false, hide the widget.
	void SetVisible(bool v) { if(v) Show(); else Hide(); }
	/// Returns true if widget is visible.
	bool IsVisible() const { return Visible; }

	// functions for changing draw order
	/** Moves the widget up one in the draw order.
	 */
	void MoveUp() {
		WidgetTree.MoveUp(WidgetTree.find_widget(this));
		NeedsRepaint();
	}
	/** Move the widget to the front of the draw order.
	 */
	void MoveToFront() {
		WidgetTree.MoveToFront(WidgetTree.find_widget(this));
		NeedsRepaint();
	}

	// functions for enabling/disabiling a widget
	/** Enables or disables a widget.  This \textit{usually} means that the 
	 ** widget will be greyed out and won't receive input, but its actual
	 ** interpretation is up to the widget writter.
	 */
	void SetEnabled(bool _enable) { 
		Enabled = _enable; 
		NeedsRepaint(); 
	}
	
	/** Enables the widget.
	 ** @see SetEnabled
	 */
	void Enable() { SetEnabled(true); }
	
	/** Disables the widget/
	 ** @see SetEnabled
	 */
	void Disable() { SetEnabled(false); }

	/** Returns true if the widget is enabled, false otherwise.
	 ** @see SetEnabled
	 */
	bool IsEnabled() const { return Enabled; }

	/** Marks a widget for repaint.  On the next draw cycle, the widgets
	 ** draw function will be called with REPAINT.  
	 ** 
	 ** @see Draw, DrawType, SetAutoClearBuffer
	 */
	void NeedsRepaint() { draw_flag = dfREPAINT; }

	/** Marks a widget for update.  On the next draw cycle, the widgets
	 ** draw function will be called with REPAINT.
	 ** @see Draw, DrawType
	 */
	void NeedsUpdate() { if(draw_flag < dfUPDATE) draw_flag = dfUPDATE; } 

	/** Marks a widget as changed.  Changed is reset to false after each 
	 ** draw cycle is AutoResetChanged is true.  How changed is actually 
	 ** used depends mostly on the widget writter.  The only time that 
	 ** Changed is used as a standard part of widget is when AutoSaveBuffer 
	 ** is set to true.  (\textit{NOTE: This function does not imply any
	 ** draw flag.  You must call either NeedsRepaint or NeedsUpdate to
	 ** actually draw the changes.})
	 */
	void MarkAsChanged() { Changed = true; }

	/** Works just like MarkAsChange except that it marks all the widget's
	 ** children as well.
	 ** 
	 ** @see MarkAsChanged
	 */
	void MarkAllAsChanged();

	/** Marks the widget as un-changed.
	 ** 
	 ** @see MarkAsChanged, MarkAllAsChanged
	 */
	void ResetChanged() { Changed = false; }

	/** Returns the Changed flag.
	 ** @see NeedsRepaint
	 */
	bool GetChanged() const { return Changed; }

	/** Returns true if this widget will accept input.
	 */
	virtual bool CanAcceptInput() const = 0;

	/** Returns true if this widget will accept mouse input.
	 */
	virtual bool CanAcceptMouseInput(int X, int Y) const = 0;
protected:
	/// Returns true if it got focus.  For use by children.  
	bool GrabKeyboardFocus();
	/// Returns true if it got focus.  For use by children.  
	bool GrabJoystickFocus();

	/// A flag describing what type of draw is requested.
	enum DrawType { 
		/** Only update the appearance of the widget.  This can be used to just update
		 ** the parts of the widget that actively change.  For example, drawing and 
		 ** undrawing a cursor to move it around the screen.
		 */
		UPDATE = 1,
		/** "Re-draw" the entire widget.
		 */
		REPAINT = 2
	};

	/** Returns the TCanvas associated with the widgets main surface.
	 **/
	TCanvas* GetCanvas() {
		Canvas.SetSurface(GetSurface());
		return &Canvas;
	}

	/** Draws the widget background according to the parameters set, effectively 
	 ** clearing the buffer.  Called on Repaint or on Update and Draw when
	 ** AutoClearBuffer is set to true.
	 ** 
	 ** @see SetBackgroundColor, SetBackgroundImage,
	 ** 	 SetUseBackgroundColor, SetUseBackgroundImage, SetStretchBackground,
	 ** 	 SetTileBackground
	 */
	void ClearBuffer();

	/** Recreates the buffer for this widget and all of its children using the
	 ** function RecreateBuffer.  This can be used when this widget needs to
	 ** create a different kind of surface for its buffer.  This kind of thing 
	 ** is common when you change the video backend.
	 */
	void RecreateAllBuffers();

	/** Draw the widget to the requested canvas.  The 
	 ** type of draw required will be passed as the second argument.  All widgets
	 ** must implement this method.  Returns true if anything has actually been 
	 ** drawn.  \textit{If the widget has in fact drawn to the screen but returns
	 ** false anyway, the part of the screen which was changed, may not be updated
	 ** (depending on specific settings and the output target)}.
	 ** 
	 ** @see DrawType
	 */
	virtual bool Draw(TCanvas*, DrawType dt) = 0;

	/** Update the internal state of the widget.  In a "\textit{coupled}" gameloop,
	 ** Cycle is always called before Draw.  All widgets must implement this
	 ** method.
	 ** 
	 ** @see Decoupled Game Loop
	 */
	virtual void Cycle() = 0;

	/** @name Widget Implementation Functions
	 ** @memo These functions define some basic behaviors for this particular
	 ** widget implementation.  They are meant to be defined for a whole class
	 ** of widgets and not redefined for each individual end widget.
	 */
	//@{
	/** Asks the widget to recreate its buffer(s).  This is necessary for when 
	 ** the video mode changes.
	 */
	virtual void RecreateBuffer() = 0;

	/** Called upon by DrawWidgetTree to handle actual widget drawing for each 
	 ** individual widget.  Should call the widgets Draw function.  This is meant
	 ** to define an overall policy for a \textsl{type} of widget, while leaving
	 ** Draw to be implemented only be the ultimate child class.
	 */
	virtual bool DrawWidget(DrawType mode) = 0;

	/** Copies the widget to its parent, if necessary.  Call RequiresUpdate() in
	 ** order to check if this is necessary.
	 */
	virtual void UpdateWidget() = 0;

	/** Returns true if this widget must be updated after draw.
	 */
	virtual bool RequiresUpdate() const = 0;
	//@}

	/** When set to true, this widget will not accept children.  When a widget
	 ** tries to use a widget as its parent that doesn't allow it, will have its
	 ** parent set to NULL.
	 ** 
	 ** \textbf{Default: true}
	 ** 
	 ** @see GetAllowChildren
	 */
	void SetAllowChildren(bool _ac)	{ flags.set(ALLOW_CHILDREN, _ac); }

	/** When set to true, the widget will only draw when explicitly told to.  In normal
	 ** operation, the each widget is called with UPDATE on every draw cycle.  With
	 ** draw on demand this is not so.
	 ** 
	 ** \textbf{Default: false}
	 ** 
	 ** @see GetDrawOnDemand
	 */
	void SetDrawOnDemand(bool _dod)	{ 
		flags.set(DRAW_ON_DEMAND, _dod); 
		if(!_dod) NeedsRepaint(); 
	}

	/** @name Built-in Event Functions
	 ** 
	 ** Override in child class to provided custom behaviors for a custom
	 ** widget type.
	 ** 
	 ** @memo Corresponding to events with names prepended by: On*
	 */
	//@{
	/// 
	virtual void Enter() { };
	///
	virtual void Exit() { };
	///
	virtual void KeyDown(const TKey& key) { };
	///
	virtual void KeyUp(const TKey& key) { };
	///
	virtual void MouseMove(int X, int Y, int RelX, int RelY, TMouseButton button) { };
	///
	virtual void MouseDown(int X, int Y, TMouseButton button) { };
	///
	virtual void MouseUp(int X, int Y, TMouseButton button) { };
	///
	virtual void JoyAxisMove(Uint8 joy, Uint8 axis, Sint16 value) { };
	///
	virtual void JoyBallMove(Uint8 joy, Uint8 ball, Sint16 X, Sint16 Y) { };
	///
	virtual void JoyHatMove(Uint8 joy, Uint8 hat, TJoyHatPosition pos) { };
	///
	virtual void JoyDown(Uint8 joy, Uint8 button) { };
	///
	virtual void JoyUp(Uint8 joy, Uint8 button) { };
	///
	virtual void VisibilityEvent(bool _visible) { };
	///
	virtual bool ResizeEvent(int w, int h) { return true; }
	//@}
private:
	/** Stores information about each widgets draw-state.  Should only be
	 ** manipulated by TWidget::DrawWidgetTree.
	 */
	struct DrawInfo {
		unsigned int mode;
		bool got_rect;
	};

	TWidget* Parent;
	std::bitset<FLAGS_MAX> flags;
	DrawFlag draw_flag;
	bool Changed;
	TColor BackgroundColor, Color;
	TFont* Font;
	TImage* BackgroundImage;
	bool Visible, Enabled;
	DrawInfo draw_info;
	TCanvas Canvas;

	// Little helper functions.  These define some of the most important widget
	// policies and are defined here inorder to easily change them.
	inline bool CanDraw();
	inline void ResetDrawFlags();

	// hold a global widget tree.
	static TWidgetTree WidgetTree;

	// for drawing and cycling the whole widget tree
	static bool DrawWidgetTree(std::list<TRect>* DirtsRects = NULL);
	static void CycleWidgetTree();
};
//---------------------------------------------------------------------------  
// Inline functions
//---------------------------------------------------------------------------  
inline void TWidget::DoEnter()
{
	if(CanAcceptInput()) {
		Enter();
		OnEnter(this);
	}
}
//--------------------------------------------------------------------------- 
inline void TWidget::DoExit()
{
	if(CanAcceptInput()) {
		Exit();
		OnExit(this);
	}
}
//--------------------------------------------------------------------------- 
inline void TWidget::DoKeyDown(const TKey& key)
{
	if(Parent) {
		if(Parent->GetKeyPreview()) Parent->DoKeyDown(key);
	}

	if(CanAcceptInput()) {
		KeyDown(key);
		OnKeyDown(this, key);
	}
}
//--------------------------------------------------------------------------- 
inline void TWidget::DoKeyUp(const TKey& key)
{
	if(Parent) {
		if(Parent->GetKeyPreview()) Parent->DoKeyDown(key);
	}

	if(CanAcceptInput()) {
		KeyUp(key);
		OnKeyUp(this, key);
	}
}
//---------------------------------------------------------------------------  
inline void TWidget::DoMouseDown(int X, int Y, TMouseButton button)
{
	if(CanAcceptInput()) {
		MouseDown(X, Y, button);
		OnMouseDown(this, X, Y, button);
	}
}
//---------------------------------------------------------------------------  
inline void TWidget::DoMouseUp(int X, int Y, TMouseButton button)
{
	if(CanAcceptInput()) {
		MouseUp(X, Y, button);
		OnMouseUp(this, X, Y, button);
	}
}
//---------------------------------------------------------------------------  
inline void TWidget::DoMouseMove(int X, int Y, int RelX, int RelY, TMouseButton button)
{
	if(CanAcceptInput()) {
		MouseMove(X, Y, RelX, RelY, button);
		OnMouseMove(this, TPoint2D(X, Y), RelX, RelY, button);
	}
}
//--------------------------------------------------------------------------- 
inline void TWidget::DoJoyAxisMove(Uint8 joy, Uint8 axis, Sint16 value)
{
	if(CanAcceptInput()) {
		JoyAxisMove(joy, axis, value);
		OnJoyAxisMove(this, joy, axis, value);
	}
}
//--------------------------------------------------------------------------- 
inline void TWidget::DoJoyBallMove(Uint8 joy, Uint8 ball, Sint16 X, Sint16 Y)
{
	if(CanAcceptInput()) {
		JoyBallMove(joy, ball, X, Y);
		OnJoyBallMove(this, joy, ball, X, Y);
	}
}
//--------------------------------------------------------------------------- 
inline void TWidget::DoJoyHatMove(Uint8 joy, Uint8 hat, TJoyHatPosition pos)
{
	if(CanAcceptInput()) {
		JoyHatMove(joy, hat, pos);
		OnJoyHatMove(this, joy, hat, pos);
	}
}
//--------------------------------------------------------------------------- 
inline void TWidget::DoJoyDown(Uint8 joy, Uint8 button)
{
	if(CanAcceptInput()) {
		JoyDown(joy, button);
		OnJoyDown(this, joy, button);
	}
}
//--------------------------------------------------------------------------- 
inline void TWidget::DoJoyUp(Uint8 joy, Uint8 button)
{
	if(CanAcceptInput()) {
		JoyUp(joy, button);
		OnJoyUp(this, joy, button);
	}
}
//---------------------------------------------------------------------------  
inline bool TWidget::CanDraw() 
{
	if(Visible) {
		if(GetSurface() != NULL) {
			return true;
		} else {
			LOG(3, "Can't draw to a surface-less widget");
		}
	}
	
	return false;
}
//---------------------------------------------------------------------------  
inline void TWidget::ResetDrawFlags() 
{
	// reset draw flag
	if(GetDrawOnDemand()) {
		draw_flag = dfNO_DRAW;
	} else {
		draw_flag = dfUPDATE;
	}
}
//---------------------------------------------------------------------------  
}; // namespace ksd
//--------------------------------------------------------------------------- 
#endif

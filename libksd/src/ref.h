//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		TRefCounter,
//					(TRefCounter::TRefData)
//	
//	Purpose:		Provides a simple way implement reference counting.
//	
//	Checked-in by:	$Author$
//					$Date$
//--------------------------------------------------------------------------- 
#ifndef _KSD_refH
#define _KSD_refH
//---------------------------------------------------------------------------  
#include <stddef.h>
//--------------------------------------------------------------------------- 
namespace ksd {	
//---------------------------------------------------------------------------  
/** A template class that can be used to implement reference counting.
 ** 
 ** Example:
 ** 
\begin{verbatim}
	class TestClassData {
	public:
		int type, id;
	};
	
	class TestClass {
	public:
		TestClass(const TestClass& V) { // copy constructor
			ref = V.ref;
		}
		
		int GetType() 	{ return ref.GetData().type; }
		int GetID() 	{ return ref.GetData().id; }
	protected:
		TRefCounter<TestClassData> ref;
	};
\end{verbatim}
 ** In this example TestClass reference counts TestClassData.  It is very
 **	plausible (and in fact very simple), for a class to referece count multiple 
 **	data classes.  Just use two or more TRefCounter members and copy them in
 **	the copy constructor.
 ** 
 ** Template arguments:
 ** \begin{itemize}
 ** \item \textbf{ref_data} The actual object to be reference counted
 ** \end{itemize}
 */
template<typename ref_data>
class TRefCounter {
private:
	typedef TRefCounter<ref_data> this_type;
	
	// class for storing the actual data
	class TRefData {
	public:
		TRefData() { 
			count = 0;
		}

		void AddRef() { count++; }
		void RemoveRef() { count--; }
		bool CanDelete() const { return count == 0; }
		int	 GetCount() const { return count; }

		ref_data data;
	private:
		unsigned int count;
	};
public:
	///
	TRefCounter() {
		ref = CreateRefData();
		ref->AddRef(); 
	}

	/** When a reference counter is destroyed, the data's reference count is
	 ** decremented and if required, destroyed.
	 */
	virtual ~TRefCounter() {
		if(ref) {
			ref->RemoveRef();
			if(ref->CanDelete()) {
				DestroyRefData(ref);
			}
		}
	}

	/** Makes this reference counter into a refer to the data refered to by
	 ** "V", first removing reference to whatever object it used to refer
	 ** to.
	 */
	this_type& operator = (const this_type& V) { 
		// remove reference to current object
		if(ref) {
			ref->RemoveRef();
			if(ref->CanDelete()) {
				DestroyRefData(ref);
			}
		}
		
		// make reference to new object
		ref = ((this_type*)&V)->GetRef();
		if(ref) {
			ref->AddRef();
		}

		return *this;
	}

	/** Returns true if both TRefCounter's point to the same peice of
	 ** internal data.
	 */
	bool operator == (const this_type& V) {
		if(ref) {
			if(ref == V.ref) return true;
		}
		return false;
	}

	/** Returns true if both TRefCounter's \textit{don't} point to the same
	 ** piece of internal data.
	 */
	bool operator != (const this_type& V) {
		if(!ref || ref != V.ref) return true;
		return false;
	}

	/** This operator allows you to perform operations on the TRefCounter as
	 ** if it were the object that it wrapped.
	 */
	operator ref_data& () {
		return GetRef()->data;
	}

	/** Assigning a data object to this object will copy to the internal data wrapped
	 ** by this reference counter.  What this means is:
	 ** 
	 ** RefCounterObject = RefDataObject
	 ** 
	 ** will actually assign to the ref data stored in the RefCounterObject.
	 */
	TRefCounter<ref_data&> operator = (const ref_data& V) {
		GetRef()->data = V.data;
		return *this;
	}

	/** Removes reference to current data and creates a new, unique peice of data
	 ** to refer to.
	 */
	void Clear() {
		// remove reference to current object
		if(ref) {
			ref->RemoveRef();
			if(ref->CanDelete()) {
				DestroyRefData(ref);
			}
		}

		// create new object
		ref = CreateRefData();
		ref->AddRef();
	}

	/** Like clear but it won't delete the reference data.  This is a very dangerous 
	 ** function that should only be used when you know exactly what your doing.  In 
	 ** most cases, what you really want is Clear.  This function can be useful, if
	 ** the ref data is unique and you want to use it outside of the reference counter.
	 ** You can then "unhook" it to prevent the reference counter from automatically
	 ** destroying it.
	 */
	void Unhook() {
		ref = CreateRefData();
		ref->AddRef();
	}

	/// Returns the data object.
	ref_data& GetData() { return GetRef()->data; } 
	/// Returns the data object (const).
	const ref_data&	GetData() const { 
		// a dumb yet necessary hack
		return ((this_type*)this)->GetRef()->data; // const cast
	}
	/// Returns the number of objects refering to this data object.
	int	GetRefCount() const	{ 
		if(ref) {
			ref->GetCount(); 
		} else {
			// since we can't create it on a const function, return a
			// count of one, which is exactly what it should be if it
			// were created.
			return 1;
		}
	}
	/// Add a reference to this data object.
	void AddRef() { GetRef()->AddRef(); }
	/// Check if this data object is deletable.
	bool CanDelete() const { 
		if(ref) {
			return ref->CanDelete(); 
		} else {
			// since we cannot create it on a const functions, return false -
			// which is exactly what it should be, had we created it on
			// this function call.
			return false;
		}
	}
	/// Remove a reference to this data object.
	void RemoveRef() { // if too many references removed, clear object
		if(ref) {
			ref->RemoveRef(); 
			if(ref->CanDelete()) { 
				DestroyRefData(ref);
				ref = CreateRefData();
				ref->AddRef();
			}
		}
	}

	/** Assign to the data object referred to by this object.  This of course
	 ** affects all reference counters that refer to the same data object as
	 ** this one.
	 */
	void SetData(const ref_data& _data) { GetRef()->data = _data; }
private:
	TRefData* ref;

	TRefData* GetRef() {
		if(!ref) {
			ref = CreateRefData();
			ref->AddRef();
		}
		return ref;
	}

	TRefData* CreateRefData() {
		TRefData* temp = new TRefData;
		return temp;
	}

	void DestroyRefData(TRefData* _data) {
		delete _data;
	}
};
//---------------------------------------------------------------------------  
/** Used to provide auxilary reference counting.  You can think of it as a
 ** reference counter where the reference data is the reference count itself.
 */
KSD_EXPORT_CLASS class TAuxRefCounter {
public:
	/// Create new auxilary reference counter
	TAuxRefCounter() {
		Ref = new unsigned int;
		*Ref = 1;
	}

	/// Destroy an auxilary reference counter
	~TAuxRefCounter() {
		RemoveRef();
		if(GetCount() == 0) {
			delete Ref;
		}
	}

	/// Refer to a new object.
	void Clear() {
		if(GetCount() > 1) {
			RemoveRef();
			Ref = new unsigned int;
			*Ref = 1;
		}
	}

	/// Make refer to a difference auxilary refernce counter.
	TAuxRefCounter& operator = (const TAuxRefCounter& V) {
		// remove ref to old object
		RemoveRef();
		if(GetCount() == 0) {
			delete Ref;
		}

		// make referer to new object
		Ref = V.Ref;
		AddRef();

        return *this;
	}

	/// Returns the number of refernces to this data.
	int GetCount() const { return *Ref; }
	/// Add a reference.
	void AddRef() { (*Ref)++; }
	/// Remove a reference.
	void RemoveRef() { (*Ref)--; }
private:
	unsigned int* Ref;
};
//--------------------------------------------------------------------------- 
}; // namespace ksd
//--------------------------------------------------------------------------- 
#endif


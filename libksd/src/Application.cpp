//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	$Header$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Checked-in by:	$Author$
//
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma implementation
#endif
//--------------------------------------------------------------------------- 
#define KSD_BUILD_LIB
#include <SDL/SDL.h>
#include <iostream>
#include "Application.h"
#include "init.h"
#include "log.h"

#include "Backend.h"
#include "backends/default_backends.h"

#ifdef KSD_USE_TTF
#include <SDL/SDL_ttf.h>
#endif

#ifdef USE_LTDL
#include <ltdl.h>
#endif

using namespace ksd;
using namespace std;
//--------------------------------------------------------------------------- 
// library initialized flag
static bool LibraryInitialized = false;
// application object
TApplication* ksd::Application = NULL;
//---------------------------------------------------------------------------  
void ksd::InitLibrary()
{
	if(!LibraryInitialized) {
		// init sdl
		if(SDL_Init(SDL_INIT_VIDEO 
		#ifdef __DEBUG__
		| SDL_INIT_NOPARACHUTE
		#endif
		#if !defined(KSD_MERGE_CYCLE_AND_EVENT_THREAD) && defined(KSD_SUPPORT_EVENT_THREAD)
		| SDL_INIT_EVENTTHREAD
		#endif
		) < 0) {
			cerr << SDL_GetError() << endl;
			// dirty exit because nothing has been initialized yet
			exit(1);
		}

		#ifdef USE_LTDL
		// Start ltdl and setup library paths to default values
		lt_dlinit();
		// TODO: add an environment variable that the user can set
		lt_dlsetsearchpath(".");
		# ifndef KSD_PLUGIN_DIR
	    #  ifdef __BORLANDC__
		#   pragma warning No default plugin directory has been defined!
	    #  else
	    #   warning No default plugin directory has been defined!
	    #  endif
		# else
		lt_dladdsearchdir(KSD_PLUGIN_DIR);
		# endif
		#endif
		
		// default unicode to disabled
		SDL_EnableUNICODE(0);

		#ifdef KSD_USE_TTF 
		TTF_Init();
		#endif

		// load the backends
		LoadDefaultBackends(TBackendSystem::GetInstance());

		// scan for joys
		// NOTE: We shouldn't do this!  The user to should have to scan for joysticks as
		// part of their code.  This should also init the joystick sub-system on
		// demand.
		//JoystickList.Scan();

		// set initialized flag
		LibraryInitialized = true;

		// Module start-up stuff
		ModuleInit::DoInitFunctions();
	}
}
//---------------------------------------------------------------------------  
void ksd::ShutdownLibrary()
{
	if(LibraryInitialized) {
		// Eliminate a stray Application object (this *really* should be
		// removed before the library is shutdown) first so that it can't
		// affect objects after shutdown.
		if(Application) {
			Application->Shutdown();
			delete Application;
			Application = NULL;
		}

		// Module shutdown stuff
		ModuleInit::DoShutdownFunctions();
	
		#ifdef KSD_USE_TTF
		TTF_Quit();
		#endif

		// NOTE: should this be done by hand here?  Or should we provide a generic method
		// for adding stuff to the shutdown sequence and have the joystick code add
		// itself?
		//TJoystickSubsystem::GetInstance()->Close();

		#ifdef USE_LTDL
		// shut down ltdl
		lt_dlexit();
		#endif

		// lastly, shutdown sdl
		SDL_Quit();

		// clear library initialized flag
		LibraryInitialized = false;
	}
}
//---------------------------------------------------------------------------  
void ksd::Quit(int error_code)
{
	// TODO: make error_level setting/reading and shutdown thread safe and 
	// synchronized (ie. don't do SDL_Quit before all the threads have finished)

	// preforms all necessary library shutdown
	ShutdownLibrary();
	
	exit(error_code);
}
//--------------------------------------------------------------------------- 
// It is decided!!!  I will use an event thread.
// 
// Reasons:
// 
// 	When using a draw thread, the thread is choppy and is in high demand.  The high demand 
// 	thread should be the main one...
class TApplication::EventThread : public TThread {
public: 
	EventThread(TApplication* _parent) : TThread() { Parent = _parent; }
protected:
	TApplication* Parent;

	void RunThread() {
		while(IsRunning()) {
			Parent->CycleWidgetTree();
			#ifdef KSD_MERGE_CYCLE_AND_EVENT_THREAD
			Parent->EventLoop();
			#endif
			SDL_Delay(1);
		}
	}
};
//--------------------------------------------------------------------------- 
TApplication::TApplication() : TWidget(NULL)
{
	if(Application != NULL && Application != this) {
		throw("Cannot create application object because one already exists.");
	}

	error_level = -1;
	Decoupled = false;
	paint_type = PartialUpdate;
	dt = new EventThread(this);
	
	Screen = NULL;
	EventDispatcher = NULL;
	CurrentBackend = "none";

	// init widget tree
	// NOTE: can this be here?  Or is this not constructor material?
	WidgetTree.Init(this);

	// force set application
	if(!Application) Application = this;
}
//--------------------------------------------------------------------------- 
TApplication::~TApplication()
{
	// stop and destroy the event thread
	if(dt) {
		dt->Stop();
		delete dt;
	}

	// destroy screen and event dispatcher
	if(Screen) {
		delete Screen;
	}
	if(EventDispatcher) {
		delete EventDispatcher;
	}
}
//--------------------------------------------------------------------------- 
void TApplication::Run(int argc, char* argv[])
{
	// Store command values for possible parsing in the future
	ArgV = argv;
	ArgC = argc;

	// run application start-up
	try {
		Init();
	} catch(::ksd::exception& e) {
		cerr << "Uncaught excpetion stemming from TApplication::Init()" << endl;
		e.report(cerr);
		cerr << flush;
		Quit(1);
	}

	// only run the main loop if we have a screen
	if(Screen) {
		std::list<TRect> DirtyRects;

		// THE main loop.
		while(true) {
			// cycle the widgets
			if(!Decoupled) {
				CycleWidgetTree();
			}

			// draw
			if(Screen && Screen->IsValid()) {
				if(paint_type == FullUpdate || Screen->GetDoubleBuffered()) {
					// only update screen when changes have been made
					if(DrawWidgetTree() && Screen->IsChanged()) Screen->Update();
				} else {
					DirtyRects.clear();
					if(DrawWidgetTree(&DirtyRects) && Screen->IsChanged()) {
						Screen->Update(DirtyRects);
					}
				}
			}

			// capture and echo events
			#ifdef KSD_MERGE_CYCLE_AND_EVENT_THREAD
			if(!Decoupled) {
				EventLoop();
			}
			#else
			EventLoop();
			#endif
		}
	}

	// NOTE: don't call Quit() here!  If libksd is run from a custom main 
	// function (which happens when we embed it), then we want to return
	// from Run.
	//Quit(0);
}
//--------------------------------------------------------------------------- 
void TApplication::CreateScreen(std::string backend, int width, int height,
								int bpp, unsigned int screen_flags, void* extra)
{
	if(width == 0 || height == 0) {
		throw ECannotInitDisplay("Cannot init a display without valid dimensions", "", width, height, bpp, screen_flags, extra);
	}
		
	// only allocate a new object if we are actually changing backends or
	// no screen object exists.
	if(CurrentBackend != backend || !Screen) {
		// destroy old screen
		if(Screen) {
			delete Screen;
			Screen = NULL; // must do!!
		}

		// destroy old event dispatcher
		if(EventDispatcher) {
			delete EventDispatcher;
			EventDispatcher = NULL; // must do!!
		}

		// get backend object
		TBackend* BackendObj = TBackendSystem::GetInstance()->GetBackend(backend);
		if(BackendObj == NULL) {
			throw EUnsupported("Application", string("Cannot create screen with unknown backend: " + backend));
		}

		// create screen and event dispatcher
		Screen = BackendObj->CreateScreen();
		EventDispatcher = BackendObj->CreateEventDispatcher(&WidgetTree);

		// set joystick defaults
		EventDispatcher->SetJoystickEvents(true);
		EventDispatcher->SetJoystickAxisRepeat(true);

		// store backend string
		CurrentBackend = backend;
	}

	// initialize screen
	Screen->Init(width, height, bpp, screen_flags, extra);
}
//---------------------------------------------------------------------------  
bool TApplication::IsBackendSupported(const std::string& backend)
{
	return TBackendSystem::GetInstance()->GetBackend(backend) != NULL;
}
//--------------------------------------------------------------------------- 
std::string TApplication::GetCaption() const
{
	char c_Name[255];
	SDL_WM_GetCaption((char**)&c_Name, NULL);

	string Name(c_Name);
	return Name;
}
//---------------------------------------------------------------------------  
void TApplication::SetCaption(const std::string& _caption) 
{
	SDL_WM_SetCaption(_caption.c_str(), NULL);
}
//--------------------------------------------------------------------------- 
void TApplication::SetDecoupled(bool enable)
{
	if(enable != Decoupled) {
		Decoupled = enable;

		if(Decoupled) {
			dt->Start();
		} else {
			dt->Stop();
		}
	}
}
//---------------------------------------------------------------------------  
void TApplication::SetPaintType(PaintType _type) 
{
	if(paint_type != _type) {
		paint_type = _type;
	}
}
//---------------------------------------------------------------------------  
void TApplication::Resize(int X, int Y)
{
	if(Screen) {
		Screen->Resize(X, Y);
	} else {
		throw ENotInitialized("Application", "Trying to resize screen without creating one.");
	}
}
//--------------------------------------------------------------------------- 
void TApplication::EventLoop()
{
	EventDispatcher->PollEvents();
}
//---------------------------------------------------------------------------  

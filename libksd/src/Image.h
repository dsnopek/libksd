//---------------------------------------------------------------------------
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//
//---------------------------------------------------------------------------
//	File:			$RCSfile$
//
//	Author:			David Snopek
//	Revised by:		Andrew Sterling Hanenkamp
//	Version:		$Revision$
//
//	Objects:		TImage
//
//	Purpose:		An image class
//
//	Checked-in by:	$Author$
//					$Date$
//---------------------------------------------------------------------------
#ifndef _KSD_ImageH_
#define _KSD_ImageH_
//---------------------------------------------------------------------------
#include <sigc++/sigc++.h>
#include "Surface.h"
#include "Canvas.h"
#include "FormatSys.h"
#include "export_ksd.h"

// forward decl
class SDL_Surface;
//---------------------------------------------------------------------------
#ifdef __GNUG__
#pragma interface
#endif
//---------------------------------------------------------------------------
namespace ksd {
//---------------------------------------------------------------------------
/** An image surface.  TImage is a surface that you can construct to any
 ** artibrary size, load files, save files, and draw to.  TImage objects
 ** are copied on write.  This means that when one image is assigned to 
 ** another, its data is shared until one of the images is actually written
 ** to.
 */
KSD_EXPORT_CLASS class TImage : public TSurface { // friend TFileFormat<TImage>;
public:
	// image loader with plug-in support
	static char TypeName[]; // = "Image";
	typedef TFormatSystem<TImage, TypeName> ImageLoader;
	static ImageLoader Loader;

	/** Constructs an un-initialized image.  Use Create or Load to make
	 ** a valid image.
	 */
	TImage();
	
	/** Construct an image with the given attributes.  Uses the same
	 ** arguments as Create.
	 */
	TImage(int width, int height,
		   TPixelFormatType pixel_format = TPixelFormat::DISPLAY,
		   Uint8 image_type = TSurfaceData::SYSTEM_MEMORY);
		   
	/** Construct an image with the given attributes.  Uses the same 
	 ** arguments as Create.
	 */
	TImage(int width, int height, Uint8 bpp,
		   Uint8 image_type = TSurfaceData::SYSTEM_MEMORY);

	/** Contruct an image loaded from the given file.  Uses the same 
	 ** arguments as Load.
	 */
	TImage(const char* filename, char* type = "AUTODETECT");

	/** Contruct an image as a copy of an other image.  The image is not actually
	 ** copied until the copy is written to (called "copy on write").
	 */
	TImage(const TImage& V);

	/** Assign this image to the data of another image.  The image is not actually
	 ** copied until the copy is written to (called "copy on write").
	 */
	TImage& operator = (const TImage& V);

	/// Signal called when image is modified.
	SigC::Signal1<void, TImage*> OnChange;

	/** Initialize this image with the given attributes.
	 ** 
	 ** @param width The desired width.
	 ** @param height The desired height.
	 ** @param pixel_format	The desired TPixelFormatType for this image.
	 ** @param image_type Controls the type of image that is created.
	 */
	void Create(int width, int height,
				TPixelFormatType pixel_format = TPixelFormat::DISPLAY,
				Uint8 image_type = TSurfaceData::SYSTEM_MEMORY);
				
	/** Initialize this image with the given attributes.
	 ** 
	 ** @param width The desired width.
	 ** @param height The desired height.
	 ** @param pixel_format	The desired TPixelFormatType for this image.
	 ** @param image_type Controls the type of image that is created.
	 */
	void Create(int width, int height, Uint8 bpp, Uint8 image_flags = TSurfaceData::SYSTEM_MEMORY);

	/// Convert an image to the given format.
	TImage Convert(const TPixelFormat &pixel_format, Uint8 image_type);
	/// Convert an image to the given format.
	TImage Convert(const TPixelFormat &pixel_format) {
		return Convert(pixel_format, GetData()->GetFlags());
	}

	/** Load the image data from a file.  The image loader uses a plugin system,
	 ** so make sure that you have the appropriate plugins loaded.  You can 
	 ** specify an image type string as the type argument as one of the following:
	 ** 
	 ** \begin{itemize}
	 ** \item \textbf{AUTODETECT} - Check the given file and use the appropriate
	 ** 	plugin to load it.
	 ** \item \textbf{PNG} - Load PNG images.
	 ** \item \textbf{BMP} - Load BMP images.
	 ** \item \textbf{TIF} - Load tiff images.
	 ** \item \textbf{JGP} - Load jpeg images.
	 ** \item \textbf{PCX} - Load PCX images.
	 ** \end{itemize}
	 */
	void Load(const char* Filename, char* type = "AUTODETECT");
	
	/** Save the image data to a file.  The image loaded uses a plugin system,
	 ** so make sure that you have the approprate plugins loaded.  Currently,
	 ** the only valid image type string is "BMP".
	 */
	void Save(const char* Filename, char* type);

	/// Call this function to perform "copy on write".
	void OnWrite();

	/// Creates a sub-surface with the given dimensions
	TSubSurface* CreateSubSurface(int X, int Y, int Width, int Height, bool Buffered);
	/// Clears the image with the given color.
	void Fill(const TColor& Color);

	/** Sets this image to an sdl_surface.  The reference to previous surface is 
	 ** first cleared.
	 */
	void SetSurface(SDL_Surface* sdl_surface, bool owned = true);

	/** Returns the images clip rect.
	 */
	TRect GetClipRect(); 

	/** Sets the images clip rect. This can be used to set a standard cliprect
	 ** for this TImage and will affect all TCanvas objects that use this TImage.
	 */
	void SetClipRect(const TRect& clip_rect);

	// helps when using Get/Set Surface --> clears reference without destroying
	// the refered object.  May cause unusual behavoir!!  Only use when you are sure
	// the only one reference exists
	using TSurface::Unhook;
	using TSurface::IsUnique; // checks to see that only one ref exists
private:
	TAuxRefCounter AuxRef;

	void Recreate();
};
//---------------------------------------------------------------------------
typedef TSurfaceData::Type 		TImageType;
typedef TImage::ImageLoader		TImageLoader;
//---------------------------------------------------------------------------
}; // namespace ksd
//---------------------------------------------------------------------------
#endif


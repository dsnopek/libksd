//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	$Header$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Checked-in by:	$Author$
//	
//---------------------------------------------------------------------------
#ifdef __GNUG__
#pragma implementation
#endif
//--------------------------------------------------------------------------- 
#define KSD_BUILD_LIB
#include <stdlib.h>
#include "Rect.h"
//--------------------------------------------------------------------------- 
using ksd::TPoint2D;
using ksd::TLine;
using ksd::Dimension;
using ksd::fixed;
//---------------------------------------------------------------------------  
ksd::TRect::TRect(int X1, int Y1, int X2, int Y2) 
{
	// X's
   	if(X1 > X2) {
       	Left = X2;
       	Right = X1;
   	} else {
       	Left = X1;
       	Right = X2;
   	}

   	// Y's
   	if(Y1 > Y2) {
       	Top = Y2;
       	Bottom = Y1;
    } else {
   	    Top = Y1;
       	Bottom = Y2;
    }
}
//---------------------------------------------------------------------------  
ksd::TRect::TRect(const TPoint2D& point1, const TPoint2D& point2) 
{
	// X's
    if(point1.X > point2.X) {
   	    Left = point2.X;
       	Right = point1.X;
   	} else {
       	Left = point1.X;
       	Right = point2.X;
    }

   	// Y's
    if(point1.Y > point2.Y) {
   	    Top = point2.Y;
       	Bottom = point1.Y;
    } else {
   	    Top = point1.Y;
       	Bottom = point2.Y;
    }
}
//---------------------------------------------------------------------------
void ksd::TRect::SetPosition(const TPoint2D& point)
{
    int Width, Height;

    Width = Right - Left;
    Height = Bottom - Top;

    Left = point.X;
    Top = point.Y;
    Right = Left + Width;
    Bottom = Top + Height;
}
//---------------------------------------------------------------------------
void ksd::TRect::Set(int X1, int Y1, int X2, int Y2)
{
	// X's
    if(X1 > X2) {
        Left = X2;
        Right = X1;
    } else {
        Left = X1;
        Right = X2;
    }

    // Y's
    if(Y1 > Y2) {
        Top = Y2;
        Bottom = Y1;
    } else {
        Top = Y1;
        Bottom = Y2;
    }
}
//---------------------------------------------------------------------------
Dimension ksd::TRect::GetDimensions() const
{
    Dimension dim;

    dim.Width = Right - Left;
    dim.Height = Bottom - Top;

    return dim;
}
//---------------------------------------------------------------------------
bool ksd::TRect::IsInside(const TPoint2D& V) const
{
    if(V.X > Left && V.X < Right) {
        if(V.Y > Top && V.Y < Bottom) {
            return true;
        }
    }
    return false;
}
//---------------------------------------------------------------------------
void ksd::TRect::Move(int X, int Y)
{
    Left += X;
    Top += Y;
    Right += X;
    Bottom += Y;
}
//---------------------------------------------------------------------------
void ksd::TRect::MoveTo(int X, int Y)
{
    int Width, Height;

    Width = Right - Left;
    Height = Bottom - Top;

    Left = X;
    Top = Y;
    Right = X + Width;
    Bottom = Y + Height;
}
//--------------------------------------------------------------------------- 
// TODO: implement!
/*bool ksd::TRect::ClipLine(ksd::TLine& line) const
{
    return false;
}*/
//---------------------------------------------------------------------------
bool ksd::TRect::ClipLine(int& x1, int& y1, int& x2, int& y2) const
{
    int Step, EdgeHeight;
    fixed Slope;

    // Test Against left
    if(x1 < Left) {
        if(x2 < Left) return false; // if both points are before the left, then its not visible

        if(x2 - x1 != 0) {
            Step = Left - x1;
            EdgeHeight = x2 - x1;
            Slope = (fixed)abs(y2 - y1) / EdgeHeight;
            if(y2 < y1) y1 = y1 - (Slope * Step).ToInt();
            else y1 = y1 + (Slope * Step).ToInt();
        }

        x1 = Left;
    }
    else if(x2 < Left) {
        if(x1 - x2 != 0) {
            Step = Left - x2;
            EdgeHeight = x1 - x2;
            Slope = (fixed)abs(y1 - y2) / EdgeHeight;
            if(y1 < y2) y2 = y2 - (Slope * Step).ToInt();
            else y2 = y2 + (Slope * Step).ToInt();
        }

        x2 = Left;
    }

    // Test against top
    if(y1 < Top) {
        if(y2 < Top) return false; // if both points are above the top, then its not visible

        if(y2 - y1 != 0) {
            Step = Top - y1;
            EdgeHeight = y2 - y1;
            Slope = (fixed)abs(x2 - x1) / EdgeHeight;
            if(x2 < x1) x1 = x1 - (Slope * Step).ToInt();
            else x1 = x1 + (Slope * Step).ToInt();
        }

        y1 = Top;
    }
    else if(y2 < Top) {
        if(y1 - y2 != 0) {
            Step = Top - y2;
            EdgeHeight = y1 - y2;
            Slope = (fixed)abs(x1 - x2) / EdgeHeight;
            if(x1 < x2) x2 = x2 - (Slope * Step).ToInt();
            else x2 = x2 + (Slope * Step).ToInt();
        }

        y2 = Top;
    }

    // Test against right
    if(x1 > Right) {
        if(x2 > Right) return false; // if both points are before the left, then its not visible

        if(x1 - x2 != 0) {
            Step = x1 - Right;
            EdgeHeight = x1 - x2;
            Slope = (fixed)abs(y1 - y2) / EdgeHeight;
            if(y2 < y1) y1 = y1 - (Slope * Step).ToInt();
            else y1 = y1 + (Slope * Step).ToInt();
        }

        x1 = Right;
    }
    else if(x2 > Right) {
        if(x2 - x1 != 0) {
            Step = x2 - Right;
            EdgeHeight = x2 - x1;
            Slope = (fixed)abs(y2 - y1) / EdgeHeight;
            if(y1 < y2) y2 = y2 - (Slope * Step).ToInt();
            else y2 = y2 + (Slope * Step).ToInt();
        }

        x2 = Right;
    }

    // Test against bottom
    if(y1 > Bottom) {
        if(y2 > Bottom) return false; // if both points are below the bottom, then its not visible

        if(y2 - y1 != 0) {
            Step = y1 - Bottom;
            EdgeHeight = y1 - y2;
            Slope = (fixed)abs(x1 - x2) / EdgeHeight;
            if(x2 < x1) x1 = x1 - (Slope * Step).ToInt();
            else x1 = x1 + (Slope * Step).ToInt();
        }

        y1 = Bottom;
    }
    else if(y2 > Bottom) {
        if(y1 - y2 != 0) {
            Step = y2 - Bottom;
            EdgeHeight = y2 - y1;
            Slope = (fixed)abs(x2 - x1) / EdgeHeight;
            if(x1 < x2) x2 = x2 - (Slope * Step).ToInt();
            else x2 = x2 + (Slope * Step).ToInt();
        }

        y2 = Bottom;
    }

    return true;
}
//---------------------------------------------------------------------------
bool ksd::TRect::ClipRect(ksd::TRect& rect) const
{
	return ClipRect(rect.Left, rect.Top, rect.Right, rect.Bottom);
}
//---------------------------------------------------------------------------
bool ksd::TRect::ClipRect(int& _left, int& _top, int& _right, int& _bottom) const
{
    if(_left > Right || _right < Left || _top > Bottom || _bottom < Top) return false;

	if(_left < Left) _left = Left;
    if(_right > Right) _right = Right;
    if(_top < Top) _top = Top;
    if(_bottom > Bottom) _bottom = Bottom;

    return true;
}
//---------------------------------------------------------------------------
void ksd::TRect::ClipPoint(ksd::TPoint2D& point) const
{
	int X = point.X;
	int Y = point.Y;
	ClipPoint(X, Y);
	point.X = X;
	point.Y = Y;
}
//---------------------------------------------------------------------------
void ksd::TRect::ClipPoint(int& X, int& Y) const
{
	if(X < Left) X = Left;
    if(X > Right) X = Right;
    if(Y < Top) Y = Top;
    if(Y > Top) Y = Bottom;
}
//---------------------------------------------------------------------------


//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		TCommandLine
//	
//	Purpose:		@DESCRIPTION@
//	
//	Checked-in by:	$Author$
//					$Date$
//---------------------------------------------------------------------------
#ifndef _KSD_CommandLine_H_
#define _KSD_CommandLine_H_
//---------------------------------------------------------------------------
#include <string>
#include <vector>
#include <map>
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma interface
#endif
//---------------------------------------------------------------------------
namespace ksd {
//---------------------------------------------------------------------------
class TCommandLine {
public:
	//TCommandLine();
	~TCommandLine() { Clear(); }

	void Clear(); // cleans-up the command line object entirely
	void Reset(); // clears parsed data

	enum DataType {
		String,
		Double,
		Int,
		Flag,
		Bool
	};

	// used to set-up command line interface
	void AddOption(const std::string& name, const std::string long_name, DataType _type,
				   bool Mandatory = false);
	//void AddList(const std::string& name, DataType _type, bool Mandatory = false);
	//void AddMap(const std::string& name, DataType _type, bool Mandatory = false);

	// public "get" interface
	std::string GetStringOption(const std::string& name);
	double 		GetStringDouble(const std::string& name);
	int			GetIntOption(const std::string& name);
	bool		GetFlagOption(const std::string& name);
	bool		GetBoolOption(const std::string& name);

	std::vector<std::string> GetFileList() { return FileList; }

	// does the parsing
	void Parse(int argc, char** argv);
private:
	// TODO: maybe make an internal data rep that deals with the different types 
	// and then use that for all the option types...

	// TODO: make into private part of Option
	union Data {
		std::string* String;
		double* Double;
		int* Int;
		bool* Flag, Bool;
	};

	// TODO: make full featured class
	// TODO: make heirarchy with common interface for all option types (single, 
	// list, and map).  Maybe only include the parent class in this header file 
	// and put the rest in the cpp.
	struct Option {
		Option()  { 
			memset(&_data, 0, sizeof(_data)); 
			_isset = _mandatory = false; 
		}
		~Option() { Clear(); }

		void Clear() {
			if(_isset) {
				switch(_type) {
					case String:
						delete _data.String;
						break;
					case Double:
						delete _data.Double;
						break;
					case Int:
						delete _data.Int;
						break;
					case Flag:
					case Bool:
						delete _data.Bool;
						break;
				}
			}
			memset(&_data, 0, sizeof(_data)); 
			_isset = false;
		}
	
		Data _data;
		DataType _type;
		bool _isset, _mandatory;
	};

	std::map<std::string, Option> OptionList;
	std::vector<std::string> FileList;	 	
};
//---------------------------------------------------------------------------  
}; // namespace ksd
//---------------------------------------------------------------------------  
#endif



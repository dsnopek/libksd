//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	$Header$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Checked-in by:	$Author$
//
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma implementation
#endif
//--------------------------------------------------------------------------- 
#define KSD_BUILD_LIB
#include <SDL/SDL_image.h>
#include <SDL/SDL_rwops.h>
#include "backends/sdl/sdlconv.h"
#include "exception.h"
#include "BitmapFont.h"

#define DEFAULT_SFONT_PTSIZE		24

using namespace ksd;
//---------------------------------------------------------------------------  
SFont_FontInfo* ksd::CreateSFont(const TFontAttr& attr, const char* filename)
{
	SFont_FontInfo* Font = SFont_OpenFont(filename);
	
	if(Font) {
		// transform font as described in attr
		
		if(attr.Size != DEFAULT_SFONT_PTSIZE) {
			// TODO: Find a way to pass the calculated point size along with
			// the font attr and file name...
			SFont_ResizeFont(Font, DEFAULT_SFONT_PTSIZE, attr.Size);
		}

		if(attr.Style != TFont::Normal)
			SFont_StylizeFont(Font, attr.Style);

		// only color-ize when a non-black color is given.  I know this is
		// unintuitive but I want bitmap fonts to look really colorful by default
		if(attr.Color != TColor::black) {
			SFont_ColorizeFont(Font, KSDtoSDL(attr.Color));
		}
	}

	return Font;
}
//--------------------------------------------------------------------------- 
void TBitmapFont::Open(const std::string& filename)
{
	DB.SetFilename(filename.c_str());
	SFont_FontInfo* Font = DB.GetFont(DefaultFontAttr);
	
	// check for sfont errors
	if(!Font) {
		throw EFileError("Font", SDL_GetError(), filename);
	}

	// extract point size from the name of the file
	// Karl Bartel started a de facto convention of putting the point size 
	// int the beginning of the name (ex. 24P_NeonYellow_Arial.png)
	/*string size_str;
	int filename_len = strlen(filename);
	for(int i = 0; i < filename_len; i++) {
		if(filename[i] < '1' || filename[i] > '9') break;
		size_str += filename[i];
	}
	pt_size = atoi(size_str.c_str());
	if(pt_size == 0) pt_size = DEFAULT_SFONT_PTSIZE;*/
}
//--------------------------------------------------------------------------- 
int TBitmapFont::GetHeight(const TFontAttr& attr)
{
	SFont_FontInfo* Font = DB.GetFont(attr);

	return Font ? Font->h : 0;
}
//---------------------------------------------------------------------------  
int TBitmapFont::GetAscent(const TFontAttr& attr)
{
	SFont_FontInfo* Font = DB.GetFont(attr);

	return Font ? Font->base : 0;
}
//---------------------------------------------------------------------------  
int TBitmapFont::GetDescent(const TFontAttr& attr)
{
	SFont_FontInfo* Font = DB.GetFont(attr);

	return Font ? Font->base - Font->h : 0;
}
//---------------------------------------------------------------------------  
int TBitmapFont::GetLineSkip(const TFontAttr& attr)
{
	/*SFont_FontInfo* Font = DB.GetFont(attr);*/
	
	// I haven't worked this one out yet
	return 0;
}
//--------------------------------------------------------------------------- 
void TBitmapFont::GetTextSize(const std::string& text, const TFontAttr& attr,
							  int& width, int& height)
{
	SFont_FontInfo* Font = DB.GetFont(attr);

	if(Font) {
		width = SFont_TextWidth(Font, text.c_str());
		height = Font->h;
	} else {
		width = 0;
		height = 0;
	}
}
//--------------------------------------------------------------------------- 
void TBitmapFont::DrawString(SDL_Surface* surf, int X, int Y, 
							 const std::string& text, TRect* ClipRect,
							 const TFontAttr& attr)
{
	SFont_FontInfo* Font = DB.GetFont(attr);

	if(Font) {
		if(ClipRect) {
			SDL_Rect temp = KSDtoSDL(*ClipRect);
			SFont_DrawString(surf, Font, X, Y, text.c_str(), &temp);
		} else {
			SFont_DrawString(surf, Font, X, Y, text.c_str(), NULL);
		}
	}
}
//--------------------------------------------------------------------------- 


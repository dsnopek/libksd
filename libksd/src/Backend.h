//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		TBackend
//	
//	Purpose:		@DESCRIPTION@
//	
//	Checked-in by:	$Author$
//					$Date$
//---------------------------------------------------------------------------
#ifndef _KSD_BackendH_
#define _KSD_BackendH_
//---------------------------------------------------------------------------
#include "PluginSys.h"
#include "export_ksd.h"
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma interface
#endif
//---------------------------------------------------------------------------
namespace ksd {

// forward decls
class TScreen;
class TWidgetTree;
class TEventDispatcher;
//---------------------------------------------------------------------------
/** Defines a libksd backend.  Each backend has two parts: a screen type and
 ** an event dispatcher.  This object creates the appropriate screen or
 ** event dispatcher, for use by the application.
 */
KSD_EXPORT_CLASS class TBackend {
public:
	/** Construct a backend object.
	 ** 
	 ** @param _name The name used to identify this backend.
	 */
	TBackend(const std::string& _name) {
		Name = _name;
	}
	virtual ~TBackend() { };

	/** Returns the name of the backend.
	 */
	std::string GetName() const { return Name; }

	/** Called by TApplication to construct the screen object.
	 */
	virtual TScreen* CreateScreen() = 0;

	/** Called by TApplication to construct the event dispatcher.
	 */
	virtual TEventDispatcher* CreateEventDispatcher(TWidgetTree*) = 0;

	/// The string used for the plugin system.  Value = "Backend".
	static char TypeName[]; // = "Backend"
private:
	std::string Name;
};
//---------------------------------------------------------------------------  
/** The backend system.  Stores and retrieves TBackend pointers on demand. 
 ** Can load plugins and and plugin lists.
 */
KSD_EXPORT_CLASS class TBackendSystem : public TPluginSystem<TBackend, TBackend::TypeName> {
public:
	/** Adds a backend to the system.
	 */
	void AddBackend(TBackend* backend) {
		Register(backend, backend->GetName());
	}

	/** Returns the single instance of TBackendSystem.  Implements the singleton
	 ** pattern for this class.
	 */
	static TBackendSystem* GetInstance() {
		return &Instance;
	}

	// renamed to more logical names!
	/// Returns the backend with the given name.
	TBackend* GetBackend(const std::string& name) { return GetPlugin(name); }
	/// Returns the number of backends.
	unsigned int GetCount() { return GetPluginCount(); }
private:
	// hide a couple of the functions
	typedef TPluginSystem<TBackend, TBackend::TypeName> parent_type;
	using parent_type::GetPlugin;
	using parent_type::GetPluginCount;
	using parent_type::Register;
	using parent_type::Remove;
	using parent_type::Clear;

	// hide constructor
	TBackendSystem() : parent_type() { }

	static TBackendSystem Instance;
};
//---------------------------------------------------------------------------  
}; // namespace ksd
//---------------------------------------------------------------------------  
#endif


//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//	
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		TSubSurface
//	
//	Purpose:		A surface that is mearly a section of anouther surface.
//	
//	Checked-in by:	$Author$
//					$Date$
//--------------------------------------------------------------------------- 
#ifndef _KSD_SubSurfaceH_
#define _KSD_SubSurfaceH_
//---------------------------------------------------------------------------
#include "Surface.h"
#include "export_ksd.h"
//---------------------------------------------------------------------------
#ifdef __GNUG__
#pragma interface
#endif
//---------------------------------------------------------------------------
namespace ksd {
//---------------------------------------------------------------------------
// forward decl
class TImage;
//---------------------------------------------------------------------------
KSD_EXPORT_CLASS class TSubSurface : public TSurface {
public:
	virtual void SetScroll(unsigned int X, unsigned int Y) = 0;
	virtual void Reposition(int X, int Y) = 0;
	virtual void Reset(const TRect& rect) = 0;

	virtual TPoint2D GetScroll() const = 0;
	virtual TPoint2D GetPosition() const = 0;

	virtual bool IsBuffered() const = 0;
	virtual void CopyToParent() = 0;
};
//---------------------------------------------------------------------------  
/* TODO:  Replace recursion with a tree design.  With how much this class is 
 * used, recursion causes too many repetitive calculation.  When a 
 * Begin/End draw type functionality is added to the API, use that and a
 * tree design to stop re-calculating data.
 */
KSD_EXPORT_CLASS class TGenericSubSurface : public TSubSurface {
public:
	/** Creates a sub-surface to an other surface. Using this constructor implies that
	 ** this is a first level sub-surface.
	 */
	TGenericSubSurface(TSurface* _surf, int X, int Y, int width, int height, bool buffered);
	~TGenericSubSurface();
	
	int GetWidth() const { return Width; }
	int GetHeight() const { return Height; }

	// functions for modifying what the sub-surface includes
	void Resize(int width, int height);
	void Reposition(int x, int y);
	void Reset(const TRect& rect) {
		X = rect.GetLeft();
		Y = rect.GetTop();
		Width = rect.GetWidth();
		Height = rect.GetHeight();
	}

	// the key!
	ImageIterator make_iterator(ImageIterator::Type type) {
		ImageIterator iter = Surface->make_iterator(type);
		iter.clip(X, Y, Width, Height);
		iter.scroll(ScrollX, ScrollY);
		return iter;
	}
	
	// functions for "scrolling" a surface
	void SetScroll(unsigned int _sx, unsigned int _sy);
	TPoint2D GetScroll() const { return TPoint2D(ScrollX, ScrollY); }
	TPoint2D GetPosition() const { return TPoint2D(X, Y); }

	// use actual surfaces own desired on-write behavior
	void OnWrite() { Surface->OnWrite(); }

	// fill implemented for a subsurface
	void Fill(const TColor& _color);

	TSubSurface* CreateSubSurface(int _x, int _y, int _w, int _h, bool _buffered) {
		// should call second constructor
		return  new TGenericSubSurface(this, _x, _y, _w, _h, _buffered);
	}

	// returns true if this is a buffered sub-surface
	bool IsBuffered() const { return Buffered; }

	// draws to parent surface
	void CopyToParent();

	// returns the *real* position relative to the TSurfaceData
	TPoint2D GetRootPosition() {
		int X, Y, H, W;
		GetRealPosition(X, Y, H, W);
		return TPoint2D(X, Y);
	}

	// returns the scrolled clip rect
	TRect GetClipRect() {
		// TODO: is there a way to cache this?
		// get absolute position
		int real_x, real_y, real_w, real_h;
		if(!GetRealPosition(real_x, real_y, real_w, real_h)) {
			ClipRect.Set(0, 0, 0, 0);
		} else {
			MakeClipRect();
		}
		return ClipRect;
	}
protected:
	/** Creates a sub-surface to an other TSubSurfaceSDL.  This *must* be used to 
	 ** create second level and heigher sub-surfaces.
	 */
	TGenericSubSurface(TGenericSubSurface* _surf, int X, int Y, int width, int height, bool buffered);

	/** Calculates the absolute position, relative to the actual surface.  Returns
	 ** false if the surface is not visible.
	 */
	bool GetRealPosition(int& _x, int& _y, int& _w, int& _h);
private:
	// TODO: combine these two members with a union.
	TSurface* Surface;
	TGenericSubSurface* Parent;

	TImage* Buffer;
	int X, Y, Width, Height;
	unsigned int ScrollX, ScrollY;
	TRect ClipRect;
	bool Buffered;

	void MakeClipRect();
};
//---------------------------------------------------------------------------  
}; // namespace ksd
//--------------------------------------------------------------------------- 
#endif


//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//	
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		TInputFilter, TInputDevice, TInputEvent
//	
//	Purpose:		Defines a standard interface for dealing with input.  Children 
//					will include:
// 						TKeyboard, TKeyboardFilter 
// 						TJoystick, TJoystickFilter
// 						TMouse, TMouseFilter
// 						TAIScript, TAIScriptFilter 
// 						TNetworkInput, TNetworkInputFilter
//	
//	Checked-in by:	$Author$
//					$Date$
//---------------------------------------------------------------------------
#ifndef _KSD_InputH_
#define _KSD_InputH_
//---------------------------------------------------------------------------
#include "ksdconfig.h"
#include "export_ksd.h"
//---------------------------------------------------------------------------
namespace ksd {
//---------------------------------------------------------------------------
// forward declarations
class TInputDevice;
//---------------------------------------------------------------------------
// InputEvent
// 		ID = name of event (events will be or'ed together for multiple events)
// 		Count = number of members in data
// 		Data = an interger array that holds information about the events
//---------------------------------------------------------------------------
struct TInputEvent {
	TInputEvent()  { ID = Count = 0;
					 Data = NULL; }
	~TInputEvent() { if(Data) { delete[] Data; } }

	int ID, Count;
	int* Data;
};	
//---------------------------------------------------------------------------
// InputFilter
// 		Filters the input from a device into logical events.  (Like controls
// 	in a video game).  It allows lots of different devices to be handled in 
// 	the same way.
//---------------------------------------------------------------------------
KSD_EXPORT_CLASS class TInputFilter {
public:
	TInputFilter(TInputDevice* dev) { Device = dev; }
	virtual ~TInputFilter() { }; 

	// the device that this object is filtering
	TInputDevice* GetDevice() { return Device; }
	
	// pass the address of where the result will be stored
	virtual void Filter(TInputEvent& evt) = 0; 

	// the next event recieved by the owned device will from now on recieve 
	// an ID of InpEvntID.  All the normal data associated with this type of
	// event will be sent too.  This is an attempt to be able to set events 
	// with no idea what type of device the filter uses.
	virtual void Detect(int InpEvntID, int time_out = 1000) = 0;

	// loads/saves custom filter infromation - (think of like controls)
	virtual void Load(char* FileName) = 0;
	virtual void Save(char* FileName) = 0;
private:
	TInputDevice* Device;
};
//---------------------------------------------------------------------------
// InputDevice
//		Represents a device from which input can be recieved.	
//---------------------------------------------------------------------------
KSD_EXPORT_CLASS class TInputDevice {
public:
	// creates the defualt filter type for this input device
	virtual TInputFilter* CreateFilter() = 0;	

	// gets the input from the device
	virtual void Update() = 0;
};
//---------------------------------------------------------------------------
}; // namespace ksd
//---------------------------------------------------------------------------
#endif


//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	$Header$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Checked-in by:	$Author$
//	
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma implementation
#endif
//---------------------------------------------------------------------------
#define KSD_BUILD_LIB
#include <math.h>
#include <algorithm>
#include "Line.h"
//---------------------------------------------------------------------------
using ksd::TPoint2D;
using namespace std;
//---------------------------------------------------------------------------
ksd::TLine::TLine()
{
    Verticle = false;
    Start = Stop = 0;
}
//---------------------------------------------------------------------------
ksd::TLine::TLine(ksd::fixed slope, ksd::fixed yintercept, int start, int stop)
{
    Verticle = false;
    Slope = slope;
    Intercept = yintercept;
    if(start < stop) {
        Start = start;
        Stop = Stop;
    } else {
        Start = stop;
        Stop = start;
    }
}
//---------------------------------------------------------------------------
ksd::TLine::TLine(int xintercept, int top, int bottom)
{
    Verticle = true;
    Slope = 0;
    Intercept = xintercept;
    if(top < bottom) {
        Start = top;
        Stop = bottom;
    } else {
        Start = bottom;
        Stop = top;
    }
}
//---------------------------------------------------------------------------
void ksd::TLine::CreateLine(ksd::fixed slope, ksd::fixed yintercept, int start, int stop)
{
    Verticle = false;
    Slope = slope;
    Intercept = yintercept;
    if(start < stop) {
        Start = start;
        Stop = Stop;
    } else {
        Start = stop;
        Stop = start;
    }
}
//---------------------------------------------------------------------------
void ksd::TLine::CreateHLine(int y, int start, int stop)
{
    Verticle = false;
    Slope = 0;
    Intercept = y;
    if(start < stop) {
        Start = start;
        Stop = stop;
    } else {
        Start = stop;
        Stop = start;
    }
}
//---------------------------------------------------------------------------
void ksd::TLine::CreateVLine(int x, int start, int stop)
{
    Verticle = true;
    Slope = 0;
    Intercept = x;
    if(start < stop) {
        Start = start;
        Stop = stop;
    } else {
        Start = stop;
        Stop = start;
    }
}
//---------------------------------------------------------------------------
ksd::TLine ksd::TLine::PerpendicularLine(const TPoint2D& start, bool& Touches) const
{
    TLine line;
    int temp1(0), temp2(0);

    if(!Verticle && Slope != 0) { // normal line
        ksd::fixed inter_diff, slope_diff;

        line.Slope = (ksd::fixed(-1) / Slope);
        line.Intercept = (ksd::fixed)start.Y - (line.Slope * start.X);

        inter_diff = line.Intercept - Intercept;
        slope_diff = Slope - line.Slope;
        temp1 = (inter_diff / slope_diff).RoundInt();
        temp2 = start.X;

        // check to see if lines touch
        if(temp1 >= Start && temp1 <= Stop) {
            Touches = true;
        } else Touches = false;
    }
    else if(!Verticle && Slope == 0) { // line is horizontal
        line.Verticle = true;
        line.Slope = 0;
        line.Intercept = start.X;

        temp1 = start.Y;
        temp2 = Intercept.RoundInt();

        // check to see if lines touch
        if(line.Intercept >= Start && line.Intercept <= Stop) {
            Touches = true;
        } else Touches = false;
    }
    else if(Verticle) { // line is verticle
        line.Slope = 0;
        line.Intercept = start.Y;

        temp1 = start.X;
        temp2 = Intercept.RoundInt();

        // check to see if lines touch
        if(line.Intercept >= Start && line.Intercept <= Stop) {
            Touches = true;
        } else Touches = false;
    }

    // assign limits
    if(temp1 < temp2) {
        line.Start = temp1;
        line.Stop = temp2;
    } else {
        line.Start = temp2;
        line.Stop = temp1;
    }

    return line;
}
//---------------------------------------------------------------------------
ksd::fixed ksd::TLine::Length()
{
    TPoint2D point1, point2;
    int X, Y;
    ksd::fixed value;

    point1 = GetStartPoint();
    point2 = GetEndPoint();

    X = point2.X - point1.X;
    Y = point2.Y - point1.Y;

    value = sqrt((X * X) + (Y * Y));

    return value;
}
//---------------------------------------------------------------------------
ksd::fixed ksd::TLine::Solve(int Value)
{
    ksd::fixed temp;

    if(!Verticle) { // value is a x value
        temp = (Slope * Value) + Intercept;
    } else { // value is a y value
        temp = Intercept;
    }

    return temp;
}
//---------------------------------------------------------------------------
void ksd::TLine::SetEndPoints(const TPoint2D& point1, const TPoint2D& point2)
{
    if(point1.X == point2.X) {
        Verticle = true;
        Slope = 0;
        Intercept = point1.X;
        if(point1.Y < point2.Y) {
            Start = point1.Y;
            Stop = point2.Y;
        } else {
            Start = point2.Y;
            Stop = point1.Y;
        }
        return;
    } else Verticle = false;

    if(point1.X < point2.X) {
        Start = point1.X;
        Stop = point2.X;
    } else {
        Start = point2.X;
        Stop = point1.X;
    }

    Slope = ksd::fixed(point2.Y - point1.Y) / ksd::fixed(point2.X - point1.X);
    Intercept = (ksd::fixed)point1.Y - (Slope * point1.X);
}
//---------------------------------------------------------------------------
TPoint2D ksd::TLine::GetStartPoint()
{
    TPoint2D V;

    if(!Verticle) {
        V.X = Start;
        V.Y = ((Slope * Start) + Intercept).RoundInt();
    }
    else {
        V.X = Intercept.RoundInt();
        V.Y = Start;
    }

    return V;
}
//---------------------------------------------------------------------------
TPoint2D ksd::TLine::GetEndPoint()
{
    TPoint2D V;

    if(!Verticle) {
        V.X = Stop;
        V.Y = ((Slope * Stop) + Intercept).RoundInt();
    }
    else {
        V.X = Intercept.RoundInt();
        V.Y = Stop;
    }

    return V;
}
//---------------------------------------------------------------------------
bool ksd::TLine::IsInside(const TPoint2D& V) const
{
    if(!Verticle) {
        int Y = ((Slope * V.X) + Intercept).RoundInt();
        if(V.Y == Y) return true;
    } else {
        if(V.X == Intercept.RoundInt()) {
            if(V.Y > Start && V.Y < Stop) {
                return true;
            }
        }
    }

    return false;
}
//---------------------------------------------------------------------------
void ksd::TLine::Move(int X, int Y)
{
    if(!Verticle) {
        Intercept = Intercept - (Slope * X) + Y;

        Start += X;
        Stop += X;
    }
    else { // if verticle
        Intercept += X;
        Start += Y;
        Stop += Y;
    }
}
//---------------------------------------------------------------------------
void ksd::TLine::MoveTo(int X, int Y)
{
    TPoint2D start = GetStartPoint();
    int VX = X - start.X, VY = Y - start.Y; // (VX, VY) <-- vector of change

    if(!Verticle) {
        Intercept = Intercept - (Slope * VX) + VY;

        Start += VX;
        Stop += VX;
    }
    else { // if verticle
        Intercept += VX;
        Start += VY;
        Stop += VY;
    }
}
//--------------------------------------------------------------------------- 


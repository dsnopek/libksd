//---------------------------------------------------------------------------
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//
//---------------------------------------------------------------------------
//	$Header$
//
//	Author:			David Snopek
//	Revised by:		Andrew Sterling Hanenkamp
//	Version:		$Revision$
//
//	Checked-in by:	$Author$
//
//---------------------------------------------------------------------------
#ifdef __GNUG__
#pragma implementation
#endif
//---------------------------------------------------------------------------
#define KSD_BUILD_LIB
#include "backends/sdl/sdlconv.h"
#include "Image.h"
#include "Application.h"
#include "log.h"
#include "ksdconfig.h"

#include "Plugin.h"
#include "init.h"

using namespace ksd;
//---------------------------------------------------------------------------
// set image type name
char TImage::TypeName[] = "Image";

#if defined(KSD_NO_PLUGINS) && defined(KSD_ENABLE_IMGLIB)
// gives us access to the plugin function
#include "plugins/image/ImagePlugin.h"
#endif

// create image loader plugin sys
TImageLoader ksd::TImage::Loader;

// register the plugin system on startup
static void StartupImageModule()
{
	RegisterPluginSystem(&ksd::TImage::Loader);
	#if defined(KSD_NO_PLUGINS) && defined(KSD_ENABLE_IMGLIB)
	// initialize loader with default file formats
	PluginImage(&ksd::TImage::Loader);
	#endif
}

static void ShutdownImageModule()
{
	UnregisterPluginSystem(&ksd::TImage::Loader);
}

_MODULE(&StartupImageModule, &ShutdownImageModule);
//---------------------------------------------------------------------------
TImage::TImage() : TSurface()
{
}
//---------------------------------------------------------------------------  
TImage::TImage(int width, int height, TPixelFormatType pixel_format, 
	Uint8 image_type) : TSurface()
{
	Create(width, height, pixel_format, image_type);
}
//---------------------------------------------------------------------------  
TImage::TImage(int width, int height, Uint8 bpp, Uint8 image_type)
	: TSurface()
{
	Create(width, height, bpp, image_type);
}
//---------------------------------------------------------------------------  
TImage::TImage(const char* filename, char* type) 
	: TSurface()
{
	Load(filename, type);
}
//---------------------------------------------------------------------------  
TImage::TImage(const TImage &copy) 
	: TSurface()
{
	MakeReferer(copy);
	AuxRef = copy.AuxRef;
}
//---------------------------------------------------------------------------  
TImage& TImage::operator = (const TImage& copy)
{
	MakeReferer(copy);
	AuxRef = copy.AuxRef;
	return *this;
}
//---------------------------------------------------------------------------  
void TImage::Create(int width, int height, TPixelFormatType pixel_format,
					Uint8 image_type)
{
	// remove ref to old
	Recreate();

	// build new
	GetData()->CreateImage(width, height, pixel_format, image_type);
}
//---------------------------------------------------------------------------
void TImage::Create(int width, int height, Uint8 bpp, Uint8 image_type)
{
	// remove ref to old
	Recreate();

	// build new
	GetData()->CreateImage(width, height, bpp, image_type);
}
//---------------------------------------------------------------------------
TImage TImage::Convert(const TPixelFormat &pixel_format, Uint8 image_type)
{
	LOG_ENTER_FUNC("TImage::Convert(TPixelFormat&, TImageType)");

	TImage temp;

	// convert an sdl surface
	temp.SetData(GetData()->Convert(pixel_format, image_type));

	LOG_EXIT_FUNC("TImage::Create(TPixelFormat&, TImageType)");

	return temp;
}
//---------------------------------------------------------------------------
void TImage::Load(const char* Filename, char* type)
{
	// remove old reference
	Recreate();

	if(strcmp(type, "AUTODETECT") == 0) {
		if(!TImage::Loader.AutoDetect(Filename, this)) {
			throw EFileError("Image", "Unable to auto detect the format of the specified file", Filename);
		}
	} else {
		TImage::Loader.Load(type, Filename, this);
	}
}
//---------------------------------------------------------------------------
void TImage::Save(const char* Filename, char* type)
{
	TImage::Loader.Save(type, Filename, this);
}
//---------------------------------------------------------------------------
void TImage::OnWrite()
{
	// call on change handler
	OnChange(this);

	// implement copy-on write behavior
	if(AuxRef.GetCount() > 1) { // TODO: un-lock before blit
		SDL_Surface* temp = GetHandle();
		Create(GetWidth(), GetHeight(), GetFormat().GetType(), GetData()->GetFlags());
		SDL_BlitSurface(temp, NULL, GetHandle(), NULL);
	}
}
//---------------------------------------------------------------------------  
TSubSurface* TImage::CreateSubSurface(int X, int Y, int Width, int Height, bool Buffered)
{
	return new TGenericSubSurface(this, X, Y, Width, Height, Buffered);
}
//---------------------------------------------------------------------------  
void TImage::Fill(const TColor& Color)
{
	OnWrite();
	GetData()->Fill(Color);
}
//---------------------------------------------------------------------------  
void TImage::SetSurface(SDL_Surface* sdl_surface, bool owned)
{
	// remove ref to old surface
	Recreate();	

	// set surface
	GetData()->SetSurface(sdl_surface, owned);
}
//---------------------------------------------------------------------------  
TRect TImage::GetClipRect()
{
	if(IsValid()) {
		SDL_Surface* h = GetHandle();
		return SDLtoKSD(h->clip_rect);
	}

	return TRect();
}
//---------------------------------------------------------------------------  
void TImage::SetClipRect(const TRect& clip_rect)
{
	if(IsValid()) {
		SDL_Rect temp = KSDtoSDL(clip_rect);
		SDL_SetClipRect(GetHandle(), &temp);
	}
	// TODO: when invalid, throw exception?
}
//---------------------------------------------------------------------------  
void TImage::Recreate()
{
	if(AuxRef.GetCount() > 1) {
		// clear old aux ref
		AuxRef.Clear();

		// make new underlying surface data object
		Clear();
	}
}
//---------------------------------------------------------------------------


//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2001 Arthur Peters
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	$Header$
//	
//	Author:			Arthur Peters
//	Revised by:		
//	Version:		$Revision$
//	
//	Checked-in by:	$Author$
//                            $Date$    
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma implementation
#endif
//--------------------------------------------------------------------------- 
#define KSD_BUILD_LIB
#include "Sample.h"
#include "log.h"

#include <SDL/SDL_rwops.h>

using namespace ksd;
//---------------------------------------------------------------------------
/**
   Initialize the object as a empty sample. If you try to play
   it the mixer will return an error.
*/ 
TSample::TSample()
{
	SampleData = NULL;
	ReferenceCount = 0;
}
//--------------------------------------------------------------------------- 
TSample::~TSample()
{
	FreeSampleData();
}
//--------------------------------------------------------------------------- 
/**
   Load a sample from a file. \\
   {\bf Only WAV files are supported right now. I will add more latter. }
*/
void TSample::Load(const char* Filename, const char* type )
{
	if( SampleData )
		{
			FreeSampleData();
		}

	SDL_RWops* file;
   
	// open file 
	file = SDL_RWFromFile(Filename, "rb");
	if(!file) {
		throw EFileError("Sample", "Unable to open specified audio file", Filename);
	}

	SampleData = Mix_LoadWAV_RW( file, 0 );
	if(SampleData == NULL) {
		SDL_RWclose(file);
		throw EFileError("Sample", "Unable to read audio data from specified file", Filename);
	}

	SDL_RWclose(file);
}
//--------------------------------------------------------------------------- 
/**
   Set the volume of this sample when it is played.
*/
void TSample::SetVolume( int vol ) throw( ENotInitialized )
{
	if( SampleData != NULL ) {      
		Mix_VolumeChunk( SampleData, vol );
	} else {
		throw ENotInitialized("Sample", "Cannot set volume because the sample has not been initialized");
	}
}
//--------------------------------------------------------------------------- 
/**
   Get the volume of this sample.
*/
int TSample::GetVolume() throw( ENotInitialized )
{
	if( SampleData != NULL ) {      
		return Mix_VolumeChunk( SampleData, -1 );
	} else {
		throw ENotInitialized("Sample", "Cannot get volume because the sample has not been initialized.");
	}
}
//--------------------------------------------------------------------------- 
void TSample::FreeSampleData()
{
	if( ReferenceCount > 0 ) {
		LogMessage( 0, __FILE__, __LINE__, "A TSample object that is in use is begin freed."
					" The underlying memory will not be freed. This will cause a memory leak.");
	} else {
		Mix_FreeChunk( SampleData );
		SampleData = NULL;
	}
}


/*
  libuta - a C++ widget library based on SDL (Simple Direct Layer)
  Copyright (C) 1999  Karsten Laux

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Library General Public
  License as published by the Free Software Foundation; either
  version 2 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Library General Public License for more details.

  You should have received a copy of the GNU Library General Public
  License along with this library; if not, write to the
  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
  Boston, MA  02111-1307, SA.
*/
//---------------------------------------------------------------------------
//	File:			$RCSfile$
//
//	Author:			Karsten Laux, June 1999
//	Revised by:		David Snopek, Andrew Sterling Hanenkamp
//	Version:		$Revision$
//
//	Objects:		TPixelFormat
//
//	Purpose:		Stores the pixel format
//
//	Checked-in by:	$Author$
//					$Date$
//---------------------------------------------------------------------------
#ifndef _KSD_PixelFormatH_
#define _KSD_PixelFormatH_
//---------------------------------------------------------------------------
#include <SDL/SDL_types.h>
#include <string>
#include "Palette.h"
#include "export_ksd.h"

// forward decls
struct SDL_PixelFormat;
//---------------------------------------------------------------------------
#ifdef __GNUG__
#pragma interface
#endif
//---------------------------------------------------------------------------
namespace ksd {
//---------------------------------------------------------------------------
KSD_EXPORT_CLASS class TPixelFormat {
public:
	/// pixel format types
	enum Type {
		/// Match the display format of the display
		DISPLAY = 0, 
		/// ABGR8888
		ABGR8888, 
		/// RGBA8888
		RGBA8888, 
		/// ARGB8888
		ARGB8888, 
		/// BGRA8888
		BGRA8888, 
		/// RGB888
		RGB888,
		/// BGR888
		BGR888, 
		/// RGB565
		RGB565, 
		/// RGB555
		RGB555, 
		/// IND8
		IND8, 
		/// UNKNOWN
		UNKNOWN 
	};

	TPixelFormat();
	TPixelFormat(Type _pt);
	TPixelFormat(SDL_PixelFormat* _pf);
	~TPixelFormat();

	/** Sets this object to the data stored in an SDL_PixelFormat.
	 */
	void Set(SDL_PixelFormat* _pf);

	/** Sets this object to the specified pixel format.
	 */
	void Set(Type _pt);

	/** Creates a string based on the pixel format.
	 */
	std::string ToString() const;

	/** Returns the current palette.  Will return NULL is this is not
	 ** an indexed image.
	 */
	TPalette* GetPalette() { return Palette; }

	/** Builds a "raw" pixel value based on a TColor object, using this pixel
	 ** format.
	 */
	Uint32 	MapToPixel(const TColor& Color) const;

	/** Builds a TColor object based on a "raw" pixel value, using this pixel 
	 ** format.
	 */
	TColor 	MapToColor(Uint32 Pixel) const;

	/// Returns the number of bits in a raw pixel in this format.
	int 	GetBitsPerPixel() const 	{ return BitsPerPixel; }
	/// Returns the number of bytes in a raw pixel in this format.
	int		GetBytesPerPixel() const	{ return BytesPerPixel; }
	/// Returns the red mask of the raw pixel in this format.
	Uint32 	GetRedMask() const 			{ return RedMask; }
	/// Returns the green mask of the raw pixel in this format.
	Uint32 	GetGreenMask() const 		{ return GreenMask; }
	/// Returns the blue mask of the raw pixel in this format.
	Uint32 	GetBlueMask() const 		{ return BlueMask; }
	/// Returns the alpha mask of the raw pixel in this format.
	Uint32 	GetAlphaMask() const 		{ return AlphaMask; }
	/// Retutn the TPixelFormatType of this format.
	Type	GetType() const 			{ return FormatType; }

	/// Returns the TPixelFormatType for the given data.
	static Type Identify(Uint8 _bpp,
			Uint32 rmask, Uint32 gmask, Uint32 bmask, Uint32 amask);

	/// Returns the bpp and masks for the given TPixelFormatType.
	static int 	GetInfo(Type _type,
			Uint32& rmask, Uint32& gmask, Uint32& bmask, Uint32& amask);

	/// Allocates an sdl pixel format for based on this format object.
	SDL_PixelFormat* CreateSDLFormat() const;
private:
	Uint32 RedMask, GreenMask, BlueMask, AlphaMask;
	int RedLoss, GreenLoss, BlueLoss, AlphaLoss;
	int RedShift, GreenShift, BlueShift, AlphaShift;
	int BitsPerPixel, BytesPerPixel;
	TPalette* Palette;
	Type FormatType;

	static TPixelFormat GetDisplayFormat();

	static int GetShift(Uint32 mask);
	static int GetLoss(Uint32 mask);

	static const char* Names[];
	static Uint32 Masks[][4];
	static int Bytes[]; 
};
//---------------------------------------------------------------------------
typedef TPixelFormat::Type TPixelFormatType;
//---------------------------------------------------------------------------
}; // namespace ksd
//---------------------------------------------------------------------------
#endif


//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		TScreen
//	
//	Purpose:		The base class for all libksd screen types.
//	
//	Checked-in by:	$Author$
//					$Date$
//---------------------------------------------------------------------------
#ifndef _KSD_ScreenH_
#define _KSD_ScreenH_
//---------------------------------------------------------------------------
#include <list>
#include "Surface.h"
#include "Rect.h"
#include "exception.h"
#include "export_ksd.h"
//---------------------------------------------------------------------------
#ifdef __GNUG__
#pragma interface
#endif
//---------------------------------------------------------------------------
namespace ksd {
//---------------------------------------------------------------------------
/** Thrown when the screen is unable to initialize the display to a particular
 ** set of attributes.  (\textit{Note: This is only thrown when the screen is
 ** completely unable to set the video.  It will try to fall back on different
 ** values first, before throwing this excpetion.})  This is usually only thrown
 ** by Init(...) and Resize(...).
 */
KSD_EXPORT_CLASS class ECannotInitDisplay : public ksd::exception {
public:
	/** Create a screen error.
	 **
	 ** @param _message The message of the exception
	 ** @param _system_error The error returned by the underlying system
	 ** @param _width The requested width
	 ** @param _height The requested height
	 ** @param _bpp The request bits per pixel
	 ** @param _flags The screen attributes requested
	 ** @param _extra The extra data used to initialize the screen
	 */
	ECannotInitDisplay(const std::string& _message, const std::string& _system_error,
		int _width, int _height, int _bpp, unsigned int _flags, void* _extra)
		: ksd::exception("Screen", _message, "cannot init display error")
	{
		SystemError = _system_error;
		Width = _width;
		Height = _height;
		Bpp = _bpp;
		Flags = _flags;
		Extra = _extra;
	}

	/// Returns the stored sytem error.
	std::string GetSystemError() const { return SystemError; }
	/// Returns the stored width.
	int GetWidth() const { return Width; }
	/// Return the stored height.
	int GetHeight() const { return Height; }
	/// Returns the stored bits-per-pixel.
	int GetBpp() const { return Bpp; }
	/// Returns the stored screen attributes.
	unsigned int GetFlags() const { return Flags; }
	/// Returns the stored extra data pointer.
	void* GetExtra() const { return Extra; }

	void report(std::ostream&) const;
private:
	std::string SystemError;
	int Width, Height, Bpp;
	unsigned int Flags;
	void* Extra;
};
//---------------------------------------------------------------------------
/** The base class for all libksd screen types.  This allows you to create
 ** a graphical backend for libksd.  Currently, libksd ships with two backends:
 ** SDL and OpenGL/SDL.  In the near future we hope to support wxWindows as well.
 */
KSD_EXPORT_CLASS class TScreen : public TSurface {
public:
	virtual ~TScreen() { };

	/** Screen flags.  An OR'd group of these flags are passed to Init, requesting
	 ** certain attributes for the screen.  Note: \textit{These attributes won't
	 ** necessarily be honored.  Check to see what was actually created.}
	 */
	enum {
		/// Default.
		DEFAULT = 0x00,
		/// Full screen.
		FULLSCREEN = 0x01,
		/// Use a hardware screen buffer.
		HARDWARE_BUFFER = 0x02,
		/// Use hardware double buffering.
		DOUBLE_BUFFERED = 0x04
	};

	/** Initializes the screen.  May be called more than once inorder to change
	 ** the attributes of the screen.
	 ** 
	 ** @param width The desired width of the screen.
	 ** @param height The desired height of the screen.
	 ** @param bpp The desired bits-per-pixel.  If bpp is zero then the bits-per-pixel of the current display will be used.
	 ** @param flags An OR'd group of screen flags, requesting certain attributes.
	 ** @param extra Extra data needed to initialize the screen.  See the documentation with the specific backend for more information.
	 */
	virtual void Init(int width, int height, int bpp = 0, unsigned int flags = DEFAULT, 
		void* extra = NULL) throw(ECannotInitDisplay) = 0;

	/** Returns true if the screen was drawn to.  Note: \textit{Not all video backends
	 ** may support this.  When not supported it will always return true.  Most notably, 
	 ** OpenGL does not support this!}
	 */
	virtual bool IsChanged() = 0;
	
	/** Updates the screen.  Is not required by all targets, but will always be
	 ** supported.
	 */
	virtual void Update() = 0;
	
	/** Updates only certain portions of the screen.  Note: \textit{Not all backends
	 ** may support this.  If GetDoubleBuffered returns true then this function will
	 ** just call Update and draw the whole screen. Calling this function will always 
	 ** be harmless (whether it is supported or not) but do not waste your time 
	 ** creating an UpdateRects list if it is not used.}
	 */
	virtual void Update(const std::list<TRect>& UpdateRects) = 0;

	/// Returns true if this screen is in fullscreen mode
	virtual bool GetFullscreen() const = 0;
	/// Returns true if this screen uses the hardware buffer.
	virtual bool GetHardwareBuffer() const = 0;
	/// Returns true if this screen is using hardware double buffering.
	virtual bool GetDoubleBuffered() const = 0;
};
//---------------------------------------------------------------------------  
}; // namespace ksd
//---------------------------------------------------------------------------  
#endif


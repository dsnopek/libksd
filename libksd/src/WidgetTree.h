//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		TWidgetTree
//	
//	Purpose:		Defines a tree of widgets.
//	
//	Checked-in by:	$Author$
//					$Date$
//---------------------------------------------------------------------------
#ifndef _KSD_WidgetTreeH_
#define _KSD_WidgetTreeH_
//---------------------------------------------------------------------------
#include <vector>
#include "Rect.h"
#include "exception.h"
#include "tree.h"
#include "export_ksd.h"
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma interface
#endif
//---------------------------------------------------------------------------
namespace ksd {

// forward decls
class TWidget;
//---------------------------------------------------------------------------
/** Manages a widget heirarchy.  This class is used to store and manipulate
 ** the relationships between the widgets.  This is only used internally, and
 ** you should never have to deal with it directly.
 */
KSD_EXPORT_CLASS class TWidgetTree {
public:
	/** Initializes the widget tree to this root widget.  A "root widget" is the
	 ** only widget that is allowed to exist in the widget tree without a parent.
	 */
	void Init(TWidget* root) { Tree.set_root(root); }

	/** Inserts this widget into is proper location in the widget tree.  All widgets
	 ** \textit{must} be dynamically allocated.  The memory will be freed automatically
	 ** this widget or one of its parents, is removed from the widget tree.
	 */
	void Add(TWidget* widget);

	/** Remove a widget and all of its children from the widget tree.  De-allocates
	 ** the widgets.
	 */
	void Remove(TWidget* widget);

	/** Returns the root widget of this tree.
	 */
	TWidget* GetRoot() { return Tree.has_root() ? Tree.get_root() : NULL; }
private:
	tree<TWidget*> Tree;
public:
	typedef tree<TWidget*>::iterator iterator;
	typedef tree<TWidget*>::reverse_iterator reverse_iterator;
	typedef tree<TWidget*>::layer_iterator layer_iterator;
	typedef tree<TWidget*>::reverse_layer_iterator reverse_layer_iterator;
	typedef tree<TWidget*>::iterator_range iterator_range;
	typedef tree<TWidget*>::reverse_iterator_range reverse_iterator_range;
	typedef tree<TWidget*>::reverse_layer_iterator_range reverse_layer_iterator_range;

	/// Returns an iterator that points to the beginning of the widget tree.
	iterator begin() { return Tree.begin(); }
	/// Returns an iterator that points to the end of the widget tree.
	iterator end() { return Tree.end(); }
	/// Returns a layer iterator that points to the beginning og the widget tree.
	layer_iterator layer_begin() { return Tree.layer_begin(); }
	/// Returns a layer iterator that points to the end of the widget tree.
	layer_iterator layer_end() { return Tree.layer_end(); }
	/// Returns a reverse iterator that points to the end of the widget tree.
	reverse_iterator rbegin() { return Tree.rbegin(); }
	/// Returns a reverse iterator that points to the end of the widget tree.
	reverse_iterator rend() { return Tree.rend(); }
	/// Returns the number of widgets in this tree.
	unsigned int size() const { return Tree.size(); }

	/// gets a reverse layer range
	reverse_layer_iterator_range GetReverseLayerRange(iterator iter) {
		return Tree.get_reverse_child_layer_range(iter);
	}

	// TODO: make the iterator STL compliant so we can use std::find
	/** Returns an iterator that points to the widget requested.  If the 
	 ** widget is not in the tree, it will return end().
	 */
	iterator find_widget(TWidget* w) { return Tree.find_item(w); }

	/** Moves this widget up one layer in the draw order.
	 */
	void MoveUp(iterator i) { Tree.move_backward(i); }

	/** Moves this widget to the top layer in the draw order.
	 */
	void MoveToFront(iterator i) { Tree.move_to_back(i); }
};
//---------------------------------------------------------------------------  
}; // namespace ksd
//---------------------------------------------------------------------------  
#endif


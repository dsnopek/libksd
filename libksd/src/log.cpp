//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	$Header$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Checked-in by:	$Author$
//
//--------------------------------------------------------------------------- 
// NOTICE: this a great tool IDEA -- it cannot perform in the desired fashion
// yet.  Here is the nature of the problem so that you may avoid it or 
// possibly help fix it:
// 		- Any log function calls made from a constructor or destructor have
// 		the possiblity of crashing the program!!!
// 		- This is ONLY a problem in classes that are global and created/destroyed
// 		before/after main!!
// 	This happens because I have no way to make sure the logger is created before 
// everything else and destroyed last.  In some compilers there are #pragma's 
// for that but not g++ which is the target compiler.
//--------------------------------------------------------------------------- 
#define KSD_BUILD_LIB
#include <string>
#include <map>
#include <stdio.h>
#include <stdarg.h>
#include <iostream>
#include <time.h>
#include <sys/timeb.h>
#include "init.h"
#include "log.h"

using namespace ksd;
using namespace std;
//--------------------------------------------------------------------------- 
class LogFile {
public:
	LogFile() 					{ Outfile = NULL; }
	LogFile(const string& fn) 	{ Open(fn); }

	void Open(const string& filename);
	void Close();
	void Write(const string&);
	bool IsOpen() { return Outfile ? true : false; }
private:
	FILE* Outfile;
};
//--------------------------------------------------------------------------- 
class Logger {
public:
	Logger();
	~Logger() {
		// at shutdown, close all the log files
		for(map<string, LogFile>::iterator i = Files.begin(); i != Files.end(); i++) {
			(*i).second.Write("\n"); // write trailing new-line
			(*i).second.Close();
		}
		Files.clear();

		#ifdef __DEBUG_DANERGOUSLY__
		cout << "Logger destroyed...\n" << flush;
		#endif
	}

	// accessor functions
	void SetOptions(logger::options _options) 		{ Options = _options; }
	void SetFilename(const string& _filename) 	{ Filename = _filename; }

	// Does the actual work
	void Log(int level, const string& file, int line, const string& message);
private:
	map<string, LogFile> Files;
	logger::options Options;
	string Filename;

	// I know so little about time structures, this is the only way I know how to do 
	// this.  I would much appreciate if some one could fix it...
	timeb StartTime; 	// for elapsed time
	string FileTime; 	// for file name time
};
//--------------------------------------------------------------------------- 
static Logger* logger_inst = NULL;

// Temp buffers for string manipulation
#define BUFFER_SIZE			1024
static char Buffer1[BUFFER_SIZE];

// Log level (default = 3)
static int LogLevel = 3;

static void StartupLogModule()
{
	logger_inst = new Logger();
}

static void ShutdownLogModule()
{
	if(logger_inst) {
		delete logger_inst;
		logger_inst = NULL;
	}
}

_MODULE(&StartupLogModule, &ShutdownLogModule);
//--------------------------------------------------------------------------- 
//	GLOBAL LOG FUNCTIONS - The glue that binds the world to the logger
//--------------------------------------------------------------------------- 
void ksd::SetLogOptions(long _options)
{
	if(logger_inst) {
		logger_inst->SetOptions((logger::options)_options);

		// TODO: make string version of log options and say what the options
		// are actually changed to
		LogMessage(0, "logger", -1, "LOG OPTIONS set to: %d", _options);
	}
}
//--------------------------------------------------------------------------- 
void ksd::SetLogLevel(int _level)
{
	if(logger_inst) {
		LogLevel = _level;

		// broadcast log level change
		LogMessage(0, "logger", -1, "LOG LEVEL set to: %d", _level);
	}
}
//--------------------------------------------------------------------------- 
void ksd::SetLogFilename(char* filename)
{
	if(logger_inst) {
		logger_inst->SetFilename(filename);
	}
}
//--------------------------------------------------------------------------- 
void ksd::LogMessage(int _level, char* file, int line, char* message, ...)
{
	if(logger_inst) {
		if(_level <= LogLevel) {
			va_list argp;
			va_start(argp, message);

			// make formatted string
			vsprintf(Buffer1, message, argp);
		
			logger_inst->Log(_level, string(file), line, Buffer1);
		}
	}
}
//--------------------------------------------------------------------------- 
void ksd::SimpleLogMessage(int _level, char* message, ...)
{
	if(logger_inst) {
		if(_level <= LogLevel) {
			va_list argp;
			va_start(argp, message);

			// make formatted string
			vsprintf(Buffer1, message, argp);
		
			logger_inst->Log(_level, "unknown", -1, Buffer1);
		}
	}
}
//--------------------------------------------------------------------------- 
//	LOG FILE FUNCTIONS -- 	The functions for opening, closing and writing to
//							a log file
//--------------------------------------------------------------------------- 
void LogFile::Open(const string& filename)
{
	if((Outfile = fopen(filename.c_str(), "wt")) == NULL) {
		printf("LOGGER ERROR: Unable to open file \"%s\" for write!!\n", filename.c_str());
		// TODO: set a flag or something, letting the system know that things aren't going so well
	}
} 
//--------------------------------------------------------------------------- 
void LogFile::Close() 
{
	if(Outfile) {
		if(fclose(Outfile) != 0) {
			printf("LOGGER ERROR: Unable to close file!!\n");
			// TODO: set a flag or something, letting the system know that things aren't going so well
		}
		Outfile = NULL;
	}
}
//--------------------------------------------------------------------------- 
void LogFile::Write(const string& line)
{
	if(Outfile) {
		const char* str = line.c_str();
		fwrite(str, line.length(), 1, Outfile);
	}
}
//--------------------------------------------------------------------------- 
//	LOGGER CLASS FUNCTIONS -- the functionality of a logger
//--------------------------------------------------------------------------- 
Logger::Logger()
{
	Filename = "runlog"; // default file name
	Options = logger::MODE_A;
	ftime(&StartTime); // store start time

	// create file time <<-- replace some day
	time_t ltime;
	tm *today; 
	time(&ltime);
	today = localtime(&ltime);
	strftime(Buffer1, BUFFER_SIZE - 1,"%d-%b-%Y-%H-%M-%S.log",today);
	FileTime = Buffer1;
}
//--------------------------------------------------------------------------- 
void Logger::Log(int level, const string& file, int line, const string& message)
{
	static char Temp[256];
	string module, output; // module name and ouput string
	int exti;
	timeb current_time;

	// Extract module name
	exti = file.rfind('.');
	if(exti != -1) { // has an extension, needs to be removed 
		module = file.substr(0, exti);
	} else {
		if(file == "") {
			module = "unknown";
		} else module = file;
	}

	// get cuurent time
	ftime(&current_time);

	// Create output string
	if(Options & logger::SHOW_TIMESTAMP) {
		char* TimeString;
		TimeString = ctime(&(current_time.time));
		sprintf(Temp, "%.15s.%3hu - ", &TimeString[4], current_time.millitm);
		output += Temp;
	}
	if(Options & logger::SHOW_ELAPSED_TIME) {
		double elapsed_time;
		elapsed_time =  difftime(current_time.time, StartTime.time);
		elapsed_time += (current_time.millitm - StartTime.millitm) * .001;
		sprintf(Temp, "(%.3f) - ", elapsed_time); // orig: "(%.3lf) - "
		output += Temp;
	}
	if(Options & logger::SHOW_LOG_LEVEL) {
		sprintf(Temp, "<%d> - ", level);
		output += Temp;
	}
	if(Options & logger::SHOW_MODULE_NAME) {
		output += '<' + module + "> - ";
	}
	if((Options & logger::SHOW_LINE_NUMBER) && (line > 0)) { // only do if we have a valid line number
		sprintf(Temp, "at %s line #%d - ", file.c_str(), line);
		output += Temp;
	}
	output += (message + '\n');
	
	// Write to log medium
	if(Options & logger::LOG_TO_EXPLICIT_PATH) {
		LogFile& log_file = Files[Filename];
		if(!log_file.IsOpen()) { // if not already opened
			log_file.Open(Filename);

			#ifdef __DEBUG_DANGEROUSLY__
			cout << "Opening " << Filename << endl << flush;
			#endif
		}
		if(level != 0) // avoid repetition
			log_file.Write(output);
	}
	if(Options & logger::LOG_TO_MODULE_NAME) {
		LogFile& log_file = Files[module];
		if(!log_file.IsOpen()) { // if not already opened
			log_file.Open(module + ".log"); // open "module.log" as log file

			#ifdef __DEBUG_DANGEROUSLY__
			cout << "Opening " << module << ".log" << endl << flush;
			#endif
		}
		if(level != 0) // avoid repetition
			log_file.Write(output);
	}
	if(Options & logger::LOG_TO_DATE_TIME_FILENAME) { // <<-- REPLACE SOMEDYA
		LogFile& log_file = Files[FileTime];
		if(!log_file.IsOpen()) { // if not already opened
			log_file.Open(FileTime);
		}
		if(level != 0) // avoid repetition
			log_file.Write(output);
	}
	if(Options & logger::LOG_TO_STANDARD_OUTPUT) {
		cout << output << flush;
	}

	// if level == 0 then broadcast message to all files
	if(level == 0) {
		for(map<string, LogFile>::iterator i = Files.begin(); i != Files.end(); i++) {
			LogFile& log_file = (*i).second;
			log_file.Write(output);
		}
	}
}
//---------------------------------------------------------------------------


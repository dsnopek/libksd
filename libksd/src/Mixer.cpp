//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2001 Arthur Peters
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	$Header$
//	
//	Author:			Arthur Peters
//	Revised by:		
//	Version:		$Revision$
//	
//	Checked-in by:	$Author$
//                            $Date$    
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma implementation
#endif
//--------------------------------------------------------------------------- 
#define KSD_BUILD_LIB
#include "Mixer.h"
#include "Sample.h"
#include "Song.h"
#include "log.h"

#include <SDL/SDL_mixer.h>
//--------------------------------------------------------------------------- 
using namespace ksd;
TMixer ksd::Mixer; // initialize mixer
//---------------------------------------------------------------------------
/**
   Private! This is the object used to communicate between
   \Ref{TMixer} and \Ref{TSampleInstance}.
 */
class TSampleInstance::Data { friend class TSampleInstance; friend class TMixer;
	// the friendships aren't needed, but I think it's the right thing (TM).
public:
	/**
	   Initialize the object with {\it channel} and {\it sample}.
	*/
	Data( int channel, TSample* sample ) {
		Channel = channel;
		Sample = sample;
	}

	/**
	   The number of the SDL_mixer channel used by this sample
	   instance.
	*/
	int Channel;

	/**
	   A pointer to the sample being played.
	*/
	TSample* Sample;

	/**
	   The TSampleInstance to be told when this data object is deleted.
	*/
	TSampleInstance* Instance;
};
//---------------------------------------------------------------------------
/**
   Initialize a TSampleInstance with a data object.
*/
TSampleInstance::TSampleInstance(Data* data)
{
	LOG_ENTER_FUNC(__PRETTY_FUNCTION__);
	InstanceData = data;
	InstanceData->Instance = this;
	LOG_EXIT_FUNC(__PRETTY_FUNCTION__);
}
//---------------------------------------------------------------------------
/*
  This function should never be called. It is a private member.
 */
TSampleInstance::TSampleInstance(TSampleInstance& inst)
{
	LOG_ENTER_FUNC(__PRETTY_FUNCTION__);
	LogMessage( 0, __FILE__, __LINE__, "A copy of a TSampleInstance object is being made."
				"The object from which the copy was made will be made invalid.");

	InstanceData = inst.InstanceData;
	inst.InstanceData = NULL;
	
	if( InstanceData != NULL ) {
		InstanceData->Instance = this;
	}
	LOG_EXIT_FUNC(__PRETTY_FUNCTION__);
}
//---------------------------------------------------------------------------
TSampleInstance::~TSampleInstance()
{
	LOG_ENTER_FUNC(__PRETTY_FUNCTION__);
	if( InstanceData != NULL )
		{
			InstanceData->Instance = NULL;
			InstanceData = NULL;
		}
	LOG_EXIT_FUNC(__PRETTY_FUNCTION__);
}
//---------------------------------------------------------------------------
/**
   @return true if the instance is playing and false otherwise.
*/
bool TSampleInstance::IsPlaying() const
{
	if( InstanceData != NULL ) {
		return Mix_Playing(InstanceData->Channel) && ! Mix_Paused(InstanceData->Channel);
	}
	return false;
}
//---------------------------------------------------------------------------
/**
   @return true if the instance is paused and false otherwise.
*/
bool TSampleInstance::IsPaused() const
{
	if( InstanceData != NULL ) {
		return Mix_Paused(InstanceData->Channel) && Mix_Playing(InstanceData->Channel);
	}
	return false;
}
//---------------------------------------------------------------------------
/**
   @return true if the instance is stoped and false otherwise.
*/
bool TSampleInstance::IsStopped() const
{
	return InstanceData == NULL;
}
//---------------------------------------------------------------------------
/**
   Stop the instance after fading it for {\it fadeout} ms.
*/
bool TSampleInstance::Stop( int fadeout )
{
	LOG_ENTER_FUNC(__PRETTY_FUNCTION__);
	if( InstanceData != NULL ) {
		if( fadeout == 0) {
			return Mix_HaltChannel( InstanceData->Channel );
		} else {
			return Mix_FadeOutChannel( InstanceData->Channel, fadeout );
		}
	}
	LOG_EXIT_FUNC(__PRETTY_FUNCTION__);
	return false;
}
//---------------------------------------------------------------------------
/**
   Stop the instance after {\it ms} ms.
*/
bool TSampleInstance::StopIn( int ms )
{
	if( InstanceData != NULL )
		return Mix_ExpireChannel( InstanceData->Channel, ms );
	return false;
}
//---------------------------------------------------------------------------
/**
   Pause the instance.
*/
void TSampleInstance::Pause()
{
	if( InstanceData != NULL )
		Mix_Pause( InstanceData->Channel );
}
//---------------------------------------------------------------------------
/**
   Resume playing the instance if it was paused.
*/
void TSampleInstance::Resume()
{
	if( InstanceData != NULL )
		Mix_Resume( InstanceData->Channel );
}
//---------------------------------------------------------------------------
/**
   Set the volume of the instance. This does not affect othe
   instances of the same sample.
*/
void TSampleInstance::SetVolume( int vol )
{
	if( InstanceData != NULL )
		Mix_Volume( InstanceData->Channel, vol );
}
//---------------------------------------------------------------------------
/**
   @return The volume of the instance.
*/
int TSampleInstance::GetVolume() const
{
	if( InstanceData != NULL )
		return Mix_Volume( InstanceData->Channel, -1 );
	return 0;
}
//---------------------------------------------------------------------------
/**
   The TSampleInstance object will connect this function to the
   \Ref{TSampleInstanceData::FinishedPlaying} signal in {\it Data}.
   The idea is that once the mixer has called this it can free
   the data object and not risk causing segfaults in the main
   program.
   
   @see TSampleInstance::InstanceData
*/
void TSampleInstance::FinishedPlaying()
{
	LOG( 9, "%s for isntance data %p", __FUNCTION__, InstanceData );

	InstanceData = NULL;
}
//--------------------------------------------------------------------------- 
TMixer::TMixer()
{
	if( this != &Mixer ) {
		throw EMixerError("Someone tried to crete a second TMixer.  Use ksd::Mixer.");
	}
}
//--------------------------------------------------------------------------- 
TMixer::~TMixer()
{
	LOG_FUNCTION(__PRETTY_FUNCTION__);
   
	// It really seems like this ought to call Close(), but the causes
	// a crash since this destructor is run after SDL is shutdown.
}
//--------------------------------------------------------------------------- 
/**
   Initialize the mixer. Mixer parameters are provided as
   seperate values. No other mixer functions can be used before
   this is called.
*/
void TMixer::Init(int sampling_rate, Uint16 format,int output_channels, int chunksize )
{
   
   
	if( Mix_OpenAudio(sampling_rate, format, output_channels, chunksize) < 0 ) {
		// an error occured
		throw EMixerError("Cannot initialize the audio mixer.");
	}

	Mix_HookMusicFinished( TMixer::MusicFinishedCallback );
	Mix_SetPostMix( TMixer::PostMixCallback, NULL );
}
//--------------------------------------------------------------------------- 
/**
   @memo Close the mixer.
   Stop all playing samples and music and close the mixer.
*/
void TMixer::Close()
{
	LOG_ENTER_FUNC(__PRETTY_FUNCTION__);
	/*StopMusic();

	PlayingInstances_Lock.Lock();
	TPlayingInstanceList::iterator iter = PlayingInstances.begin();

	for( ; iter < PlayingInstances.end(); iter++ )
	{
	LOG( 9, "Stop instance in instance data %p", (*iter) );

	if( (*iter)->Instance != NULL )
	(*iter)->Instance->Stop();
	}
	PlayingInstances_Lock.Unlock();
	*/
	Mix_CloseAudio();
	LOG_EXIT_FUNC(__PRETTY_FUNCTION__);
}
//--------------------------------------------------------------------------- 
/**
   Set the number of mixer channels (you should never {\it need}
   to do this). This is useful if you know you are going to be
   using a lot of channels and don't want them to be allocated
   dynamicly.
*/
int TMixer::SetChannels( int channels )
{
	PlayingInstances.reserve( channels );
	return Mix_AllocateChannels( channels );
}
//--------------------------------------------------------------------------- 
/**
   @return The number channels currently allocated.
   @see TMixer::SetChannels
*/
int TMixer::GetChannels() const
{
	int ret;
	Mix_QuerySpec( NULL, NULL, &ret );
	return ret;
}
//--------------------------------------------------------------------------- 
/**
   @return The sampling rate of the mixer.
*/
int TMixer::GetSamplingRate() const
{
	int ret;
	Mix_QuerySpec( &ret, NULL, NULL );
	return ret;
}
//--------------------------------------------------------------------------- 
/**
   @return The size of the chunk beginning mixed and sent to the audio device.
*/
int TMixer::GetChunkSize() const
{
	// *** IMPLEMENT
	throw EMixerError("GetChunkSize is not implemenented, yet");
   
	return -1;
}
//--------------------------------------------------------------------------- 
/**
   @return The sample format.
*/
Uint16 TMixer::GetFormat() const
{
	Uint16 ret;
	Mix_QuerySpec( NULL, &ret, NULL );
	return ret;
}
//--------------------------------------------------------------------------- 
/**
   Start playing {\it sample}. It will be looped {\it loops}
   times, faded in for {\it fadein} ms and stoped after {\it
   playfor} ms.
*/
std::auto_ptr<TSampleInstance> TMixer::StartSample( TSample& sample, int loops,
													int volume, int fadein,
													int playfor )
	throw( EMixerError )
{
	LOG_ENTER_FUNC(__PRETTY_FUNCTION__);
	 
	int channel = Mix_GroupAvailable( -1 );
	if( channel < 0 ) {
		SetChannels( GetChannels() + 4 );
		channel = Mix_GroupAvailable( -1 );
      
		if( channel < 0 ) {
			throw EMixerError("Cannot setup a channel for the sample.");
		}
	}

	Mix_Volume( channel, volume );

	int mix_err = 0;

	if( fadein == 0 ) {
		mix_err = Mix_PlayChannelTimed( channel, sample.SampleData, loops - 1, playfor );
	} else {
		mix_err = Mix_FadeInChannelTimed( channel, sample.SampleData, loops - 1,
										  fadein, playfor );
	}

	if( mix_err < 0 ) {
		throw EMixerError("Cannot play sample.");
	}

	// Increment the ref count
	sample.ReferenceCount++;
   
	TSampleInstance::Data* data = new TSampleInstance::Data(channel, &sample);

	PlayingInstances_Lock.Lock();   
	PlayingInstances.push_back(data);
	PlayingInstances_Lock.Unlock();
   
	LOG_EXIT_FUNC(__PRETTY_FUNCTION__);
	return std::auto_ptr<TSampleInstance>(new TSampleInstance(data));
}
//--------------------------------------------------------------------------- 
/**
   Start playing {\it song}. It will be looped {\it loops}
   time and faded in for {\it fadein} ms.
*/
void TMixer::StartMusic( TSong& song, int loops, int fadein ) throw(EMixerError)
{
	int ret;
	if( fadein == 0 ) {
		ret = Mix_PlayMusic( song.SongData, loops );
	} else {
		ret = Mix_FadeInMusic( song.SongData, loops, fadein );
	}

	if( ret < 0 ) {
		throw EMixerError("Cannot start music.");
	}

	// This is not very good. I need to implement some sort of reference counting.
	song.InUse = true;
	CurrentSong = &song;
}
//--------------------------------------------------------------------------- 
/**
   Pause the music.
*/
void TMixer::PauseMusic()
{
	Mix_PauseMusic();
}
//--------------------------------------------------------------------------- 
/**
   Resume playing the music.
*/
void TMixer::ResumeMusic()
{
	Mix_ResumeMusic();
}
//--------------------------------------------------------------------------- 
/**
   Rewind the music.
*/
void TMixer::RewindMusic()
{
	Mix_RewindMusic();
}
//--------------------------------------------------------------------------- 
/**
   Stop the music after fading for {\it fadeout} ms.
*/
void TMixer::StopMusic( int fadeout )
{
	LOG_ENTER_FUNC(__PRETTY_FUNCTION__);
	if( fadeout == 0 ) {
		Mix_HaltMusic();
	} else {
		Mix_FadeOutMusic( fadeout );
	}
	LOG_EXIT_FUNC(__PRETTY_FUNCTION__);
}
//--------------------------------------------------------------------------- 
/**
   @return true if music is paused and false otherwise.
*/
bool TMixer::IsMusicPaused() const
{
	if( Mix_PausedMusic() && Mix_PlayingMusic() )
		return true;
	return false;
}
//--------------------------------------------------------------------------- 
/**
   @return true if music is playing and false otherwise.
*/
bool TMixer::IsMusicPlaying() const
{
	if( ! Mix_PausedMusic() && Mix_PlayingMusic() )
		return true;
	return false;
}
//--------------------------------------------------------------------------- 
bool TMixer::IsMusicStopped() const
{
	if( Mix_PausedMusic() && ! Mix_PlayingMusic() )
		return true;
	return false;
}
//--------------------------------------------------------------------------- 
/**
   Set the music volume.
*/
void TMixer::SetMusicVolume( int vol )
{
	Mix_VolumeMusic( vol );
}
//--------------------------------------------------------------------------- 
/**
   Get the music volume.
   @return the current music volume.
*/
int TMixer::GetMusicVolume() const
{
	return Mix_VolumeMusic( -1 );
}
//--------------------------------------------------------------------------- 
void TMixer::MusicFinishedCallback()
{
	// Maybe I should implement some sort of reference counting, but
	// since only one song can be playin at a time it should be fine.
	Mixer.CurrentSong->InUse = false;
	Mixer.CurrentSong = NULL;

	// this is an attempt to force all types of music rewind when they
	// are stoped (as opposed to paused). I'm not sure it
	// works. Without help SDL_mixer will restart from the beginning on
	// MOD music, but not MP3.
	Mixer.RewindMusic();
   
	Mixer.MusicFinished.emit();
}
//--------------------------------------------------------------------------- 
void TMixer::PostMixCallback( void *udata, Uint8 *stream, int len )
{
	Mixer.CheckInstances();
}
//--------------------------------------------------------------------------- 
void TMixer::CheckInstances()
{
	TMutexLocker locker( PlayingInstances_Lock );
	TPlayingInstanceList::iterator iter = PlayingInstances.begin();

	for( ; iter < PlayingInstances.end(); iter++ ) {
		LOG( 9, "Checking instance data %p", (*iter) );
		
		if( ! Mix_Playing( (*iter)->Channel ) ) {
			LOG( 9, "Found finished instance data %p", (*iter) );
			// The channel is not playing. Tell the TSampleInstance that
			// it is done and delete the Data object than remove it from
			// the list.
			if( (*iter)->Instance != NULL )
				(*iter)->Instance->FinishedPlaying();
	 
			// Decrement the ref count
			(*iter)->Sample->ReferenceCount--;
			delete *iter;
			*iter = NULL; // <-- just to make sure.
			PlayingInstances.erase( iter );
			LOG( 9, "Cleaned finished instance data" );
		}
	}

	// Clear the InUse flag and rewind if the the music is stopped and
	// Mixer.CurrentSong is still set. That is do it is the music is
	// stopped, but MusicFinishedCallback has not been called, yet.
	if( IsMusicStopped() && Mixer.CurrentSong != NULL ) {
		// Maybe I should implement some sort of reference counting, but
		// since only one song can be playin at a time it should be fine.
		Mixer.CurrentSong->InUse = false;
		Mixer.CurrentSong = NULL;
      
		// this is an attempt to force all types of music rewind when they
		// are stoped (as opposed to paused). I'm not sure it
		// works. Without help SDL_mixer will restart from the beginning on
		// MOD music, but not MP3.
		Mixer.RewindMusic();
	}
}







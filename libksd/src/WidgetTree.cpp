//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	$Header$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Checked-in by:	$Author$
//	
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma implementation
#endif
//---------------------------------------------------------------------------
#define KSD_BUILD_LIB
#include <algorithm>
#include "WidgetTree.h"
#include "Widget.h"

using namespace ksd;
using namespace std;
//---------------------------------------------------------------------------  
void TWidgetTree::Add(TWidget* widget) 
{
	if(widget->GetParent() == NULL) {
		throw exception("TWidgetTree", "Trying to add a parentless widget to the widget tree!");
	} else {
		Tree.insert_first(widget->GetParent(), widget);
	}
}
//---------------------------------------------------------------------------  
void TWidgetTree::Remove(TWidget* widget)
{
	iterator widget_iter;
	reverse_iterator_range children;
	reverse_iterator i;

	// find the widget in the tree
	widget_iter = find_widget(widget);
	if(widget_iter == end()) {
		throw exception("TWidgetTree", "Trying to remove a widget that isn't in the widget tree!");
	}

	// get a list of this widget's children
	children = Tree.get_reverse_child_range(widget_iter);

	// delete the children -- NOTE: we do this in reverse order because the widgets
	// are stored in the tree in parent-before-child order.  If we did it the other
	// way arround, the children widgets would try to remove them selves from the 
	// tree after this widget had already removed them.
	for(i = children.begin(); i != children.end(); ) {
		// delete (which in turn removes it self from the list so we must do the 
		// post increment trick so that the iterator doesn't get invalidated).
		delete *i++;
	}

	// remove the widget from list
	Tree.erase(widget_iter);
}
//---------------------------------------------------------------------------  



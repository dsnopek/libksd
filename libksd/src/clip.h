//--------------------------------------------------------------------------- 
#ifndef _KSD_clipH_
#define _KSD_clipH_
//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//	
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		<none>
//	
//	Purpose:		Provides macros (instead of functions) for fast (simple) clipping
//					routines
//	
//	Checked-in by:	$Author$
//					$Date$
//--------------------------------------------------------------------------- 
// TODO: make sure macros can accept expressions correctly, adding "()"
//--------------------------------------------------------------------------- 
//	CLIP_HORZ_LINE(int& X, int& length, int& Y, int minx, int miny, int maxx, int maxy);
//		- NOTE: if length is set to zero, no line is to be drawn
#define CLIP_HORZ_LINE(X, length, Y, minx, miny, maxx, maxy) \
	{ \
		if(X < minx) { \
			length = length - (minx - X); \
			X = minx; \
		} \
		if(X > maxx || Y < miny || Y > maxy) { \
			length = 0; \
		} \
		if(length) { \
			 if(X + length > maxx) { \
			 	length = maxx - X; \
			 } \
		} \
	}
//--------------------------------------------------------------------------- 
//	CLIP_VERT_LINE(int& Y, int& length, int& X, int minx, int miny, int maxx, int maxy);
//		- NOTE: if length is set to zero, no line is to be drawn
#define CLIP_VERT_LINE(Y, length, X, minx, miny, maxx, maxy) \
	{ \
		if(Y < miny) { \
			length = length - (miny - Y); \
			Y = miny; \
		} \
		if(Y > maxy || X < minx || X > maxx) { \
			length = 0; \
		} \
		if(length) { \
			if(Y + length > maxy) { \
				length = maxy - Y; \
			} \
		} \
	}
//--------------------------------------------------------------------------- 
//	bool CLIP_POINT(int X, int Y, int minx, int miny, int maxx, int maxy)
//	- returns true when point should be drawn, false otherwise
#define CLIP_POINT(X, Y, minx, miny, maxx, maxy) \
	((X) >= (minx) && (X) <= (maxx) && (Y) >= (miny) && (Y) <= (maxy))
//--------------------------------------------------------------------------- 
// Swaps sides to make a valid rect
#define MAKE_RECT_VALID(Left, Top, Right, Bottom) \
	{ \
		int temp; \
		if(Left > Right) { \
			temp = Left; \
			Left = Right; \
			Right = temp; \
		} \
		if(Top > Bottom) { \
			temp = Top; \
			Top = Bottom; \
			Bottom = temp; \
		} \
	}
//--------------------------------------------------------------------------- 
//	Determines if rect is visible
#define IS_RECT_VISIBLE(Left, Top, Right, Bottom, minx, miny, maxx, maxy) \
	(!(Left > maxx || Right < minx || Top > maxy || Bottom < miny))
//--------------------------------------------------------------------------- 
//	Simple clip rect - only works in visible/valid rects
#define CLIP_RECT(Left, Top, Right, Bottom, minx, miny, maxx, maxy) \
	{ \
		if(Left < minx) Left = minx; \
		if(Top < miny) Top = miny; \
		if(Right > maxx) Right = maxx; \
		if(Bottom > maxy) Bottom = maxy; \
	}
//---------------------------------------------------------------------------  
//  Check for a valid clip rect
#define IS_VALID_CLIP(minx, miny, maxx, maxy) \
	(!(maxx <= minx && maxy <= miny))
//--------------------------------------------------------------------------- 
#endif

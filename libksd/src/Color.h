/*
  libuta - a C++ widget library based on SDL (Simple Direct Layer)
  Copyright (C) 1999  Karsten Laux

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Library General Public
  License as published by the Free Software Foundation; either
  version 2 of the License, or (at your option) any later version.
  
  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Library General Public License for more details.
  
  You should have received a copy of the GNU Library General Public
  License along with this library; if not, write to the
  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
  Boston, MA  02111-1307, SA.
*/
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			Karsten Laux 
//	Revised by:		David Snopek
//	Version:		$Revision$
//	
//	Objects:		TColor
//	
//	Purpose:		Color object
//	
//	Checked-in by:	$Author$
//					$Date$
//--------------------------------------------------------------------------- 
#ifndef _KSD_ColorH_
#define _KSD_ColorH_
//--------------------------------------------------------------------------- 
#include <SDL/SDL_types.h>
#include <algorithm>
#include "export_ksd.h"
//---------------------------------------------------------------------------
#ifdef __GNUG__
#pragma interface
#endif
//---------------------------------------------------------------------------
namespace ksd {
//---------------------------------------------------------------------------
KSD_EXPORT_CLASS class TColor
{
public:
	TColor();
	//TColor(SDL_Color* _color);
	TColor(Uint8 red, Uint8 green, Uint8 blue, Uint8 alpha = 0x00);

	//void CopyTo(SDL_Color*) const; // copy into an SDL_Color

	// this only compares colors (without alpha channel !)
	bool operator == (const TColor& V) const
	{
		return V.Red == Red && V.Green == Green && V.Blue == Blue;
	} 

	// this only compares colors (without alpha channel !)
	bool operator != (const TColor& V) const
	{
		return V.Red != Red || V.Green != Green || V.Blue != Blue;
	}

	Uint8 Red, Green, Blue, Alpha;

	// color operations (TODO: add more, including scalars)
	TColor operator + (const TColor& _c) const {
		return TColor(std::min(Red + _c.Red, 255), 
					  std::min(Green + _c.Green, 255),
					  std::min(Blue + _c.Blue, 255),
					  std::min(Alpha + _c.Alpha,255));
	}

	TColor operator - (const TColor& _c) const {
		return TColor(std::max(Red - _c.Red, 255), 
					  std::max(Green - _c.Green, 255),
					  std::max(Blue - _c.Blue, 255),
					  std::max(Alpha - _c.Alpha, 255));
	}

	TColor operator + (int scalar) const {
		return TColor(std::min(Red + scalar, 255),
					  std::min(Green + scalar, 255),
					  std::min(Blue + scalar, 255),
					  std::min(Alpha + scalar, 255));
	}

	TColor operator - (int scalar) const {
		return TColor(std::max(Red - scalar, 0),
					  std::max(Green - scalar, 0),
					  std::max(Blue - scalar, 0),
					  std::max(Alpha - scalar, 0));
	}
public:
    static TColor transparent; 
    static TColor darkgrey;
    static TColor darkgreen;
    static TColor darkslateblue;
    static TColor lightblue;
    static TColor grey;
    static TColor lightgrey;
    static TColor seagreen;
    static TColor snow ;
    static TColor GhostWhite ;
    static TColor WhiteSmoke ;
    static TColor gainsboro ;
    static TColor FloralWhite ;
    static TColor OldLace ;
    static TColor linen ;
    static TColor AntiqueWhite ;
    static TColor PapayaWhip ;
    static TColor BlanchedAlmond ;
    static TColor bisque ;
    static TColor PeachPuff ;
    static TColor NavajoWhite ;
    static TColor moccasin ;
    static TColor cornsilk ;
    static TColor ivory ;
    static TColor LemonChiffon ;
    static TColor seashell ;
    static TColor honeydew ;
    static TColor MintCream ;
    static TColor azure ;
    static TColor AliceBlue ;
    static TColor lavender ;
    static TColor LavenderBlush ;
    static TColor MistyRose ;
    static TColor white ;
    static TColor black ;
    static TColor DarkSlateGray ;
    static TColor DarkSlateGrey ;
    static TColor DimGray ;
    static TColor DimGrey ;
    static TColor SlateGray ;
    static TColor SlateGrey ;
    static TColor LightSlateGray ;
    static TColor LightSlateGrey ;
    static TColor gray ;
    static TColor LightGrey ;
    static TColor LightGray ;
    static TColor MidnightBlue ;
    static TColor navy ;
    static TColor NavyBlue ;
    static TColor CornflowerBlue ;
    static TColor DarkSlateBlue ;
    static TColor SlateBlue ;
    static TColor MediumSlateBlue ;
    static TColor LightSlateBlue ;
    static TColor MediumBlue ;
    static TColor RoyalBlue ;
    static TColor blue ;
    static TColor DodgerBlue ;
    static TColor DeepSkyBlue ;
    static TColor SkyBlue ;
    static TColor LightSkyBlue ;
    static TColor SteelBlue ;
    static TColor LightSteelBlue ;
    static TColor LightBlue ;
    static TColor PowderBlue ;
    static TColor PaleTurquoise ;
    static TColor DarkTurquoise ;
    static TColor MediumTurquoise ;
    static TColor turquoise ;
    static TColor cyan ;
    static TColor LightCyan ;
    static TColor CadetBlue ;
    static TColor MediumAquamarine ;
    static TColor aquamarine ;
    static TColor DarkGreen ;
    static TColor DarkOliveGreen ;
    static TColor DarkSeaGreen ;
    static TColor SeaGreen ;
    static TColor MediumSeaGreen ;
    static TColor LightSeaGreen ;
    static TColor PaleGreen ;
    static TColor SpringGreen ;
    static TColor LawnGreen ;
    static TColor green ;
    static TColor chartreuse ;
    static TColor MediumSpringGreen ;
    static TColor GreenYellow ;
    static TColor LimeGreen ;
    static TColor YellowGreen ;
    static TColor ForestGreen ;
    static TColor OliveDrab ;
    static TColor DarkKhaki ;
    static TColor khaki ;
    static TColor PaleGoldenrod ;
    static TColor LightGoldenrodYellow ;
    static TColor LightYellow ;
    static TColor yellow ;
    static TColor 	gold ;
    static TColor LightGoldenrod ;
    static TColor goldenrod ;
    static TColor DarkGoldenrod ;
    static TColor RosyBrown ;
    static TColor IndianRed ;
    static TColor SaddleBrown ;
    static TColor sienna ;
    static TColor peru ;
    static TColor burlywood ;
    static TColor beige ;
    static TColor wheat ;
    static TColor SandyBrown ;
    static TColor tan ;
    static TColor chocolate ;
    static TColor firebrick ;
    static TColor brown ;
    static TColor DarkSalmon ;
    static TColor salmon ;
    static TColor LightSalmon ;
    static TColor orange ;
    static TColor DarkOrange ;
    static TColor coral ;
    static TColor LightCoral ;
    static TColor tomato ;
    static TColor OrangeRed ;
    static TColor red ;
    static TColor HotPink ;
    static TColor DeepPink ;
    static TColor pink ;
    static TColor LightPink ;
    static TColor PaleVioletRed ;
    static TColor maroon ;
    static TColor MediumVioletRed ;
    static TColor VioletRed ;
    static TColor magenta ;
    static TColor violet ;
    static TColor plum ;
    static TColor orchid ;
    static TColor MediumOrchid ;
    static TColor DarkOrchid ;
    static TColor DarkViolet ;
    static TColor BlueViolet ;
    static TColor purple ;
    static TColor MediumPurple ;
    static TColor thistle ;
    static TColor snow1 ;
    static TColor snow2 ;
    static TColor snow3 ;
    static TColor snow4 ;
    static TColor seashell1 ;
    static TColor seashell2 ;
    static TColor seashell3 ;
    static TColor seashell4 ;
    static TColor AntiqueWhite1 ;
    static TColor AntiqueWhite2 ;
    static TColor AntiqueWhite3 ;
    static TColor AntiqueWhite4 ;
    static TColor bisque1 ;
    static TColor bisque2 ;
    static TColor bisque3 ;
    static TColor bisque4 ;
    static TColor PeachPuff1 ;
    static TColor PeachPuff2 ;
    static TColor PeachPuff3 ;
    static TColor PeachPuff4 ;
    static TColor NavajoWhite1 ;
    static TColor NavajoWhite2 ;
    static TColor NavajoWhite3 ;
    static TColor NavajoWhite4 ;
    static TColor LemonChiffon1 ;
    static TColor LemonChiffon2 ;
    static TColor LemonChiffon3 ;
    static TColor LemonChiffon4 ;
    static TColor cornsilk1 ;
    static TColor cornsilk2 ;
    static TColor cornsilk3 ;
    static TColor cornsilk4 ;
    static TColor ivory1 ;
    static TColor ivory2 ;
    static TColor ivory3 ;
    static TColor ivory4 ;
    static TColor honeydew1 ;
    static TColor honeydew2 ;
    static TColor honeydew3 ;
    static TColor honeydew4 ;
    static TColor LavenderBlush1 ;
    static TColor LavenderBlush2 ;
    static TColor LavenderBlush3 ;
    static TColor LavenderBlush4 ;
    static TColor MistyRose1 ;
    static TColor MistyRose2 ;
    static TColor MistyRose3 ;
    static TColor MistyRose4 ;
    static TColor azure1 ;
    static TColor azure2 ;
    static TColor azure3 ;
    static TColor azure4 ;
    static TColor SlateBlue1 ;
    static TColor SlateBlue2 ;
    static TColor SlateBlue3 ;
    static TColor SlateBlue4 ;
    static TColor RoyalBlue1 ;
    static TColor RoyalBlue2 ;
    static TColor RoyalBlue3 ;
    static TColor RoyalBlue4 ;
    static TColor blue1 ;
    static TColor blue2 ;
    static TColor blue3 ;
    static TColor blue4 ;
    static TColor DodgerBlue1 ;
    static TColor DodgerBlue2 ;
    static TColor DodgerBlue3 ;
    static TColor DodgerBlue4 ;
    static TColor SteelBlue1 ;
    static TColor SteelBlue2 ;
    static TColor SteelBlue3 ;
    static TColor SteelBlue4 ;
    static TColor DeepSkyBlue1 ;
    static TColor DeepSkyBlue2 ;
    static TColor DeepSkyBlue3 ;
    static TColor DeepSkyBlue4 ;
    static TColor SkyBlue1 ;
    static TColor SkyBlue2 ;
    static TColor SkyBlue3 ;
    static TColor SkyBlue4 ;
    static TColor LightSkyBlue1 ;
    static TColor LightSkyBlue2 ;
    static TColor LightSkyBlue3 ;
    static TColor LightSkyBlue4 ;
    static TColor SlateGray1 ;
    static TColor SlateGray2 ;
    static TColor SlateGray3 ;
    static TColor SlateGray4 ;
    static TColor LightSteelBlue1 ;
    static TColor LightSteelBlue2 ;
    static TColor LightSteelBlue3 ;
    static TColor LightSteelBlue4 ;
    static TColor LightBlue1 ;
    static TColor LightBlue2 ;
    static TColor LightBlue3 ;
    static TColor LightBlue4 ;
    static TColor LightCyan1 ;
    static TColor LightCyan2 ;
    static TColor LightCyan3 ;
    static TColor LightCyan4 ;
    static TColor PaleTurquoise1 ;
    static TColor PaleTurquoise2 ;
    static TColor PaleTurquoise3 ;
    static TColor PaleTurquoise4 ;
    static TColor CadetBlue1 ;
    static TColor CadetBlue2 ;
    static TColor CadetBlue3 ;
    static TColor CadetBlue4 ;
    static TColor turquoise1 ;
    static TColor turquoise2 ;
    static TColor turquoise3 ;
    static TColor turquoise4 ;
    static TColor cyan1 ;
    static TColor cyan2 ;
    static TColor cyan3 ;
    static TColor cyan4 ;
    static TColor DarkSlateGray1 ;
    static TColor DarkSlateGray2 ;
    static TColor DarkSlateGray3 ;
    static TColor DarkSlateGray4 ;
    static TColor aquamarine1 ;
    static TColor aquamarine2 ;
    static TColor aquamarine3 ;
    static TColor aquamarine4 ;
    static TColor DarkSeaGreen1 ;
    static TColor DarkSeaGreen2 ;
    static TColor DarkSeaGreen3 ;
    static TColor DarkSeaGreen4 ;
    static TColor SeaGreen1 ;
    static TColor SeaGreen2 ;
    static TColor SeaGreen3 ;
    static TColor SeaGreen4 ;
    static TColor PaleGreen1 ;
    static TColor PaleGreen2 ;
    static TColor PaleGreen3 ;
    static TColor PaleGreen4 ;
    static TColor SpringGreen1 ;
    static TColor SpringGreen2 ;
    static TColor SpringGreen3 ;
    static TColor SpringGreen4 ;
    static TColor green1 ;
    static TColor green2 ;
    static TColor green3 ;
    static TColor green4 ;
    static TColor chartreuse1 ;
    static TColor chartreuse2 ;
    static TColor chartreuse3 ;
    static TColor chartreuse4 ;
    static TColor OliveDrab1 ;
    static TColor OliveDrab2 ;
    static TColor OliveDrab3 ;
    static TColor OliveDrab4 ;
    static TColor DarkOliveGreen1 ;
    static TColor DarkOliveGreen2 ;
    static TColor DarkOliveGreen3 ;
    static TColor DarkOliveGreen4 ;
    static TColor khaki1 ;
    static TColor khaki2 ;
    static TColor khaki3 ;
    static TColor khaki4 ;
    static TColor LightGoldenrod1 ;
    static TColor LightGoldenrod2 ;
    static TColor LightGoldenrod3 ;
    static TColor LightGoldenrod4 ;
    static TColor LightYellow1 ;
    static TColor LightYellow2 ;
    static TColor LightYellow3 ;
    static TColor LightYellow4 ;
    static TColor yellow1 ;
    static TColor yellow2 ;
    static TColor yellow3 ;
    static TColor yellow4 ;
    static TColor gold1 ;
    static TColor gold2 ;
    static TColor gold3 ;
    static TColor gold4 ;
    static TColor goldenrod1 ;
    static TColor goldenrod2 ;
    static TColor goldenrod3 ;
    static TColor goldenrod4 ;
    static TColor DarkGoldenrod1 ;
    static TColor DarkGoldenrod2 ;
    static TColor DarkGoldenrod3 ;
    static TColor DarkGoldenrod4 ;
    static TColor RosyBrown1 ;
    static TColor RosyBrown2 ;
    static TColor RosyBrown3 ;
    static TColor RosyBrown4 ;
    static TColor IndianRed1 ;
    static TColor IndianRed2 ;
    static TColor IndianRed3 ;
    static TColor IndianRed4 ;
    static TColor sienna1 ;
    static TColor sienna2 ;
    static TColor sienna3 ;
    static TColor sienna4 ;
    static TColor burlywood1 ;
    static TColor burlywood2 ;
    static TColor burlywood3 ;
    static TColor burlywood4 ;
    static TColor wheat1 ;
    static TColor wheat2 ;
    static TColor wheat3 ;
    static TColor wheat4 ;
    static TColor tan1 ;
    static TColor tan2 ;
    static TColor tan3 ;
    static TColor tan4 ;
    static TColor chocolate1 ;
    static TColor chocolate2 ;
    static TColor chocolate3 ;
    static TColor chocolate4 ;
    static TColor firebrick1 ;
    static TColor firebrick2 ;
    static TColor firebrick3 ;
    static TColor firebrick4 ;
    static TColor brown1 ;
    static TColor brown2 ;
    static TColor brown3 ;
    static TColor brown4 ;
    static TColor salmon1 ;
    static TColor salmon2 ;
    static TColor salmon3 ;
    static TColor salmon4 ;
    static TColor LightSalmon1 ;
    static TColor LightSalmon2 ;
    static TColor LightSalmon3 ;
    static TColor LightSalmon4 ;
    static TColor orange1 ;
    static TColor orange2 ;
    static TColor orange3 ;
    static TColor orange4 ;
    static TColor DarkOrange1 ;
    static TColor DarkOrange2 ;
    static TColor DarkOrange3 ;
    static TColor DarkOrange4 ;
    static TColor coral1 ;
    static TColor coral2 ;
    static TColor coral3 ;
    static TColor coral4 ;
    static TColor tomato1 ;
    static TColor tomato2 ;
    static TColor tomato3 ;
    static TColor tomato4 ;
    static TColor OrangeRed1 ;
    static TColor OrangeRed2 ;
    static TColor OrangeRed3 ;
    static TColor OrangeRed4 ;
    static TColor red1 ;
    static TColor red2 ;
    static TColor red3 ;
    static TColor red4 ;
    static TColor DeepPink1 ;
    static TColor DeepPink2 ;
    static TColor DeepPink3 ;
    static TColor DeepPink4 ;
    static TColor HotPink1 ;
    static TColor HotPink2 ;
    static TColor HotPink3 ;
    static TColor HotPink4 ;
    static TColor pink1 ;
    static TColor pink2 ;
    static TColor pink3 ;
    static TColor pink4 ;
    static TColor LightPink1 ;
    static TColor LightPink2 ;
    static TColor LightPink3 ;
    static TColor LightPink4 ;
    static TColor PaleVioletRed1 ;
    static TColor PaleVioletRed2 ;
    static TColor PaleVioletRed3 ;
    static TColor PaleVioletRed4 ;
    static TColor maroon1 ;
    static TColor maroon2 ;
    static TColor maroon3 ;
    static TColor maroon4 ;
    static TColor VioletRed1 ;
    static TColor VioletRed2 ;
    static TColor VioletRed3 ;
    static TColor VioletRed4 ;
    static TColor magenta1 ;
    static TColor magenta2 ;
    static TColor magenta3 ;
    static TColor magenta4 ;
    static TColor orchid1 ;
    static TColor orchid2 ;
    static TColor orchid3 ;
    static TColor orchid4 ;
    static TColor plum1 ;
    static TColor plum2 ;
    static TColor plum3 ;
    static TColor plum4 ;
    static TColor MediumOrchid1 ;
    static TColor MediumOrchid2 ;
    static TColor MediumOrchid3 ;
    static TColor MediumOrchid4 ;
    static TColor DarkOrchid1 ;
    static TColor DarkOrchid2 ;
    static TColor DarkOrchid3 ;
    static TColor DarkOrchid4 ;
    static TColor purple1 ;
    static TColor purple2 ;
    static TColor purple3 ;
    static TColor purple4 ;
    static TColor MediumPurple1 ;
    static TColor MediumPurple2 ;
    static TColor MediumPurple3 ;
    static TColor MediumPurple4 ;
    static TColor thistle1 ;
    static TColor thistle2 ;
    static TColor thistle3 ;
    static TColor thistle4 ;
    static TColor gray0 ;
    static TColor grey0 ;
    static TColor gray1 ;
    static TColor grey1 ;
    static TColor gray2 ;
    static TColor grey2 ;
    static TColor gray3 ;
    static TColor grey3 ;
    static TColor 	gray4 ;
    static TColor 	grey4 ;
    static TColor 	gray5 ;
    static TColor 	grey5 ;
    static TColor 	gray6 ;
    static TColor 	grey6 ;
    static TColor 	gray7 ;
    static TColor 	grey7 ;
    static TColor 	gray8 ;
    static TColor 	grey8 ;
    static TColor 	gray9 ;
    static TColor 	grey9 ;
    static TColor 	gray10 ;
    static TColor 	grey10 ;
    static TColor 	gray11 ;
    static TColor 	grey11 ;
    static TColor 	gray12 ;
    static TColor 	grey12 ;
    static TColor 	gray13 ;
    static TColor 	grey13 ;
    static TColor 	gray14 ;
    static TColor 	grey14 ;
    static TColor 	gray15 ;
    static TColor 	grey15 ;
    static TColor 	gray16 ;
    static TColor 	grey16 ;
    static TColor 	gray17 ;
    static TColor 	grey17 ;
    static TColor 	gray18 ;
    static TColor 	grey18 ;
    static TColor 	gray19 ;
    static TColor 	grey19 ;
    static TColor 	gray20 ;
    static TColor 	grey20 ;
    static TColor 	gray21 ;
    static TColor 	grey21 ;
    static TColor 	gray22 ;
    static TColor 	grey22 ;
    static TColor 	gray23 ;
    static TColor 	grey23 ;
    static TColor 	gray24 ;
    static TColor 	grey24 ;
    static TColor 	gray25 ;
    static TColor 	grey25 ;
    static TColor 	gray26 ;
    static TColor 	grey26 ;
    static TColor 	gray27 ;
    static TColor 	grey27 ;
    static TColor 	gray28 ;
    static TColor 	grey28 ;
    static TColor 	gray29 ;
    static TColor 	grey29 ;
    static TColor 	gray30 ;
    static TColor 	grey30 ;
    static TColor 	gray31 ;
    static TColor 	grey31 ;
    static TColor 	gray32 ;
    static TColor 	grey32 ;
    static TColor 	gray33 ;
    static TColor 	grey33 ;
    static TColor 	gray34 ;
    static TColor 	grey34 ;
    static TColor 	gray35 ;
    static TColor 	grey35 ;
    static TColor 	gray36 ;
    static TColor 	grey36 ;
    static TColor 	gray37 ;
    static TColor 	grey37 ;
    static TColor 	gray38 ;
    static TColor 	grey38 ;
    static TColor 	gray39 ;
    static TColor 	grey39 ;
    static TColor 	gray40 ;
    static TColor 	grey40 ;
    static TColor 	gray41 ;
    static TColor 	grey41 ;
    static TColor 	gray42 ;
    static TColor 	grey42 ;
    static TColor 	gray43 ;
    static TColor 	grey43 ;
    static TColor 	gray44 ;
    static TColor 	grey44 ;
    static TColor 	gray45 ;
    static TColor 	grey45 ;
    static TColor 	gray46 ;
    static TColor 	grey46 ;
    static TColor 	gray47 ;
    static TColor 	grey47 ;
    static TColor 	gray48 ;
    static TColor 	grey48 ;
    static TColor 	gray49 ;
    static TColor 	grey49 ;
    static TColor 	gray50 ;
    static TColor 	grey50 ;
    static TColor 	gray51 ;
    static TColor 	grey51 ;
    static TColor 	gray52 ;
    static TColor 	grey52 ;
    static TColor 	gray53 ;
    static TColor 	grey53 ;
    static TColor 	gray54 ;
    static TColor 	grey54 ;
    static TColor 	gray55 ;
    static TColor 	grey55 ;
    static TColor 	gray56 ;
    static TColor 	grey56 ;
    static TColor 	gray57 ;
    static TColor 	grey57 ;
    static TColor 	gray58 ;
    static TColor 	grey58 ;
    static TColor 	gray59 ;
    static TColor 	grey59 ;
    static TColor 	gray60 ;
    static TColor 	grey60 ;
    static TColor 	gray61 ;
    static TColor 	grey61 ;
    static TColor 	gray62 ;
    static TColor 	grey62 ;
    static TColor 	gray63 ;
    static TColor 	grey63 ;
    static TColor 	gray64 ;
    static TColor 	grey64 ;
    static TColor 	gray65 ;
    static TColor 	grey65 ;
    static TColor 	gray66 ;
    static TColor 	grey66 ;
    static TColor 	gray67 ;
    static TColor 	grey67 ;
    static TColor 	gray68 ;
    static TColor 	grey68 ;
    static TColor 	gray69 ;
    static TColor 	grey69 ;
    static TColor 	gray70 ;
    static TColor 	grey70 ;
    static TColor 	gray71 ;
    static TColor 	grey71 ;
    static TColor 	gray72 ;
    static TColor 	grey72 ;
    static TColor 	gray73 ;
    static TColor 	grey73 ;
    static TColor 	gray74 ;
    static TColor 	grey74 ;
    static TColor 	gray75 ;
    static TColor 	grey75 ;
    static TColor 	gray76 ;
    static TColor 	grey76 ;
    static TColor 	gray77 ;
    static TColor 	grey77 ;
    static TColor 	gray78 ;
    static TColor 	grey78 ;
    static TColor 	gray79 ;
    static TColor 	grey79 ;
    static TColor 	gray80 ;
    static TColor 	grey80 ;
    static TColor 	gray81 ;
    static TColor 	grey81 ;
    static TColor 	gray82 ;
    static TColor 	grey82 ;
    static TColor 	gray83 ;
    static TColor 	grey83 ;
    static TColor 	gray84 ;
    static TColor 	grey84 ;
    static TColor 	gray85 ;
    static TColor 	grey85 ;
    static TColor 	gray86 ;
    static TColor 	grey86 ;
    static TColor 	gray87 ;
    static TColor 	grey87 ;
    static TColor 	gray88 ;
    static TColor 	grey88 ;
    static TColor 	gray89 ;
    static TColor 	grey89 ;
    static TColor 	gray90 ;
    static TColor 	grey90 ;
    static TColor 	gray91 ;
    static TColor 	grey91 ;
    static TColor 	gray92 ;
    static TColor 	grey92 ;
    static TColor 	gray93 ;
    static TColor 	grey93 ;
    static TColor 	gray94 ;
    static TColor 	grey94 ;
    static TColor 	gray95 ;
    static TColor 	grey95 ;
    static TColor 	gray96 ;
    static TColor 	grey96 ;
    static TColor 	gray97 ;
    static TColor 	grey97 ;
    static TColor 	gray98 ;
    static TColor 	grey98 ;
    static TColor 	gray99 ;
    static TColor 	grey99 ;
    static TColor 	gray100 ;
    static TColor 	grey100 ;
    static TColor DarkGrey ;
    static TColor DarkGray ;
    static TColor DarkBlue ;
    static TColor DarkCyan ;
    static TColor DarkMagenta ;
    static TColor DarkRed ;
    static TColor LightGreen ;
};
//--------------------------------------------------------------------------- 
}; // namespace ksd
//--------------------------------------------------------------------------- 
#endif

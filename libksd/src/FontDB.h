//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//	
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		FontDB
//	
//	Purpose:		A template class meant to be used internally by TFont 
//					implementations.
//	
//	Checked-in by:	$Author$
//					$Date$
//--------------------------------------------------------------------------- 
#ifndef _KSD_FontDBH
#define _KSD_FontDBH
//--------------------------------------------------------------------------- 
#include <string>
#include <list>
#include "Font.h"
//--------------------------------------------------------------------------- 
namespace ksd {
//---------------------------------------------------------------------------  
template<class FontType, 
		 FontType* (*CreateFunc)(const TFontAttr&, const char*),
		 void (*CloseFunc)(FontType*)>
class FontDB {
public:
	FontDB() 	{ Filename = ""; }
	~FontDB() 	{ Clear(); }

	void SetFilename(const char* _filename) { 
		Clear(); Filename = std::string(_filename); 
	}
	void Optimize(TFontAttr* _list, int n);
	void Clear();

	FontType* GetFont(const TFontAttr&);
private:
	std::string Filename;
	std::list<std::pair<TFontAttr, FontType*> > List;
};
//---------------------------------------------------------------------------  
// TODO: it seems like I should be able to do this with a single nested loop
// but I haven't quite figured it out yet.
template<class FontType, 
		 FontType* (*CreateFunc)(const TFontAttr&, const char*),
		 void (*CloseFunc)(FontType*)>
void FontDB<FontType, CreateFunc, CloseFunc>::Optimize(TFontAttr* _list, int n)
{
	typename std::list<std::pair<TFontAttr, FontType*> >::iterator i;
	bool found;
	FontType* Font;

	// create default optimize list
	if(_list == NULL) {
		_list = new TFontAttr[1];
		_list[0] = DefaultFontAttr;
		n = 1;
	}

	// remove all fonts whose attrs are not on the list
	for(i = List.begin(); i != List.end(); i++) {
		found = false;
		for(int e = 0; e < n; e++) {
			if((*i).first == _list[e]) {
				found = true;
				break;
			}
		}
		if(!found) {
			CloseFunc((*i).second);
			List.erase(i);
		}
	}

	// add a font attrs on the list that aren't there
	for(int e = 0; e < n; e++) {
		for(i = List.begin(); i != List.end(); i++) {
			if((*i).first == _list[e]) break;
		}
		
		if(i == List.end()) {
			Font = CreateFunc(_list[e], Filename.c_str());
			if(Font)
				List.push_back(std::pair<TFontAttr, FontType*>(_list[e], Font));
		}
	}
}
//---------------------------------------------------------------------------  
template<class FontType, 
		 FontType* (*CreateFunc)(const TFontAttr&, const char*),
		 void (*CloseFunc)(FontType*)>
void FontDB<FontType, CreateFunc, CloseFunc>::Clear()
{
	for(typename std::list<std::pair<TFontAttr, FontType*> >::iterator i = List.begin(); i != List.end(); i++) {
		CloseFunc((*i).second);
	}
	List.clear();
	Filename = "";
}
//---------------------------------------------------------------------------  
template<class FontType, 
		 FontType* (*CreateFunc)(const TFontAttr&, const char*),
		 void (*CloseFunc)(FontType*)>
FontType* FontDB<FontType, CreateFunc, CloseFunc>::GetFont(const TFontAttr& attr)
{
	// cannot operate without a filename
	if(Filename == "")
		return NULL;

	typename std::list<std::pair<TFontAttr, FontType*> >::iterator i;
	FontType* font;

	// look for it
	//i = List.find(attr);
	for(i = List.begin(); i != List.end(); i++) {
		if((*i).first == attr) break;
	}

	// if not found --
	if(i == List.end()) {
		// create new
		font = CreateFunc(attr, Filename.c_str());

		// if creation is successful
		if(font)
			List.push_back(std::pair<TFontAttr, FontType*>(attr, font));
	} else {
		font = (*i).second;
	}

	return font;
}
//---------------------------------------------------------------------------  
}; // namespace ksd
//---------------------------------------------------------------------------  
#endif


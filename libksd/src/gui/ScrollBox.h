//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//	
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		TScrollBox
//	
//	Purpose:		Manages scrolling (both TScroller's and TScrollBar's).  Can act
//					as a decoration and normal widget.
//	
//	Checked-in by:	$Author$
//					$Date$
//--------------------------------------------------------------------------- 
#ifndef _KSD_ScrollBoxH_
#define _KSD_ScrollBoxH_
//--------------------------------------------------------------------------- 
#include "Control.h"
#include "Scroller.h"
#include "ScrollBar.h"
#include "Timer.h"
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma interface
#endif
//--------------------------------------------------------------------------- 
namespace ksd_gui {
//---------------------------------------------------------------------------  
KSDGUI_EXPORT_CLASS class TScrollBox : public TDecoration {
public:
	TScrollBox(ksd::TWidget* _parent);

	std::string GetControlName() const { return "ksd_scrollbox"; }

	SigC::Signal5<void, TScrollBox*, Uint16, Uint16, int, int> OnScroll;

	enum Type {
		NoScrollControls, UseScrollers, UseScrollBars
	};

	void SetType(Type _sc);
	void SetHorzScrolling(bool _enable);
	void SetVertScrolling(bool _enable);
	void SetHorzRange(Uint16 Range) 		{ HorzScrollBar->SetRange(Range); }
	void SetHorzIncrement(Uint16 Inc) 		{ HorzScrollBar->SetIncrement(Inc); }
	void SetVertRange(Uint16 Range)			{ VertScrollBar->SetRange(Range); }
	void SetVertIncrement(Uint16 Inc)		{ VertScrollBar->SetIncrement(Inc); }

	Type 	GetType() const 			{ return scroll_type; }
	bool 	GetHorzScrolling() const 	{ return HorzScroll; }
	bool 	GetVertScrolling() const 	{ return VertScroll; }
	Uint16 	GetHorzRange() const 		{ return HorzScrollBar->GetRange(); }
	Uint16	GetVertRange() const 		{ return VertScrollBar->GetRange(); }

	// used to retrieve scroll position
	ksd::TPoint2D GetScrollPosition() const	{ 
		return ksd::TPoint2D(HorzScrollBar->GetScrollPosition(),
						VertScrollBar->GetScrollPosition()); 
	}

	// used to set the speed of the scroller repeat delay/interval
	static void SetScrollerRepeatDelay(unsigned int _delay) 	  { ScrollerDelay = _delay; }
	static void SetScrollerRepeatInterval(unsigned int _interval) { ScrollerInterval = _interval; }
private:
	enum ScrollDir {
		None, Left, Up, Right, Down
	};

	Type scroll_type;
	TScrollBar* VertScrollBar, *HorzScrollBar;
	TScroller* TopScroller, *BottomScroller, *LeftScroller, *RightScroller;
	bool HorzScroll, VertScroll;
	ksd::TSystemTimer DelayTimer;
	ScrollDir Dir;
	Uint16 ScrollX, ScrollY;

	// scroller repeat times
	static unsigned int ScrollerDelay, ScrollerInterval;

	inline TScroller* InitScroller(TScroller* _scroller) {
		_scroller->OnPress.connect(slot(*this, &TScrollBox::OnScrollerPress));
		_scroller->OnRelease.connect(slot(*this, &TScrollBox::OnScrollerRelease));
		return _scroller;
	}

	inline TScrollBar* InitScrollBar(TScrollBar* _scrollbar) {
		_scrollbar->OnScroll.connect(slot(*this, &TScrollBox::OnScrollBar));
		return _scrollbar;
	}

	void DoScroll(Uint16 NewScrollX, Uint16 NewScrollY);

	void OnScrollerPress(TScroller*);
	void OnScrollerRelease(TScroller*);
	void OnScrollBar(TScrollBar*, Uint16, int);
	Uint32 OnDelay(Uint32);
};
//---------------------------------------------------------------------------  
}; // namespace ksd_gui
//---------------------------------------------------------------------------  
#endif


//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//	
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		TTheme
//	
//	Purpose:		Acts as a "flyweight" factory for TStyle objects.
//	
//	Checked-in by:	$Author$
//					$Date$
//--------------------------------------------------------------------------- 
#ifndef _KSD_ThemeH_
#define _KSD_ThemeH_
//--------------------------------------------------------------------------- 
#include "Control.h"
#include "PluginSys.h"
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma interface
#endif
//--------------------------------------------------------------------------- 
namespace ksd_gui {
//---------------------------------------------------------------------------  
KSDGUI_EXPORT extern char ThemeTypeName[]; // = "Theme"
//---------------------------------------------------------------------------
KSDGUI_EXPORT_CLASS class TTheme : public ksd::TPluginSystem<TControlStyle, ThemeTypeName> {
public:
	TTheme() : ksd::TPluginSystem<TControlStyle, ThemeTypeName>() { };

	// adapt TPluginSys interface to fit theme
	void			AddStyle(TControlStyle* style, const std::string& type) { Register(style, type); }
	void			AddStyle(TControlStyle* style) 							{ Register(style, style->GetSupportedType()); }
	TControlStyle* 	GetStyle(const std::string& ControlType) 				{ return GetPlugin(ControlType); }
	int				GetStyleCount() 										{ return GetPluginCount(); }
private:
	using ksd::TPluginSystem<TControlStyle, ThemeTypeName>::Register;
	using ksd::TPluginSystem<TControlStyle, ThemeTypeName>::GetPlugin;
	using ksd::TPluginSystem<TControlStyle, ThemeTypeName>::GetPluginCount;
};
//---------------------------------------------------------------------------  
// Theme manipulating functions
void 		AddTheme(const std::string& Name, TTheme* _theme);
void 		SetCurrentTheme(const std::string& Name);
TTheme* 	GetCurrentTheme();
std::string GetCurrentThemeName();
//---------------------------------------------------------------------------  
// created by TTheme when loading themes from XML.  Won't be implemented for 
// a little while.
/*class TSoftStyle : public TControlStyle {
public:
	//
};*/
//---------------------------------------------------------------------------  
}; // namespace ksd_gui
//---------------------------------------------------------------------------  
#endif


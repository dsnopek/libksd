//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		TTextControl
//	
//	Purpose:		The parent class of all control that allow text editing
//	
//	Checked-in by:	$Author$
//					$Date$
//---------------------------------------------------------------------------
#ifndef _KSD_TextControlH_
#define _KSD_TextControlH_
//---------------------------------------------------------------------------
#include "Control.h"
#include "Bevel.h"
#include "ScrollBox.h"
#include "TextBox.h"
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma interface
#endif
//---------------------------------------------------------------------------
namespace ksd_gui {
//---------------------------------------------------------------------------
KSDGUI_EXPORT_CLASS class TTextControl : public TControl {
public:
	TTextControl(ksd::TWidget* _parent);

	void SetBevelType(TBevel::Type _btype) { Bevel->SetType(_btype); }

	// selection accessors
	unsigned int GetSelectionStart() 	{ return SelLen > 0 ? SelStart : SelStart + SelLen; }
	unsigned int GetSelectionLength() 	{ return (unsigned int)abs(SelLen); }

	// make text attr more easily accessable
	typedef ksd::TTextBox::TextAttr TextAttr;
protected:
	ksd::TTextBox TextBox;

	void SetScrolling(bool _enable) { 
		AutoScroll = _enable; 
		if(AutoScroll) {
			ScrollBox->Show();
			UpdateScrollBox();
		} else {
			SetScroll(0, 0);
			ScrollBox->Hide(); 
		}
	}
	
	virtual bool ResizeEvent(int Width, int Height);
private:
	unsigned int SelStart; 
	int SelLen, Margin;
	TScrollBox* ScrollBox;
	TBevel* Bevel;
	bool AutoScroll;
	bool WasResized, Clicked;
	//TextAttr ActiveAttr;
	//unsigned int ActivePos;
	//bool UseActiveAttr;

	bool Draw(ksd::TCanvas* Canvas, DrawType dt);
	void KeyDown(const ksd::TKey& key);
	void MouseDown(int X, int Y, ksd::TMouseButton btn);
	void MouseUp(int X, int Y, ksd::TMouseButton btn);
	void MouseMove(int X, int Y, int RelX, int RelY, ksd::TMouseButton btn);
	void OnTextBoxChange();

	void OnScroll(TScrollBox*, Uint16 X, Uint16 Y, int, int) { SetScroll(X, Y); }
	void UpdateScrollBox() {
		int height = TextBox.GetHeight(), 
			width = TextBox.GetWidth(); // NOTE: this won't work with no_wrap

		// enable vert scrolling when needed
		if(ScrollBox->GetVertScrolling()) {
			if(height <= GetClientHeight()) {
				ScrollBox->SetVertScrolling(false);
				WasResized = true;
				NeedsRepaint();
			}
		} else {
			if(height > GetClientHeight()) {
				ScrollBox->SetVertScrolling(true);
				WasResized = true;
				NeedsRepaint();
			}
		}

		// enable horz scrolling when needed
		if(ScrollBox->GetHorzScrolling()) {
			if(width <= GetClientWidth()) {
				ScrollBox->SetHorzScrolling(false);
				NeedsRepaint();
			}
		} else {
			if(width > GetClientWidth()) {
				ScrollBox->SetHorzScrolling(true);
				NeedsRepaint();
			}
		}
	}
};
//---------------------------------------------------------------------------  
}; // namespace ksd_gui
//---------------------------------------------------------------------------  
#endif


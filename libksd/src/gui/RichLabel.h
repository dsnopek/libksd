//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		TRichLabel
//	
//	Purpose:		A static text widget that can contain text with multiple
//					text attributes.
//	
//	Checked-in by:	$Author$
//					$Date$
//---------------------------------------------------------------------------
#ifndef _KSD_RichLabelH_
#define _KSD_RichLabelH_
//---------------------------------------------------------------------------
#include "Control.h"
#include "TextBox.h"
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma interface
#endif
//---------------------------------------------------------------------------
namespace ksd_gui {
//---------------------------------------------------------------------------
KSDGUI_EXPORT_CLASS class TRichLabel : public TControl {
public:
	TRichLabel(ksd::TWidget* _parent);

	std::string GetControlName() const { return "ksd_label"; }

	// move TextAttr up for convience
	typedef ksd::TTextBox::TextAttr TextAttr;

	// functions for set text and its attributes
	void SetText(const std::string& text) {
		try {
			TextBox.SetText(text, TextBox.GetTextAttr(0));
		} catch(...) {
			LOG(1, "Unable to set label text");
		}
	}
	void SetTextAttr(const TextAttr& attr, unsigned int Pos = 0, unsigned int Len = 0) {
		if(Len > 0) {
			TextBox.SetTextAttr(attr, Pos, Len);
		} else {
			TextBox.SetTextAttr(attr, Pos);
		}
	}
protected:
	ksd::TTextBox TextBox;
private:
	bool ResizeEvent(int Width, int Height);
	bool Draw(ksd::TCanvas* Canvas, DrawType dt);
};
//---------------------------------------------------------------------------  
}; // namespace ksd_gui
//---------------------------------------------------------------------------  
#endif


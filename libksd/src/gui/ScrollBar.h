//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//	
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		TScrollBar
//	
//	Purpose:		Draws a standard scroll bar
//	
//	Checked-in by:	$Author$
//					$Date$
//--------------------------------------------------------------------------- 
#ifndef _KSD_ScrollBarH_
#define _KSD_ScrollBarH_
//--------------------------------------------------------------------------- 
#include "Control.h"
#include "Timer.h"
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma interface
#endif
//--------------------------------------------------------------------------- 
namespace ksd_gui {
//---------------------------------------------------------------------------  
KSDGUI_EXPORT_CLASS class TScrollBar : public TDecoration {	
public:
	enum Type {
		Horizontal, 
		Verticle,
	};

	TScrollBar(ksd::TWidget* _parent, Type _type = Horizontal);

	std::string GetControlName() const { return "ksd_scroll_bar"; }

	SigC::Signal3<void, TScrollBar*, Uint16, int> OnScroll;

	void SetRange(Uint16 _range)		{ Range = _range; NeedsRepaint(); }
	void SetIncrement(Uint16 _inc)		{ Increment = _inc; NeedsRepaint(); }
	void SetPageIncrement(Uint16 _inc)	{ PageIncrement = _inc; NeedsRepaint(); }
	//void SetPosition(Uint16 _pos)		{ DoScroll(_pos - Position); }
	void SetType(Type _type) { 
		bar_type = _type;
		if(bar_type == Horizontal) {
			SetAlign(alBottom);
		} else if(bar_type == Verticle) {
			SetAlign(alRight);
		}
		
		NeedsRepaint(); 
	}

	// TODO: implement a DragIncrement, to allow apps to keep the scroll bar 
	// to a sane width ...
	Uint16 GetScrollPosition() const	{ return Position; }
	Uint16 GetRange() const 			{ return Range; }
	Uint16 GetIncrement() const 		{ return Increment; }
	Uint16 GetPageIncrement() const 	{ return PageIncrement; }
	Type GetType() const 				{ return bar_type; }

	// used to manually scroll
	void ScrollBy(int _count) { DoScroll(Increment * _count); }
	void ScrollTo(int _pos)   { DoScroll(_pos - Position); }

	// for setting the delay to auto increment
	static void SetRepaintDelay(unsigned int _delay) 		{ RepeatDelay = _delay; }
	static void SetRepaintInterval(unsigned int _interval) 	{ RepeatInterval = _interval; }
protected:
	bool Draw(ksd::TCanvas* Canvas, DrawType dt);

	void MouseDown(int X, int Y, ksd::TMouseButton button);
	void MouseUp(int X, int Y, ksd::TMouseButton button);
	void MouseMove(int X, int Y, int RelX, int RelY, ksd::TMouseButton button);
private:
	enum ScrollDir {
		None, Left, Up, Right, Down, Button
	};

	Type bar_type;
	ScrollDir Dir;
	int Position;
	Uint16 ScreenPosition, ScreenRange, Increment, Range, PageIncrement;
	Uint16 BarWidth, BarStart, BarClick;
	ksd::TSystemTimer DelayTimer;

	void DoScroll(int Rel);
	Uint32 OnDelay(Uint32);

	static unsigned int RepeatDelay, RepeatInterval;
};
//---------------------------------------------------------------------------  
}; // namespace ksd_gui
//---------------------------------------------------------------------------  
#endif


//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//	
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		TControl, TControlStyle, TDecoration, TDumbStyle
//	
//	Purpose:		Base class for control-type widgets, and the classes for 
//					theming them.  This is geared toward a specific wiget set
//					and theming mechanism.  Use TCustomWidget to make your own
//					widget with its own theming mechanisms or none at all.
//	
//	Checked-in by:	$Author$
//					$Date$
//--------------------------------------------------------------------------- 
#ifndef _KSD_ControlH_
#define _KSD_ControlH_
//--------------------------------------------------------------------------- 
#include <map>
#include "CustomWidget.h"
#include "export_ksdgui.h"
//---------------------------------------------------------------------------
#ifdef __GNUG__
#pragma interface
#endif
//---------------------------------------------------------------------------
namespace ksd_gui {

// forward decls
class TControl;
class TDecoration;
class TDumbStyle;
class TTheme;
//---------------------------------------------------------------------------
// All control styles should descend from TControlStyle..
KSDGUI_EXPORT_CLASS class TControlStyle {
public:
	virtual ~TControlStyle() { };

	// makes sure that the control is of the proper type
	inline bool Validate(TControl*);

	// returns the control type name of the control that this style works for.  This
	// is be the same value returned by TControl::GetControlName().
	virtual std::string GetSupportedType() = 0;

	// draws a specific part of a control identified by the "id".  If we are using
	// list style widgets the "index" refers to which element of the list and "pos"
	// refers to its offset (if the list is scrolled).
	virtual void Draw(TControl*, const std::string& id, ksd::TCanvas*,
					  int index = 0, const ksd::TRect& rect = ksd::TRect()) = 0;

	// gets the locations of different elements in the widget.  These can be the
	// elements of a list or buttons (ie. a scroll bar).  Item number 0 is always
	// the base client rect of the control...
	virtual ksd::TRect GetItemRect(TControl*, int index) = 0;
};
//---------------------------------------------------------------------------
KSDGUI_EXPORT_CLASS class TControl : public ksd::TCustomWidget {
public:
	TControl(ksd::TWidget* _parent, TControlStyle* _style = NULL) : ksd::TCustomWidget(_parent) {
		Style = _style;
	}
	virtual ~TControl() { };

	// some more fake RTTI -- used by TTheme
	virtual std::string GetControlName() const = 0;

	// accessors
	void SetStyle(TControlStyle* _style) {
		if(_style) {
			if(_style->Validate(this)) {
				Style = _style;
				_NeedsRealign(); // <-- is this needed ??
				NeedsRepaint();
			}
		} else Style = NULL; // <-- this is dangerous
	}
	TControlStyle*	GetStyle() const;
	bool GetUseCustomStyle() const { return Style ? 1 : 0; }

	void Resize(int w, int h);
protected:
	// to be used by child class to add decorations.  Some decorations include:
	// TBevel, and TScrollBar.
	void AddDecoration(TDecoration* _decor);

	// Deals with re-aligning the controls.  Be sure to call it from child classes.
	virtual bool Draw(ksd::TCanvas* Canvas, DrawType dt);

	// de-virginize (make un-pure :-) cycle function
	virtual void Cycle() { }
private:
	TControlStyle* Style;
	std::list<TDecoration*> DecorationList;

// borland c doesn't like us accessing private member functions from within a
// class of the same type
#ifdef __BORLANDC__
protected:
#endif

    // functions for dealing with realign internally
	inline void _NeedsRealign(); 	// forces the control to realign on next draw cycle
	inline bool _IsRealignTime(); 	// checks to see if its time to realign

	void Realign(); // adjust decoration positions to allow drawing
};
//---------------------------------------------------------------------------
KSDGUI_EXPORT_CLASS class TDecoration : public TControl { friend class TControl;
public:
	TDecoration(ksd::TWidget* _parent) : TControl(_parent) {
		RealignFlag = true; // might need to be true, but not sure
		Align = alNone;
		WidthConst = HeightConst = 0;
	}
	virtual ~TDecoration() { }

	// sets which part of the control the decoration should be attached to.
	// alNone = no alignment.  Used to "hide" a decoration.
	// alClient = changes the whole client rect -- occupies all sides
	// alLeft through alBottom = occupies specific side
	enum Alignment {
		alNone, alClient, alLeft, alRight, alTop, alBottom
	};

	Alignment GetAlign() const { return Align; }
protected:
	// used by decorations to decide where it shall attach.  Some controls will
	// allow this to be changes in some way but not neccessarily directly.
	// move to public if decoration allows.
	void SetAlign(Alignment _align) { Align = _align; RealignFlag = true; }

	// used by decoration to signify that its client rect has changed. (Note:
	// it can't actually _change_ the client rect per se.  It means it changed
	// modes of some sort and a refresh realign will effect that change.)
	void NeedsRealign() { SetRealignFlag(true); }

	// used to set the _minimum_ width/height !!  Important for decorations that
	// use alRight, alLeft, alTop, or alBottom.
	void SetConstraints(int _const_w, int _const_h) {
		WidthConst = _const_w;
		HeightConst = _const_h;

		if(!IsClientWidget()) {
			NeedsRealign();
		} else {
			// NOTE: this causes the original problem of resize in the const
			//Resize(_const_w, _const_h);
		}
	}

	// make sure to re-align when a decorations visibity is altered
	void VisibilityEvent(bool) {
		NeedsRealign();
	}
private:
	Alignment Align;
	bool RealignFlag;
	int WidthConst, HeightConst;

	// allow TControl to check and modify realign flag
	bool GetRealignFlag() const			{ return RealignFlag; }
	void SetRealignFlag(bool _enable) 	{ RealignFlag = _enable; }

	// make an embeded widget
	bool IsClientWidget() const { return false; }
};
//---------------------------------------------------------------------------
KSDGUI_EXPORT_CLASS class TDumbStyle : public TControlStyle {
public:
	TDumbStyle(const std::string& _name) {
		Name = _name;
	}

	std::string GetSupportedType() { return Name; }
	ksd::TRect GetItemRect(TControl* Control, int index) {
		if(index == 0) {
			return ksd::TRect(0, 0, Control->GetWidth(), Control->GetHeight());
		}
		return ksd::TRect(0, 0, 0, 0);
	}
	void Draw(TControl*, const std::string&, ksd::TCanvas*, int, const ksd::TRect&) { }
private:
	std::string Name;
};
//---------------------------------------------------------------------------  
inline bool TControlStyle::Validate(TControl* _control)
{
	return _control->GetControlName() == GetSupportedType();
}
//---------------------------------------------------------------------------  
inline void TControl::_NeedsRealign()
{
	if(DecorationList.size() > 0) {
		DecorationList.front()->SetRealignFlag(true);
	}
}
//---------------------------------------------------------------------------  
inline bool TControl::_IsRealignTime() 
{
	std::list<TDecoration*>::iterator i;
	for(i = DecorationList.begin(); i != DecorationList.end(); i++) {
		if((*i)->GetRealignFlag()) return true;
	}

	return false;
}
//---------------------------------------------------------------------------  
}; // namespace ksd_gui
//---------------------------------------------------------------------------  
#endif


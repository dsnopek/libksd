//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//	
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		TMemo
//	
//	Purpose:		A simple muti-line text edit control.
//	
//	Checked-in by:	$Author$
//					$Date$
//--------------------------------------------------------------------------- 
#ifndef _KSD_MemoH_
#define _KSD_MemoH_
//--------------------------------------------------------------------------- 
#include "TextControl.h"
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma interface
#endif
//--------------------------------------------------------------------------- 
namespace ksd_gui {
//--------------------------------------------------------------------------- 
KSDGUI_EXPORT_CLASS class TMemo : public TTextControl {
public:
	TMemo(ksd::TWidget* _parent);

	std::string GetControlName() const { return "ksd_memo"; }

	// accessors 
	int 			GetFontSize()	{ return TextAttr.FontAttr.Size; }
	Uint8 			GetFontStyle() 	{ return TextAttr.FontAttr.Style; }
	ksd::TColor 	GetFontColor()	{ return TextAttr.FontAttr.Color; }
	ksd::TFontAttr	GetFontAttr()	{ return TextAttr.FontAttr; }
	
	void SetTextAttr(ksd::TFont* Font, Uint8 _style, Uint8 _size, const ksd::TColor& _color) {
		TextAttr.Font = Font;
		TextAttr.FontAttr.Style = _style;
		TextAttr.FontAttr.Size = _size;
		TextAttr.FontAttr.Color = _color;

		TextBox.SetTextAttr(TextAttr);
		NeedsRepaint();
	}

	void SetFontSize(Uint8 _size) {
		TextAttr.FontAttr.Size = _size;
		TextBox.SetTextAttr(TextAttr);
		NeedsRepaint();
	}

	void SetFontStyle(Uint8 _style) {
		TextAttr.FontAttr.Style = _style;
		TextBox.SetTextAttr(TextAttr);
		NeedsRepaint();
	}

	void SetFontColor(const ksd::TColor& _color) {
		TextAttr.FontAttr.Color = _color;
		TextBox.SetTextAttr(TextAttr);
		NeedsRepaint();
	}
private:
	ksd::TTextBox::TextAttr TextAttr;
};
//---------------------------------------------------------------------------  
}; // namespace ksd_gui
//---------------------------------------------------------------------------  
#endif


//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//	
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		TBevel
//	
//	Purpose:		Draws a border of some kind
//	
//	Checked-in by:	$Author$
//					$Date$
//--------------------------------------------------------------------------- 
#ifndef _KSD_BevelH_
#define _KSD_BevelH_
//--------------------------------------------------------------------------- 
#include "Control.h"
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma interface
#endif
//--------------------------------------------------------------------------- 
namespace ksd_gui {
//---------------------------------------------------------------------------  
KSDGUI_EXPORT_CLASS class TBevel : public TDecoration {
public:
	TBevel(ksd::TWidget* _parent);

	std::string GetControlName() const { return "ksd_bevel"; }

	enum Type {
		None, Default, Raised, Lowered, Thick, Thin
	};

	void SetType(Type _type) {
		bevel_type = _type;
		NeedsRepaint();
		NeedsRealign();
	}
	Type GetType() const { return bevel_type; }
protected:
	bool Draw(ksd::TCanvas*, DrawType);
private:
	Type bevel_type;
};
//---------------------------------------------------------------------------
}; // namespace ksd_gui
//---------------------------------------------------------------------------
#endif


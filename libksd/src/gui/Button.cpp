//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	$Header$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Checked-in by:	$Author$
//	
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma implementation
#endif
//---------------------------------------------------------------------------
#define KSDGUI_BUILD_LIB
#include "Button.h"
#include "Image.h"
#include <iostream>

using namespace ksd;
using namespace ksd_gui;
using namespace std;
//---------------------------------------------------------------------------  
TButton::TButton(TWidget* _widget) : TControl(_widget)
{
	_state = Released;
	Clicked = Borderless = Over = false;
	TextSize = 12;
	text_align = TextRight;
	Image = NULL;

	SetDrawOnDemand(true);
	SetAllowChildren(false);
}
//---------------------------------------------------------------------------  
bool TButton::Draw(TCanvas* Canvas, DrawType dt)
{
	if(dt == REPAINT) {
		string id_name;

		if(!Borderless || Over) {
			if(IsEnabled()) {
				if(_state == Pressed)
					id_name = "pressed";
				else id_name = "released";
			} else id_name = "disabled";

			// draw button border
			GetStyle()->Draw(this, id_name, Canvas);
		}

		// draw the text
		Canvas->Font = GetFont();
		Canvas->FontAttr = TFontAttr(TFont::Normal, GetTextSize(), GetColor());
		if(GetCaption().size() > 0 && Canvas->Font) {
			int text_w, text_h, text_x, text_y, img_x = 0, img_y = 0;

			// calc image/text position
			Canvas->Font->GetTextSize(GetCaption(), Canvas->FontAttr, text_w, text_h);
			if(Image) {
				switch(text_align) {
					case TextLeft:
						text_x = (GetWidth() - (Image->GetWidth() + 3 + text_w)) / 2;
						text_y = (GetHeight() - text_h) / 2;
						img_x = text_x + text_w + 3;
						img_y = (GetHeight() - Image->GetHeight()) / 2;
						break;
					case TextRight:
						img_x = (GetWidth() - (Image->GetWidth() + 3 + text_w)) / 2;
						img_y = (GetHeight() - Image->GetHeight()) / 2;
						text_x = img_x + Image->GetWidth() + 3;
						text_y = (GetHeight() - text_h) / 2;
						break;
					case TextBottom:
						img_x = (GetWidth() - Image->GetWidth()) / 2;
						img_y = (GetHeight() - (Image->GetHeight() + text_h)) / 2;
						text_x = (GetWidth() - text_w) / 2;
						text_y = img_y + Image->GetHeight();
						break;
					case TextTop:
						text_x = (GetWidth() - text_w) / 2;
						text_y = (GetHeight() - (Image->GetHeight() + text_h)) / 2;
						img_x = (GetWidth() - Image->GetWidth()) / 2;
						img_y = text_y + text_h;
						break;
				}
			} else {
				text_x = (GetWidth() - text_w) / 2;
				text_y = (GetHeight() - text_h) / 2;
			}
						
			// shift the image/text on press
			if(_state == Pressed) {
				text_x += 1;
				text_y += 1;
				img_x++;
				img_y++;
			}

			// do actual drawing
			if(Image) {
				Canvas->Blit(img_x, img_y, *Image);
			}	
			Canvas->DrawString(text_x, text_y, GetCaption());
		} else {
			if(Image) {
				int img_x = (GetWidth() - Image->GetWidth()) / 2, 
					img_y = (GetHeight() - Image->GetHeight()) / 2;

				if(_state == Pressed) {
					img_x++;
					img_y++;
				}

				Canvas->Blit(img_x, img_y, *Image);
			} else {
				// complain when there is no font
				LOG(3, "Trying to draw button text with no font");
			}
		}

		return true;
	}

	return false;
}
//---------------------------------------------------------------------------  
void TButton::MouseDown(int X, int Y, TMouseButton button)
{
	// change to clicked
	_state = Pressed;
	Clicked = true;
	NeedsRepaint();
}
//---------------------------------------------------------------------------  
void TButton::MouseUp(int X, int Y, TMouseButton button)
{
	// check to see if the mouse is still over the button
	if(X < 0 || Y < 0 || X > GetWidth() || Y > GetHeight()) {
		Over = false;
	} else Over = true;
	if(Borderless) NeedsRepaint();

	// change to un-clicked
	if(_state == Pressed) {		
		_state = Released;
		OnPress(this); // call event handler
		NeedsRepaint();
	}
	Clicked = false;
}
//---------------------------------------------------------------------------  
void TButton::MouseMove(int X, int Y, int RelX, int RelY, TMouseButton button)
{
	if(Clicked) {
		if(X < 0 || Y < 0 || X > GetWidth() || Y > GetHeight()) {
			if(_state == Pressed) {
				_state = Released;
				NeedsRepaint();
			}
		} else if(_state == Released) {
			_state = Pressed;
			NeedsRepaint();
		}
	}
}
//---------------------------------------------------------------------------  
void TButton::Enter()
{	
	Over = true;
	if(Borderless) NeedsRepaint();
}
//---------------------------------------------------------------------------  
void TButton::Exit()
{
	Over = false;
	if(Borderless) NeedsRepaint();
}
//---------------------------------------------------------------------------


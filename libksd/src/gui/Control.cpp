//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	$Header$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Checked-in by:	$Author$
//
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma implementation
#endif
//--------------------------------------------------------------------------- 
#define KSDGUI_BUILD_LIB
#include <algorithm>
#include "Control.h"
#include "Theme.h"

using namespace ksd;
using namespace ksd_gui;
using namespace std;
//---------------------------------------------------------------------------  
// NOTE: very crucial function to the proper operation of the widget set!!
TControlStyle* TControl::GetStyle() const 
{
	// TODO: add diagnostic output!!  Not LOGGING since style/theme info should
	// always be available due to the dynamic nature of theming
	// NOTE: what if the style from the theme is not valid!!  Should problably 
	// notify user then either throw and exception or use the default!!

	if(Style)
		return Style;
	else {
		if(GetCurrentTheme()) {
			TControlStyle* temp = GetCurrentTheme()->GetStyle(GetControlName());
			if(temp)
				return temp;
		}
	}

	return NULL;
}
//---------------------------------------------------------------------------  
void TControl::AddDecoration(TDecoration* _decor)
{
	//_decor->SetEmbedInParent(true);
	DecorationList.push_back(_decor);
}
//---------------------------------------------------------------------------  
bool TControl::Draw(TCanvas* Canvas, DrawType dt) 
{
	// only deal with decorations if this control has them
	if(DecorationList.size() > 0) {
		if(_IsRealignTime()) Realign();
	}

	return false;
}
//---------------------------------------------------------------------------  
void TControl::Realign() 
{
	// TODO: re-align, remember to set ALL of the RealignFlags back to false

	TRect ClientRect;

	// reset client rect to base value
	if(GetStyle()) {
		ClientRect = GetStyle()->GetItemRect(this, 0);
	} else {
		LOG(3, "Trying to realign a \"%s\" control with no style", GetControlName().c_str());
		ClientRect.Set(0, 0, GetWidth(), GetHeight());
	}

	std::list<TDecoration*>::iterator i;
	TPoint2D pos;
	for(i = DecorationList.begin(); i != DecorationList.end(); i++) {
		// reset realign flag
		(*i)->SetRealignFlag(false);

		// only align the visible decorations
		if(!(*i)->IsVisible()) continue;
		
		switch((*i)->GetAlign()) {
			case TDecoration::alNone:
				// Do nothing with un-aligned decorations.  This means that the control
				// that owns it must mangage it.
				break;
			case TDecoration::alClient:
				// put in proper position
				(*i)->SetPosition(ClientRect.GetLeft(), ClientRect.GetTop());
				(*i)->Resize(ClientRect.GetWidth(), ClientRect.GetHeight());

				// if the decoration also needs to have its decorations realigned
				// then do so before we look at is client rect
				if((*i)->_IsRealignTime()) (*i)->Realign();

				// use that decorations client as this client rect
				pos = (*i)->GetPosition();
				ClientRect = (*i)->GetClientRect();
				ClientRect.Move(pos.X, pos.Y);
				break;
			case TDecoration::alLeft:
				(*i)->SetPosition(ClientRect.GetLeft(),
								  ClientRect.GetTop());
				(*i)->Resize(std::max((*i)->GetWidth(), (*i)->WidthConst),
							 ClientRect.GetHeight());

				// if the decoration also needs to have its decorations realigned
				// then do so before we look at is client rect
				if((*i)->_IsRealignTime()) (*i)->Realign();

				// add the widgets dimensions to the client rect
				ClientRect.SetLeft(ClientRect.GetLeft() + (*i)->GetWidth());
				break;
			case TDecoration::alTop:
				(*i)->SetPosition(ClientRect.GetLeft(),
								  ClientRect.GetTop());
				(*i)->Resize(ClientRect.GetWidth(), 
							 std::max((*i)->GetHeight(), (*i)->HeightConst));

				// if the decoration also needs to have its decorations realigned
				// then do so before we look at is client rect
				if((*i)->_IsRealignTime()) (*i)->Realign();

				// add the widgets dimensions to the client rect
				ClientRect.SetTop(ClientRect.GetTop() + (*i)->GetHeight());
				break;
			case TDecoration::alRight:
				(*i)->SetPosition(ClientRect.GetRight() -
								  std::max((*i)->GetWidth(), (*i)->WidthConst),
								  ClientRect.GetTop());
				(*i)->Resize(std::max((*i)->GetWidth(), (*i)->WidthConst),
							 ClientRect.GetHeight());

				// if the decoration also needs to have its decorations realigned
				// then do so before we look at is client rect
				if((*i)->_IsRealignTime()) (*i)->Realign();

				// add the widgets dimensions to the client rect
				pos = (*i)->GetPosition();
				ClientRect.SetRight(pos.X);
				break;
			case TDecoration::alBottom:
				(*i)->SetPosition(ClientRect.GetLeft(),
								  ClientRect.GetBottom() - 
								  std::max((*i)->GetHeight(), (*i)->HeightConst));
				(*i)->Resize(ClientRect.GetWidth(), 
							 std::max((*i)->GetHeight(), (*i)->HeightConst));

				// if the decoration also needs to have its decorations realigned
				// then do so before we look at is client rect
				if((*i)->_IsRealignTime()) (*i)->Realign();

				// add the widgets dimensions to the client rect
				pos = (*i)->GetPosition();
				ClientRect.SetBottom(pos.Y);
				break;
		}	
	}

	// set client rect
	SetClientRect(ClientRect);
}
//---------------------------------------------------------------------------  
void TControl::Resize(int _width, int _height) 
{
	TCustomWidget::Resize(_width, _height);

	_NeedsRealign();
	NeedsRepaint();

	// TODO: we shouldn't need to do this!!  This should be handled on the next
	// draw cycle when we re-align.  Comment this out to see the disaster
	if(GetStyle()) {
		SetClientRect(GetStyle()->GetItemRect(this, 0));
	} else {
		LOG(3, "Trying to resize a control with no style!!");
		SetClientRect(TRect(0, 0, GetWidth(), GetHeight()));
	}
}
//--------------------------------------------------------------------------- 


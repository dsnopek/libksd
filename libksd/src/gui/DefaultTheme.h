//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		DefaultBevelStyle
//	
//	Purpose:		Defines all of the default styles for the pre-built
//					widgets.  TApplication assembles a default theme based
//					on these.  This header should NOT be installed with the
//					other KSD headers; it is meant for internal use only!!
//	
//	Checked-in by:	$Author$
//					$Date$
//---------------------------------------------------------------------------
#ifndef _KSD_DefaultThemeH_
#define _KSD_DefaultThemeH_
//---------------------------------------------------------------------------
#include "Control.h"
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma interface
#endif
//---------------------------------------------------------------------------
using namespace ksd;
using namespace ksd_gui;
//---------------------------------------------------------------------------
class DefaultBevelStyle : public TControlStyle {
public:
	std::string GetSupportedType() { return "ksd_bevel"; }
	TRect 		GetItemRect(TControl*, int);
	void 		Draw(TControl*, const std::string& id, TCanvas* Canvas, 
					 int index, const TRect& Rect);
};
//---------------------------------------------------------------------------  
class DefaultButtonStyle : public TControlStyle {
public:
	std::string GetSupportedType() { return "ksd_button"; }
	TRect 		GetItemRect(TControl*, int);
	void 		Draw(TControl*, const std::string& id, TCanvas* Canvas, 
					 int index, const TRect& Rect);
};
//---------------------------------------------------------------------------  
class DefaultScrollerStyle : public TControlStyle {
public:
	std::string GetSupportedType() { return "ksd_scroller"; }
	TRect 		GetItemRect(TControl*, int);
	void 		Draw(TControl*, const std::string& id, TCanvas* Canvas, 
					 int index, const TRect& Rect);
};
//---------------------------------------------------------------------------  
class DefaultScrollBarStyle : public TControlStyle {
public:
	std::string GetSupportedType() { return "ksd_scroll_bar"; }
	TRect 		GetItemRect(TControl*, int);
	void 		Draw(TControl*, const std::string& id, TCanvas* Canvas, 
					 int index, const TRect& Rect);
};
//---------------------------------------------------------------------------  
// etc..
//---------------------------------------------------------------------------  
#endif



//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		TRichMemo
//	
//	Purpose:		A text widget that allows multiple fonts, sizes and colors 
//					to be used simultaneously.
//	
//	Checked-in by:	$Author$
//					$Date$
//---------------------------------------------------------------------------
#ifndef _KSD_RichMemoH_
#define _KSD_RichMemoH_
//---------------------------------------------------------------------------
#include "TextControl.h"
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma interface
#endif
//---------------------------------------------------------------------------
namespace ksd_gui {
//---------------------------------------------------------------------------
KSDGUI_EXPORT_CLASS class TRichMemo : public TTextControl {
public:
	TRichMemo(ksd::TWidget* _parent);

	std::string GetControlName() const { return "ksd_richmemo"; }

	// changes text attributes within a certain range
	void SetTextAttr(unsigned int start, unsigned int len, const TextAttr& attr) 	{ TextBox.SetTextAttr(attr, start, len); }
	void SetTextAttr(unsigned int pos, const TextAttr& attr) 						{ TextBox.SetTextAttr(attr, pos); }
	void SetTextAttr(const TextAttr& attr) { 
		unsigned int Len = GetSelectionLength();

		if(Len == 0) {
			TextBox.SetActiveAttr(attr, GetSelectionStart()); 
		} else {
			TextBox.SetTextAttr(attr, GetSelectionStart(), GetSelectionLength());
		}
	}

	// pass a function or functor style object that takes TextAttr& as its only
	// argument.  This allows you to modify the text attrs contained within the
	// specified range and effect only those.  This may (and most probably will)
	// involve adding more text attrs than were origanally present.
	template<typename FunctorType>
	void ChangeTextAttr(unsigned int Start, unsigned int Len, FunctorType func);

	// does the same as the function above except that it used the selection as
	// its range.
	template<typename FunctorType>
	void ChangeTextAttr(FunctorType func) {
		ChangeTextAttr(GetSelectionStart(), GetSelectionLength(), func);
	}
};
//---------------------------------------------------------------------------  
template<typename FunctorType>
void TRichMemo::ChangeTextAttr(unsigned int Start, unsigned int Len, FunctorType func)
{
	if(Len == 0 && Start == GetSelectionStart()) { // special case for point change
		try {
			TextAttr attr = TextBox.GetTextAttr(Start);
			func(attr);
			TextBox.SetActiveAttr(attr, GetSelectionStart());
		} catch(...) {
			LOG(1, "Unable to change text attr");
		}
	} else {
		std::vector<TextAttr*> List;
		TextAttr* First, *Last;

		try {
			// get (and add if required) first and last text attrs
			First = TextBox.GetUniqueTextAttr(Start);
			if(!First) { TextBox.SetTextAttr(TextBox.GetTextAttr(Start), Start); }
			Last = TextBox.GetUniqueTextAttr(Start + Len);
			if(!Last) { TextBox.SetTextAttr(TextBox.GetTextAttr(Start + Len), Start + Len); }

			// get list of applicable text attrs
			List = TextBox.GetUniqueTextAttrs(Start, Len - 1);

			// modify the list by func
			for(unsigned int i = 0; i < List.size(); i++) {
				func(*List[i]);
			}

			// update the text box
			TextBox.UpdateText(Start, Len);
			NeedsRepaint();
		} catch(...) {
			LOG(1, "Unable to change text attr");
		}
	}
}
//---------------------------------------------------------------------------  
}; // namespace ksd_gui
//---------------------------------------------------------------------------  
#endif


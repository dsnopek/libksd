/*
    libksd -- KewL STuFf DirectMedium
    Copyright 2000-2002 David Snopek

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef _KSD_export_ksdguiH
#define _KSD_export_ksdguiH

#include "Export.h"

/* header meat */
#ifdef KSDGUI_BUILD_LIB
# define KSDGUI_EXPORT_PLUGIN_FUNC DO_EXPORT_PLUGIN_FUNC
# define KSDGUI_EXPORT_CLASS DO_EXPORT_CLASS
# define KSDGUI_EXPORT_TEMPLATE DO_EXPORT_TEMPLATE
# define KSDGUI_EXPORT DO_EXPORT
#else
# define KSDGUI_EXPORT_PLUGIN_FUNC DO_IMPORT_PLUGIN_FUNC
# define KSDGUI_EXPORT_CLASS DO_IMPORT_CLASS
# define KSDGUI_EXPORT_TEMPLATE DO_IMPORT_TEMPLATE
# define KSDGUI_EXPORT DO_IMPORT
# define KSDGUI_IMPORTING_LIB 1
#endif

/* _KSD_export_ksdguiH */
#endif


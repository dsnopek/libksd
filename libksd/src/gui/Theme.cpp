//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	$Header$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Checked-in by:	$Author$
//
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma implementation
#endif
//--------------------------------------------------------------------------- 
#define KSDGUI_BUILD_LIB
#include <iostream>
#include "Application.h"
#include "Theme.h"
#include "DefaultTheme.h"

using namespace ksd;
using namespace ksd_gui;
using namespace std;
using namespace SigC;
//---------------------------------------------------------------------------  
char ksd_gui::ThemeTypeName[] = "Theme";
//---------------------------------------------------------------------------  
static std::map<std::string, TTheme*> ThemeList;
static TTheme* CurrentTheme = NULL;
//---------------------------------------------------------------------------  
static void ShutdownTheme();
//---------------------------------------------------------------------------  
void ksd_gui::AddTheme(const string& Name, TTheme* _theme)
{
	std::map<std::string, TTheme*>::iterator i = ThemeList.find(Name);
	if(i != ThemeList.end()) {
		// replace old themes of the same name with new theme
		if((*i).second)
			delete (*i).second;
		(*i).second = _theme;
	} else {
		//ThemeList[Name] = _theme; 
		ThemeList.insert(std::pair<std::string, TTheme*>(Name, _theme));
	}
}
//---------------------------------------------------------------------------  
void ksd_gui::SetCurrentTheme(const string& Name)
{
	std::map<std::string, TTheme*>::iterator i = ThemeList.find(Name);
	if(i != ThemeList.end()) {
		if((*i).second) {
			CurrentTheme = (*i).second;
			Application->NeedsRepaint();
		} else {
			cerr << "Cannot set theme to \"" << Name << "\" because it is NULL\n";
		}
	} else {
		cerr << "Cannot set theme to \"" << Name << "\" because no theme exists under that name\n";
	}
}
//---------------------------------------------------------------------------  
TTheme* ksd_gui::GetCurrentTheme()
{
	if(!CurrentTheme) {
		// construct the default theme
		CurrentTheme = new TTheme;
		CurrentTheme->AddStyle(new DefaultBevelStyle);
		CurrentTheme->AddStyle(new DefaultButtonStyle);
		CurrentTheme->AddStyle(new DefaultScrollerStyle);
		CurrentTheme->AddStyle(new DefaultScrollBarStyle);
		CurrentTheme->AddStyle(new TDumbStyle("ksd_memo"));
		CurrentTheme->AddStyle(new TDumbStyle("ksd_richmemo"));
		CurrentTheme->AddStyle(new TDumbStyle("ksd_label"));
		CurrentTheme->AddStyle(new TDumbStyle("ksd_panel"));
		CurrentTheme->AddStyle(new TDumbStyle("ksd_scrollbox"));
		ThemeList.insert(make_pair(string("default"), CurrentTheme));

		// add shutdown code to application
		Application->OnQuit.connect(slot(&ShutdownTheme));
	}

	return CurrentTheme;
}
//---------------------------------------------------------------------------  
string ksd_gui::GetCurrentThemeName()
{
	std::map<std::string, TTheme*>::const_iterator i;
	for(i = ThemeList.begin(); i != ThemeList.end(); i++) {
		if((*i).second == CurrentTheme)
			return (*i).first;
	}

	return "";
}
//---------------------------------------------------------------------------  
void ShutdownTheme()
{
	// destroy themes in list
	std::map<std::string, TTheme*>::iterator i;
	for(i = ThemeList.begin(); i != ThemeList.end(); i++) {
		delete (*i).second;
	}
}
//---------------------------------------------------------------------------  


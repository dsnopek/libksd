//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		TButton
//	
//	Purpose:		A standard button widget.
//	
//	Checked-in by:	$Author$
//					$Date$
//---------------------------------------------------------------------------
#ifndef _KSD_ButtonH_
#define _KSD_ButtonH_
//---------------------------------------------------------------------------
#include "Control.h"
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma interface
#endif
//---------------------------------------------------------------------------
namespace ksd_gui {
//---------------------------------------------------------------------------
KSDGUI_EXPORT_CLASS class TButton : public TControl {
public:
	TButton(ksd::TWidget* _widget);

	std::string GetControlName() const { return "ksd_button"; }

	// button press event
	SigC::Signal1<void, TButton*> OnPress;

	enum TextAlign {
		TextLeft, TextRight, TextTop, TextBottom
	};
	
	// gets
	int   			GetTextSize() 	{ return TextSize; }
	TextAlign		GetTextAlign()	{ return text_align; }
	ksd::TImage* 	GetImage()  	{ return Image; }
	bool			GetBorderless() { return Borderless; }
	
	// sets
	void SetTextSize(int _size) 		{ TextSize = _size; NeedsRepaint(); }
	void SetTextAlign(TextAlign _ta)	{ text_align = _ta; NeedsRepaint(); }
	void SetImage(ksd::TImage* _image)	{ Image = _image; NeedsRepaint(); }
	void SetBorderless(bool _enable)	{ Borderless = _enable; NeedsRepaint(); }
protected:
	bool Draw(ksd::TCanvas* Canvas, DrawType dt);
	void MouseDown(int X, int Y, ksd::TMouseButton button);
	void MouseUp(int X, int Y, ksd::TMouseButton button);
	void MouseMove(int X, int Y, int RelX, int RelY, ksd::TMouseButton button);
	void Enter();
	void Exit();
private:
	enum State {
		Pressed,
		Released
	};

	State _state;
	TextAlign text_align;
	bool Clicked, Over, Borderless;
	int TextSize;
	ksd::TImage* Image;
};
//---------------------------------------------------------------------------  
}; // namespace ksd_gui
//---------------------------------------------------------------------------  
#endif



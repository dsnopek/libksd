//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//	
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		TPanel
//	
//	Purpose:		Holds other controls.  Has a bevel and scroll box.
//	
//	Checked-in by:	$Author$
//					$Date$
//--------------------------------------------------------------------------- 
#ifndef _KSD_PanelH_
#define _KSD_PanelH_
//--------------------------------------------------------------------------- 
#include "Control.h"
#include "Bevel.h"
#include "ScrollBox.h"
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma interface
#endif
//--------------------------------------------------------------------------- 
namespace ksd_gui {
//---------------------------------------------------------------------------  
KSDGUI_EXPORT_CLASS class TPanel : public TControl {
public:
	TPanel(ksd::TWidget* _parent);

	std::string GetControlName() const { return "ksd_panel"; }

	void SetBevelType(TBevel::Type _btype) 					{ Bevel->SetType(_btype); NeedsRepaint(); }
	void SetScrollType(TScrollBox::Type _sc) 				{ ScrollBox->SetType(_sc); NeedsRepaint(); }
	void SetScrollBoxColor(const ksd::TColor& _c) 			{ ScrollBox->SetColor(_c); }
	void SetScrollBoxBackgroundColor(const ksd::TColor& _c)	{ ScrollBox->SetBackgroundColor(_c); }

	void SetHorzScrollRange(Uint16 Range) 				{ ScrollBox->SetHorzRange(Range); }
	void SetHorzScrollIncrement(Uint16 Inc)				{ ScrollBox->SetHorzIncrement(Inc); }
	void SetVertScrollRange(Uint16 Range)				{ ScrollBox->SetVertRange(Range); }
	void SetVertScrollIncrement(Uint16 Inc)				{ ScrollBox->SetVertIncrement(Inc); }

	TBevel::Type 		GetBevelType() const 	{ return Bevel->GetType(); }
	TScrollBox::Type 	GetScrollType() const	{ return ScrollBox->GetType(); }

	// expose double buffering
	using ksd::TCustomWidget::SetDoubleBuffered; // temp
private:
	TBevel* Bevel;
	TScrollBox* ScrollBox;

	void OnScroll(TScrollBox*, Uint16, Uint16, int, int);
};
//---------------------------------------------------------------------------  
}; // namespace ksd_gui
//---------------------------------------------------------------------------  
#endif


//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	$Header$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Checked-in by:	$Author$
//	
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma implementation
#endif
//---------------------------------------------------------------------------
#define KSDGUI_BUILD_LIB
#include "Scroller.h"
#include "Timer.h"

using namespace ksd;
using namespace ksd_gui;
using namespace SigC;
//---------------------------------------------------------------------------  
TScroller::TScroller(TWidget* _parent, Orientation _dir) : TDecoration(_parent)
{
	// scroller widget flags
	SetTransparentBackground(true);
	SetDrawOnDemand(true);
	SetAllowChildren(false);

	state = "on";
	Blink = true;
	Pressed = false;
	SetOrientation(_dir);

	// set-up timer callback
	TBlinkTimer* timer = GetBlinkTimer(1000, 500);
	timer->OnShow.connect(slot(*this, &TScroller::OnShowBtn));
	timer->OnHide.connect(slot(*this, &TScroller::OnHideBtn));
}
//---------------------------------------------------------------------------
void TScroller::SetOrientation(Orientation _dir)
{
	TRect ButtonSize = GetStyle()->GetItemRect(this, (int)_dir);
	SetConstraints(ButtonSize.GetWidth(), ButtonSize.GetHeight());
	
	scroller_dir = _dir;
	switch(scroller_dir) {
		case Left:
			SetAlign(alLeft);
			break;
		case Top:
			SetAlign(alTop);
			break;
		case Right:
			SetAlign(alRight);
			break;
		case Bottom:
			SetAlign(alBottom);
			break;
	}
	
	NeedsRepaint();		
}
//---------------------------------------------------------------------------  
void TScroller::SetBlink(bool _enable)
{
	Blink = _enable;
	NeedsRepaint();
}
//---------------------------------------------------------------------------  
bool TScroller::Draw(TCanvas* Canvas, DrawType dt)
{
	if(dt == REPAINT) {
		if(!Blink || Pressed) state = "on";
		GetStyle()->Draw(this, state, Canvas, (int)scroller_dir);
		return true;
	}

	return false;
}
//---------------------------------------------------------------------------  
void TScroller::MouseDown(int X, int Y, TMouseButton button)
{
	TRect ButtonRect = GetStyle()->GetItemRect(this, (int)scroller_dir);
	if(ButtonRect.IsInside(TPoint2D(X, Y))) {
		Pressed = true;
		OnPress(this);
		NeedsRepaint();
	}
}
//---------------------------------------------------------------------------  
void TScroller::MouseUp(int X, int Y, TMouseButton button)
{
	if(Pressed) {
		Pressed = false;
		OnRelease(this);
		NeedsRepaint();
	}
}
//---------------------------------------------------------------------------  
void TScroller::OnShowBtn()
{
	if(Blink) {
		state = "on";
		NeedsRepaint();
	}
}
//---------------------------------------------------------------------------  
void TScroller::OnHideBtn()
{
	if(Blink) {
		state = "off";
		NeedsRepaint();
	}
}
//---------------------------------------------------------------------------  


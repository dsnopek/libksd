//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		TScroller
//	
//	Purpose:		Provide an edge scroll button.  In pairs they perform the same 
//					function as a scroll bar.
//	
//	Checked-in by:	$Author$
//					$Date$
//---------------------------------------------------------------------------
#ifndef _KSD_ScrollerH_
#define _KSD_ScrollerH_
//---------------------------------------------------------------------------
#include "Control.h"
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma interface
#endif
//---------------------------------------------------------------------------
namespace ksd_gui {
//---------------------------------------------------------------------------
KSDGUI_EXPORT_CLASS class TScroller : public TDecoration {
public:
	enum Orientation {
		Left = 1, Top, Right, Bottom
	};

	TScroller(ksd::TWidget* _parent, Orientation _dir = Left);

	std::string GetControlName() const { return "ksd_scroller"; }

	// scroller press
	SigC::Signal1<void, TScroller*> OnPress;
	SigC::Signal1<void, TScroller*> OnRelease;

	// accessors
	void SetOrientation(Orientation _dir);
	void SetBlink(bool enable);
protected:
	bool Draw(ksd::TCanvas* Canvas, DrawType dt);
	void MouseDown(int X, int Y, ksd::TMouseButton);
	void MouseUp(int X, int Y, ksd::TMouseButton);

	void OnShowBtn();
	void OnHideBtn();
private:
	Orientation scroller_dir;
	std::string state;
	bool Blink, Pressed;
};
//---------------------------------------------------------------------------  
}; // namespace ksd_gui
//---------------------------------------------------------------------------  
#endif


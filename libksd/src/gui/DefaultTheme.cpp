//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	$Header$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Checked-in by:	$Author$
//	
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma implementation
#endif
//---------------------------------------------------------------------------
#define BUILD_LIB
#include "Application.h"
#include "DefaultTheme.h"
#include "Bevel.h"
#include "Scroller.h"
//---------------------------------------------------------------------------  
#define SCROLLER_WIDTH		5
#define SCROLLER_HEIGHT		3
#define SCROLLER_MARGIN		3
//---------------------------------------------------------------------------  
// some color converters:
// NOTE: I didn't write this code, and I actually don't have the name of the author 
// that I took it from.
struct RGBColor {
	void Set(float _r, float _g, float _b) {
		R = _r; G = _g; B = _b;
	}

	float R, G, B;
};

struct HSVColor {
	float H, S, V;
};

#if 0
// TODO: make these work properly!!
static void RGBtoHSV(RGBColor& rgb, HSVColor& hsv)
{
	float v, x, f;
	int i;

	x = min(min(rgb.R, rgb.G), rgb.B);
	v = max(max(rgb.R, rgb.G), rgb.B);

	// undefined HSV type
	if(v == x) { 
		hsv.H = hsv.S = 0;
		hsv.V = v;
		return;
	}

	// calc actual HSV value
	f = (rgb.R == x) ? rgb.G - rgb.B : ((rgb.G == x) ? rgb.B - rgb.R : rgb.R - rgb.G);
	i = (rgb.R == x) ? 3 : ((rgb.G == x) ? 5 : 1);
	hsv.H = i - f / (v - x);
	hsv.S = (v - x) / v;
	hsv.V = v;
}

// TODO: make these work properly
static void HSVtoRGB(HSVColor& hsv, RGBColor& rgb) 
{
	float m, n, f;
	int i;

	i = int(hsv.H); //floor(hsv.H);
	f = hsv.H - i;

	if(!(i && 1)) f = 1 - f;
	m = hsv.V * (1 - hsv.S);
	n = hsv.V * (1 - hsv.S * f);

	switch(i) {
		case 6:
		case 0:
			rgb.Set(hsv.V, n, m);
			break;
		case 1:
			rgb.Set(n, hsv.V, m);
			break;
		case 2:
			rgb.Set(m, hsv.V, n);
			break;
		case 3:
			rgb.Set(m, n, hsv.V);
			break;
		case 4:
			rgb.Set(n, m, hsv.V);
			break;
		case 5:
			rgb.Set(hsv.V, m, n);
			break;
	}
}
#endif
//---------------------------------------------------------------------------  
// this functions takes "color" and calculates a light and dark version of it
static void GetBevelColors(const TColor& color, TColor& light, TColor& dark)
{
	// NOTE: this function uses HSV get colors:
	// light = more V, less S
	// dark = less V, more S

	// this code doesn't work right
	#if 0
	RGBColor rgb;
	HSVColor hsv, temp;

	// first convert our start color to HSV
	rgb.Set(float(color.Red), float(color.Green), float(color.Blue));
	RGBtoHSV(rgb, hsv);

	// now make the light color and convert to rgb
	temp = hsv;
	temp.S -= 25;
	//temp.V += 25;
	HSVtoRGB(hsv, rgb);
	light.Red = (Uint8)rgb.R;
	light.Green = (Uint8)rgb.G;
	light.Blue = (Uint8)rgb.B;

	// now make the color darker an convert to rgb
	temp = hsv;
	//temp.S += 25;
	temp.V -= 25;
	HSVtoRGB(hsv, rgb);
	dark.Red = (Uint8)rgb.R;
	dark.Green = (Uint8)rgb.G;
	dark.Blue = (Uint8)rgb.B;
	#else
	// but this doesn't do the job with certain colors
	light = color + 100;
	dark = color - 25;
	#endif
}
//---------------------------------------------------------------------------  
// this function draws a two color square bevel with based on two colors:  the
// upper_color (for top/left edges) and lower_color (for right/bottom edges).
// The bevel is 2 pixels thick.
static void DrawBevel(TCanvas* Canvas, const TRect& area, 
					  const TColor& upper_color, const TColor& lower_color)
{
	// draw top part of bevel
	Canvas->Pen.Color = upper_color;
	Canvas->VertLine(area.GetLeft(), area.GetTop(), area.GetBottom() - 2);
	Canvas->VertLine(area.GetLeft() + 1, area.GetTop(), area.GetBottom() - 3);
	Canvas->HorzLine(area.GetTop(), area.GetLeft(), area.GetRight() - 2);
	Canvas->HorzLine(area.GetTop() + 1, area.GetLeft(), area.GetRight() - 3);

	// draws the bottom part of bevel
	Canvas->Pen.Color = lower_color;
	Canvas->VertLine(area.GetRight() - 1, area.GetTop(), area.GetBottom() - 2);
	Canvas->VertLine(area.GetRight() - 2, area.GetTop() + 1, area.GetBottom() - 3);
	Canvas->HorzLine(area.GetBottom() - 1, area.GetLeft(), area.GetRight() - 2);
	Canvas->HorzLine(area.GetBottom() - 2, area.GetLeft() + 1, area.GetRight() - 3);
}
//---------------------------------------------------------------------------  
// 
// ** Default Style for TBevel **
// 
//---------------------------------------------------------------------------  
void DefaultBevelStyle::Draw(TControl* Control, const std::string& id, TCanvas* Canvas, 
							 int index, const TRect& Rect)
{
	Canvas->Pen.Color = Control->GetColor();

	// draw the various types of bevels
	if(id == "default" || id == "thick") {
		Canvas->FrameRect(0, 0, Control->GetWidth() - 1, Control->GetHeight() - 1);
		Canvas->FrameRect(2, 2, Control->GetWidth() - 3, Control->GetHeight() - 3);
	} else if(id == "thin") {
		Canvas->FrameRect(0, 0, Control->GetWidth() - 1, Control->GetHeight() - 1);
	} else if(id == "raised") {
		TColor upper, lower;

		GetBevelColors(Control->GetBackgroundColor(), upper, lower);
		DrawBevel(Canvas, TRect(0, 0, Control->GetWidth(), Control->GetHeight()), 
				  upper, lower);
	} else if(id == "lowered") {
		TColor upper, lower;

		GetBevelColors(Control->GetBackgroundColor(), lower, upper);
		DrawBevel(Canvas, TRect(0, 0, Control->GetWidth(), Control->GetHeight()), 
				  upper, lower);
	}
}
//---------------------------------------------------------------------------  
// TODO: I don't like the casting -- it won't translate to XML well ...
// MAYBE: XML will just have to set a static client rect - an advantage to C++ themes
TRect DefaultBevelStyle::GetItemRect(TControl* Control, int index)
{
	TBevel* Bevel = (TBevel*)Control;
	TRect ClientRect;

	switch(Bevel->GetType()) {
		case TBevel::None:
			ClientRect.Set(0, 0, Bevel->GetWidth(), Bevel->GetHeight());
			break;
		case TBevel::Default:
		case TBevel::Thick:
			ClientRect.Set(4, 4, Bevel->GetWidth() - 4, Bevel->GetHeight() - 4);
			break;
		case TBevel::Thin:
			ClientRect.Set(2, 2, Bevel->GetWidth() - 2, Bevel->GetHeight() - 2);
			break;
		case TBevel::Lowered:
		case TBevel::Raised:
			ClientRect.Set(3, 3, Bevel->GetWidth() - 3, Bevel->GetHeight() - 3);
			break;
	}

	return ClientRect;
}
//---------------------------------------------------------------------------  
// 
// ** Default Style for TButton **
// 
//---------------------------------------------------------------------------  
TRect DefaultButtonStyle::GetItemRect(TControl* Control, int index)
{
	// has no decorations
	return TRect();
}
//---------------------------------------------------------------------------  
void DefaultButtonStyle::Draw(TControl* Control, const std::string& id, TCanvas* Canvas,
							  int index, const TRect& Rect)
{
	TColor Dark, Light, Top, Bottom;

	// make the button colors
	GetBevelColors(Control->GetBackgroundColor(), Light, Dark);

	// find proper colors
	if(id == "pressed") {
		Top = Dark;
		Bottom = Light;
	} else if(id == "released") {
		Top = Light;
		Bottom = Dark;
	} else if(id == "disabled") {
		Top = Bottom = Dark;
	} else {
		LOG(3, "Invalid \"%s\" id passed to DefaultButtonStyle", id.c_str());
		return;
	}

	// draw actual button edge
	DrawBevel(Canvas, TRect(0, 0, Control->GetWidth(), Control->GetHeight()), Top, Bottom);
}
//---------------------------------------------------------------------------  
// 
// ** Default Style for TScroller **
// 
//---------------------------------------------------------------------------  
TRect DefaultScrollerStyle::GetItemRect(TControl* Control, int index)
{
	switch((TScroller::Orientation)index) {
		case TScroller::Left:
			return TRect(SCROLLER_MARGIN, 
						 (Control->GetHeight() >> 1) - SCROLLER_WIDTH,
						 SCROLLER_WIDTH + (SCROLLER_MARGIN << 1) + 1, 
						 (Control->GetHeight() >> 1) + SCROLLER_WIDTH);
			break;
		case TScroller::Top:
			return TRect((Control->GetWidth() >> 1) - SCROLLER_WIDTH, 
						 SCROLLER_MARGIN, 
						 (Control->GetWidth() >> 1) + SCROLLER_WIDTH, 
						 SCROLLER_HEIGHT + (SCROLLER_MARGIN << 1) + 1);
			break;
		case TScroller::Right:
			return TRect(0, 
						 (Control->GetHeight() >> 1) - SCROLLER_WIDTH,
						 SCROLLER_WIDTH + SCROLLER_MARGIN + 1, 
						 (Control->GetHeight() >> 1) + SCROLLER_WIDTH);
			break;
		case TScroller::Bottom:
			return TRect((Control->GetWidth() >> 1) - SCROLLER_WIDTH, 
						 0, 
						 (Control->GetWidth() >> 1) + SCROLLER_WIDTH, 
						 SCROLLER_HEIGHT + SCROLLER_MARGIN + 1);
			break;
	}

	return TRect();
}
//---------------------------------------------------------------------------  
void DefaultScrollerStyle::Draw(TControl* Control, const std::string& id, TCanvas* Canvas,
								int index, const TRect& Rect)
{
	TPoint2D pos;

	// draw arrow
	Canvas->Pen.Color = id == "on" ? Control->GetColor() : Control->GetBackgroundColor();
	switch((TScroller::Orientation)index) {
		case TScroller::Left:
			pos = TPoint2D(SCROLLER_MARGIN, Control->GetHeight() >> 1);
			Canvas->Line(pos.X, pos.Y, pos.X + SCROLLER_HEIGHT, pos.Y - SCROLLER_WIDTH);
			Canvas->Line(pos.X, pos.Y, pos.X + SCROLLER_HEIGHT, pos.Y + SCROLLER_WIDTH);
			Canvas->Line(pos.X + SCROLLER_HEIGHT, pos.Y - SCROLLER_WIDTH, 
						 pos.X + SCROLLER_HEIGHT, pos.Y + SCROLLER_WIDTH);
			break;
		case TScroller::Top:
			pos = TPoint2D(Control->GetWidth() >> 1, SCROLLER_MARGIN);
			Canvas->Line(pos.X, pos.Y, pos.X - SCROLLER_WIDTH, pos.Y + SCROLLER_HEIGHT);
			Canvas->Line(pos.X, pos.Y, pos.X + SCROLLER_WIDTH, pos.Y + SCROLLER_HEIGHT);
			Canvas->HorzLine(pos.Y + SCROLLER_HEIGHT, pos.X - SCROLLER_WIDTH,
							 pos.X + SCROLLER_WIDTH);
			break;
		case TScroller::Right:
			pos = TPoint2D(Control->GetWidth() - SCROLLER_MARGIN, Control->GetHeight() >> 1);
			Canvas->Line(pos.X, pos.Y, pos.X - SCROLLER_HEIGHT, pos.Y - SCROLLER_WIDTH);
			Canvas->Line(pos.X, pos.Y, pos.X - SCROLLER_HEIGHT, pos.Y + SCROLLER_WIDTH);
			Canvas->Line(pos.X - SCROLLER_HEIGHT, pos.Y - SCROLLER_WIDTH, 
						 pos.X - SCROLLER_HEIGHT, pos.Y + SCROLLER_WIDTH);
			break;
		case TScroller::Bottom:
			pos = TPoint2D(Control->GetWidth() >> 1, Control->GetHeight() - SCROLLER_MARGIN);
			Canvas->Line(pos.X, pos.Y, pos.X - SCROLLER_WIDTH, pos.Y - SCROLLER_HEIGHT);
			Canvas->Line(pos.X, pos.Y, pos.X + SCROLLER_WIDTH, pos.Y - SCROLLER_HEIGHT);
			Canvas->Line(pos.X - SCROLLER_WIDTH, pos.Y - SCROLLER_HEIGHT, 
						 pos.X + SCROLLER_WIDTH, pos.Y - SCROLLER_HEIGHT);
			break;
	}
}
//---------------------------------------------------------------------------  
// 
// ** Default Style for TScrollBar **
// 
//---------------------------------------------------------------------------  
TRect DefaultScrollBarStyle::GetItemRect(TControl* Control, int index)
{
	TRect rect;

	switch(index) {
		case 0: break;
		case 1:
			rect.Set(0, 
					 0, 
					 (SCROLLER_HEIGHT << 1) + SCROLLER_MARGIN + 1, 
					 Control->GetHeight());
			break;
		case 2: 
			rect.Set(0, 
					 0,
					 Control->GetWidth(), 
					 (SCROLLER_HEIGHT << 1) + SCROLLER_MARGIN + 1);
			break;
		case 3:
			rect.Set(Control->GetWidth() - ((SCROLLER_HEIGHT << 1) + SCROLLER_MARGIN) - 1, 
					 0,
					 Control->GetWidth(), 
					 Control->GetHeight());
			break;
		case 4: 
			rect.Set(0, 
					 Control->GetHeight() - ((SCROLLER_HEIGHT << 1) + SCROLLER_MARGIN) - 1,
					 Control->GetWidth(), 
					 Control->GetHeight());
			break;
	}

	return rect;		
}
//---------------------------------------------------------------------------  
void DefaultScrollBarStyle::Draw(TControl* Control, const std::string& id, TCanvas* Canvas,
								 int index, const TRect& Rect)
{
	TPoint2D pos;
	int half_width;

	Canvas->Pen.Color = Control->GetColor();

	// draw border
	Canvas->FrameRect(0, 0, Control->GetWidth() - 1, Control->GetHeight() - 1);

	switch(index) {
		case 1: // left
			pos = TPoint2D(SCROLLER_MARGIN, Control->GetHeight() >> 1);
			half_width = Control->GetHeight() >> 1;

			if(id == "pressed") {			
				Canvas->Line(pos.X, pos.Y, pos.X + SCROLLER_HEIGHT, pos.Y - half_width);
				Canvas->Line(pos.X, pos.Y, pos.X + SCROLLER_HEIGHT, pos.Y + half_width);
				Canvas->Line(pos.X + SCROLLER_HEIGHT, pos.Y - half_width, 
							 pos.X + SCROLLER_HEIGHT, pos.Y + half_width);
			} else {
				for(int i = 0; i < SCROLLER_HEIGHT << 1; i++) {
					Canvas->VertLine(pos.X + i, pos.Y - std::min(i, half_width), 
									 pos.Y + std::min(i, half_width));
				}
			}
			break;
		case 2: // top
			pos = TPoint2D(Control->GetWidth() >> 1, SCROLLER_MARGIN);
			half_width = Control->GetWidth() >> 1;

			if(id == "pressed") {
				Canvas->Line(pos.X, pos.Y, pos.X - half_width, pos.Y + SCROLLER_HEIGHT);
				Canvas->Line(pos.X, pos.Y, pos.X + half_width, pos.Y + SCROLLER_HEIGHT);
				Canvas->HorzLine(pos.Y + SCROLLER_HEIGHT, pos.X - half_width,
								 pos.X + half_width);
			} else {
				for(int i = 0; i < SCROLLER_HEIGHT << 1; i++) {
					Canvas->HorzLine(pos.Y + i, pos.X - std::min(i, half_width), 
									 pos.X + std::min(i, half_width));
				}
			}
			break;
		case 3: // right 
			pos = TPoint2D(Control->GetWidth() - SCROLLER_MARGIN, Control->GetHeight() >> 1);
			half_width = Control->GetHeight() >> 1;
			
			if(id == "pressed") {
				Canvas->Line(pos.X, pos.Y, pos.X - SCROLLER_HEIGHT, pos.Y - half_width);
				Canvas->Line(pos.X, pos.Y, pos.X - SCROLLER_HEIGHT, pos.Y + half_width);
				Canvas->Line(pos.X - SCROLLER_HEIGHT, pos.Y - half_width, 
							 pos.X - SCROLLER_HEIGHT, pos.Y + half_width);
			} else {
				for(int i = 0; i < SCROLLER_HEIGHT << 1; i++) {
					Canvas->VertLine(pos.X - i, pos.Y - std::min(i, half_width), 
									 pos.Y + std::min(i, half_width));
				}
			}
			break;
		case 4: // bottom
			pos = TPoint2D(Control->GetWidth() >> 1, Control->GetHeight() - SCROLLER_MARGIN);
			half_width = Control->GetWidth() >> 1;

			if(id == "pressed") {
				Canvas->Line(pos.X, pos.Y, pos.X - half_width, pos.Y - SCROLLER_HEIGHT);
				Canvas->Line(pos.X, pos.Y, pos.X + half_width, pos.Y - SCROLLER_HEIGHT);
				Canvas->Line(pos.X - half_width, pos.Y - SCROLLER_HEIGHT, 
							 pos.X + half_width, pos.Y - SCROLLER_HEIGHT);
			} else {
				for(int i = 0; i < SCROLLER_HEIGHT << 1; i++) {
					Canvas->HorzLine(pos.Y - i, pos.X - std::min(i, half_width), 
									 pos.X + std::min(i, half_width));
				}
			}
			break;
		case 5: // bar
			TColor upper, lower;

			// make bar dimensions
			// TODO: maybe have TScrollBar set actual dimensions instead of partial 
			// ones and then have the theme shrink it??
			TRect temp = Rect;
			if(id == "vertical") {
				temp.SetLeft(1);
				temp.SetRight(Control->GetWidth() - 1);
			} else {
				temp.SetTop(1);
				temp.SetBottom(Control->GetHeight() - 1);
			}

			// draw bar
			Canvas->Brush.Color = Control->GetColor();
			Canvas->FillRect(temp.GetLeft(), temp.GetTop(), temp.GetRight() - 1, temp.GetBottom() - 1);
			GetBevelColors(Control->GetColor(), upper, lower);
			DrawBevel(Canvas, temp, upper, lower);
			break;
	}
}
//---------------------------------------------------------------------------  

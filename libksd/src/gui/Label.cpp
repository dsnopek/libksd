//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	$Header$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Checked-in by:	$Author$
//	
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma implementation
#endif
//---------------------------------------------------------------------------
#define KSDGUI_BUILD_LIB
#include "Label.h"

using namespace ksd;
using namespace ksd_gui;
//---------------------------------------------------------------------------  
TLabel::TLabel(TWidget* _parent) : TControl(_parent)
{
	// TODO: if use parent font and use parent color are set, we need to check
	// at every repaint to see if font or color has changed.
	// set-up text appearance info
	attr.Font = GetFont();
	attr.FontAttr.Color = GetColor();
	TextBox.SetTextAttr(attr);
}
//---------------------------------------------------------------------------  
bool TLabel::ResizeEvent(int Width, int Height)
{
	TextBox.SetWidth(Width);
	return true;
}
//---------------------------------------------------------------------------  
bool TLabel::Draw(TCanvas* Canvas, DrawType dt) 
{
	if(dt == REPAINT) {
		TextBox.Draw(Canvas, 0, 0);

		return true;
	}

	return false;
}
//---------------------------------------------------------------------------


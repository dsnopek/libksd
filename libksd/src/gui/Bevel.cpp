//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	$Header$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Checked-in by:	$Author$
//
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma implementation
#endif
//--------------------------------------------------------------------------- 
#define KSDGUI_BUILD_LIB
#include "Bevel.h"

using namespace ksd;
using namespace ksd_gui;
//---------------------------------------------------------------------------  
TBevel::TBevel(TWidget* _parent) : TDecoration(_parent)
{
	// default setting for bevel
	SetAlign(alClient);
	SetTransparentBackground(true);
	//SetAcceptInput(false); // instead of ignoring input, eat it
	SetHollowClientArea(true);
	SetAllowChildren(false);
	//DrawOnDemand(true); // temp
	
	bevel_type = Default;
}
//---------------------------------------------------------------------------  
bool TBevel::Draw(TCanvas* Canvas, DrawType dt)
{
	if(dt == REPAINT && bevel_type != None) {
	   std::string id_name;

		// get id from bevel type
		switch(bevel_type) {
			case None:
				// do nothing for none !!
				return false;
				break;
			case Default:
				id_name = "default";
				break;
			case Raised:
				id_name = "raised";
				break;
			case Lowered:
				id_name = "lowered";
				break;
			case Thick:
				id_name = "thick";
				break;
			case Thin:
				id_name = "thin";
				break;
		}

		GetStyle()->Draw(this, id_name, Canvas);

		return true;
	}

	return false;
}
//---------------------------------------------------------------------------  


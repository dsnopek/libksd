//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	$Header$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Checked-in by:	$Author$
//
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma implementation
#endif
//--------------------------------------------------------------------------- 
#define KSDGUI_BUILD_LIB
#include "ScrollBar.h"

using namespace ksd;
using namespace ksd_gui;
using namespace SigC;
//---------------------------------------------------------------------------  
unsigned int ksd_gui::TScrollBar::RepeatDelay = 500;
unsigned int ksd_gui::TScrollBar::RepeatInterval = 100;
//---------------------------------------------------------------------------  
TScrollBar::TScrollBar(TWidget* _parent, Type _type) : TDecoration(_parent)
{
	// default setting for bevel
	SetAllowChildren(false);
	SetDrawOnDemand(true);

	Dir = None;
	Position = 0;
	ScreenPosition = ScreenRange = Range = BarStart = BarWidth = BarClick = 0;
	Increment = 2;
	PageIncrement = 10;
	DelayTimer.OnTimer.connect(slot(*this, &TScrollBar::OnDelay));

	// give it initial size when used as a decoration
	SetConstraints(15, 15);

	// init type
	SetType(_type);
}
//---------------------------------------------------------------------------  
bool TScrollBar::Draw(TCanvas* Canvas, DrawType dt)
{
	// TODO: take advantage of update draws as well as Repaints..
	
	if(dt == REPAINT) {
		// if style provides, let it change the background
		GetStyle()->Draw(this, "default", Canvas, 0);
	
		// redraw all
		if(bar_type == Horizontal) {
			// draw buttons
			GetStyle()->Draw(this, Dir == Left ? "pressed" : "released", Canvas, 1);
			GetStyle()->Draw(this, Dir == Right ? "pressed" : "released", Canvas, 3);

			// calculate the bar width
			BarStart = GetStyle()->GetItemRect(this, 1).GetRight();
			ScreenRange = GetStyle()->GetItemRect(this, 3).GetLeft() - BarStart; 
			if(Range != 0 && Range > ScreenRange) {
				BarWidth = 5;
				ScreenRange -= BarWidth;
				ScreenPosition = ((ScreenRange * Position) / Range) + BarStart;
			} else {
				BarWidth = ScreenRange - Range;
				ScreenPosition = Position + BarStart;
			}

			// draw bar
			GetStyle()->Draw(this, "horizontal", Canvas, 5, 
							 TRect(ScreenPosition, 0, 
							 ScreenPosition + BarWidth, GetHeight()));
		} else {
			// draw buttons
			GetStyle()->Draw(this, Dir == Up ? "pressed" : "released", Canvas, 2);
			GetStyle()->Draw(this, Dir == Down ? "pressed" : "released", Canvas, 4);

			// calculate the bar width
			BarStart = GetStyle()->GetItemRect(this, 2).GetBottom();
			ScreenRange = GetStyle()->GetItemRect(this, 4).GetTop() - BarStart;
			if(Range != 0 && Range > ScreenRange) {
				BarWidth = 5;
				ScreenRange -= BarWidth;
				ScreenPosition = ((ScreenRange * Position) / Range) + BarStart;
			} else {
				BarWidth = ScreenRange - Range;
				ScreenPosition = Position + BarStart;
			}
			
			// draw bar
			GetStyle()->Draw(this, "vertical", Canvas, 5, 
							 TRect(0, ScreenPosition, GetWidth(), 
							 ScreenPosition + BarWidth));
		}

		return true;
	}

	return false;
}
//---------------------------------------------------------------------------  
void TScrollBar::MouseDown(int X, int Y, TMouseButton button) 
{
	TPoint2D ClickPos(X, Y);

	if(bar_type == Horizontal) {
		if(X > ScreenPosition && X < ScreenPosition + BarWidth) {
			Dir = Button;
			BarClick = ClickPos.X - ScreenPosition;
			return;
		} else if(GetStyle()->GetItemRect(this, 1).IsInside(ClickPos)) {
			Dir = Left;
			DoScroll(-Increment);
		} else if(GetStyle()->GetItemRect(this, 3).IsInside(ClickPos)) {
			Dir = Right;
			DoScroll(Increment);
		} else return;

		// do when button is clicked
		DelayTimer.SetInterval(RepeatDelay);
		DelayTimer.Start();
	} else {
		if(Y > ScreenPosition && Y < ScreenPosition + BarWidth) {
			Dir = Button;
			BarClick = ClickPos.Y - ScreenPosition;
			return;
		} else if(GetStyle()->GetItemRect(this, 2).IsInside(ClickPos)) {
			Dir = Up;
			DoScroll(-Increment);
		} else if(GetStyle()->GetItemRect(this, 4).IsInside(ClickPos)) {
			Dir = Down;
			DoScroll(Increment);
		} else return;

		// do when button is clicked
		DelayTimer.SetInterval(RepeatDelay);
		DelayTimer.Start();
	}
}
//---------------------------------------------------------------------------  
void TScrollBar::MouseUp(int X, int Y, TMouseButton button)
{
	Dir = None;
	DelayTimer.Stop();
	NeedsRepaint();
}
//---------------------------------------------------------------------------  
void TScrollBar::MouseMove(int X, int Y, int RelX, int RelY, TMouseButton button)
{
	if(Dir == Button) {
		if(bar_type == Horizontal) {
			DoScroll((X - int(BarClick)) - (int)ScreenPosition);
		} else {
			DoScroll((Y - int(BarClick)) - (int)ScreenPosition);
		}
	}	
}
//---------------------------------------------------------------------------  
void TScrollBar::DoScroll(int Rel)
{
	Uint16 NewPos = std::max(Position + Rel, 0);

	// clip new pos to range
	if(NewPos > Range) NewPos = Range;
	
	OnScroll(this, NewPos, Rel);
	
	Position = NewPos;
	NeedsRepaint();
}
//---------------------------------------------------------------------------  
Uint32 TScrollBar::OnDelay(Uint32 _val)
{
	switch(Dir) {
		case None: break;
		case Button: break;
		case Left:
		case Up:
			DoScroll(-Increment);
			break;
		case Right:
		case Down:
			DoScroll(Increment);
			break;
	}
			

	// switch from delay to interval
	if(_val == RepeatDelay)
		return RepeatInterval;

	return _val;	
}
//---------------------------------------------------------------------------  


//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	$Header$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Checked-in by:	$Author$
//	
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma implementation
#endif
//---------------------------------------------------------------------------
#define KSDGUI_BUILD_LIB
#include "TextControl.h"

using namespace ksd;
using namespace ksd_gui;
using namespace std;
//---------------------------------------------------------------------------
TTextControl::TTextControl(TWidget* _parent) : TControl(_parent)
{
	SelStart = 0;
	SelLen = 0;
	Margin = 2;
	WasResized = Clicked = false;
	AutoScroll = false;
	//ActivePos = 0;
	///UseActiveAttr = false;
	TextBox.OnChange = SigC::slot(*this, &TTextControl::OnTextBoxChange);

	Bevel = new TBevel(this);
	Bevel->SetType(TBevel::Thin);
	AddDecoration(Bevel);

	ScrollBox = new TScrollBox(this);
	ScrollBox->SetType(TScrollBox::UseScrollBars);
	ScrollBox->Hide();
	ScrollBox->SetHorzScrolling(false);
	ScrollBox->SetVertScrolling(false);
	ScrollBox->OnScroll.connect(slot(*this, &TTextControl::OnScroll));
	AddDecoration(ScrollBox);
	
	SetAllowChildren(false);
	SetDrawOnDemand(true);
}
//---------------------------------------------------------------------------  
bool TTextControl::ResizeEvent(int Width, int Height) 
{
	WasResized = true;
	return true;
}
//---------------------------------------------------------------------------  
bool TTextControl::Draw(TCanvas* Canvas, DrawType dt)
{
	// call parent draw function
	TControl::Draw(Canvas, dt);

	if(dt == REPAINT) {
		TCanvas ClientCanvas(GetClientSurface());
		TRect temp = GetClientRect();
		int x, y;

		if(WasResized) {
			TextBox.SetWidth(temp.GetWidth() - Margin);
			WasResized = false;
		}

		// deal with text box size changes
		if(ScrollBox->GetVertScrolling()) {
			if((TextBox.GetHeight() + Margin) - temp.GetHeight() != ScrollBox->GetVertRange()) {
				ScrollBox->SetVertRange((TextBox.GetHeight() + Margin) - temp.GetHeight());
			}
		}
		if(ScrollBox->GetHorzScrolling()) {
			if((TextBox.GetWidth() + Margin) - temp.GetWidth() != ScrollBox->GetHorzRange()) {
				ScrollBox->SetHorzRange((TextBox.GetWidth() + Margin) - temp.GetWidth());
			}
		}

		// draw text
		TextBox.Draw(&ClientCanvas, Margin, Margin, 0);

		// draw selection
		if(SelLen != 0) {
			unsigned int start_pos, end_pos, start_line, end_line;
			int x1, y1, x2, y2;

			// get start and end position
			if(SelLen > 0) {
				start_pos = SelStart;
				end_pos = SelStart + SelLen;
			} else {
				start_pos = SelStart + SelLen;
				end_pos = SelStart;
			}

			// get start and end line
			start_line = TextBox.GetScreenPos(start_pos, x1, y1);
			end_line = TextBox.GetScreenPos(end_pos, x2, y2);

			// set-up canvas
			// NOTE: we always use white, because it will always effect the
			// text color.  This prevent us from accidentally using a color like
			// black, that would have no effect!!
			ClientCanvas.Pen.Color = TColor::white;
			ClientCanvas.Brush.Color = TColor::white;
			ClientCanvas.Mode = OP_XOR;

			if(start_line == end_line) {
				// draw if only on one line
				ClientCanvas.FillRect(x1 + Margin, y1 + Margin, x2 + Margin, 
									   y1 + Margin + TextBox.GetLineHeight(start_line)
									   			   + TextBox.GetLineSkip(start_line));
			} else {
				int height = y1 + Margin + TextBox.GetLineHeight(start_line)
										 + TextBox.GetLineSkip(start_line), temp;
			
				// draw start line
				ClientCanvas.FillRect(x1 + Margin, y1 + Margin, TextBox.GetWidth(), height);

				// draw in between lines
				for(unsigned int i = start_line + 1; i < end_line; i++) {
					temp = height;
					height += (TextBox.GetLineHeight(i) + TextBox.GetLineSkip(i));
					
					ClientCanvas.FillRect(Margin, temp, TextBox.GetWidth(), height);
				}

				// draw end line
				ClientCanvas.FillRect(Margin, height, x2 + Margin,
									   y2 + Margin + TextBox.GetLineHeight(end_line)
									   			   + TextBox.GetLineSkip(end_line));
			}						   
		}

		// draw cursor
		unsigned int line = TextBox.GetScreenPos(SelStart, x, y);
		ClientCanvas.Mode = OP_REPLACE;
		ClientCanvas.Pen.Color = GetColor();
		ClientCanvas.VertLine(x + Margin, y + Margin, y + Margin + TextBox.GetLineHeight(line));

		return true;
	}

	return false;
}
//---------------------------------------------------------------------------  
void TTextControl::KeyDown(const TKey& key)
{
	// cannot operate without a valid text box
	if(!TextBox.IsValid()) return;

	if(key.GetKeyCode() == SDLK_LEFT) {
		if(SelStart > 0) SelStart--;
		SelLen = 0;
	} else if(key.GetKeyCode() == SDLK_RIGHT) {
		if(SelStart < TextBox.GetLength()) SelStart++;
		SelLen = 0;
	} else if(key.GetKeyCode() == SDLK_UP) {
		int X, Y;
		unsigned int line = TextBox.GetCurrentLine(SelStart);

		if(line == 0) {
			SelStart = 0;
		} else {
			TextBox.GetScreenPos(SelStart, X, Y);
			Y -= TextBox.GetLineSkip(line - 1);
			SelStart = TextBox.GetCharPos(X, Y);
		}
		SelLen = 0;
	} else if(key.GetKeyCode() == SDLK_DOWN) {
		int X, Y;
		unsigned int line;

		line = TextBox.GetScreenPos(SelStart, X, Y);
		Y += TextBox.GetLineHeight(line) + 2;
		SelStart = TextBox.GetCharPos(X, Y);
		SelLen = 0;
	} else if(key.GetKeyCode() == SDLK_HOME) {
		SelStart = TextBox.GetLineStart(TextBox.GetCurrentLine(SelStart));
		SelLen = 0;

		// TODO: don't adondon active if it exists on begininng of the line
	} else if(key.GetKeyCode() == SDLK_END) {
		unsigned int line = TextBox.GetCurrentLine(SelStart);
		if(line + 1 == TextBox.GetLineCount()) {
			SelStart = TextBox.GetLength();
		} else {
			SelStart = TextBox.GetLineEnd(line) - (TextBox.GetLineExplicit(line) ? 1 : 0);
		}
		SelLen = 0;
		
		// TODO: don't abondon active attr if it exists on the end of the line
	} else if((key.GetKeyCode() == SDLK_BACKSPACE || key.GetKeyCode() == SDLK_DELETE) 
			  && SelLen != 0) {
		// remove selected text
		TextBox.RemoveText(GetSelectionStart(), GetSelectionLength());

		// put sel start at the proper place
		SelStart = GetSelectionStart();
		SelLen = 0;
	} else {
		// overwrite selected text
		if(SelLen != 0) {
			TextBox.RemoveText(GetSelectionStart(), GetSelectionLength());
			SelLen = 0;
		}

		// deal with input style keys
		if(key.GetKeyCode() == SDLK_BACKSPACE) {
			if(SelStart > 0) {
				// remove character just before the cursor
				TextBox.RemoveText(SelStart - 1, 1);
				SelStart--;
			}
		} else if(key.GetKeyCode() == SDLK_DELETE) {
			// remove character at the cursor
			TextBox.RemoveText(SelStart, 1);
		} else if(key.GetKeyCode() == SDLK_RETURN) {
			// insert an explicit line at cursor
			TextBox.InsertLine(SelStart);
			SelStart++;

			goto dont_abondon_active_attr;
		} else {
			// check for valid printable characters
			if(key.GetUnicode() == 0) {
				LOG(10, "Trying to type into a TTextControl without unicode translation!!");
				return;
			}

			// actually insert characters
			TextBox.InsertText(string(1, (char)key.GetUnicode()), SelStart);

			// move cursor forward one
			SelStart++;
		}
	}

	TextBox.AbondonActiveAttr();

	dont_abondon_active_attr:
	NeedsRepaint();
}
//---------------------------------------------------------------------------  
void TTextControl::MouseDown(int X, int Y, TMouseButton btn)
{
	TControl::MouseDown(X, Y, btn);

	GrabKeyboardFocus();
	TPoint2D ScrollPos = ScrollBox->GetScrollPosition();
	SelStart = TextBox.GetCharPos(X - Margin + ScrollPos.X, Y - Margin + ScrollPos.Y);
	SelLen = 0;

	// discard active attr is neccessary
	/*if(UseActiveAttr && SelStart != ActivePos) {
		UseActiveAttr = false;
	}*/
	// this won't work right in all situations, but it'll be fine for now
	TextBox.AbondonActiveAttr();
	
	NeedsRepaint();
	Clicked = true;
}
//---------------------------------------------------------------------------  
void TTextControl::MouseUp(int X, int Y, TMouseButton btn)
{
	TControl::MouseUp(X, Y, btn);

	if(Clicked) {
		// get sel len
		TPoint2D ScrollPos = ScrollBox->GetScrollPosition();
		SelLen = TextBox.GetCharPos(X - Margin + ScrollPos.X, Y - Margin + ScrollPos.Y) - SelStart;

		Clicked = false;
		NeedsRepaint();
	}
}
//---------------------------------------------------------------------------  
void TTextControl::MouseMove(int X, int Y, int RelX, int RelY, TMouseButton btn)
{
	TControl::MouseMove(X, Y, RelX, RelY, btn);

	if(Clicked) {
		// get sel len
		TPoint2D ScrollPos = ScrollBox->GetScrollPosition();
		SelLen = TextBox.GetCharPos(X - Margin + ScrollPos.X, Y - Margin + ScrollPos.Y) - SelStart;

		NeedsRepaint();
	}
}
//---------------------------------------------------------------------------  
void TTextControl::OnTextBoxChange() 
{
	if(AutoScroll) UpdateScrollBox();
	NeedsRepaint();
}
//---------------------------------------------------------------------------  


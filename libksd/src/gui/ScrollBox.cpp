//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	$Header$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Checked-in by:	$Author$
//
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma implementation
#endif
//--------------------------------------------------------------------------- 
#define KSDGUI_BUILD_LIB
#include "Timer.h"
#include "ScrollBox.h"

using namespace ksd;
using namespace ksd_gui;
//---------------------------------------------------------------------------  
unsigned int ksd_gui::TScrollBox::ScrollerDelay = 500;
unsigned int ksd_gui::TScrollBox::ScrollerInterval = 100;
//---------------------------------------------------------------------------  
TScrollBox::TScrollBox(TWidget* _parent) : TDecoration(_parent)
{
	// surrounds the whole control when used as decoration
	SetAlign(alClient);

	// widget flags
	SetTransparentBackground(true);
	SetHollowClientArea(true);

	// set-up scroll bars
	VertScrollBar = InitScrollBar(new TScrollBar(this, TScrollBar::Verticle));
	HorzScrollBar = InitScrollBar(new TScrollBar(this, TScrollBar::Horizontal));
	
	// set-up scrollers
	LeftScroller = InitScroller(new TScroller(this, TScroller::Left));
	TopScroller = InitScroller(new TScroller(this, TScroller::Top));
	RightScroller = InitScroller(new TScroller(this, TScroller::Right));
	BottomScroller = InitScroller(new TScroller(this, TScroller::Bottom));

	// add all the decorations -- order is important
	AddDecoration(HorzScrollBar);
	AddDecoration(VertScrollBar);
	AddDecoration(TopScroller);
	AddDecoration(BottomScroller);
	AddDecoration(LeftScroller);
	AddDecoration(RightScroller);

	SetConstraints(15, 15);
	SetAllowChildren(false); // <-- Children mess with hollow client area

	// set-up vars
	Dir = None;
	ScrollX = ScrollY = 0;
	HorzScroll = VertScroll = true;
	DelayTimer.OnTimer.connect(slot(*this, &TScrollBox::OnDelay));

	// set-up scrolling default
	SetType(NoScrollControls);
}
//---------------------------------------------------------------------------  
void TScrollBox::SetType(Type _sc)
{
	scroll_type = _sc;
	switch(scroll_type) {
		case NoScrollControls:
			HorzScrollBar->Hide();
			VertScrollBar->Hide();
			TopScroller->Hide();
			BottomScroller->Hide();
			LeftScroller->Hide();
			RightScroller->Hide();
			break;
		case UseScrollers:
			HorzScrollBar->Hide();
			VertScrollBar->Hide();
			if(HorzScroll) {
				TopScroller->Show();
				BottomScroller->Show();
			}
			if(VertScroll) {
				LeftScroller->Show();
				RightScroller->Show();
			}
			break;
		case UseScrollBars:
			if(HorzScroll) HorzScrollBar->Show();
			if(VertScroll) VertScrollBar->Show();
			TopScroller->Hide();
			BottomScroller->Hide();
			LeftScroller->Hide();
			RightScroller->Hide();
			break;
	}

	// this isn't here to realign this controls decorations.  This is here for 
	// when TScrollBox is used as a decoration and it is tell its parent control
	// to realign this decoration.
	NeedsRealign();
}
//---------------------------------------------------------------------------  
void TScrollBox::SetHorzScrolling(bool _enable)
{
	HorzScroll = _enable;

	switch(scroll_type) {
		case NoScrollControls: break;
		case UseScrollers:
			LeftScroller->SetVisible(HorzScroll);
			RightScroller->SetVisible(HorzScroll);
			break;
		case UseScrollBars:
			HorzScrollBar->SetVisible(HorzScroll);
			break;
	}

	// this isn't here to realign this controls decorations.  This is here for 
	// when TScrollBox is used as a decoration and it is tell its parent control
	// to realign this decoration.
	NeedsRealign();
}
//---------------------------------------------------------------------------  
void TScrollBox::SetVertScrolling(bool _enable)
{
	VertScroll = _enable;

	switch(scroll_type) {
		case NoScrollControls: break;
		case UseScrollers:
			TopScroller->SetVisible(VertScroll);
			BottomScroller->SetVisible(VertScroll);
			break;
		case UseScrollBars:
			VertScrollBar->SetVisible(VertScroll);
			break;
	}

	// this isn't here to realign this controls decorations.  This is here for 
	// when TScrollBox is used as a decoration and it is tell its parent control
	// to realign this decoration.
	NeedsRealign();
}
//---------------------------------------------------------------------------  
void TScrollBox::DoScroll(Uint16 NewScrollX, Uint16 NewScrollY)
{	
	OnScroll(this, NewScrollX, NewScrollY, ScrollX - NewScrollX, ScrollY - NewScrollY);

	ScrollX = NewScrollX;
	ScrollY = NewScrollY;
}
//---------------------------------------------------------------------------  
void TScrollBox::OnScrollerPress(TScroller* Scroller)
{
	if(Scroller == TopScroller) {
		Dir = Up;
		VertScrollBar->ScrollBy(-1);
	} else if(Scroller == BottomScroller) {
		Dir = Down;
		VertScrollBar->ScrollBy(1);
	} else if(Scroller == LeftScroller) {
		Dir = Left;
		HorzScrollBar->ScrollBy(-1);
	} else if(Scroller == RightScroller) {
		Dir = Right;
		HorzScrollBar->ScrollBy(1);
	}

	// this SHOULD never need be done, but I worry!
	if(DelayTimer.IsRunning()) DelayTimer.Stop();

	// start delay timer
	DelayTimer.SetInterval(ScrollerDelay);
	DelayTimer.Start();
}
//---------------------------------------------------------------------------  
void TScrollBox::OnScrollerRelease(TScroller* Scroller)
{
	DelayTimer.Stop();
	Dir = None;
}
//---------------------------------------------------------------------------  
void TScrollBox::OnScrollBar(TScrollBar* Bar, Uint16 NewScroll, int Rel)
{
	if(Bar == HorzScrollBar) {
		DoScroll(NewScroll, ScrollY);
	} else {
		DoScroll(ScrollX, NewScroll);
	}
}
//---------------------------------------------------------------------------  
Uint32 TScrollBox::OnDelay(Uint32 _val)
{
	switch(Dir) {
		case None: break;
		case Up:
			VertScrollBar->ScrollBy(-1);
			break;
		case Down:
			VertScrollBar->ScrollBy(1);
			break;
		case Left:
			HorzScrollBar->ScrollBy(-1);
			break;
		case Right:
			HorzScrollBar->ScrollBy(1);
			break;
	}

	// change from delay to interval
	if(_val == ScrollerDelay) 
		return ScrollerInterval;
		
	return _val;
}
//---------------------------------------------------------------------------  


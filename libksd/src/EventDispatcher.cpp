//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	$Header$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Checked-in by:	$Author$
//	
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma implementation
#endif
//---------------------------------------------------------------------------
#define KSD_BUILD_LIB
#include "EventDispatcher.h"

using namespace ksd;
using namespace std;
//---------------------------------------------------------------------------
void TEventDispatcher::HandleMouseDown(int X, int Y, TMouseButton button)
{
	TWidgetTree::layer_iterator parent_iter;
	TWidgetTree::reverse_layer_iterator_range children;
	TWidgetTree::reverse_layer_iterator i;
	TPoint2D child_pos;

	// NOTE: this isn't exactly right, because some widgets won't take keyboard 
	// focus.  Add a widget flag ??
	// Clear keyboard focus!
	KeyboardFocus = WidgetTree->GetRoot();

	// parent is tree root
	parent_iter = WidgetTree->layer_begin();
	children = WidgetTree->GetReverseLayerRange(parent_iter.base());
	i = children.begin();

	while(i != children.end()) {
		// check inside
		if((*i)->CanAcceptInput() && (*i)->IsInsideLocal(X, Y)) {
			child_pos = (*i)->GetParent()->GetChildPosition((*i));

			// keep input if child does not accept it
			if((*i)->CanAcceptMouseInput(X - child_pos.X, Y - child_pos.Y)) {
				// modify X and Y
				X -= child_pos.X;
				Y -= child_pos.Y;

				// now we will search the children of this node!
				parent_iter = i.base();
				children = WidgetTree->GetReverseLayerRange(parent_iter.base());
				i = children.begin();
				continue; // avoid normal increment
			}
		}

		// normal increment
		i++;
	}

	// pass event to the destination widget
	(*parent_iter)->DoMouseDown(X, Y, button);

	// the control that was clicked on gets the mouse focus so that the mouse up
	// handlers get correctly called. 
	MouseFocus = (*parent_iter);
}
//---------------------------------------------------------------------------  
void TEventDispatcher::_DoHandleMouseMove(int X, int Y, int RelX, int RelY, TMouseButton button)
{
	TWidgetTree::layer_iterator parent_iter;
	TWidgetTree::reverse_layer_iterator_range children;
	TWidgetTree::reverse_layer_iterator i;
	TPoint2D child_pos;

	// used to maintain a mouse over state so that mouse enter and exit events are 
	// passed properly!  Initialize to the tree root.
	static TWidget* MouseOver = WidgetTree->GetRoot();

	// parent is tree root
	parent_iter = WidgetTree->layer_begin();
	children = WidgetTree->GetReverseLayerRange(parent_iter.base());
	i = children.begin();

	while(i != children.end()) {
		// check inside
		if((*i)->CanAcceptInput() && (*i)->IsInsideLocal(X, Y)) {
			child_pos = (*i)->GetParent()->GetChildPosition((*i));

			// keep input if child does not accept it
			if((*i)->CanAcceptMouseInput(X - child_pos.X, Y - child_pos.Y)) {
				// modify X and Y
				X -= child_pos.X;
				Y -= child_pos.Y;

				// now we will search the children of this node!
				parent_iter = i.base();
				children = WidgetTree->GetReverseLayerRange(parent_iter.base());
				i = children.begin();
				continue; // avoid normal increment
			}
		}

		// normal increment
		i++;
	}

	// deal with enter/exit
	if(*parent_iter != MouseOver) {
		MouseOver->DoExit();
		MouseOver = *parent_iter;
		MouseOver->DoEnter();
	}

	// pass mouse move event to the destination widget
	(*parent_iter)->DoMouseMove(X, Y, RelX, RelY, button);
}
//---------------------------------------------------------------------------  




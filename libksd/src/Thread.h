//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//	
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		TThread, IRunnable
//	
//	Purpose:		Encapsulation of thread
//	
//	Checked-in by:	$Author$
//					$Date$
//--------------------------------------------------------------------------- 
#ifndef _KSD_ThreadH_
#define _KSD_ThreadH_
//--------------------------------------------------------------------------- 
#include <SDL/SDL_thread.h>
#include "export_ksd.h"

#ifndef NULL
// I am including this file in order to define NULL.  If someone
// finds a platform where doing this is unportable, PLEASE let me know.
#include <stddef.h>
#ifndef NULL
#error We have no NULL!!  Please see the file "Thread.h" and e-mail me, ASAP!!
#endif
#endif
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma interface
#endif
//--------------------------------------------------------------------------- 
namespace ksd {
//--------------------------------------------------------------------------- 
KSD_EXPORT int ThreadCallback(void*);
//---------------------------------------------------------------------------
KSD_EXPORT_CLASS class TMutex {
public:
	TMutex() 				{ Handle = SDL_CreateMutex(); }
	TMutex(const TMutex& V) { Handle = V.Handle; }
	~TMutex() 				{ SDL_DestroyMutex(Handle); }

	// TODO: deal with lock/unlock errors
	void Lock() 	{ SDL_mutexP(Handle); }
	void Unlock() 	{ SDL_mutexV(Handle); }
private:
	SDL_mutex* Handle;
};
//--------------------------------------------------------------------------- 
KSD_EXPORT_CLASS class TMutexLocker {
public:
	TMutexLocker(TMutex& _m) {
		Mutex = &_m;
		Mutex->Lock();
	}

	TMutexLocker(TMutex* _m) {
		Mutex = _m;
		Mutex->Lock();
	}

	~TMutexLocker() {
		Mutex->Unlock();
	}
private:
	TMutex* Mutex;
};
//--------------------------------------------------------------------------- 
KSD_EXPORT_CLASS class TThread { friend int ThreadCallback(void*);
public:
	TThread();
	virtual ~TThread();

	void WaitForStop() { if(IsRunning()) SDL_WaitThread(Handle, NULL); }

	void Start();
	void Stop();
	bool IsRunning();
protected:
	// overridden to create a thread
	virtual void RunThread() = 0;
	virtual void InitThread() 		{  };
	virtual void ShutdownThread() 	{  };
private:
	bool Running;
	TMutex RunningMutex;
	SDL_Thread* Handle;
};
//--------------------------------------------------------------------------- 
}; // namespace ksd
//--------------------------------------------------------------------------- 
#endif


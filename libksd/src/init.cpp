
/** This code is in the Public Domain.  Use wisely! **/

#include <list>
#include "init.h"

using namespace std;

typedef list<ModuleInit::FuncPtr> ListType;
_GLOBAL_DECL(ListType, InitList);
_GLOBAL_DECL(ListType, ShutdownList);

void ModuleInit::AddInitFunction(FuncPtr f)
{
    _GLOBAL_CREATE(InitList, new ListType);
    InitList->push_back(f);
}

void ModuleInit::AddShutdownFunction(FuncPtr f)
{
    _GLOBAL_CREATE(ShutdownList, new ListType);
    ShutdownList->push_back(f);
}

void ModuleInit::DoInitFunctions()
{
    if(_GLOBAL_EXISTS(InitList)) {
        for(ListType::iterator i = InitList->begin(); i != InitList->end(); i++)
            (*i)();
    }
}

void ModuleInit::DoShutdownFunctions()
{
    if(_GLOBAL_EXISTS(ShutdownList)) {
        for(ListType::iterator i = ShutdownList->begin(); i != ShutdownList->end(); i++)
            (*i)();
    }
}

void ModuleInit::Cleanup()
{
    _GLOBAL_DESTROY(InitList);
    _GLOBAL_DESTROY(ShutdownList);
}

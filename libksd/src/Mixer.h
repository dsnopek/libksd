//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2001 Arthur Peters
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//	
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			Arthur Peters
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		TMixer, TSampleInstance
//	
//	Purpose:        TMixer = An audio mixer implementation based on SDL_mixer.
//                  TSampleInstance = Represents a playing instance of a sample.
//	
//	Checked-in by:	$Author$
//					$Date$
//--------------------------------------------------------------------------- 
#ifndef _KSD_MixerH_
#define _KSD_MixerH_
//---------------------------------------------------------------------------
#include <SDL/SDL_types.h>
#include <sigc++/sigc++.h>
#include <vector>
#include <memory>
#include "Thread.h"
#include "exception.h"
#include "export_ksd.h"
//---------------------------------------------------------------------------
#ifdef __GNUG__
#pragma interface
#endif
//---------------------------------------------------------------------------
namespace ksd {
//---------------------------------------------------------------------------
// forward declares
class TMixer;
class TSampleInstance;

class TSample; // these are not really declared in this file but the
class TSong;   // full declaration is not needed and this saves compile
               // time
//---------------------------------------------------------------------------
KSD_EXPORT_CLASS class EMixerError : public ksd::exception {
public:
	EMixerError(const std::string& _message) 
		: ksd::exception("Mixer", _message, "mixer error") { }
};
// TODO: Create other exception types that descend from EMixerError that
// carry data specific to mixer functions.  For example, a ESampleError that
// wil hold the channel, volume, and if possible the sample object.  This
// information could then be used by calling classes to deal with the error
// or output by report() to help debug the problem.
//---------------------------------------------------------------------------
/**
   The object used to access a currently playing instance of a
   \Ref{TSample}. It provides function to start, stop, pause, resume
   and fade the instance. It also provides function to query the state
   of the instance.
*/   
KSD_EXPORT_CLASS class TSampleInstance : public SigC::Object { friend class TMixer;
protected:
	class Data;
 
	/**
	   Initialize a TSampleInstance with a data object.
	*/
	TSampleInstance(Data* data);
	
private:
	/**
	   Don't allow making a copy of a sample instance.
	*/
	TSampleInstance(TSampleInstance& inst);

public:
	/**
	   Make an empty sample instance.
	*/
	TSampleInstance()
		{
			InstanceData = NULL;
		}

	~TSampleInstance();

	/**
	   @return true if the instance is playing and false otherwise.
	*/
	bool IsPlaying() const;
	/**
	   @return true if the instance is paused and false otherwise.
	*/
	bool IsPaused() const;
	/**
	   @return true if the instance is stoped and false otherwise.
	*/
	bool IsStopped() const;

	/**
	   Stop the instance after fading it for {\it fadeout} ms.
	   @return true if the channel was playing and is now stoped, false otherwise.
	*/
	bool Stop( int fadeout = 0 );
	/**
	   Stop the instance after {\it ms} ms. This may not work I think
	   there is a bug in SDL_mixer.

	   @return True if the channel is playing and will stop in {\it ms} ms, false otherwise.
	*/
	bool StopIn( int ms );

	/**
	   Pause the instance.
	*/
	void Pause();
	/**
	   Resume playing the instance if it was paused.
	*/
	void Resume();

	/**
	   Set the volume of the instance. This does not affect othe
	   instances of the same sample.
	*/
	void SetVolume( int vol );
	/**
	   @return The volume of the instance.
	*/
	int GetVolume() const;

private:

	/**
	   This function will be called when the mixer finishs playing
	   this instance. The idea is that once the mixer has called this
	   it can free the data object and not risk causing segfaults in
	   the main program.

	   @see TSampleInstance::InstanceData
	*/
	void FinishedPlaying();

	/**
	   A pointer to the object used to communicate with the
	   mixer. When instance is finished playing the mixer will call
	   \Ref{FinishedPlaying}. This way the TSampleInstance object
	   knows that it is finished playing and will not try to make any
	   SDL_mixer calls or access {\it Data}.
	 
	   @see TSampleInstance::FinishedPlaying
	*/
	Data* InstanceData;
};

//---------------------------------------------------------------------------
/**
   The overall mixer object. It provides all the sample and music
   playing function.
*/
KSD_EXPORT_CLASS class TMixer {
public:
	TMixer();
	~TMixer();

	/**
	   This structure contains all the parameters nessecary to
	   initialize the mixer.

	   @see TMixerAttr
	*/
	struct Attr {
		int SamplingRate;
		Uint16 Format;
		int ChunkSize, OutputChannels;
	};

	/**
	   Initialize the mixer. Mixer parameters are provided as
	   seperate values. No other mixer functions can be used before
	   this is called.
	*/
	void Init(int sampling_rate, Uint16 format, int output_channels = 2, int chunksize = -1);
	/**
	   Initialize the mixer. Mixer parameters are provided as a
	   \Ref{TMixerAttr} object. No other mixer functions can be used
	   before this is called.
	*/
	void Init(const Attr& attr)
		{ Init( attr.SamplingRate, attr.Format, attr.OutputChannels, attr.ChunkSize ); }

	/**
	   @memo Close the mixer.
	   Stop all playing samples and music and close the mixer.
	*/
	void Close();

	/**
	   Set the number of mixer channels (you should never {\it need}
	   to do this). This is useful if you know you are going to be
	   using a lot of channels and don't want them to be allocated
	   dynamicly.

	   @return the number of mixer channels allocated at the end of the operation.
	*/
	int SetChannels( int channels );

	/**
	   @return The number channels currently allocated.
	   @see TMixer::SetChannels
	*/
	int GetChannels() const;

	/**
	   @return The sampling rate of the mixer.
	*/
	int GetSamplingRate() const;
      
	/**
	   @return The size of the chunk beginning mixed and sent to the audio device.
	*/
	int GetChunkSize() const;     
      
	/**
	   @return The sample format.
	*/
	Uint16 GetFormat() const;

	/**
	   Start playing {\it sample}.

	   Don't do: \\
	   std::auto_ptr<TSampleInstance> inst; \\
	   inst = Mixer.StartSample(...); \\

	   Do: \\
	   std::auto_ptr<TSampleInstance> inst; \\
	   inst.reset( Mixer.StartSample(...).release() ); \\

	   Or: \\
	   TSampleInstance* inst; \\
	   inst = Mixer.StartSample(...).release(); \\

	   This is because G++ versions before 3.0 have a broken auto_ptr.

	   @param loops Setting {\it loops} to 1 will play the sample one setting it to 2 will play it twice.
	   @param volume The volume at which to start the sample. (In the range of 0-128.)
	   @param fadein The number of ms over which to fade in the sample.
	   @param playfor Automaticly stop the sample after this number of ms. If it is -1 don't stop it.

	   @return An auto_ptr to a TSampleInstance representing the sample that was started.
	*/
	std::auto_ptr<TSampleInstance> StartSample( TSample& sample, int loops = 1,
												int volume = 128, int fadein = 0,
												int playfor = -1 )
		throw( EMixerError );

	/**
	   Start playing {\it song}. It will be looped {\it loops} time
	   and faded in for {\it fadein} ms. \\

	   The fade in feature doesn't work and I don't know why. (help
	   me!)
	*/
	void StartMusic( TSong& song, int loops = 1, int fadein = 0 )
		throw(EMixerError);

	/**
	   Pause the music.
	*/
	void PauseMusic();
	/**
	   Resume playing the music.
	*/
	void ResumeMusic();
	/**
	   Rewind the music. This can cause some problems when used on
	   MP3s.
	*/
	void RewindMusic();
	/**
	   Stop the music after fading for {\it fadeout} ms.
	*/
	void StopMusic( int fadeout = 0 );
	/**
	   Set the music volume.
	*/
	void SetMusicVolume( int vol );
	/**
	   Get the music volume.
	   @return the current music volume.
	*/
	int GetMusicVolume() const;

	/**
	   @return true if music is paused and false otherwise.
	*/
	bool IsMusicPaused() const;
	/**
	   @return true if music is playing and false otherwise.
	*/
	bool IsMusicPlaying() const;
	/**
	   @return true if music is stopped (not paused, stopped) and false otherwise.
	*/
	bool IsMusicStopped() const;

	/**
	   Signal called when the current song finishes.
	*/
	SigC::Signal0<void> MusicFinished;
      
private:
      
	typedef std::vector< TSampleInstance::Data * > TPlayingInstanceList;

	/**
	   A list containing all of the currently playing sample
	   instances. All accesses to this should be protected by
	   locking \Ref{TMixer::PlayingInstances_Lock}.
	*/
	TPlayingInstanceList PlayingInstances;
	/**
	   The mutex used to protect accesses to
	   \Ref{TMixer::PlayingInstances}.
	*/
	TMutex PlayingInstances_Lock;

	TSong* CurrentSong;

	/**
	   Called when SDL_mixer finishes playing a song. It emits the
	   \Ref{TMixer::MusicFinished} signal.
	*/
	static void MusicFinishedCallback();
	/**
	   Called after processing each block of audio. It calls
	   \Ref{TMixer::CheckInstances}.
	*/
	static void PostMixCallback( void *udata, Uint8 *stream, int len );

	/**
	   Checks each TSampleInstance in PlayingInstances to see if it
	   has finished. If it has, the finction takes appropriate
	   actions.
	*/
	void CheckInstances();
};
//--------------------------------------------------------------------------- 
/**
   This structure contains all the parameters nessecary to initialize
   the mixer.

   @see TMixer::Init
*/
typedef TMixer::Attr TMixerAttr;
   
/**
   This is the global mixer object. You should access this rather than
   constructing your own TMixer object.

   @see TMixer
*/
KSD_EXPORT extern TMixer Mixer;
//---------------------------------------------------------------------------
}; // namespace ksd
//--------------------------------------------------------------------------- 
#endif

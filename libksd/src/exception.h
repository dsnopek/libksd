//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2001 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//	
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		exception, ENotInitializedError, EFileError
//	
//	Purpose:	    The base class for all libksd exceptions, and some basic
//					utility exceptions.
//	
//	Checked-in by:	$Author$
//					$Date$
//--------------------------------------------------------------------------- 
#ifndef _KSD_exceptionH_
#define _KSD_exceptionH_
//---------------------------------------------------------------------------
#include <string>
#include <iosfwd>
#include "export_ksd.h"
#include "log.h"
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma interface
#endif
//--------------------------------------------------------------------------- 
namespace ksd {
//---------------------------------------------------------------------------  
/** The base class for all libksd exceptions.
 */
KSD_EXPORT_CLASS class exception {
public:
	/** Create an exception.
	 ** 
	 ** @param _sender Refers to the unit that threw it. (ex. Screen, Application)
	 ** @param _message A generic text message.  If you need to leave more then add 
	 ** 	it in a subclass and overload report.
	 */
	exception(const std::string& _sender, const std::string& _message, const std::string& _type = "generic exception") throw() {
		Sender = _sender;
		Message = _message;
		Type = _type;

		LOG(1, "%s: %s", Sender.c_str(), Message.c_str());
	}

	/// Destructor.
	virtual ~exception() { }

	/// Returns the sender string.
	std::string who() const throw() { return Sender; }
	/// Returns the message string.
	std::string what() const throw() { return Message; }
	/// Returns a type name for this exception.
	std::string type() const throw() { return Type; }

	/** Prints a detailed description of the exception to an output stream.  
	 ** Sub-classes should overload this to provide output for the extra data 
	 ** that they carry and leave the exception message unpolluted.
	 */
	virtual void report(std::ostream& output) const;

	/** This just calls the other report with the standard error stream.  In general,
	 ** you'll want to call this function instead of the other, just for easy of use.
	 */
	void report() const;
private:
	std::string Sender, Message, Type;
};
//---------------------------------------------------------------------------  
/** Throw when a system should be initialized to perform a certain operation
 ** but in fact it is not.
 */
KSD_EXPORT_CLASS class ENotInitialized : public ::ksd::exception {
public:
	/// Constructor.
	ENotInitialized(const std::string& _unit, const std::string& _operation)
		: ::ksd::exception(_unit, _operation, "not initialized error") { }
};
//---------------------------------------------------------------------------
/** Throw when a certain feature is requested but not support under the
 ** current platform.
 */
KSD_EXPORT_CLASS class EUnsupported : public ::ksd::exception {
public:
	/// Constructor.
	EUnsupported(const std::string& _unit, const std::string& _feature) 
		: ::ksd::exception(_unit, _feature, "unsupported error") { }
};
//---------------------------------------------------------------------------  
/** This exception is meant to be thrown as a "user error" exception.  It 
 ** usually means that the libksd user has passed some invalid input to a
 ** function but it could also mean that a buggy module passed some bad 
 ** input to another module.
 */
KSD_EXPORT_CLASS class EInvalidInput : public ::ksd::exception {
public:
	/// Constructor
	EInvalidInput(const std::string& _unit, const std::string& _message)
		: ::ksd::exception(_unit, _message, "invalid input error") { }
};
//---------------------------------------------------------------------------  
/** A generic file error.  Keeps track of the filename in question.  This
 ** is a very useful interface to catch at any file related function.
 */
KSD_EXPORT_CLASS class EFileError : public ::ksd::exception {
public:
	/// Constructor.
	EFileError(const std::string& _unit, const std::string& _message, 
			   const std::string& _filename) 
			   : ::ksd::exception(_unit, _message, "file error")
	{
		Filename = _filename;
	}

	/// Returns the filename.
	std::string GetFilename() const { return Filename; }

	void report(std::ostream& stream) const;
private:
	std::string Filename;
};
//--------------------------------------------------------------------------- 
/** Thrown when a module doesn't grant access for a specific type of 
 ** operation.  For example, writting to an object marked as read-only.
 */
KSD_EXPORT_CLASS class EPermissionError : public ::ksd::exception {
public:
	/// Constructor.
	EPermissionError(const std::string& _unit, const std::string& _message)
		: ::ksd::exception(_unit, _message, "permission error") { }
};
//--------------------------------------------------------------------------- 
}; // namespace ksd
//---------------------------------------------------------------------------
#endif

//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2001 Arthur Peters
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//	
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			Arthur Peters
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		TSample
//	
//	Purpose:	        TSample contains audio that can be played with TMixer
//	
//	Checked-in by:	$Author$
//					$Date$
//--------------------------------------------------------------------------- 
#ifndef _KSD_SongH_
#define _KSD_SongH_
//---------------------------------------------------------------------------
#include <SDL/SDL_mixer.h> 
#include "export_ksd.h"
//---------------------------------------------------------------------------
#ifdef __GNUG__
#pragma interface
#endif
//---------------------------------------------------------------------------
namespace ksd {
//---------------------------------------------------------------------------
class TMixer;
//---------------------------------------------------------------------------
KSD_EXPORT_CLASS
class TSong { friend class TMixer;
public:
	/**
	   Initialize the object as a empty song. If you try to play
	   it the mixer will return an error.
	*/ 
	TSong();
	/**
	   Initialize the object by loading a song from a file.
	*/
	TSong( const char* filename )
		{
			Load( filename );
		}

	~TSong();

	/**
	   Load a song from a file. The supported formats include: MIDI
	   (using timidity), MOD, XM, IT, S3M (all module format use
	   MikMod), MP3 (using SMPEG) and OGG (using the vorbis
	   library). All formats are auto-detected.
	*/
	void Load( const char* filename );

private:

	void FreeSongData();
      
	bool InUse;
      
	/**
	   The SDL_mixer struct the this class wraps.
	*/
	Mix_Music* SongData;
};
//---------------------------------------------------------------------------
}; // namespace ksd
//--------------------------------------------------------------------------- 
#endif

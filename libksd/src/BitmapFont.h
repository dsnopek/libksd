//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//	
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		TFont
//	
//	Purpose:		A bitmaped font class.  Uses SFont for implementation.
//	
//	Checked-in by:	$Author$
//					$Date$
//--------------------------------------------------------------------------- 
#ifndef _KSD_BitmapFontH_
#define _KSD_BitmapFontH_
//--------------------------------------------------------------------------- 
#include "Font.h"
#include "SFont.h"
#include "FontDB.h"
#include "export_ksd.h"
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma interface
#endif
//--------------------------------------------------------------------------- 
namespace ksd {
//---------------------------------------------------------------------------  
KSD_EXPORT SFont_FontInfo* CreateSFont(const TFontAttr&, const char*);
//--------------------------------------------------------------------------- 
/** A font class for using bitmap fonts.  I don't suggest using it directly.
 ** Instead, use TFontList.
 ** 
 ** @see TFont, TFontList
 */
KSD_EXPORT_CLASS class TBitmapFont : public TFont { 
	friend SFont_FontInfo* CreateSFont(const TFontAttr&, const char*);
public:
	///
	TBitmapFont() { }
	///
	TBitmapFont(const std::string& filename) { Open(filename); }

	void Open(const std::string& filename);
	void Optimize(TFontAttr* List, int n) { DB.Optimize(List, n); }

	int GetHeight(const TFontAttr&);
	int GetAscent(const TFontAttr&);
	int GetDescent(const TFontAttr&);
	int GetLineSkip(const TFontAttr&);

	void GetTextSize(const std::string& text, const TFontAttr&,
					 int& width, int& height);
protected:
	void DrawString(SDL_Surface*, int X, int Y, const std::string& text, 
					TRect* ClipRect, const TFontAttr& attr);
private:
	FontDB<SFont_FontInfo, &CreateSFont, &SFont_CloseFont> DB;
	//Uint16 pt_size;
};
//--------------------------------------------------------------------------- 
}; // namespace ksd
//--------------------------------------------------------------------------- 
#endif


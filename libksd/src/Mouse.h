//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//	
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		TKeyboard, TKeyboardFilter, TKey
//	
//	Purpose:		Encapsulates the keyboard.
//	
//	Checked-in by:	$Author$
//					$Date$
//--------------------------------------------------------------------------- 
#ifndef _KSD_MouseH_
#define _KSD_MouseH_
//--------------------------------------------------------------------------- 
#include <SDL/SDL_mouse.h>
#include "Point2D.h"
#include "Input.h"
#include "export_ksd.h"
//---------------------------------------------------------------------------
#ifdef __GNUG__
#pragma interface
#endif
//---------------------------------------------------------------------------
namespace ksd {

// forward decls
class TMouse;
//---------------------------------------------------------------------------
KSD_EXPORT_CLASS class TMouseFilter : public TInputFilter {
public:
	TMouseFilter(TMouse* ms);

	void Filter(TInputEvent& evt) { }
	void Detect(int id, int time_out = 1000) { }
	void Load(char*) { }
	void Save(char*) { }
};
//---------------------------------------------------------------------------
// I don't usually like to copy values from SDL because it makes mantainance
// more difficult than it needs to be, but for consistancy I need an enumeration
// for mouse button. Also it is cleaner to specify mouse instead of just button.
enum TMouseButton {
	MOUSE_NONE		= 0,
	MOUSE_LEFT 		= SDL_BUTTON_LEFT,
	MOUSE_MIDDLE	= SDL_BUTTON_MIDDLE,
	MOUSE_RIGHT		= SDL_BUTTON_RIGHT
};
//---------------------------------------------------------------------------
// TODO: Add mouse enabled flag -- if set false, then don't process mouse events in
// widget -- it will speed things up (in fact, use an event filter to make sure
// none of the events even reach the queue -- maybe use the same to stop joy axis
// move for axis repeat) + hide cursor
KSD_EXPORT_CLASS class TMouse : public TInputDevice {
public:
	TMouse() : TInputDevice() { }

	// accessors
	bool 	IsButtonPressed(TMouseButton b) { return SDL_BUTTON(b); }
	int		GetX()							{ return X; }
	int		GetY()							{ return Y; }
	TPoint2D	GetPosition()				{ return TPoint2D(X, Y); }

	// input device funcs
	TInputFilter* CreateFilter() 	{ return new TMouseFilter(this); }
	void Update()					{ button = SDL_GetMouseState(&X, &Y); }
protected:
	Uint8 button;
	int X, Y;
};
//---------------------------------------------------------------------------
KSD_EXPORT extern TMouse Mouse;
//---------------------------------------------------------------------------
}; // namespace ksd
//---------------------------------------------------------------------------
#endif


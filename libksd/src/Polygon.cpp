//---------------------------------------------------------------------------
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//
//---------------------------------------------------------------------------
//	$Header$
//
//	Author:			Andrew Sterling Hanenkamp
//	Revised by:
//	Version:		$Revision$
//
//	Checked-in by:	$Author$
//
//---------------------------------------------------------------------------
#ifdef __GNUG__
#pragma implementation
#endif
//---------------------------------------------------------------------------
#define KSD_BUILD_LIB
#include "ksd.h"
#include "Polygon.h"

using namespace ksd;
using namespace std;
//---------------------------------------------------------------------------
TEdgeTable::TEdgeTable(const int _length, const TPoint2D *polygon, const TRect &_cliprect)
	: cliprect(_cliprect)
{
	const TPoint2D *top, *bottom;
	list<Edge>::iterator line;
	list<Edge*>::iterator actline, copyline;

	lasty = 0;
	for(int i = 0, j = 1; i < _length; i++, j++) {
		if(j == _length) j = 0;

		if(polygon[i].Y < polygon[j].Y) {
			top    = polygon+i;
			bottom = polygon+j;
		} else {
			top    = polygon+j;
			bottom = polygon+i;
		}
		if(bottom->Y > lasty) lasty = bottom->Y;

		for(line = alloc.begin();
			line != alloc.end() && line->start.Y < top->Y;
			line++);
		alloc.insert(line, Edge(*top, *bottom));
	}

	for(line = alloc.begin(); line != alloc.end(); line++) {
		lines.push_back(&(*line));
	}

	if(lasty > cliprect.GetBottom()) lasty = cliprect.GetBottom();
	else lasty++;

	currenty = lines.front()->start.Y < cliprect.GetTop() ? cliprect.GetTop() :
			   lines.front()->start.Y;
	firsty = currenty;
}
//---------------------------------------------------------------------------
TEdgeTable::~TEdgeTable()
{
	//
}
//---------------------------------------------------------------------------
bool TEdgeTable::NextLine()
{
	if(currenty < lasty) {
		LOG(10, "----------- STARTING NEXTLINE ------------");
		list<Edge*>::iterator line, order;

		// Remove any old lines
		while(!active.empty() && active.front()->end.Y <= currenty) {
			active.erase(active.begin());
		}

		// Add any new lines
		while(!lines.empty() && lines.front()->start.Y == currenty) {
			for(line = active.begin();
				line != active.end() && (*line)->end.Y < lines.front()->end.Y;
				line++);
			active.insert(line, lines.front());
			lines.erase(lines.begin());
		}

		// Order all the active lines by X
		ordered.clear();
		for(line = active.begin(); line != active.end(); line++) {
			for(order = ordered.begin();
				order != ordered.end() && (*order)->current.a < (*line)->current.a;
				order++);
			ordered.insert(order, *line);
		}

		// Reset the even/odd counter
		even = false;

		// Increment the current Y counter
		currenty++;

		// Let the caller know that there's stuff to do this scan line
		return true;
	} else {
		// After the last line, no more scan lines to convert
		return false;
	}
}
//---------------------------------------------------------------------------
bool TEdgeTable::ScanLine(int &startx, int &endx, bool &visible, int &type)
{
	if(!ordered.empty()) {
		LOG(10, "----------- STARTING SCANLINE ------------");
		if(ordered.front()->current.inc == 0) {
			LOG(10, "inc == 0");
			endx = ordered.front()->current.a+1;
			startx = ordered.front()->current.a+ordered.front()->current.dx+1;
			visible = true;
			type = 0;
			ordered.erase(ordered.begin());
			if(startx < cliprect.GetLeft()) startx = cliprect.GetLeft();
			if(endx > cliprect.GetRight()) endx = cliprect.GetRight();
		} else {
			startx = ordered.front()->current.a;
			type = even ? 1 : -1;
			LOG(10, "startx = %d, type = %d", startx, type);
			if(ordered.front()->current.inc > 0) {
				LOG(10, "inc > 0");
				ordered.front()->current.NextLine();
				endx = ordered.front()->current.a;
				LOG(10, "endx = %d", endx);
			} else {
				LOG(10, "inc < 0");
				endx = ordered.front()->current.olda;
				ordered.front()->current.NextLine();
				LOG(10, "endx = %d", endx);
			}
			if(!even) {
				startx++;
				endx++;
				LOG(10, "even edge, fixing startx = %d, endx = %d", startx, endx);
			}

			if(endx <= cliprect.GetLeft()) {
				LOG(10, "(endx = %d) <= (cliprect.GetLeft() = %d)", endx, cliprect.GetLeft());
				// We're off the left edge of the clipping area. We need to make
				// sure that we are relavant to the drawing area.
				//
				// A line is relevant iff it is an odd line followed by an even
				// line that is within the clipping area or it is a line within
				// the clipping area.
				//
				// Because we want this algorithm to be as quick as possible we
				// want to skip all irrelevant edges on this first call and only
				// return relevant edges to the caller. We will scan to find the
				// first relevant edge, which could be the first.
				//
				// This scanning will, necessarily, result in finding either a
				// "offscreen" odd edge followed by an "onscreen" even edge. Or we
				// will encounter an "onscreen" odd edge. (That is, if we encounter
				// an "onscreen" even edge, we have missed the immediately previous
				// "offscreen" odd edge we should have found first. By the Law of
				// Noncontradiction, this is a false possibility.)
				//
				// In the former ("offscreen" odd), we clip the edge and tell the
				// Polygon caller that it is invisible. In the latter ("onscreen"
				// odd), we tell the Polygon caller to draw the edge normally.

				// Find the next off-odd or on-odd
				LOG(10, "Starting while...");
				while(ordered.begin() != ordered.end()) {
					LOG(10, "ordered.size() = %d, even = %d", ordered.size(), even);
					if(!even && (*(++ordered.begin()))->current.a > cliprect.GetLeft()) {
						// We found an offscreen odd followed by an onscreen even
						LOG(10, "found offscreen odd followed by onscreen even");
						startx = cliprect.GetLeft()-1;
						endx = cliprect.GetLeft();
						visible = false;
						type = even ? 1 : -1;
						break; // exit the loop and return
					} else if(!even && ordered.front()->current.a > cliprect.GetLeft()) {
						// We found an onscreen odd
						LOG(10, "found onscreen odd");
						startx = ordered.front()->current.a;
						type = even ? 1 : -1;
						if(ordered.front()->current.inc > 0) {
							ordered.front()->current.NextLine();
							endx = ordered.front()->current.a;
						} else {
							endx = ordered.front()->current.olda;
						}
						startx++; endx++;
						visible = true;
						break; // exit the loop and return
					} else {
						// Nothing found yet--or this is even, so we keep scanning
						// Make sure we update the X though for the next iteration
						ordered.front()->current.NextLine();
						ordered.erase(ordered.begin());
						even = !even;
						LOG(10, "Nothing found yet...moving on...");
					}
				}
				LOG(10, "Ending while...");
				// Remove this line, and advance the even/odd counter for the next
				// scan line iteration
				ordered.erase(ordered.begin());
				even = !even;
			} else if(startx >= cliprect.GetRight()) {
				// If this is an even edge, then we should terminate the fill area
				// to our left. Then, kill the rest of the edges because there is
				// no need to continue. If this is an odd edge, there is no need
				// to draw this line, so kill the rest of the edges and return
				// false to hald the scanline conversion.
				if(even) {
					startx = endx = cliprect.GetRight()+1;
					visible = false;
					// Make sure we update the X's though for the next iteration
					while(ordered.begin() != ordered.end()) {
						// don't bother even because it will be reset
						ordered.front()->current.NextLine();
						ordered.erase(ordered.begin());
					}
				} else {
					// Make sure we update the X's though for the next iteration
					while(ordered.begin() != ordered.end()) {
						// don't bother even because it will be reset
						ordered.front()->current.NextLine();
						ordered.erase(ordered.begin());
					}
					return false;
				}
			} else {
				// All other cases are visible edges and should be drawn normally

				// Clip startx if necessary
	//			if(startx < ordered.front()->start.X) startx = ordered.front()->start.X;
	//			if(startx < ordered.front()->end.X)   startx = ordered.front()->start.X;
				if(startx < cliprect.GetLeft())      startx = cliprect.GetLeft();

				// Clip endx if necessary
	//			if(endx > ordered.front()->start.X) endx = ordered.front()->start.X+1;
	//			if(endx > ordered.front()->end.X)   endx = ordered.front()->end.X+1;
				if(endx > cliprect.GetRight())     endx = cliprect.GetRight()+1;

				// Remove this line, and advance the even/odd counter for the next
				// scan line iteration
				ordered.erase(ordered.begin());
				even = !even;
				visible = true;
			}

			if(visible && startx == endx) endx++;
		}

		LOG(10, "-- FINAL -- : startx = %d, endx = %d, visible = %d, type = %d",
				startx, endx, visible, type);
		return true;
	} else {
		// No edges left, nothing to be done for this scan line
		return false;
	}
}
//---------------------------------------------------------------------------

//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//	
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		TTrueTypeFont
//	
//	Purpose:		Class for manipulating true type fonts.
//	
//	Checked-in by:	$Author$
//					$Date$
//--------------------------------------------------------------------------- 
#ifndef _KSD_TrueTypeFontH
#define _KSD_TrueTypeFontH
//--------------------------------------------------------------------------- 
#include <SDL/SDL_ttf.h>
#include "Font.h"
#include "FontDB.h"
#include "export_ksd.h"
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma interface
#endif
//--------------------------------------------------------------------------- 
namespace ksd {
//---------------------------------------------------------------------------  
KSD_EXPORT TTF_Font* CreateTTF(const TFontAttr&, const char*);
//---------------------------------------------------------------------------  
/** A font class for using true type fonts.  I don't suggest using it directly.
 ** Instead, use TFontList.
 ** 
 ** @see TFont, TFontList
 */
KSD_EXPORT_CLASS class TTrueTypeFont : public TFont {
public:
	///
	TTrueTypeFont() { };
	///
	TTrueTypeFont(const std::string& filename) { Open(filename); }

	void Open(const std::string& filename);

	int GetHeight(const TFontAttr&);
	int GetAscent(const TFontAttr&);
	int GetDescent(const TFontAttr&);
	int GetLineSkip(const TFontAttr&);

	void GetTextSize(const std::string& text, const TFontAttr&,
					 int& width, int& height);
protected:
	void DrawString(SDL_Surface*, int X, int Y, const std::string& text, 
					TRect* ClipRect, const TFontAttr&);
private:
	FontDB<TTF_Font, &CreateTTF, &TTF_CloseFont> DB;
};
//---------------------------------------------------------------------------  
}; // namespace ksd
//---------------------------------------------------------------------------  
#endif


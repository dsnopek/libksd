//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		TShape2D
//	
//	Purpose:		Defines a simple generic interface to 2-dimensional shapes
//					in screen space.
//	
//	Checked-in by:	$Author$
//					$Date$
//---------------------------------------------------------------------------
#ifndef _KSD_Shape2DH
#define _KSD_Shape2DH
//---------------------------------------------------------------------------
#include "Point2D.h"
//---------------------------------------------------------------------------
namespace ksd {

// forward decls
class TRect;
class TLine;
class TCircle;
//---------------------------------------------------------------------------
/** A generic interface to a 2D dimensional shape.  Created for dealing in 
 ** screen space.  The precision of the shape library is not suited to
 ** complex physical simulations.
 */
KSD_EXPORT_CLASS class TShape2D {
public:
    virtual ~TShape2D() { };

	/** Returns true if the point is inside of this shape.
	 */
    virtual bool IsInside(const TPoint2D& V) const = 0;
    
	/** Move the shape by the amount given.  This works in an incremental 
	 ** fashion.
	 */
	virtual void Move(int X, int Y) = 0;

	/** Moves the shape to the given lcation.
	 */
    virtual void MoveTo(int X, int Y) = 0;

   	/** Move the shape by the amount given.  This works in an incremental 
	 ** fashion.
	 */
	void Move(const TPoint2D& p) { Move(p.X, p.Y); }

	/** Moves the shape to the given lcation.
	 */
	void MoveTo(const TPoint2D& p) { MoveTo(p.X, p.Y); }

	/** Used to add new operations to a shape without modifying the 
	 ** source.
	 */
	class Visitor {
	public:
		virtual void VisitRect(TRect*) = 0;
		virtual void VisitCircle(TCircle*) = 0;
		virtual void VisitLine(TLine*) = 0;
	};
	
	/// Shape will perform visitor operation on self
	virtual void Visit(Visitor*) = 0;
};
//---------------------------------------------------------------------------
}; // namespace ksd
//---------------------------------------------------------------------------
#endif


//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		tree
//	
//	Purpose:		Describes a generic n-childnode tree. 
//	
//	Checked-in by:	$Author$
//					$Date$
//---------------------------------------------------------------------------
#ifndef _KSD_treeH_
#define _KSD_treeH_
//---------------------------------------------------------------------------
#include <list>
#include "exception.h"
//---------------------------------------------------------------------------
namespace ksd {
//---------------------------------------------------------------------------
/** A generic tree container.  Allows you to build a tree were each node can 
 ** have any number of children.  It is stored in parent-before-child order 
 ** and also allows iterating in a layered fashion from sibling to sibling.
 */
template<class T>
class tree {
public:
	tree() { }
	~tree() { 
		clear();
	}

	/** Sets the root of the tree to the given item.  A tree cannot be used until it
	 ** has a root.
	 */
	void set_root(const T& root_data) {
		if(has_root()) {
			throw ::ksd::exception("tree", "Cannot set root when the tree already has one");
		} else {
			Node* root_node;
			root_node = new Node(root_data, TreeList.end());
			TreeList.push_front(root_node);
			root_node->last_child = TreeList.begin();
			root_node->next_sibling = TreeList.end();
			root_node->prev_sibling = TreeList.begin();
		}
	}

	/// Returns true if this tree has a root.
	bool has_root() const { return TreeList.size() > 0; }
	/// Returns a reference to the root of this tree
	T& get_root() { return TreeList.front()->Data; }
	/// Returns a reference to the root of this tree
	T  get_root() const { return TreeList.front()->Data; }

	/// Returns the number of items in the tree
	unsigned int size() const { return TreeList.size(); }
private:
	struct Node;
	typedef std::list<Node*> list_type;
	typedef typename list_type::iterator list_iterator;

	struct Node {
		Node(const T& _data, const list_iterator& _parent) { 
			Data = _data;
			parent_iter = _parent;
		}
	
		T Data;
		list_iterator last_child;
		list_iterator next_sibling;
		list_iterator prev_sibling;
		list_iterator parent_iter;
	};
	
	list_type TreeList;

	list_iterator find_node(const T& item) {
		list_iterator i;
		for(i = TreeList.begin(); i != TreeList.end(); i++) {
			if((*i)->Data == item) break;
		}
		return i;
	}
public:
	/** The basic tree iterator type.  Allows to step through every element in a 
	 ** tree or just skip to the next/previous siblings or the first/last child.
	 */
	class iterator {
	public:
		// making unintialized iterators
		iterator() { }
	
		/// steps forward (in preorder) through the tree.
		void next() { internal++; }
		/// steps backward (in preorder) through the tree.
		void previous() { internal--; }
		/// steps over this items's children to its next sibling.
		void next_sibling() { internal = get_node()->next_sibling; }
		/// steps over the previous item's children, to the previous sibling
		void previous_sibling() { internal = get_node()->prev_sibling; }
		/// steps to this item's first child
		void first_child() { next(); }
		/// steps to this item's last child
		void last_child() { internal = get_node()->last_child; }
		/// steps up to this item's parent
		void up_to_parent() { internal = get_node()->parent_iter; }
		/// Returns true if this widget has children
		bool has_children() { return get_node()->last_child != internal; }

		/// Returns the item at this iterator.
		T& get_data() { return get_node()->Data; }
		/// Returns the item at this iterator.
		const T& get_data() const { return get_node()->Data; }

		/// steps forward (in preorder) through the tree.
		iterator& operator++ () { next(); return *this; }
		/// steps backward (in preorder) through the tree.
		iterator& operator-- () { previous(); return *this; }

		/// steps forward (in preorder) through the tree.
		iterator operator++ (int) { 
			iterator temp = *this;
			next();
			return temp; 
		}

		/// steps backward (in preorder) through the tree.
		iterator operator-- (int) { 
			iterator temp = *this;
			previous(); 
			return temp; 
		}

		/// Returns the item at this iterator.
		T& operator* () { return get_data(); }
		/// Returns the item at this iterator.
		const T& operator* () const { return get_data(); }

		/// Returns false if the iterators point to the same widget.
		bool operator!= (const iterator& _other) const { 
			return internal != _other.internal;
		}

		/// Returns true is the iterators point to the same object
		bool operator== (const iterator& _other) const {
			return internal == _other.internal;
		}

		/// Assign this iterator to the value of another
		iterator& operator= (const iterator& V) {
			internal = V.internal;
			return *this;
		}
	private:
		// only tree<T> can make valid iterators!
		friend class tree<T>;
		explicit iterator(tree<T>::list_iterator _iter) {
			internal = _iter;
		}
	
		tree<T>::Node* get_node() { return *internal; }
		const tree<T>::Node* get_node() const { return *internal; }
		
		tree<T>::list_iterator& get_list_iterator() { 
			return internal; 
		}
	
		tree<T>::list_iterator internal;
	};
	friend class iterator;

	/** A special iterator that mimics the interface of the standard iterator but 
	 ** the increment and decrement operators move by sibling.  The only way to
	 ** access the children is but using the first_child and last_child functions.
	 */
	class layer_iterator {
	public:
		typedef tree<T>::iterator tree_iterator;

		// wraps the given iterator
		layer_iterator(const tree_iterator& _iter) {
			internal = _iter;
		}
		
		// making unintialized iterators
		layer_iterator() { }
	
		/// steps to the next sibling.
		void next() { next_sibling(); }
		/// steps to the previous sibling.
		void previous() { previous_sibling(); }
		/// steps over this items's children to its next sibling.
		void next_sibling() { internal.next_sibling(); }
		/// steps over the previous item's children, to the previous sibling
		void previous_sibling() { internal.previous_sibling(); }
		/// steps to this item's first child
		void first_child() { internal.next(); }
		/// steps to this item's last child
		void last_child() { internal.last_child(); }
		/// steps up to this item's parent
		void up_to_parent() { internal.up_to_parent; }
		/// Returns true if this widget has children
		bool has_children() { return internal.has_children(); }

		/// Returns the item at this iterator.
		T& get_data() { return internal.get_data(); }
		/// Returns the item at this iterator.
		const T& get_data() const { return internal.get_data(); }

		/// steps to the next sibling.
		layer_iterator& operator++ () { next(); return *this; }
		/// steps to the previous sibling.
		layer_iterator& operator-- () { previous(); return *this; }

		/// steps to the next sibling.
		layer_iterator operator++ (int) { 
			layer_iterator temp = *this;
			next();
			return temp; 
		}

		/// steps to the previous sibling.
		layer_iterator operator-- (int) { 
			layer_iterator temp = *this;
			previous(); 
			return temp; 
		}

		/// Returns the item at this iterator.
		T& operator* () { return get_data(); }
		/// Returns the item at this iterator.
		const T& operator* () const { return get_data(); }

		/// Returns false if the iterators point to the same widget.
		bool operator!= (const layer_iterator& _other) const { 
			return internal != _other.internal;
		}

		/// Returns true is the iterators point to the same object
		bool operator== (const layer_iterator& _other) const {
			return internal == _other.internal;
		}

		/// Assign this iterator to the value of another
		layer_iterator& operator= (const layer_iterator& V) {
			internal = V.internal;
			return *this;
		}

		tree_iterator base() { return internal; }
	private:
		tree_iterator internal;
	};

	template<class iterator_type>
	class _reverse_iterator {
	public:
		typedef _reverse_iterator<iterator_type> self_type;
	
		// wraps the given iterator
		explicit _reverse_iterator(const iterator_type& _iter) {
			internal = _iter;
		}
		
		// making unintialized iterators
		_reverse_iterator() { }
	
		/// steps to the prvious node in the tree.
		void next() { internal.previous(); }
		/// steps to the next node in the tree.
		void previous() { internal.next(); }
		/// steps over the previous item's children, to the previous sibling
		void next_sibling() { internal.previous_sibling(); }
		/// steps over this items's children to its next sibling.
		void previous_sibling() { internal.next_sibling(); }
		/// steps to this item's last child
		void first_child() { internal.last_child(); }
		/// steps to this item's last child
		void last_child() { internal.first_child(); }
		/// steps up to this item's parent
		void up_to_parent() { internal.up_to_parent; }
		/// Returns true if this widget has children
		bool has_children() { return internal.has_children(); }

		/// Returns the item at this iterator.
		T& get_data() { return internal.get_data(); }
		/// Returns the item at this iterator.
		const T& get_data() const { return internal.get_data(); }

		/// steps to the previous node in the tree.
		self_type& operator++ () { next(); return *this; }
		/// steps to the next node in the tree.
		self_type& operator-- () { previous(); return *this; }

		/// steps to the previous node in the tree.
		self_type operator++ (int) { 
			self_type temp = *this;
			next();
			return temp; 
		}

		/// steps to the next node in the tree.
		self_type operator-- (int) { 
			self_type temp = *this;
			previous(); 
			return temp; 
		}

		/// Returns the item at this iterator.
		T& operator* () { return get_data(); }
		/// Returns the item at this iterator.
		const T& operator* () const { return get_data(); }

		/// Returns false if the iterators point to the same widget.
		bool operator!= (const self_type& _other) const { 
			return internal != _other.internal;
		}

		/// Returns true is the iterators point to the same object
		bool operator== (const self_type& _other) const {
			return internal == _other.internal;
		}

		/// Assign this iterator to the value of another
		self_type& operator= (const self_type& V) {
			internal = V.internal;
			return *this;
		}

		/// allow conversions to normal iterator types
		iterator_type base() {
			return internal;
		}
	private:
		iterator_type internal;
	};	

	typedef _reverse_iterator<iterator> reverse_iterator;
	typedef _reverse_iterator<layer_iterator> reverse_layer_iterator;

	template<class iterator_type>
	class range {
	public:
		range() { }
		/// Creates a range from _first to _last
		range(iterator_type _first, iterator_type _last) {
			first_iter = _first;
			last_iter = _last;
		}
		range(const range<iterator_type>&  V) {
			first_iter = V.first_iter;
			last_iter = V.last_iter;
		}

		/// Returns an iterator pointing to the beginning of the range.
		iterator_type begin() { return first_iter; }
		/// Returns an iterator pointing to the end of the list.
		iterator_type end() { return last_iter; }

		/// Returns true if this is an empty range.
		bool empty() const { return first_iter == last_iter; }
	private:
		iterator_type first_iter, last_iter;
	};

	/// A range of basic iterators.
	typedef range<iterator> iterator_range;
	/// A range of layer iterators.
	typedef range<layer_iterator> layer_iterator_range;
	/// A range of reverse basic iterators.
	typedef range<reverse_iterator> reverse_iterator_range;
	/// A range of reverse layer iterators.
	typedef range<reverse_layer_iterator> reverse_layer_iterator_range;

	/// Returns a basic iterator to the beginning of the tree.
	iterator begin() { return iterator(TreeList.begin()); }
	/// Returns a basic iterator to the end of the tree.
	iterator end() { return iterator(TreeList.end()); }
	/// Returns a layer iterator to the beginning of the tree.
	layer_iterator layer_begin() { return layer_iterator(begin()); }
	/// Returns a layer iterator to the end of the tree.
	layer_iterator layer_end() { return layer_iterator(end()); }

	/// Returns a reverse basic iterator to the end of the list.
	reverse_iterator rbegin() { return reverse_iterator(--end()); }
	/// Returns a reverse basic iterator to the beginning of the list.
	reverse_iterator rend() { return reverse_iterator(--begin()); }
	/// Returns a reverse layer iterator to the end of the list.
	reverse_layer_iterator rlayer_begin() { return reverse_layer_iterator(rbegin()); }
	/// Returns a reverse layer iterator to the beginning of the list.
	reverse_layer_iterator rlayer_end() { return reverse_layer_iterator(rend()); }

	/** Returns an iterator range which refers to all of the children of the item at 
	 ** the given parent iterator.
	 */
	iterator_range get_child_range(const iterator& parent_iter) {
		iterator begin, end;

		// get begin
		begin = parent_iter;
		begin.first_child();
		
		// get end
		end = parent_iter;
		end.next_sibling();

		return iterator_range(begin, end);
	}

	/** Returns a reverse iterator range which refers to all of the children of the 
	 ** item at the given parent iterator.
	 */
	reverse_iterator_range get_reverse_child_range(const iterator& parent_iter) {
		iterator begin, end;

		// get begin
		begin = parent_iter;
		begin.next_sibling();
		begin--;

		// get end
		end = parent_iter;

		return reverse_iterator_range(reverse_iterator(begin), reverse_iterator(end));
	}

	/** Returns a layer iterator range which refers to all of the children of the 
	 ** item at the given parent iterator.
	 */
	layer_iterator_range get_child_layer_range(const iterator& parent_iter) {
		iterator begin, end;

		// get begin
		begin = parent_iter;
		begin.first_child();

		// get end
		end = parent_iter;
		end.next_sibling();

		return layer_iterator_range(layer_iterator(begin), layer_iterator(end));
	}

	/** Returns a reverse layer iterator range which refers to all of the children 
	 ** of the item at the given parent iterator.
	 */
	reverse_layer_iterator_range get_reverse_child_layer_range(const iterator& parent_iter) {
		iterator begin, end;

		// get begin
		begin = parent_iter;
		begin.last_child();

		// get end
		end = parent_iter;
		
		return reverse_layer_iterator_range(
			reverse_layer_iterator(layer_iterator(begin)), 
			reverse_layer_iterator(layer_iterator(end))
			);
	}

	/// Destroys all the elements in the tree
	void clear() {
		while(TreeList.size() > 0) {
			delete TreeList.back();
			TreeList.pop_back();
		}
	}

	/** Returns an iterator that refers to the given item in the list.  If the item
	 ** is not present in the list it will return end().
	 */
	iterator find_item(const T& item) {
		iterator i;
		for(i = begin(); i != end(); i++) {
			if(item == *i) break;
		}
		return i;
	}		

	/** Insert the item as a child of the node refered to by the parent iterator. 
	 ** It will be inserted as the first child to this parent.
	 ** 
	 ** @see insert_last
	 */
	void insert_first(iterator parent, const T& item) {
		if(has_root()) {
			list_iterator parent_iter, dest_iter, self_iter;
			Node* new_node;
		
			// get a pointer to the parent node
			parent_iter = parent.get_list_iterator();

			// build new node
			new_node = new Node(item, parent_iter);

			// get dest iter (insert as the first child to parent)
			dest_iter = parent_iter; ++dest_iter;

			// insert			
			TreeList.insert(dest_iter, new_node);

			// set self iter
			self_iter = dest_iter; --self_iter;

			// NOTE: after this point dest_iter refers to the node after the
			// newly inserted one, and self_iter referes to the just added one

			// if there is an item before this one that isn't its parent then its
			// next sibling must point to this one
			{	list_iterator prev_iter = self_iter; --prev_iter;
				if(prev_iter != parent_iter) {
					(*prev_iter)->next_sibling = self_iter;
				}
			}

			// if the node after this one is a sibling, set its previous value
			// to point to the newly added node
			if(dest_iter != (*parent_iter)->next_sibling) {
				(*dest_iter)->prev_sibling = self_iter;
			}

			// set up node iterators
			new_node->next_sibling = dest_iter;		// next item
			new_node->last_child = self_iter; 		// has no children
			new_node->prev_sibling = parent_iter; 	// acts like the end when moving backwards

			// point parent last_child to if this is the only child
			if((*parent_iter)->last_child == parent_iter) {
				(*parent_iter)->last_child = self_iter;
			}
		} else {
			throw ::ksd::exception("tree", "Trying to add an item to a root-less tree");
		}
	}

	/** Insert the item as a child of the node refered to the item parent.
	 ** It will be inserted as the first child to this parent.
	 ** 
	 ** @see insert_last
	 */
	void insert_first(const T& parent, const T& item) {
		iterator parent_iter = find_item(parent);
		if(parent_iter == end()) {
			throw ::ksd::exception("tree", "Can't add because there is no such parent in the tree");
		}
		insert_first(parent_iter, item);
	}

	/** Insert the item as a child of the node refered to by the parent iterator. 
	 ** It will be inserted as the last child to this parent.
	 ** 
	 ** @see insert_first
	 */
	void insert_last(iterator parent, const T& item) {
		if(has_root()) {
			list_iterator parent_iter, dest_iter, self_iter;
			Node* new_node;
		
			// get a pointer to the parent node
			parent_iter = parent.get_list_iterator();

			// build new node
			new_node = new Node(item, parent_iter);

			// get dest iter (insert just before the parent's next sibling)
			dest_iter = (*parent_iter)->next_sibling;

			// insert			
			TreeList.insert(dest_iter, new_node);

			// set self iter
			self_iter = dest_iter; --self_iter;

			// NOTE: after this point dest_iter refers to the node after the
			// newly inserted one, and self_iter referes to the just added one

			// if there is an item before this one that isn't its parent then its
			// next sibling must point to this one
			{	list_iterator prev_iter = self_iter; --prev_iter;
				if(prev_iter != parent_iter) {
					(*prev_iter)->next_sibling = self_iter;
				}
			}

			// if there is a sibling before this one make sure that it points to
			// this widget as its next sibling.
			if((*parent_iter)->last_child != parent_iter) {
				(*(*parent_iter)->last_child)->next_sibling = self_iter;
			}

			// set up node iterators
			new_node->next_sibling = dest_iter;		// next item
			new_node->last_child = self_iter; 		// has no children
			new_node->prev_sibling = (*parent_iter)->last_child; // old last child

			// set this as the new last child
			(*parent_iter)->last_child = self_iter;
		} else {
			throw ::ksd::exception("tree", "Trying to add an item to a root-less tree");
		}
	}

	/** Insert the item as a child of the node refered to the item parent.
	 ** It will be inserted as the last child to this parent.
	 ** 
	 ** @see insert_first
	 */
	void insert_last(const T& parent, const T& item) {
		iterator parent_iter = find_item(parent);
		if(parent_iter == end()) {
			throw ::ksd::exception("tree", "Can't add because there is no such parent in the tree");
		}
		insert_last(parent_iter, item);
	}

	/** Removes an item from the list.
	 */
	void erase(iterator iter) {
		if(has_root()) {
			if(iter == begin()) {
				// if its the root, clean out the tree
				clear();
				return;
			}
			
			list_iterator self_iter = iter.get_list_iterator(),
				parent_iter = (*self_iter)->parent_iter, end;

			// if there is an item before this one that isn't its parent then its
			// next sibling must point to this items next sibling
			{	list_iterator prev_iter = self_iter; --prev_iter;
				if(prev_iter != parent_iter) {
					(*prev_iter)->next_sibling = (*self_iter)->next_sibling;
				}
			}

			// if there is another sibling before, adjust its next_sibling
			if((*self_iter)->prev_sibling != parent_iter) {
				(*(*self_iter)->prev_sibling)->next_sibling = (*self_iter)->next_sibling;
			}

			// if we are the last child, unlink from parent
			if((*parent_iter)->last_child == self_iter) {
				(*parent_iter)->last_child = (*self_iter)->prev_sibling;
			} 
			// if there is another item after, adjust its prev_sibling
			else {
				(*(*self_iter)->next_sibling)->prev_sibling = (*self_iter)->prev_sibling;
			}

			// set end because we are about to delete the data that holds it
			end = (*self_iter)->next_sibling;

			// delete the data contained in the nodes
			for(list_iterator i = self_iter; i != end; i++) {
				delete *i;
			}

			// remove the actual nodes from the list
			TreeList.erase(self_iter, end);
		} else {
			throw ::ksd::exception("tree", "Can't remove an element from a rootless tree");
		}
	}

	/** Moves an element towards the back of the child order.
	 */
	void move_backward(iterator self_iter) {
		if(has_root()) {
			iterator dest_iter, sibling_iter, end;

			// end is the sibling of the parent
			end = iterator(self_iter.get_node()->parent_iter);
			end.next_sibling();

			// get sibling iter
			sibling_iter = self_iter;
			sibling_iter.next_sibling();

			// check to see if it is already at the end of the list
			if(sibling_iter == end) { return; }

			// get dest
			dest_iter = sibling_iter;
			dest_iter.next_sibling();

			// adjust parent's last child iter
			if((*self_iter.get_node()->parent_iter)->last_child == sibling_iter.get_list_iterator()) {
				(*self_iter.get_node()->parent_iter)->last_child = self_iter.get_list_iterator();
			}

			// if there is a sibling before first_iter then adjust its next_sibling
			if(self_iter.get_node()->prev_sibling != self_iter.get_node()->parent_iter) {
				(*self_iter.get_node()->prev_sibling)->next_sibling = sibling_iter.get_list_iterator();
			}

			// adjust sibling links
			self_iter.get_node()->next_sibling = sibling_iter.get_node()->next_sibling;
			sibling_iter.get_node()->next_sibling = self_iter.get_list_iterator();
			sibling_iter.get_node()->prev_sibling = self_iter.get_node()->prev_sibling;
			self_iter.get_node()->prev_sibling = sibling_iter.get_list_iterator();

			TreeList.splice(dest_iter.get_list_iterator(), TreeList, 
				self_iter.get_list_iterator(), sibling_iter.get_list_iterator());
		} else {
			throw ::ksd::exception("tree", "Can't move an element if there is not root!");
		}
	}

	/** Moves an element to the back of the child order.
	 */
	void move_to_back(iterator self_iter) {
		if(has_root()) {
			iterator sibling_iter, dest_iter;

			// dest iter is the sibling of the parent
			dest_iter = iterator(self_iter.get_node()->parent_iter);
			dest_iter.next_sibling();

			// get sibling iter
			sibling_iter = self_iter;
			sibling_iter.next_sibling();

			// check to see if it is already at the end of the list
			if(sibling_iter == dest_iter) { return; }

			// if there is a sibling before self_iter then adjust its next_sibling
			if(self_iter.get_node()->prev_sibling != self_iter.get_node()->parent_iter) {
				(*self_iter.get_node()->prev_sibling)->next_sibling = sibling_iter.get_list_iterator();
			}
			
			// adjust sibling links
			self_iter.get_node()->next_sibling = (*(*self_iter.get_node()->parent_iter)->last_child)->next_sibling;
			(*(*self_iter.get_node()->parent_iter)->last_child)->next_sibling = self_iter.get_list_iterator();
			sibling_iter.get_node()->prev_sibling = self_iter.get_node()->prev_sibling;
			self_iter.get_node()->prev_sibling = (*self_iter.get_node()->parent_iter)->last_child;

			// adjust parent's last child iter
			(*self_iter.get_node()->parent_iter)->last_child = self_iter.get_list_iterator();

			TreeList.splice(dest_iter.get_list_iterator(), TreeList, 
				self_iter.get_list_iterator(), sibling_iter.get_list_iterator());
		} else {
			throw ::ksd::exception("tree", "Can't move an element if there is not root!");
		}
	}

	/** Moves an element toward the front of the child order
	 */
	void move_forward(iterator self_iter) {
		if(has_root()) {
			iterator end, sibling_iter;

			// get the previous sibling
			sibling_iter = self_iter;
			sibling_iter.previous_sibling();

			// check to see if it is already at the end of the list
			if(sibling_iter.get_list_iterator() == self_iter.get_node()->parent_iter) { return; }

			// get the end of the self iters children
			end = self_iter;
			end.next_sibling();

			// adjust parent's last child iter if this item is it
			if((*self_iter.get_node()->parent_iter)->last_child == self_iter.get_list_iterator()) {
				(*self_iter.get_node()->parent_iter)->last_child = sibling_iter.get_list_iterator();
			} else {
				// if there is a sibling after self_iter then adjust its prev_sibling
				(*self_iter.get_node()->next_sibling)->prev_sibling = self_iter.get_node()->prev_sibling;
			}
			
			// adjust sibling links
			sibling_iter.get_node()->next_sibling = self_iter.get_node()->next_sibling;
			self_iter.get_node()->next_sibling = sibling_iter.get_list_iterator();
			self_iter.get_node()->prev_sibling = sibling_iter.get_node()->prev_sibling;
			sibling_iter.get_node()->prev_sibling = self_iter.get_list_iterator();

			TreeList.splice(sibling_iter.get_list_iterator(), TreeList, 
				self_iter.get_list_iterator(), end.get_list_iterator());
		} else {
			throw ::ksd::exception("tree", "Can't move an element if there is not root!");
		}
	}

	/** Moves an element to the front of the child order
	 */
	void move_to_front(iterator self_iter) {
		if(has_root()) {
			iterator end, dest_iter, sibling_iter;

			// dest iter is the first child of the parent
			dest_iter = iterator(self_iter.get_node()->parent_iter);
			dest_iter.first_child();

			// check to see if it is already at the end of the list
			if(self_iter == dest_iter) { return; }

			// get the end of the self iters children
			end = self_iter;
			end.next_sibling();

			// get the previous sibling
			sibling_iter = self_iter;
			sibling_iter.previous_sibling();

			// adjust parent's last child iter if this item is it
			if((*self_iter.get_node()->parent_iter)->last_child == self_iter.get_list_iterator()) {
				(*self_iter.get_node()->parent_iter)->last_child = self_iter.get_node()->prev_sibling;
			} else {
				// if there is a sibling after self_iter then adjust its prev_sibling
				(*self_iter.get_node()->next_sibling)->prev_sibling = self_iter.get_node()->prev_sibling;
			}
			
			// adjust sibling links
			sibling_iter.get_node()->next_sibling = self_iter.get_node()->next_sibling;
			self_iter.get_node()->next_sibling = dest_iter.get_list_iterator();
			self_iter.get_node()->prev_sibling = self_iter.get_node()->parent_iter;

			TreeList.splice(dest_iter.get_list_iterator(), TreeList, 
				self_iter.get_list_iterator(), end.get_list_iterator());
		} else {
			throw ::ksd::exception("tree", "Can't move an element if there is not root!");
		}
	}
};
//---------------------------------------------------------------------------  
}; // namespace ksd
//---------------------------------------------------------------------------  
#endif


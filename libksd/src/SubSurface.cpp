//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	$Header$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Checked-in by:	$Author$
//
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma implementation
#endif
//--------------------------------------------------------------------------- 
#define KSD_BUILD_LIB
#include "SubSurface.h"
#include "Image.h"
#include "Canvas.h"
#include "log.h"

using namespace ksd;
//---------------------------------------------------------------------------  
TGenericSubSurface::TGenericSubSurface(TSurface* _surf, 
	int _X, int _Y, int _Width, int _Height, bool _Buffered) 
	: TSubSurface()
{
	if(!_surf) {
		// can't happen, throw exception?
		throw("Passing a NULL TSurface* to TSubSurface");
		return;
	}

	// hold surface
	Surface = _surf;
	MakeReferer(*Surface);

	// set dimensions
	X = _X;
	Y = _Y;
	Width = _Width;
	Height = _Height;
	ScrollX = ScrollY = 0;
	Buffered = _Buffered;

	// deal with buffering
	if(Buffered) {
		// create the buffer
		Buffer = new TImage;
		Buffer->Create(Width, Height, GetData()->GetFormat().GetType());
	} else {
		// we are unbuffered
		Buffer = NULL;
	}

	// set parent to null since this isn't a sub-sub-surface
	Parent = NULL;
}
//---------------------------------------------------------------------------  
TGenericSubSurface::TGenericSubSurface(TGenericSubSurface* _surf, 
	int _X, int _Y, int _Width, int _Height, bool _Buffered) 
	: TSubSurface()
{
	if(!_surf) {
		// can't happen, throw exception?
		throw("Passing a NULL TSurface* to TSubSurface");
		return;
	}

	// hold surface
	Surface = _surf;
	MakeReferer(*Surface);

	// set dimensions
	X = _X;
	Y = _Y;
	Width = _Width;
	Height = _Height;
	ScrollX = ScrollY = 0;
	Buffered = _Buffered;

	// deal with buffering
	if(Buffered) {
		// create the buffer
		Buffer = new TImage;
		Buffer->Create(Width, Height, GetData()->GetFormat().GetType());
	} else {
		// we are unbuffered
		Buffer = NULL;
	}

	// set parent because this is a sub-sub-surface
	Parent = _surf;
}
//---------------------------------------------------------------------------  
TGenericSubSurface::~TGenericSubSurface()
{
	if(Buffer) {
		delete Buffer;
	}
}
//--------------------------------------------------------------------------- 
void TGenericSubSurface::Resize(int _Width, int _Height)
{
	// TODO: make thread safe

	Width = _Width;
	Height = _Height;
}
//--------------------------------------------------------------------------- 
void TGenericSubSurface::Reposition(int _X, int _Y)
{
	// TODO: make thread safe
	
	X = _X;
	Y = _Y;
}
//---------------------------------------------------------------------------  
void TGenericSubSurface::SetScroll(unsigned int _sx, unsigned int _sy) 
{
	// TODO: make thread safe

	ScrollX = _sx;
	ScrollY = _sy;
}
//---------------------------------------------------------------------------  
#if 0
TCanvas* TGenericSubSurface::GetCanvas() 
{
	#if 0
	if(Buffered) {
		Canvas = Buffer->GetCanvas();
		Canvas->SetOffset(-(signed)ScrollX, -(signed)ScrollY);
	} else {
		int real_x, real_y, real_w, real_h;

		// get absolute position
		if(!GetRealPosition(real_x, real_y, real_w, real_h)) {
			ClipRect.Set(0, 0, 0, 0);
		} else {
			MakeClipRect();
		}

		// if no Canvas then create it
		if(!Canvas) {
			Canvas = new TCanvas(this, real_x, real_y, 
								 real_w, real_h, -(int)ScrollX, -(int)ScrollY, 
								 &ClipRect);
		} else {
			Canvas->SetDimensions(real_x, real_y, real_w, real_h);
			Canvas->SetOffset(-(int)ScrollX, -(int)ScrollY);
		}
	}
	#endif
	if(!Canvas) {
		Canvas = new TCanvas(this);
	}

	return Canvas;
}
#endif
//---------------------------------------------------------------------------  
void TGenericSubSurface::Fill(const TColor& Color)
{
	if(Buffered) {
		Buffer->Fill(Color);
	} else {
		if(IsValid()) { 
			int real_x, real_y, real_w, real_h;
		
			if(Parent) {
				int adj_x, adj_y;
				int parent_width = Parent->X + Parent->Width, 
					parent_height = Parent->Y + Parent->Height;

				// pre-clip the local values to its parent
				adj_x = adj_y = 0;
				if(X - (int)Parent->ScrollX < 0) {
					adj_x = -(X - (int)Parent->ScrollX);
					//adj_w = -adj_x;
				}
				if(Y - (int)Parent->ScrollY < 0) {
					adj_y = -(Y - (int)Parent->ScrollY);
					//adj_h = -adj_y;
				}
				
				// we don't need to clip the real w/h because GetRealPosition
				// does it for us.
				//if(X + Width - (int)Parent->ScrollX > parent_width) {
				//	adj_w += (X + Width - (int)Parent->ScrollX) - parent_width;
				//}
				//if(Y + Height - (int)Parent->ScrollY > parent_height) {
				//	adj_h += (Y + Height - (int)Parent->ScrollY) - parent_height;
				//}
				
				GetRealPosition(real_x, real_y, real_w, real_h);

				// apply clipping
				real_x += adj_x;
				real_y += adj_y;
				real_w -= adj_x;
				real_h -= adj_y;

				// make sure we don't try to draw outside the underlying surface object
				if(real_w > 0 && real_h > 0 && 
				   real_x < GetData()->GetWidth() &&
				   real_y < GetData()->GetHeight()) 
				{
					GetData()->Fill(real_x, real_y, real_w, real_h, Color);
				}
			} else {
				if(X < GetData()->GetWidth() && Y < GetData()->GetHeight() &&
				   X + Width > 0 && Y + Height > 0)
				{
					if(X < 0) real_x = 0;
					else real_x = X;
					if(Y < 0) real_y = 0;
					else real_y = Y;
					if(X + Width > GetData()->GetWidth() + 1) real_w = GetData()->GetWidth() + 1 - X;
					else real_w = Width;
					if(Y + Height > GetData()->GetHeight() + 1) real_h = GetData()->GetHeight() + 1 - Y;
					else real_h = Height;
				
					GetData()->Fill(real_x, real_y, real_w, real_h, Color);
				}
			}
		}
	}
}
//---------------------------------------------------------------------------  
// TODO: move this code to make_iterator.
#if 0
void* TGenericSubSurface::GetPixels()
{
	if(Buffered) {
		return Buffer->GetPixels();
	} else {
		int real_x, real_y, temp_w, temp_h;
		
		GetRealPosition(real_x, real_y, temp_w, temp_h);
		real_x -= ScrollX;
		real_y -= ScrollY;
		
		return ((char*)GetData()->GetPixels()) + 
				(real_y * GetData()->GetPitch()) +
				(real_x * GetData()->GetFormat().GetBytesPerPixel());
	}
}
#endif
//---------------------------------------------------------------------------  
// TODO: it should be possible to construct a sub-surface tree where the
// real positions are calculated at each node cumulatively, which should
// remove all this repeated recursion.
bool TGenericSubSurface::GetRealPosition(int& real_x, int& real_y,
									 int& real_w, int& real_h)
{
	if(Parent) {
		TPoint2D parent_scroll;
		int parent_left, parent_top, parent_right, parent_bottom, 
			my_left, my_top, my_right, my_bottom, 
			temp_w = Width, temp_h = Height;

		// get parent values
		if(!Parent->GetRealPosition(real_x, real_y, real_w, real_h)) return false;

		parent_left = real_x;
		parent_top = real_y;
		parent_right = real_x + real_w;
		parent_bottom = real_y + real_h;
		parent_scroll = Parent->GetScroll();

		// move to child position
		real_x += (X - parent_scroll.X);
		real_y += (Y - parent_scroll.Y);

		// clip to parent right
		my_right = real_x + temp_w;
		if(my_right > parent_right) {
			real_w = temp_w - (my_right - parent_right);
		} else real_w = temp_w;

		// clip to parent bottom
		my_bottom = real_y + temp_h;
		if(my_bottom > parent_bottom) {
			real_h = temp_h - (my_bottom - parent_bottom);
		} else real_h = temp_h;

		// check/mark invalid surfaces
		if(real_w <= 0 || real_h <= 0 ||
		   real_x + real_w < parent_left || 
		   real_y + real_h < parent_top) return false;
	} else {
		real_x = X;
		real_y = Y;
		real_w = Width;
		real_h = Height;
	}

	return true;
}
//---------------------------------------------------------------------------  
void TGenericSubSurface::MakeClipRect()
{ 
	// The idea behind the SystemClipRect used by sub-surface, is to create
	// a clip region that stops the current surface from drawing outside its
	// parent surface.  This really just means describing the parent surface
	// relative to self, while clipping against the parent edges.
	
	if(Parent) {
		Parent->MakeClipRect();

		// make sure parent cliprect is valid
		if(!Parent->ClipRect.IsValid()) {
			ClipRect.Set(0, 0, 0, 0);
		} else {
			// make clip rect relative to parent.  Has little practical value,
			// but allows us to use the parent cliprect to clip it.
			ClipRect.Set((int)Parent->ScrollX, (int)Parent->ScrollY,
						 (int)Parent->ScrollX + Parent->Width,
						 (int)Parent->ScrollY + Parent->Height);
		
			// (use clip rect to clip the clip rect! :-)
			Parent->ClipRect.ClipRect(ClipRect);

			// make relative to current position.
			ClipRect.Move(-X, -Y);
		}
	} else {
		ClipRect.Set(-X, -Y, -X + GetData()->GetWidth(), -Y + GetData()->GetHeight());
	}
}
//---------------------------------------------------------------------------  
void TGenericSubSurface::CopyToParent()
{
	if(Buffered) {
		Surface->OnWrite();
		
		TCanvas temp(Surface);
		temp.Blit(X, Y, *Buffer);
	}
}
//--------------------------------------------------------------------------- 


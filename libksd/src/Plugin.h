//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//	
//--------------------------------------------------------------------------- 
#ifndef _KSD_PluginH
#define _KSD_PluginH
//--------------------------------------------------------------------------- 
#include "export_ksd.h"
//---------------------------------------------------------------------------
namespace ksd {
//---------------------------------------------------------------------------
// forward decls
class TPluginSystemBase;
//class TModule;
//---------------------------------------------------------------------------
/** Load a plugin.  Will try to load the specified plugin with all of the
 ** registered plugin systems.  This is useful because you can load a plugin
 ** without having to know what Plugin System is uses.
 **
 ** @see RegisterPluginSystem
 */
KSD_EXPORT void LoadPlugin(const char* PluginName);

/** Load a plugin list.  Will try to load all of the plugins in the plugin
 ** list with each of the registered plugin systems.  This is useful because
 ** you can load a plugin list without having to know which plugins go to
 ** which Plugin Systems.
 **
 ** @see RegisterPluginSytem
 */
KSD_EXPORT void LoadPluginList(const char* ListName);

/** Adds the given plugin system to the list of registered plugin systems.
 */
KSD_EXPORT void RegisterPluginSystem(TPluginSystemBase* PluginSystem);

/** Removes the given plugin system from the list of registered plugin
 ** systems.
 */
KSD_EXPORT void UnregisterPluginSystem(TPluginSystemBase* PluginSystem);

// TODO: implement
//EXPORT_FUNC error LoadModule(TModule* Module, const char* ModuleName);
//---------------------------------------------------------------------------
}; // namespace KSLib
//---------------------------------------------------------------------------
#endif

//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	$Header$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Checked-in by:	$Author$
//
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma implementation
#endif
//--------------------------------------------------------------------------- 
#define KSD_BUILD_LIB
#include <SDL/SDL_thread.h>
#include "Thread.h"
#include "log.h"

using namespace ksd;
//--------------------------------------------------------------------------- 
int ksd::ThreadCallback(void* tptr)
{
	TThread* thread = (TThread*)tptr;

	thread->Running = true;

	LOG(7, "Starting thread %p", tptr);

	thread->InitThread();
	thread->RunThread();
	thread->ShutdownThread();

	LOG(7, "Stopping thread %p", tptr);

	thread->Running = false;
	
	return 0;
}
//--------------------------------------------------------------------------- 
TThread::TThread()
{
	Running = false;
	Handle = NULL;
}
//--------------------------------------------------------------------------- 
TThread::~TThread()
{
	Stop();
    //SDL_KillThread(Handle);
}
//--------------------------------------------------------------------------- 
void TThread::Start()
{
	LOG_FUNCTION("void TThread::Start()");

	RunningMutex.Lock();
	if(!Running) {	
		RunningMutex.Unlock();
			
		Handle = SDL_CreateThread(&ThreadCallback, this);
	} else RunningMutex.Unlock();
}
//--------------------------------------------------------------------------- 
void TThread::Stop()
{
	LOG_FUNCTION("void TThread::Stop()");

	RunningMutex.Lock();
	if(Running) {
		Running = false;
		RunningMutex.Unlock();

		// wait for thread to complete
		SDL_WaitThread(Handle, NULL);
	} else RunningMutex.Unlock();
}
//--------------------------------------------------------------------------- 
bool TThread::IsRunning()
{
	RunningMutex.Lock();
	int temp = Running;
	RunningMutex.Unlock();

	return temp;
}
//--------------------------------------------------------------------------- 


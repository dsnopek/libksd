//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	$Header$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Checked-in by:	$Author$
//
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma implementation
#endif
//--------------------------------------------------------------------------- 
#define KSD_BUILD_LIB
#include <SDL/SDL.h>
#include <map>
#include "Timer.h"
#include "log.h"
#include "init.h"

using namespace ksd;
using namespace SigC;
//---------------------------------------------------------------------------  
//
// Our module code
// 

static bool operator > (const std::pair<unsigned int, unsigned int>& first,
						const std::pair<unsigned int, unsigned int>& second)
{
	return (first.first > second.first) && (first.second > second.second);
}

typedef std::map<unsigned int, TSystemTimer*> TimerListType;
typedef std::map<std::pair<unsigned int, unsigned int>, TBlinkTimer*> BlinkTimerListType;

_GLOBAL_DECL(TimerListType, TimerList);
_GLOBAL_DECL(BlinkTimerListType, BlinkTimerList);

static void ShutdownTimerModule()
{
	if(_GLOBAL_EXISTS(TimerList)) {
		for(TimerListType::iterator i = TimerList->begin(); i != TimerList->end(); i++) {
			delete (*i).second;
		}
	}

	if(_GLOBAL_EXISTS(BlinkTimerList)) {
		for(BlinkTimerListType::iterator i = BlinkTimerList->begin(); i != BlinkTimerList->end(); i++) {
			delete (*i).second;
		}
	}

	_GLOBAL_DESTROY(TimerList);
	_GLOBAL_DESTROY(BlinkTimerList);
}
_MODULE_SHUTDOWN(&ShutdownTimerModule);
//---------------------------------------------------------------------------  
TSystemTimerBase::TSystemTimerBase(TimerCallback _callback, Uint32 _val)
{
	Interval = _val;
	id = NULL;
	DoTimer = _callback;
}
//--------------------------------------------------------------------------- 
TSystemTimerBase::~TSystemTimerBase()
{
	Stop();
}
//--------------------------------------------------------------------------- 
void TSystemTimerBase::SetInterval(Uint32 _interval)
{	
	if(IsRunning()) {
		Stop();
		Interval = _interval;
		Start();
	} else {
		Interval = _interval;
	}
}
//--------------------------------------------------------------------------- 
void TSystemTimerBase::Start() 
{
	if(!SDL_WasInit(SDL_INIT_TIMER))
		SDL_InitSubSystem(SDL_INIT_TIMER);
	
	if(!IsRunning()) {
		#ifdef KSD_USE_FAST_TIMERS
		id = SDL_AddTimer(Interval, DoTimer, this);
		#else
		id = SDL_AddTimer(Interval, &TSystemTimerBase::CallbackHook, this);
		#endif
	}
}
//--------------------------------------------------------------------------- 
void TSystemTimerBase::Stop()
{
	if(IsRunning() && SDL_WasInit(SDL_INIT_TIMER)) {
		SDL_RemoveTimer(id);
		id = NULL;
	}
}
//---------------------------------------------------------------------------  
#ifndef KSD_USE_FAST_TIMERS
Uint32 TSystemTimerBase::CallbackHook(Uint32 _val, void* tptr)
{
	TSystemTimerBase* Timer = (TSystemTimerBase*)tptr;
	return (Timer->Interval = Timer->DoTimer(_val, tptr));
}
#endif
//---------------------------------------------------------------------------  
TSystemTimer* ksd::GetTimer(unsigned int interval)
{
	_GLOBAL_CREATE(TimerList, new TimerListType);

	// return the timer if we have it
	TimerListType::iterator i = TimerList->find(interval);
	if(i != TimerList->end()) {
		return (*i).second; 
	}

	// add new timer
	TSystemTimer* timer = new TSystemTimer(interval, true);
	(*TimerList)[interval] = timer;
	timer->Start();
	return timer;
}
//---------------------------------------------------------------------------  
// Standard is 1000, 500
TBlinkTimer* ksd::GetBlinkTimer(unsigned int on, unsigned int off)
{
	_GLOBAL_CREATE(BlinkTimerList, new BlinkTimerListType);

	std::pair<unsigned int, unsigned int> interval = std::make_pair(on, off);

	// return the timer if we have it
	BlinkTimerListType::iterator i = BlinkTimerList->find(interval);
	if(i != BlinkTimerList->end()) {
		return (*i).second; 
	}

	// add new timer
	TBlinkTimer* timer = new TBlinkTimer(on, off);
	(*BlinkTimerList)[interval] = timer;
	timer->Start();
	return timer;
}
//---------------------------------------------------------------------------  


/*
  libuta - a C++ widget library based on SDL (Simple Direct Layer)
  Copyright (C) 1999  Karsten Laux

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Library General Public
  License as published by the Free Software Foundation; either
  version 2 of the License, or (at your option) any later version.
  
  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Library General Public License for more details.
  
  You should have received a copy of the GNU Library General Public
  License along with this library; if not, write to the
  Free Software Foundation, Inc., 59 Temple Place - Suite 330,
  Boston, MA  02111-1307, SA.
*/
//--------------------------------------------------------------------------- 
//	$Header$
//	
//	Author:			Karsten Laux, June 1999
//	Revised by:		David Snopek
//	Version:		$Revision$
//	
//	Checked-in by:	$Author$
//
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma implementation
#endif
//--------------------------------------------------------------------------- 
#define KSD_BUILD_LIB
#include <SDL/SDL_video.h>
#include "PixelFormat.h"
#include "Application.h"
#include "log.h"

using namespace ksd;
using namespace std;
//--------------------------------------------------------------------------- 
const char* TPixelFormat::Names[] = { "DISPLAY", "ABGR8888", "RGBA8888",
	"ARGB8888","BGRA8888","RGB888", "BGR888", "RGB565", "RGB555", "INDEXED", 
	"UNKNOWN" };
  
// RGBA bitmasks
Uint32 TPixelFormat::Masks[][4] = 
	{{0,0,0,0},
	{0x000000FF, 0x0000FF00, 0x00FF0000, 0xFF000000},
	{0xFF000000, 0x00FF0000, 0x0000FF00, 0x000000FF},
	{0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000},
	{0x0000FF00, 0x00FF0000, 0xFF000000, 0x000000FF},
	{0x00FF0000, 0x0000FF00, 0x000000FF, 0x00000000},
	{0x000000FF, 0x0000FF00, 0x00FF0000, 0x00000000},
	{0x0000F800, 0x000007E0, 0x0000001F, 0x00000000},
	{0x00007C00, 0x000003E0, 0x0000001F, 0x00000000},
	{0x00, 0x00, 0x00, 0x00},
	{0x00, 0x00, 0x00, 0x00}};

// The number of bytes in each format type
int TPixelFormat::Bytes[] = { -1, 4, 4, 4, 4, 3, 3, 2, 2, 1, 0 };
//--------------------------------------------------------------------------- 
TPixelFormat::TPixelFormat()
{
	LOG_CONSTRUCTOR("TPixelFormat");

	BytesPerPixel = BitsPerPixel = 0;
	RedMask = GreenMask = BlueMask = AlphaMask = 0;
	RedShift = GreenShift = BlueShift = AlphaShift = 0;
	RedLoss = GreenLoss = BlueLoss = AlphaLoss = 0;
	Palette = NULL;
	FormatType = UNKNOWN;
}
//--------------------------------------------------------------------------- 
TPixelFormat::TPixelFormat(SDL_PixelFormat* _pf) 
{
	LOG_CONSTRUCTOR("TPixelFormat");

	// some code depends on palette being null
	Palette = NULL;
	
	Set(_pf);
}
//---------------------------------------------------------------------------  
TPixelFormat::TPixelFormat(TPixelFormat::Type _pt)
{
	LOG_CONSTRUCTOR("TPixelFormat");

	// some code depends on palette being null
	Palette = NULL;
	
	Set(_pt);
}
//--------------------------------------------------------------------------- 
TPixelFormat::~TPixelFormat()
{
	LOG_DESTRUCTOR("TPixelFormat");

	if(Palette) {
		delete Palette;
	}
}
//--------------------------------------------------------------------------- 
void TPixelFormat::Set(SDL_PixelFormat* _pf)
{
	LOG_FUNCTION("TPixelFormat::Create(SDL_PixelFormat*)");

	FormatType = Identify(_pf->BitsPerPixel, _pf->Rmask, _pf->Gmask, _pf->Bmask, _pf->Amask);
	BitsPerPixel = _pf->BitsPerPixel;
	BytesPerPixel = _pf->BytesPerPixel;

	RedMask = _pf->Rmask;
	GreenMask = _pf->Gmask;
	BlueMask = _pf->Bmask;
	AlphaMask = _pf->Amask;

	RedShift = _pf->Rshift;
	GreenShift = _pf->Gshift;
	BlueShift = _pf->Bshift;
	AlphaShift = _pf->Ashift;

	RedLoss = _pf->Rloss;
	GreenLoss = _pf->Gloss;
	BlueLoss = _pf->Bloss;
	AlphaLoss = _pf->Aloss;

	LOG(9, "Bits per pixel =    %u", BitsPerPixel);
	LOG(9, "Red Mask =          %u", RedMask);
	LOG(9, "Green Mask =        %u", GreenMask);
	LOG(9, "Blue Mask = 		%u", BlueMask);
	LOG(9, "Alpha Mask =        %u", AlphaMask);

	// destory current palette (because no matter the type, it has to be destoryed)
	if(Palette) {
		LOG_SECTION("Destroy old palette");
		delete Palette;
		Palette = NULL;
	}

	// create palette
	if(FormatType == IND8) {
		LOG_SECTION("Create new palette");
		Palette = new TPalette;
	} 
}
//---------------------------------------------------------------------------  
void TPixelFormat::Set(TPixelFormat::Type _pt) 
{
	if(_pt == UNKNOWN) {
		FormatType = UNKNOWN;
		BytesPerPixel = BitsPerPixel = 0;
		RedMask = GreenMask = BlueMask = AlphaMask = 0;
		RedShift = GreenShift = BlueShift = AlphaShift = 0;
		RedLoss = GreenLoss = BlueLoss = AlphaLoss = 0;
		if(Palette) {
			delete Palette;
			Palette = NULL;
		}
	} else {
		if(_pt == DISPLAY) FormatType = GetDisplayFormat().GetType();
		else FormatType = _pt;

		// set bits and bytes
		BytesPerPixel = Bytes[(int)FormatType];
		BitsPerPixel = BytesPerPixel * 8;

		// set masks
		RedMask = Masks[(int)FormatType][0];
		GreenMask = Masks[(int)FormatType][1];
		BlueMask = Masks[(int)FormatType][2];
		AlphaMask = Masks[(int)FormatType][3];

		// set shifts
		RedShift = GetShift(RedMask);
		GreenShift = GetShift(GreenMask);
		BlueShift = GetShift(BlueMask);
		AlphaShift = GetShift(AlphaMask);

		// set losses
		RedLoss = GetLoss(RedMask);
		GreenLoss = GetLoss(GreenMask);
		BlueLoss = GetLoss(BlueMask);
		AlphaLoss = GetLoss(AlphaMask);

		LOG(9, "Bits per pixel =    %u", BitsPerPixel);
		LOG(9, "Red Mask =          %u", RedMask);
		LOG(9, "Green Mask =        %u", GreenMask);
		LOG(9, "Blue Mask = 		%u", BlueMask);
		LOG(9, "Alpha Mask =        %u", AlphaMask);

		// destory current palette (because no matter the type, it has to be destoryed)
		if(Palette) {
			LOG_SECTION("Destroy old palette");
			delete Palette;
			Palette = NULL;
		}

		// create palette
		if(FormatType == IND8) {
			LOG_SECTION("Create new palette");
			Palette = new TPalette;
		}
	}
}
//--------------------------------------------------------------------------- 
string TPixelFormat::ToString() const
{ 
	return string(Names[FormatType]);
}
//---------------------------------------------------------------------------  
Uint32 TPixelFormat::MapToPixel(const TColor& Color) const
{
	Uint32 pixel = 0;

	if(BitsPerPixel > 8) { // packed pixel mode
		pixel = ( ( (Color.Red 		>> RedLoss) << RedShift) |
				  ( (Color.Green 	>> GreenLoss) << GreenShift) |
				  ( (Color.Blue		>> BlueLoss) << BlueShift) |
				  ( (Color.Alpha	>> AlphaLoss) << AlphaShift) );
	} else { // palettized mode
		if(Palette) {
			pixel = Palette->FindBestMatch(Color);
		}
#ifdef __DEBUG__ 
		else { // if no palette throw big FUSS!! Has to be one..
			// error
			LOG(1, "In 8-bit mode and there is no palette!!");
		}
#endif
	}

	return pixel;
}
//--------------------------------------------------------------------------- 
TColor TPixelFormat::MapToColor(Uint32 pixel) const 
{
	TColor Color;

	if(BitsPerPixel > 8) { // packed pixel mode
		Color.Red = 	(pixel & RedMask) >> RedShift << RedLoss;
		Color.Green =	(pixel & GreenMask) >> GreenShift << GreenLoss;
		Color.Blue =	(pixel & BlueMask) >> BlueShift << GreenLoss;
		Color.Alpha = 	(pixel & AlphaMask) >> AlphaShift << AlphaLoss;
	} else { // palettized mode
		if(Palette) {
			if(pixel < Palette->GetSize())
				Color = Palette->GetColor(pixel);
		}
#ifdef __DEBUG__ 
		else { // if no palette throw big FUSS!! Has to be one..
			// error
			LOG(1, "In 8-bit mode and there is no palette!!");
		}
#endif
	}

	return Color;
}
//--------------------------------------------------------------------------- 
TPixelFormatType TPixelFormat::Identify(Uint8 _bpp, 
	Uint32 rmask, Uint32 gmask, Uint32 bmask, Uint32 amask)
{
	Type result;
	if(_bpp == 1)
		result = IND8;
	else {
		unsigned int i = 0;
		while(i < sizeof(TPixelFormat::Names)) {
			if((rmask == TPixelFormat::Masks[i][0]) &&
			   (gmask == TPixelFormat::Masks[i][1]) &&
			   (bmask == TPixelFormat::Masks[i][2]) &&
			   (amask == TPixelFormat::Masks[i][3]))
				break;
			i++;
		}
		if(i != sizeof(TPixelFormat::Names)) {
			// Found valid pixel format
			result = (Type)i;
		} else {
			// Don't know this pixel format
			result = UNKNOWN;
		}
	}
  
	return result;
}
//--------------------------------------------------------------------------- 
int TPixelFormat::GetInfo(TPixelFormatType _type,
	Uint32& rmask, Uint32& gmask, Uint32& bmask, Uint32& amask)
{
	int bpp;

	if(_type == DISPLAY)
		_type = GetDisplayFormat().GetType();

	rmask = Masks[_type][0];
	gmask = Masks[_type][1];
	bmask = Masks[_type][2];
	amask = Masks[_type][3];

	bpp = Bytes[_type] * 8;

	return bpp;
}
//---------------------------------------------------------------------------  
SDL_PixelFormat* TPixelFormat::CreateSDLFormat() const
{
	// TODO: we don't support palette formats yet cuz of API problems
	if(Palette) return NULL;

	SDL_PixelFormat* sdl_format = new SDL_PixelFormat;

	sdl_format->palette = NULL;
	sdl_format->BitsPerPixel = BitsPerPixel;
	sdl_format->BytesPerPixel = BytesPerPixel;
	sdl_format->Rmask = RedMask;
	sdl_format->Gmask = GreenMask;
	sdl_format->Bmask = BlueMask;
	sdl_format->Amask = AlphaMask;
	sdl_format->Rshift = RedShift;
	sdl_format->Gshift = GreenShift;
	sdl_format->Bshift = BlueShift;
	sdl_format->Ashift = AlphaShift;
	sdl_format->Rloss = RedLoss;
	sdl_format->Gloss = GreenLoss;
	sdl_format->Bloss = BlueLoss;
	sdl_format->Aloss = AlphaLoss;

	// TODO: support colorkey and alpha
	sdl_format->colorkey = 0;
	sdl_format->alpha = 255;

	return sdl_format;
}
//---------------------------------------------------------------------------  
TPixelFormat TPixelFormat::GetDisplayFormat()
{
	// checks for the possibility that we are running without video
	if(Application->GetScreen()) {
		return Application->GetScreen()->GetFormat();
	}

	return TPixelFormat();
}
//---------------------------------------------------------------------------  
int TPixelFormat::GetShift(Uint32 mask)
{
	int i = 0;
	Uint32 tmp = mask;

	if(tmp != 0) {
		for(tmp = mask; !(tmp & 0x01); tmp>>=1) ++i;
	}

  	return i;
}
//---------------------------------------------------------------------------  
int TPixelFormat::GetLoss(Uint32 mask)
{
	//assume we will lose all bits
	int i = 8;
	Uint32 tmp = mask;

	if(tmp != 0) {
		//scroll bits down until lsb is != 0
		for(tmp = mask; !(tmp & 0x01); tmp>>=1);
      
		//how many bits are contained ?
		for(; (tmp & 0x01); tmp >>= 1) --i;
	}

  return i;
}
//--------------------------------------------------------------------------- 


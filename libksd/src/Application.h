//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//	
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		TApplication
//	
//	Purpose:		Encapsulation of an application. Deals with 
//					start-up/shutdown code and handling events.
//	
//	Checked-in by:	$Author$
//					$Date$
//--------------------------------------------------------------------------- 
#ifndef _KSD_ApplicationH_
#define _KSD_ApplicationH_
//---------------------------------------------------------------------------
#include <SDL/SDL_main.h> 
#include "Widget.h"
#include "Canvas.h"
#include "Screen.h"
#include "EventDispatcher.h"
#include "ksdconfig.h"
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma interface
#endif
//--------------------------------------------------------------------------- 
/** Automatically generates a main function for your application class.  
 ** KSD_MAIN will create an instance of your class and set Application.  This
 ** means that your application class must override Init() (the only pure 
 ** virtual function in TApplication) and provide a constructor that takes
 ** zero arguments.
 ** 
 ** For example,
 
	\begin{verbatim}

	#include <ksd/Application.h>

	class MyApp : public TApplication {
	public:
		void Init() { }
	};

	KSD_MAIN(MyApp);

	\end{verbatim}
 **
 ** @param app The name of your application class.  Must be a descendant
 ** of TApplication.
 ** @see TApplication
 **/
#define KSD_MAIN(user_app_type) \
	extern "C" int main(int argc, char* argv[]) \
	{ \
		if(!::ksd::Application) { \
			try { \
				::ksd::InitLibrary(); \
				::ksd::Application = new user_app_type; \
			} catch(const char* msg) { \
				LOG(0, "%s", msg); \
				return 1; \
			} \
			::ksd::Application->Run(argc, argv); \
			delete ::ksd::Application; \
			::ksd::ShutdownLibrary(); \
			return 0; \
		} else { \
			LOG(0, "Application already initialized before main() !!"); \
			return 1; \
		} \
	}
//--------------------------------------------------------------------------- 
namespace ksd {
//---------------------------------------------------------------------------  
/** Initializes the library for use.  Do not call directly unless you mean to
 ** write your own custom main function, which is nobody.  Well, not nobody.
 ** If you are writting a standalone libksd application, this should \textbf{never}
 ** be necessary.  If you are trying to embed libksd inside of another program,
 ** such as a media player plugin (like xmms or WinAmp) this must used before
 ** any libksd calls.
 */
KSD_EXPORT void InitLibrary();
//---------------------------------------------------------------------------  
/** Cleans up libksd before program termination.  You shouldn't have to call
 ** this function if you are writting a standalone libksd application that
 ** uses KSD_MAIN, since it is called by Quit.  See \ref{InitLibrary} for
 ** a description of valid uses.
 */
KSD_EXPORT void ShutdownLibrary();
//---------------------------------------------------------------------------  
/** Causes the application to perform shutdown procedures and exit with the 
 ** error code passed.  \textit{This should be used in place of any standard
 ** program terminating functions, like exit().}
 ** @param error_code The error code to exit with.
 */
KSD_EXPORT void Quit(int error_code = 0);
//--------------------------------------------------------------------------- 
/** A class that encapsulates the application.  
 ** Each libksd program \textbf{must} contain a class derived from TApplication, and
 ** it \textbf{must} override Init().  This class is then passed to the KSD_MAIN macro 
 ** inorder to create a valid TApplication at startup.  By deriving a class from
 ** TApplication, you can provide start-up and shutdown behavior.
 ** 
 ** 
 ** TApplication is a widget, and can be thought of as the root widget.  Its drawing
 ** area takes up the entire ``\textit{screen}" and all widgets must (directly or
 ** indirectly) be ``\textit{children}" of it.
 ** 
 ** @see KSD_MAIN
 */
KSD_EXPORT_CLASS class TApplication : public TWidget {
public:
	TApplication();
	virtual ~TApplication();

	/** Called after everything else has been shutdown.  Can be used for clean-up if needed.
	 ** It is used by internal library functions to provide shutdown mechanisms.
	 */
	SigC::Signal0<void> OnQuit;

	/** Called by main.  Users should never have to deal with this function, unless
	 ** you need to create a custom main function (\textbf{NOT RECOMMENDED}).
	 ** @see KSD_MAIN
	 */
	void Run(int argc, char* argv[]);

	/** Override to provide start-up behavior.  Called only once, on start-up right 
	 ** after everything as been initialized.  Init() is the only function the 
	 ** TApplication derived classes have to define, and is a logical replacement for 
	 ** the main function.  All application start code should go here.  DO NOT put 
	 ** any vital start up code (with the exception of simple variable assignments) 
	 ** in the constructor, since the system won't be initialized until after the 
	 ** constructor has returned.
	 */
	virtual void Init() = 0;

	/** Override to provide a shutdown procedure.  Shutdown is called after the event
	 ** sub-system and drawing cycle is stopped but before anything else is shutdown.
	 */
	virtual void Shutdown() { };

	/** Create a screen from a particular backend, for libksd to draw it self to.  
	 ** Can be used to select the currently running backend by calling CreateScreen 
	 ** multiple times.
	 ** 
	 ** As of libksd 0.0.4, two backends are supported:
	 ** 
	 ** \begin{itemize}
	 ** \item "sdl" - This the default backend.  It allows for 2D drawing directly to
	 ** the screen surface.
	 ** \item "opengl" - Uses OpenGL.  This allows the main surface to be drawn to
	 ** using standard OpenGL calls.  When using this backend you can not perform
	 ** 2D drawing directly to the screen surface and must create buffered sub-surfaces
	 ** to do so.
	 ** \end{itemize}
	 ** 
	 ** @param backend 	The name of the backend to use.
	 ** @param width	The width of the screen to create.
	 ** @param height	The height of the screen to create.
	 ** @param bpp		The number of bits-per-pixel resolution.  Set to zero to 
	 ** 				use the bpp of the current display.
	 ** @param screen_flags Requested attributes for the screen.  See TScreen.
	 ** @param extra	Extra information used to initialize the backend.
	 */
	void CreateScreen(std::string backend, int width, int height, int bpp = 0, 
		unsigned int screen_flags = TScreen::DEFAULT, void* extra = NULL);

	/** Creates a screen for libksd, using the default backend.
	 */
	void CreateScreen(int width, int height, int bpp = 0, unsigned int screen_flags = TScreen::DEFAULT) {
		return CreateScreen("sdl", width, height, bpp, screen_flags, NULL);
	}

	/** Returns true if the given backend string is supported by the current
	 ** configuration.
	 */
	bool IsBackendSupported(const std::string& backend);

	/** Returns the name of the currently running backends.
	 */
	std::string GetCurrentBackend() const { return CurrentBackend; }

	/** Override to perform raw drawing to the screen.  Usually, this isn't used, 
	 ** because the screen needs to be broken up into several visual and input areas
	 ** which can easily be done through the use of widgets.
	 ** @see TWidget::Draw
	 */
	virtual bool Draw(TCanvas*, DrawType dt) { return false; };

	/** Override to perform decoupled data updates.
	 ** @see TWidget::Cycle
	 */
	virtual void Cycle() { }

	/// The way the screen is updated
	enum PaintType {
		/// Only update areas of the screen that report dirty rectangles
		PartialUpdate, 
		/// Update the whole screen every draw cycle
		FullUpdate
	};

	/// Returns a pointer to the screen surface
	TSurface* GetScreen() { return Screen; }
	
	/// Returns the window caption.
	std::string GetCaption() const;
	/// Returns the screen width.
	int GetWidth() const { return Screen ? Screen->GetWidth() : 0; }
	/// Return the screen height.
	int GetHeight()	const { return Screen ? Screen->GetHeight() : 0; }
	/// Returns the position of the screen.  This will always be point (0, 0).
	TPoint2D GetPosition() const { return TPoint2D(0, 0); } // The screen is located at (0, 0) by definition
	/// Return the current paint type used by the application.
	PaintType GetPaintType() const { return paint_type; }
	/// Resize the screen
	void Resize(int x, int y);
	
	/** Sets the Keyboard and Joystick focus to the widget passed.
	 ** @see SetKeyboardFocus, SetJoystickFocus
	 */
	void SetFocus(TWidget* w) {
		SetKeyboardFocus(w);
		SetJoystickFocus(w);
	}

	/** Sets the Keyboard focus.  The widget with keyboard focus recieves all keyboard
	 ** input.  Keyboard focus is also automatically passed from widget to widget 
	 ** by clicking on one, unless the widget doesn't wish to accept keyboard focus
	 ** @param w The widget that will possess keyboard focus.
	 */
	void SetKeyboardFocus(TWidget* w) { EventDispatcher->SetKeyboardFocus(w); }

	/** Sets the Joystick focus.  The widget with joystick focus recieves all joystick
	 ** input.  There is currently no method of automatically passing joystick focus.
	 ** @param w The widget that will possess joystick focus.
	 */
	void SetJoystickFocus(TWidget* w) { EventDispatcher->SetJoystickFocus(w); }

	/** Sets the window caption.  Only applies in windowed environments, such as X
	 ** or MS Windows.
	 */
	void SetCaption(const std::string&);

	/** Used to Enable/Disable a decoupled game loop.  Causes the \textit{immediate} 
	 ** creation or destruction of the second thread in order to run decoupled.
	 ** @see Game Loop
	 */
	void SetDecoupled(bool enable);

	/** Sets the updating policy of the application.
	 ** @see PaintType
	 */
	void SetPaintType(PaintType _type);
protected:
	/// Returns a list of char pointers, containing the command line arguments.
	char** GetArgList() { return ArgV; }
	/// Returns a specific argument
	char* GetArg(unsigned int index) { return ArgV[index]; }
	/// Returns the number of arguments on the command line
	int GetArgCount() { return ArgC; }
private:
	class EventThread;
	friend class EventThread;
	EventThread* dt;
	int error_level;

	bool Decoupled;
	PaintType paint_type;
	std::string CurrentBackend;
	char** ArgV;
	int ArgC;

	TScreen* Screen;
	TEventDispatcher* EventDispatcher;

	// make the exit function friendly, so it can do shutdown
	friend void ksd::Quit(int);

	// functions for cycle/event thread
	void EventLoop();
	using TWidget::CycleWidgetTree;
	
	// recreate buffer is invalid because their is a special system for
	// recreating the screen.
	void RecreateBuffer() { }

	// in theory, update widget should perform the screen re-draws, but
	// currently it doesn't work like that.
	void UpdateWidget() { }

	// this value doesn't really matter because the application is client
	// to no one.
	bool IsClientWidget() const { return true; }

	// the application will always accept input
	bool CanAcceptInput() const { return true; }
	bool CanAcceptMouseInput(int, int) const { return true; }

	// children to the application use absolute positions
	TPoint2D GetChildPosition(TWidget* Child) const { return Child->GetPosition(); }

	// application doesn't require an update
	bool RequiresUpdate() const { return false; }

	// Make SetPosition private because it should never be called.  It is an inherent
	// property of TApplication to be location at position (0, 0).
	void SetPosition(int X, int Y) { };

	// These functions are what truely define the application widget as being the base
	// widget heirarchy.  This makes the application's surface the screen.
	// NOTE: we make these private so that these can never be overriden again, and so 
	// that users aren't tempted to draw outside of the draw cycle.
	TSurface* GetSurface() { return Screen; }
	TSurface* GetClientSurface() { return Screen; }

	// clear buffer on repaint, otherwise just draw
	bool DrawWidget(DrawType mode) { 
		bool drawn = false;
		if(mode == REPAINT) {
			ClearBuffer();
			drawn = true;
		}
		drawn |= Draw(GetCanvas(), mode); 
		return drawn;
	}
};
//--------------------------------------------------------------------------- 
/** A pointer to actual application object in use.  You can use this to access
 ** the application object in an application-generic way.  In other words, deal
 ** with the application object without knowing anything about the user-defined
 ** application class.
 */
KSD_EXPORT extern TApplication* Application;
//---------------------------------------------------------------------------
}; // namespace ksd
//--------------------------------------------------------------------------- 
#endif


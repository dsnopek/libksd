//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//	
//---------------------------------------------------------------------------
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		TPluginSystemBase
//	
//	Purpose:		Defines an object that can open shared libraries and 
//					hold instances to them.  When LoadPlugin(...) is 
//					overridden by child classes, it can be used to hold
//					a list of plugins
//	
//	Checked-in by:	$Author$
//					$Date$
//--------------------------------------------------------------------------- 
#ifndef _KSD_PluginSysBaseH
#define _KSD_PluginSysBaseH
//--------------------------------------------------------------------------- 
#include <vector>
#include "Thread.h"
#include "export_ksd.h"
//---------------------------------------------------------------------------
#ifdef __GNUG__
#pragma interface
#endif
//---------------------------------------------------------------------------
namespace ksd {
//---------------------------------------------------------------------------
KSD_EXPORT_CLASS class TPluginSystemBase {
public:
	virtual ~TPluginSystemBase() { Clear(); }

	virtual void Clear();

	void LoadPluginLib(const char* LibName);
	void LoadPluginList(const char* ListName);

	// provide real plugin functionality
	virtual bool IsValidPlugin(class TSharedLibrary* LibInstance) = 0;
    virtual bool LoadPlugin(class TSharedLibrary* LibInstance) = 0;
private:
	std::vector<TSharedLibrary*> FLibrary;
	TMutex lib_cs; // critical section protecting FLibrary
};
//---------------------------------------------------------------------------
}; // namespace ksd
//---------------------------------------------------------------------------
#endif


//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	$Header$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Checked-in by:	$Author$
//
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma implementation
#endif
//--------------------------------------------------------------------------- 
#define KSD_BUILD_LIB
#include <SDL/SDL.h>
#include "Application.h"
#include "Surface.h"
#include "log.h"

using namespace ksd;
//--------------------------------------------------------------------------- 
TSurface::TSurface() 
{
	LOG_CONSTRUCTOR("TSurface");
}
//--------------------------------------------------------------------------- 
TSurface::~TSurface()
{
	LOG_DESTRUCTOR("TSurface");
}
//--------------------------------------------------------------------------- 
TSurfaceData* TSurfaceData::Convert(const TPixelFormat& _format, int image_type)
{
	TSurfaceData* temp;
	SDL_PixelFormat* sdl_format = _format.CreateSDLFormat();
	SDL_Surface* Converted;
	Uint32 sdl_flags = 0;

	if(!sdl_format) {
		// error
		LOG(1, "Unable to convert surface!!");
		return NULL;  // empty image
	}

	// make sdl_flags
	if(image_type & TSurfaceData::VIDEO_MEMORY)
		sdl_flags |= SDL_HWSURFACE;
	else if(image_type & TSurfaceData::SYSTEM_MEMORY)
		sdl_flags |= SDL_SWSURFACE;
	if(image_type & TSurfaceData::SRC_COLORKEY)
		sdl_flags |= SDL_SRCCOLORKEY;
	if(image_type & TSurfaceData::SRC_ALPHA)
		sdl_flags |= SDL_SRCALPHA;

	// do sdl convert
	Converted = SDL_ConvertSurface(GetHandle(), sdl_format, sdl_flags);

	// destroy sdl pixel format object
	delete sdl_format;

	if(!Converted) {
		// error
		LOG(1, "Unable to convert surface!!");
		return NULL;  // empty image
	}

	// create new surface data object
	temp = new TSurfaceData(Converted, true);
	temp->SetFlags(image_type);

	return temp;
}
//---------------------------------------------------------------------------  
void TSurfaceData::CreateImage(int width, int height, 
	TPixelFormatType pixel_format, unsigned int image_type)
{
	LOG_ENTER_FUNC("TSurfaceData::CreateImage(int, int, TPixelFormatType, TImageType)");

	SDL_Surface* temp;
	Uint32 flags = 0, rmask, gmask, bmask, amask;
	Uint8 bpp;

	if(Surface) DestroySurface();

	LOG(9, "Requesed: width = %d, height = %d, pixel_format = %d, image_type = %d",
		width, height, pixel_format, image_type);

	// sets flags
	if(image_type & TSurfaceData::VIDEO_MEMORY)
		flags |= SDL_HWSURFACE;
	else if(image_type & TSurfaceData::SYSTEM_MEMORY)
		flags |= SDL_SWSURFACE;
	if(image_type & TSurfaceData::SRC_COLORKEY)
		flags |= SDL_SRCCOLORKEY;
	else if(image_type & TSurfaceData::SRC_ALPHA)
		flags |= SDL_SRCALPHA;

	// find the masks/bpp for this pixel format
	bpp = TPixelFormat::GetInfo(pixel_format, rmask, gmask, bmask, amask);

	LOG(9, "BPP = %u", bpp);

	temp = SDL_CreateRGBSurface(flags, width, height, bpp,
							    rmask, gmask, bmask, amask);

	if(!temp) {
		// error
		LOG(1, "Unable to create surface!!");
	}

	SetSurface(temp, true);
	SetFlags(image_type);

	LOG_EXIT_FUNC("TSurfaceData::CreateImage(int, int, TPixelFormatType, TImageType)");
}
//---------------------------------------------------------------------------  
void TSurfaceData::CreateImage(int width, int height, unsigned int bpp,
	unsigned int image_type)
{
	LOG_ENTER_FUNC("TSurfaceData::CreateImage(int, int, Uint8, TImageType)");

	SDL_Surface* temp;
	Uint32 flags = 0, rmask, gmask, bmask, amask;
	TPixelFormatType pixel_format;

	// Check to see if image is already created, if so create new
	if(Surface) DestroySurface();

	LOG(9, "Requesed: width = %d, height = %d, bpp = %d, image_type = %d",
		width, height, pixel_format, image_type);

	// sets flags
	if(image_type & TSurfaceData::VIDEO_MEMORY)
		flags |= SDL_HWSURFACE;
	else if(image_type & TSurfaceData::SYSTEM_MEMORY)
		flags |= SDL_SWSURFACE;
	if(image_type & TSurfaceData::SRC_COLORKEY)
		flags |= SDL_SRCCOLORKEY;
	else if(image_type & TSurfaceData::SRC_ALPHA)
		flags |= SDL_SRCALPHA;

	// if bpp matches that of the screen make sure to select the same format
	// type as it
	if(Application->GetScreen() && bpp == Application->GetScreen()->GetFormat().GetBitsPerPixel()) {
		pixel_format = Application->GetScreen()->GetFormat().GetType();
	} else { // select arbitrary format
		switch(bpp) {
			case 8:
				pixel_format = TPixelFormat::IND8;
				break;
			case 16:
				pixel_format = TPixelFormat::RGB555;
				break;
			case 24:
				pixel_format = TPixelFormat::BGR888;
				break;
			case 32:
				pixel_format = TPixelFormat::ABGR8888;
				break;
			default:
				pixel_format = TPixelFormat::UNKNOWN;
				break;
		}
	}

	LOG(9, "Pixel Format Type = %d", pixel_format);

	// find the masks/bpp for this pixel format
	TPixelFormat::GetInfo(pixel_format, rmask, gmask, bmask, amask);

	temp = SDL_CreateRGBSurface(flags, width, height, bpp,
							    rmask, gmask, bmask, amask);

	if(!temp) {
		// error
		LOG(1, "Unable to create surface!!");
	}

	SetSurface(temp, true);
	SetFlags(image_type);

	LOG_EXIT_FUNC("TSurfaceData::CreateImage(int, int, Uint8, TImageType)");
}
//---------------------------------------------------------------------------
void TSurfaceData::Fill(int X, int Y, int Width, int Height, const TColor& Color)
{
	if(IsValid()) {
		Mutex.Lock();

		SDL_Rect dest;
		dest.x = X;
		dest.y = Y;
		dest.w = Width;
		dest.h = Height;

		SDL_FillRect(Surface, &dest, Format.MapToPixel(Color));

		Mutex.Unlock();
	}
}
//---------------------------------------------------------------------------  
void TSurfaceData::Resize(unsigned int width, unsigned int height)
{
	// TODO: copy the old data onto the new surface
	CreateImage(width, height, GetFormat().GetType(), GetFlags());
}
//---------------------------------------------------------------------------  
void TSurfaceData::Lock()
{
	if(Surface) {
		if(SDL_MUSTLOCK(Surface)) {
			if(SDL_LockSurface(Surface) < 0) {
				SDL_Delay(10);
				if(SDL_LockSurface(Surface) < 0) {
					// cannot lock surface!!
					LOG(1, "Unable to lock surface!!");
					// TODO: throw exception?
				}
			}
			LOG(9, "Surface Locked");
		}
		
		// lock the mutex
		Mutex.Lock();
	}
}
//---------------------------------------------------------------------------  
void TSurfaceData::Unlock()
{
	if(Surface) {
		if(SDL_MUSTLOCK(Surface))
			SDL_UnlockSurface(Surface);

		// unlock the mutex
		Mutex.Unlock();
	}
}
//---------------------------------------------------------------------------  
void TSurfaceData::ApplyPalette()
{
	if(Format.GetPalette()) {
		Format.GetPalette()->ApplyPalette(Surface);
	} else {
		if(Surface && Format.GetBitsPerPixel() == 8) { // SHOULD HAVE A PALETTE
			throw("Trying to apply palette to an 8bpp surface with no palette");
		}
	}
}
//---------------------------------------------------------------------------  
void TSurfaceData::GatherPalette()
{
	if(Format.GetPalette()) {
		Format.GetPalette()->GatherPalette(Surface);
	} else {
		if(Surface && Format.GetBitsPerPixel() == 8) { // SHOULD HAVE A PALETTE
			throw("Trying to gather palette from an 8bpp surface with no palette");
		}
	}
}
//---------------------------------------------------------------------------  

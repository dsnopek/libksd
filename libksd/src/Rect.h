//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		Rect
//	
//	Purpose:		Defines an an object representing a 2d rectangle.
//	
//	Check-in by:	$Author$
//					$Date$
//---------------------------------------------------------------------------
#ifndef _KSD_RectH
#define _KSD_RectH
//---------------------------------------------------------------------------
#include "Shape2D.h"
#include "Dimension.h"
#include "export_ksd.h"
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma interface
#endif
//---------------------------------------------------------------------------
namespace ksd {
//---------------------------------------------------------------------------
/** A rectangle shape object.  Used to describe rectangular regions in screen 
 ** space.  It is not advised that you use it for anything more complex because
 ** TRect is optimized for speed and size, not precision.
 **
 ** TRect uses a non-standard way to represent a screen rectangle.  It holds the
 ** distance that is edges are from the screen edges.  For example, the Left
 ** value is the distance from the left edge of the screen to the left edge
 ** of the rectangle.  In this way the position of the rectangle is (Left, Top)
 ** and its is defined by Right - Left.  Keep this in mind when using TRect.
 */
KSD_EXPORT_CLASS class TRect : public TShape2D {
public:
    ///
	TRect() { Left = Top = Right = Bottom = 0; }
	
    /// Create a TRect object with the given dimensions.
	TRect(int X1, int Y1, int X2, int Y2);
	
	/// Create a rect formed by these two point
    TRect(const TPoint2D& point1, const TPoint2D& point2);
    
	/// Create a rect at the given point with the given dimensions
    TRect(const TPoint2D& point, const Dimension& dim) {
		Left = point.X;
	    Top = point.Y;
    	Right = Left + dim.Width;
	    Bottom = Top + dim.Height;
	
	}
	
	/// Initia;ize this rect to the value of another
    TRect(const TRect& copy) {
		Left = copy.GetLeft();
    	Top = copy.GetTop();
    	Right = copy.GetRight();
    	Bottom = copy.GetBottom();
	}

	/** Returns the distance from the left edge of the screen to the 
	 ** left edge of the rectangle. 
	 */
    int GetLeft() const { return Left; }

	/** Returns the distance from the top edge of the screen to the
	 ** top edge of the rectangle.
	 */
    int GetTop() const { return Top; }

	/** Returns the distance from the left edge of the screen to the
	 ** right edge of the rectangle.
	 */
    int GetRight() const { return Right; }

	/** Returns the distance from the top edge of the screen to the
	 ** bottom edge of the rectangle.
	 */
    int GetBottom() const { return Bottom; }

	/** Sets the rectangles left value.
	 ** @see GetLeft
	 */
    void SetLeft(int l) { Left = l; }
    
	/** Sets the rectangles top value.
	 ** @see GetTop
	 */
	void SetTop(int t) { Top = t; }

	/** Sets the rectangles right value.
	 ** @see GetRight
	 */
    void SetRight(int r) { Right = r; }
    
	/** Sets tge rectangles bottom value.
	 ** @see GetBottom
	 */
	void SetBottom(int b) { Bottom = b; }

	/// Return the position of the upper-left corner of the rectangle.
    TPoint2D GetPosition() const { return TPoint2D(Left, Top); }
	/// Returns the width of the rectangle.
    int GetWidth() const { return Right - Left; }
	/// Returns the height of the rectangle.
    int GetHeight() const { return Bottom - Top; }

	/// Clears the rectangle.
    void Clear() { Left = Top = Right = Bottom = 0; }
	
	/** Sets the position of the rectangle.  This is actually a slower
	 ** operation than it would seem given the internal representation of
	 ** the rectangle.
	 */
    void SetPosition(const TPoint2D& point);

	/// Sets the width of the rectangle.
    void SetWidth(int Width) { Right = Left + Width; }
    /// Sets the height of the rectangle.
	void SetHeight(int Height) { Bottom = Top + Height; }
	/// Sets all the properties the rectangle.
    void Set(int X1, int Y1, int X2, int Y2);
	/// Retruns the rectangle dimensions in a Dimension object.
    Dimension GetDimensions() const;

	/// Returns true if this a valid rectangle.
	bool IsValid() const { return Left < Right && Top < Bottom; }

    // shape functions
    bool IsInside(const TPoint2D& V) const;
    void Move(int X, int Y);
    void MoveTo(int X, int Y);
	void Visit(Visitor* V) { V->VisitRect(this); }

    // clip functions
    // TODO: implement!
    //bool ClipLine(TLine& line) const;
	/// Clips a line defined by these endpoints to this rectangle.
    bool ClipLine(int& x1, int& y1, int& x2, int& y2) const;
	/// Clips an other rectangle to this rectangle.
    bool ClipRect(TRect& rect) const;
	/// Clips a rectangle defined by the given properties to this rectangle.
    bool ClipRect(int& left, int& top, int& right, int& bottom) const;
	/// Clips a point to this rectagnle.
    void ClipPoint(TPoint2D& point) const;
   	/// Clips a point to this rectagnle.
   	void ClipPoint(int& X, int& Y) const;
private:
    int Left, Top, Right, Bottom;
};
//---------------------------------------------------------------------------
}; // namepace ksd
//---------------------------------------------------------------------------
#endif
 

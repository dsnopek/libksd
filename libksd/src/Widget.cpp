//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	$Header$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Checked-in by:	$Author$
//
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma implementation
#endif
//--------------------------------------------------------------------------- 
#define KSD_BUILD_LIB
#include <algorithm>
#include "Surface.h"
#include "Widget.h"
#include "Image.h"
#include "Application.h"
#include "log.h"

using namespace ksd;
using namespace SigC;
using namespace std;
//---------------------------------------------------------------------------  
// allocate the single widget tree object
TWidgetTree ksd::TWidget::WidgetTree;
//--------------------------------------------------------------------------- 
// NOTE: flags in string are ordered from last to first compared to enum
TWidget::TWidget(TWidget* _parent) : Object(), flags(string("0010011111"))
{
	LOG_CONSTRUCTOR("TWidget");

	Parent = _parent;
	if(Parent) {
		WidgetTree.Add(this);
	}

	BackgroundImage = NULL;
	draw_flag = dfREPAINT;
	Changed = true;
	Visible = Enabled = true;
}
//--------------------------------------------------------------------------- 
TWidget::~TWidget() 
{
	LOG_DESTRUCTOR("TWidget");

	WidgetTree.Remove(this);
}
//--------------------------------------------------------------------------- 
// IsInside is fairly straight forward: it check the point relative to the 
// application's position.
bool TWidget::IsInside(int X, int Y)
{
	LOG_FUNCTION_LOOP("bool TWidget::IsInside(int, int)");
	
	// do local check
	MakeLocal(X, Y);
	if(X >= 0 && Y >= 0) {
		if(X < GetWidth() && Y < GetHeight()) {
			return true;
		}
	}

	return false;
}
//---------------------------------------------------------------------------  
// IsInsideLocal has somewhat cyptic behavior.  Its checks the point relative to 
// its direct parent widget NOT a local point like the name might suggest.
bool TWidget::IsInsideLocal(int X, int Y)
{
	LOG_FUNCTION_LOOP("bool TWidget::IsInsideLocal(int, int);");

	// establish ACTUAL position, relative to the parent's pos
	TPoint2D pos = Parent ? Parent->GetChildPosition(this) : GetPosition();

	// do check
	if(X >= pos.X && Y >= pos.Y) {
		if(X < pos.X + GetWidth() && Y < pos.Y + GetHeight()) {
			return true;
		}
	}

	return false;
}
//---------------------------------------------------------------------------  
void TWidget::MakeLocal(int& X, int& Y)
{
	TWidget* _child, *_parent;
	TPoint2D pos;
	
	for(_child = this, _parent = this->Parent; 
		_parent != NULL; _child = _parent, _parent = _child->Parent) {

		// find the actual child position
		pos = _parent->GetChildPosition(_child);

		// adjust
		X -= pos.X;
		Y -= pos.Y;
	}
}
//---------------------------------------------------------------------------  
void TWidget::MakeGlobal(int& X, int& Y)
{
	TWidget* _child, *_parent;
	TPoint2D pos;
	
	for(_child = this, _parent = this->Parent; 
		_parent != NULL; _child = _parent, _parent = _child->Parent) {

		// find the actual child position
		pos = _parent->GetChildPosition(_child);

		// adjust
		X += pos.X;
		Y += pos.Y;
	}
}
//---------------------------------------------------------------------------  
void TWidget::MarkAllAsChanged()
{
	if(WidgetTree.size() > 0) {
		TWidgetTree::iterator i, end;

		// find position on the tree
		i = WidgetTree.find_widget(this);

		// this should be impossible!!
		#ifdef __DEBUG__
		if(i == WidgetTree.end()) {
			throw ksd::exception("Widget::MarkAllAsChanged", 
				"This widget could not be found in the widget tree!");
		}
		#endif

		// point end at its next sibling
		end = i;
		end.next_sibling();

		// mark as changed
		for(; i != end; i++) {
			(*i)->MarkAsChanged();
		}
	}
}
//--------------------------------------------------------------------------- 
bool TWidget::GrabKeyboardFocus()
{
	if(Application) {
		Application->SetKeyboardFocus(this);
	}

	return true;
}
//--------------------------------------------------------------------------- 
bool TWidget::GrabJoystickFocus()
{
	if(Application) {
		Application->SetJoystickFocus(this);
	}

	return true;
}
//--------------------------------------------------------------------------- 
void TWidget::ClearBuffer()
{
	LOG_FUNCTION_LOOP("void TWidget::ClearBuffer()");

	TSurface* my_surface = GetSurface();

	if(my_surface) {
		TWidget* draw_parent = this;

		// psuedo-recursively get true background flags
		while(draw_parent->GetParent() && draw_parent->GetUseParentBackground()) {
			draw_parent = draw_parent->GetParent();
		} 

		if(draw_parent->GetDrawBackgroundColor()) {
			my_surface->Fill(GetBackgroundColor());
		}

		if(draw_parent->GetDrawBackgroundImage() && GetBackgroundImage() && GetBackgroundImage()->IsValid()) {
			// make a temporary TCanvas
			TCanvas temp(my_surface);

			if(draw_parent->GetStretchBackground()) {
				temp.StretchBlit(0, 0, GetWidth(), GetHeight(), *GetBackgroundImage());
			} else { // tile background is the default
				for(int Y = 0; Y < GetHeight(); Y += GetBackgroundImage()->GetHeight()) {
					for(int X = 0; X < GetWidth(); X += GetBackgroundImage()->GetWidth()) {
						temp.Blit(X, Y, *GetBackgroundImage());
					}
				}
			}
		}
	}
	#ifdef __DEBUG__
	else {
		LOG(1, "Trying to clear the buffer of widget with no main surface.")
	}
	#endif
}
//---------------------------------------------------------------------------  
void TWidget::RecreateAllBuffers()
{
	if(WidgetTree.size() > 0) {
		TWidgetTree::iterator i, end;

		// find position on the tree
		i = WidgetTree.find_widget(this);

		// point end at its next sibling
		end = i;
		end.next_sibling();

		// this should be impossible!!
		#ifdef __DEBUG__
		if(i == WidgetTree.end()) {
			throw ksd::exception("Widget::RecreateBuffers", 
				"This widget could not be found in the widget tree!");
		}
		#endif

		// recreate buffers
		for(; i != end; i++) {
			(*i)->RecreateBuffer();
		}

		// make sure the new buffers are repainted
		NeedsRepaint();
	}
}
//---------------------------------------------------------------------------  
// NOTE: this is piece of coding art!! 
bool TWidget::DrawWidgetTree(list<TRect>* DirtyRects) 
{
	vector<TWidget*> BufferCopyStack;
	bool Changed = false, CurrentChanged;
	TWidgetTree::iterator current = WidgetTree.begin();

	// paint the whole tree and make buffer copy stack
	while(current != WidgetTree.end()) {
		// deal with setting up the base-case and incrementing the data
		// for all other cases.
		if(current == WidgetTree.begin()) {
			// these are the defaults for when this loop is run for the 
			// first time: Use the application draw flag and we don't have
			// any rects yet.
			(*current)->draw_info.mode = (*current)->draw_flag;
			(*current)->draw_info.got_rect = false;
		} else {
			// only allow draw flags to up the type of draw, i.e. when parent
			// is set to repaint, you must repaint the children regardless
			if((*current)->draw_flag > (*current)->GetParent()->draw_info.mode) {
				(*current)->draw_info.mode = (*current)->draw_flag;
			} else {
				(*current)->draw_info.mode = (*current)->GetParent()->draw_info.mode;
			}

			// pass the "got_rect" flag down from parent to child because
			// when the parent widget has has had its dimensions passed as a
			// dirty rect, then that rect also includes the child widgets.
			(*current)->draw_info.got_rect = (*current)->GetParent()->draw_info.got_rect;
		}

		if((*current)->CanDraw()) {
			if((*current)->draw_info.mode != TWidget::dfNO_DRAW) {
				// draw the widget
				CurrentChanged = (*current)->DrawWidget((TWidget::DrawType)(*current)->draw_info.mode);

				// update the changed variable
				Changed |= CurrentChanged;
				
				// add a dirty rect arround the changed widget if necessary
				if(DirtyRects && CurrentChanged && !(*current)->draw_info.got_rect) {
					TRect dirty;
					int X = 0, Y = 0;
					(*current)->MakeGlobal(X, Y);
					
					dirty.Set(X, Y, 
							  X + (*current)->GetWidth(),
							  Y + (*current)->GetHeight());

					// TODO: replace using application with using root widget
					// clip dirty rect to screen
					if(dirty.GetLeft() < 0) dirty.SetLeft(0);
					if(dirty.GetTop() < 0) dirty.SetTop(0);
					if(dirty.GetRight() > Application->GetWidth()) {
						dirty.SetRight(Application->GetWidth());
					}
					if(dirty.GetBottom() > Application->GetHeight()) {
						dirty.SetBottom(Application->GetHeight());
					}
					
					DirtyRects->push_back(dirty);
					(*current)->draw_info.got_rect = true;
				}

				// reset draw flags
				(*current)->ResetDrawFlags();

				// if double buffered, then add to buffer copy stack so that it
				// can be copied to the screen at the proper time
				if((*current)->RequiresUpdate()) {
					BufferCopyStack.push_back(*current);
				}

				current++; // increment by one in normal operation
			} else current.next_sibling();	// if dfNO_DRAW, then skip 
		} else current.next_sibling(); 		// if widget can't draw, skip
	}

	// copy buffers to the screen
	while(BufferCopyStack.size() > 0) {
		BufferCopyStack.back()->UpdateWidget();
		BufferCopyStack.pop_back();
	}

	return Changed;
}
//---------------------------------------------------------------------------  
void TWidget::CycleWidgetTree()
{
	TWidgetTree::iterator i;

	// cycle all the widgets, parent before child
	for(i = WidgetTree.begin(); i != WidgetTree.end(); i++) {
		(*i)->Cycle();
	}
}
//--------------------------------------------------------------------------- 



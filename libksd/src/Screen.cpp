//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	$Header$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Checked-in by:	$Author$
//
//---------------------------------------------------------------------------
#ifdef __GNUG__
#pragma implementation
#endif
//--------------------------------------------------------------------------- 
#define KSD_BUILD_LIB
#include <iostream>
#include "Screen.h"
//--------------------------------------------------------------------------- 
using namespace ksd;
using namespace std;
//---------------------------------------------------------------------------  
void ECannotInitDisplay::report(std::ostream& output) const 
{
	// call parent output
	::ksd::exception::report(output);

	// output screen data
	output << "Failed Screen Initialization:" << endl;

	// basic screen info
	output << "Dimensions: (" << Width << " x " << Height << ")" << endl;
	output << "Bit Depth: " << Bpp << endl;

	// screen attributes
	output << "Screen attributes: ";
	if(Flags == TScreen::DEFAULT) {
		output << "DEFAULT";
	} else {
		if(Flags & TScreen::FULLSCREEN) output << "FULLSCREEN ";
		if(Flags & TScreen::HARDWARE_BUFFER) output << "HARDWARE_BUFFER ";
		if(Flags & TScreen::DOUBLE_BUFFERED) output << "DOUBLE_BUFFERED ";
	}
	output << endl;

	// extra
	output << "Extra data: " << Extra << endl;

	// flush the stream
	output << flush;
}
//---------------------------------------------------------------------------  


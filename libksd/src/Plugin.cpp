//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//	
//--------------------------------------------------------------------------- 
//	$Header$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Checked-in by:	$Author$
//	
//--------------------------------------------------------------------------- 
#define KSD_BUILD_LIB
#include "Plugin.h"
#include "PluginSysManager.h"
#include "init.h"
//--------------------------------------------------------------------------- 
using namespace ksd;
//---------------------------------------------------------------------------  
//
// Our very creative create on demand code
// 

_GLOBAL_DECL(TPluginSystemManager, PluginManager);
static TPluginSystemManager* GetPluginManager()
{
	_GLOBAL_CREATE(PluginManager, new TPluginSystemManager);
	return PluginManager;
}

static void ShutdownPluginModule()
{
	_GLOBAL_DESTROY(PluginManager);
}

_MODULE_SHUTDOWN(&ShutdownPluginModule);
//--------------------------------------------------------------------------- 
void ksd::LoadPlugin(const char* PluginName)
{
	GetPluginManager()->LoadPluginLib(PluginName);
}
//--------------------------------------------------------------------------- 
void ksd::LoadPluginList(const char* ListName)
{
	GetPluginManager()->LoadPluginList(ListName);
}
//--------------------------------------------------------------------------- 
void ksd::RegisterPluginSystem(TPluginSystemBase* PluginSystem)
{
	GetPluginManager()->AddPluginSystem(PluginSystem);
}
//--------------------------------------------------------------------------- 
void ksd::UnregisterPluginSystem(TPluginSystemBase* PluginSystem)
{
	GetPluginManager()->RemovePluginSystem(PluginSystem);
}
//--------------------------------------------------------------------------- 


//---------------------------------------------------------------------------
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//
//---------------------------------------------------------------------------
//	File:			$RCSfile$
//
//	Author:			Andrew Sterling Hanenkamp
//	Revised by:
//	Version:		$Revision$
//
//	Objects:		TEdgeTable
//
//	Purpose:		Polygon abstraction.
//
//	Checked-in by:	$Author$
//					$Date$
//---------------------------------------------------------------------------
#ifndef _KSD_PolygonH_
#define _KSD_PolygonH_
//---------------------------------------------------------------------------
#include <list>
#include "Point2D.h"
#include "Rect.h"
#include "export_ksd.h"
//---------------------------------------------------------------------------
#ifdef __GNUG__
#pragma interface
#endif
//---------------------------------------------------------------------------
namespace ksd {
//---------------------------------------------------------------------------
// This class is used to store the Edge Table and Active Edge table used by
// the Polygon and FillPolygon methods of TCanvas. This is a complicated
// enough set of operations to demand it's own data structure to handle it.
KSD_EXPORT_CLASS class TEdgeTable {
public:
	TEdgeTable(const int _length, const TPoint2D *polygon, const TRect &_cliprect);
	~TEdgeTable();

	int GetFirstY() const { return firsty; }

	bool NextLine();
	bool ScanLine(int &startx, int &endx, bool &visible, int &type);

protected:
	struct ScanLineCoord {
		ScanLineCoord(const TPoint2D &top, const TPoint2D &bottom) :
			a(top.X), b(0), dx(bottom.X-top.X), dy(bottom.Y-top.Y), olda(top.X) {
			if(dy == 0) {
				inc = 0;
				if(top.X > bottom.X) a = top.X;
				else { a = bottom.X; dx = -dx; }
			} else {
				if(dx < 0) { dx = -dx; inc = -1;}
				else { inc = 1; }
			}
		}

		void NextLine() {
			olda = a;
			b += dx;
			while(b >= dy) {
				b -= dy;
				a += inc;
			}
		}

		int a, b, dx, dy, inc, olda;
	};

	struct Edge {
		Edge(const TPoint2D &_start, const TPoint2D &_end) :
				start(_start), end(_end), current(_start, _end) {}

		const TPoint2D &start, &end;
		ScanLineCoord current;
	};

	std::list<Edge> alloc;
	std::list<Edge*> lines, active, ordered;
	int currenty, firsty, lasty;
	bool even;
	const TRect &cliprect;
};
//---------------------------------------------------------------------------
}; // ksd namespace
//---------------------------------------------------------------------------
#endif

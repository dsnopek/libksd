//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//	
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		TKeyboard, TKeyboardFilter, TKey
//	
//	Purpose:		Encapsulates the keyboard.
//	
//	Checked-in by:	$Author$
//					$Date$
//--------------------------------------------------------------------------- 
#ifndef _KSD_KeyboardH_
#define _KSD_KeyboardH_
//--------------------------------------------------------------------------- 
#include <SDL/SDL_keyboard.h>
#include <string>
#include "Input.h"
#include "export_ksd.h"
//---------------------------------------------------------------------------
#ifdef __GNUG__
#pragma interface
#endif
//---------------------------------------------------------------------------
namespace ksd {

// forward decls
class TKeyboard;
//---------------------------------------------------------------------------
// encapsulates SDL_keysym
KSD_EXPORT_CLASS class TKey {
public:
	// the link to SDL -- the reason I copy the value instead of holding on to it
	// and reading from it, is that this allows the framework to change values
	// and to more easily create its own key object
	TKey(const SDL_keysym& raw);

	// I would LOVE to implement these. But I don't know how to convert from
	// SDLKey to unicode and vice-versa.
	//TKey(SDLKey sym, SDLMod mod = KMOD_NONE); // virtual key constructor
	//TKey(Uint16 unicode); // unicode constructor

	SDLKey GetKeyCode() const  		{ return sym; }
	SDLMod GetModifier() const		{ return mod; }
	Uint16 GetUnicode()	const		{ return unicode; }
	std::string GetName() const;

	// can't really use these yet...
	//void SetKeyCode(SDLKey _sym);
	//void SetModifier(SDLMod _mod);
	//void SetUnicode(Uint16 _unicode);
private:
	SDLKey sym;
	SDLMod mod;
	Uint16 unicode;
};
//---------------------------------------------------------------------------
KSD_EXPORT_CLASS class TKeyboardFilter : public TInputFilter {
public:
	TKeyboardFilter(TKeyboard* kb);

	void Filter(TInputEvent& evt) { }
	void Detect(int id, int time_out = 1000) { }
	void Load(char*) { }
	void Save(char*) { }
};
//---------------------------------------------------------------------------
KSD_EXPORT_CLASS class TKeyboard : public TInputDevice {
public:
	TKeyboard() : TInputDevice() { Keys = NULL; }

	// accessors
	void 	EnableKeyRepeat(bool enable)		{ SDL_EnableKeyRepeat(enable ? SDL_DEFAULT_REPEAT_DELAY : 0, SDL_DEFAULT_REPEAT_INTERVAL); }
	void    EnableUnicode(bool enable)			{ SDL_EnableUNICODE(enable); }
	bool 	IsPressed(SDLKey key) const 		{ return Keys ? Keys[key] : false; }
	SDLMod 	GetModifier()						{ return SDL_GetModState(); }
	void	SetModifier(SDLMod mod)				{ SDL_SetModState(mod); }

	// input device items
	TInputFilter* CreateFilter() 	{ return new TKeyboardFilter(this); }
	void Update() 					{ Keys = (SDLKey*)SDL_GetKeyState(NULL); }
private:
	SDLKey* Keys;
};
//---------------------------------------------------------------------------
KSD_EXPORT extern TKeyboard Keyboard;
//---------------------------------------------------------------------------
}; // namespace ksd
//---------------------------------------------------------------------------
#endif


//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//
//---------------------------------------------------------------------------
//	File:			$RCSfile$
//
//	Author:			David Snopek
//	Revised by:
//	Version:		$Revision$
//
//	Objects:		ImageIterator
//
//	Purpose:		Using the iterator pattern to abstract all pixel
//					access.
//
//	Checked-in by:	$Author$
//					$Date$
//---------------------------------------------------------------------------
#ifndef _KSD_ImageIteratorH_
#define _KSD_ImageIteratorH_
//---------------------------------------------------------------------------
#include <strstream>
#include <SDL/SDL_types.h>  
#include "Dimension.h"
#include "exception.h"
#include "ksdconfig.h"
//---------------------------------------------------------------------------
namespace ksd {
//---------------------------------------------------------------------------
// TODO: move this to a general location.
// NOTE: this make sure that user code is inlined no matter what optimization
// level (ie. -O?) they choose.  For example a user may want the benefits of 
// -Os (optimze for size) but still have fast ImageIterator code.  In other
// words, who wouldn't want this code inlined?
#ifdef __GNUG__
# define ALWAYS_INLINE __attribute__((always_inline)) inline
#else
# define ALWAYS_INLINE inline
#endif
//---------------------------------------------------------------------------
class EDrawOutOfBoundsError : public exception {
public:
	EDrawOutOfBoundsError(const std::string& _unit, const std::string& _message)
		: ksd::exception(_unit, _message, "draw out-of-bounds error") { }
};
//---------------------------------------------------------------------------
class ImageIterator {
public:
	/// The type of iterator used.
	enum Type {
		/// Read-only iterator
		READ_ONLY,
		/// Read-write flag
		READ_WRITE,
	};
	
	/// Creates a NULL image iterator.
	ALWAYS_INLINE ImageIterator() : ImagePtr(NULL), bpp(0), Width(0), Pitch(0), Height(0), type(READ_ONLY) 
	{ 
		#ifdef KSD_DEBUG_IMAGE_ITERATOR
		CurX = 0;
		CurY = 0;
		#endif
	}
	
	/** Creates a new ImageIterator from the given void pointer.
	 ** 
	 ** @param img_ptr The address of the pixels.
	 ** @param _bpp Bytes-Per-Pixel! Not to be confused with Bits-Per-Pixel!  Should 
	 ** be a number from 1 to 4.
	 ** @param _width The width of the wrapped image.
	 ** @param _pitch The pitch of the wrapped image.
	 ** @param _height The height of the wrapped image.
	 ** @param _type The iterator permissions.
	 */
	ALWAYS_INLINE ImageIterator(void* img_ptr, int _bpp, unsigned int _width, unsigned int _pitch, unsigned int _height, \
		Type _type) 
	{
		ImagePtr = (Uint8*)img_ptr;
		Width = _width;
		Pitch = _pitch;
		Height = _height;
		type = _type;

		// Note: I am unsure about optimizing this away.  If enough users make this mistake then
		// this will be compiled in by default.
		#ifdef KSD_DEBUG_IMAGE_ITERATOR
		// make sure we get a valid bpp
		if( !(_bpp >= 1 && _bpp <= 4) ) {
			std::strstream s;
			s << "Invalid Bytes-Per-Pixel value used to create an image iterator: "
			  << _bpp << ".  Values must be between 1 and 4 inclusive.";
			throw EInvalidInput(__FILE__, s.str());
		}
		#endif
		bpp = (Uint8)_bpp;

		#ifdef KSD_DEBUG_IMAGE_ITERATOR
		CurX = 0;
		CurY = 0;
		#endif
	}

	///
	ALWAYS_INLINE ImageIterator(const ImageIterator& _copy) {
		ImagePtr = _copy.ImagePtr;
		Width = _copy.Width;
		Pitch = _copy.Pitch;
		Height = _copy.Height;
		bpp = _copy.bpp;
		type = _copy.type;

		#ifdef KSD_DEBUG_IMAGE_ITERATOR
		CurX = _copy.CurX;
		CurY = _copy.CurY;
		#endif
	}

	/// increments the interator by the number pixels
	ALWAYS_INLINE void increment(unsigned int _val) {
		ImagePtr += (_val * bpp);

		#ifdef KSD_DEBUG_IMAGE_ITERATOR
		CurX += _val;
		while(CurX > Pitch) {
			CurX -= Pitch;
			CurY++;
		}
		#endif
	}

	/// Move the image iterator by the given (X, Y) values.
	ALWAYS_INLINE void increment(unsigned int _X, unsigned int _Y) {
		ImagePtr = (ImagePtr + (bpp * _X) + (Pitch * _Y));

		#ifdef KSD_DEBUG_IMAGE_ITERATOR
		CurX += _X;
		CurY += _Y;
		#endif
	}

	/// Move the image iterator by (-X, -Y).  This is the inverse to increment.
	ALWAYS_INLINE void scroll(unsigned int _X, unsigned int _Y) {
		ImagePtr = ( ImagePtr - ((bpp * _X) + (Pitch * _Y)) );
		
		#ifdef KSD_DEBUG_IMAGE_ITERATOR
		CurX -= _X;
		CurY -= _Y;
		#endif
	}

	/// Set the current pixel to the given value
	ALWAYS_INLINE void set(Uint32 _val) {
		if(type == READ_WRITE) {
			#ifdef KSD_DEBUG_IMAGE_ITERATOR
			if(CurX >= Width || CurY >= Height) {
				std::strstream s;
				s << "Tried to set a pixel (" << CurX << ", " << CurY << ") "
				  << "that was outside the draw bounds (" << Width << ", " << Height << ").";
				throw new EDrawOutOfBoundsError(__FILE__, s.str());
			}
			#endif

			memcpy(ImagePtr, &_val, bpp);
		} else {
			// Note: optimization greatly reduces code size 
			#ifdef KSD_DEBUG_IMAGE_ITERATOR
			throw new EPermissionError(__FILE__, "Cannot write to an ImageIterator opened as READ_ONLY");
			#endif
		}
	}

	/// Gets the wrapped images dimensions
	ALWAYS_INLINE Dimension get_dimension() const {
		return Dimension(Width, Height);
	}

	/// Gets the wrapped images width
	ALWAYS_INLINE unsigned int get_width() const {
		return Width;
	}

	/// Gets the wrapped images height
	ALWAYS_INLINE unsigned int get_height() const {
		return Height;
	}

	/// Gets the iterator type.
	ALWAYS_INLINE Type get_type() const {
		return type;
	}

	/// clips the current draw area to the given rect
	ALWAYS_INLINE void clip(unsigned int minx, unsigned int miny, unsigned int maxx, unsigned int maxy) {
		unsigned int tempw = maxx - minx, 
					 temph = maxy - miny;
		increment(minx, miny);
		if(Width > tempw) {
			Width = tempw;
		}
		if(Height > temph) {
			Height = temph;
		}
	}

	///
	ALWAYS_INLINE ImageIterator& operator += (unsigned int _val) {
		increment(_val);
		return *this;
	}

	///
	ALWAYS_INLINE ImageIterator operator ++ () {
		ImageIterator copy(*this);
		increment(1);
		return copy;
	}

	///
	ALWAYS_INLINE ImageIterator& operator ++ (int) {
		increment(1);
		return *this;
	}

	///
	ALWAYS_INLINE ImageIterator operator + (unsigned int _val) {
		ImageIterator copy(*this);
		copy.increment(_val);
		return copy;
	}

	///
	ALWAYS_INLINE bool operator < (ImageIterator &_val) {
		return ImagePtr < _val.ImagePtr;
	}

	///
	ALWAYS_INLINE bool operator <= (ImageIterator &_val) {
		return ImagePtr <= _val.ImagePtr;
	}

	///
	ALWAYS_INLINE bool operator == (ImageIterator &_val) {
		return ImagePtr == _val.ImagePtr;
	}

	///
	ALWAYS_INLINE bool operator != (ImageIterator &_val) {
		return ImagePtr != _val.ImagePtr;
	}

	///
	ALWAYS_INLINE bool operator >= (ImageIterator &_val) {
		return ImagePtr >= _val.ImagePtr;
	}

	///
	ALWAYS_INLINE bool operator > (ImageIterator &_val) {
		return ImagePtr > _val.ImagePtr;
	}

	class reference {
	public:
		ALWAYS_INLINE reference(ImageIterator* _img_iter) {
			iter = _img_iter;
		}

		ALWAYS_INLINE reference& operator = (const reference& _val) {
			#ifdef KSD_DEBUG_IMAGE_ITERATOR
			if( _val.iter->bpp != iter->bpp ) {
				std::strstream s;
				s << "Cannot copy a pixel from an image with bytes-per-pixel (bpp) of "
				  << _val.iter->bpp << " to an image with a bpp of " << iter->bpp << ".";
				throw new EInvalidInput(__FILE__, s.str());
			}

			// we do this to make sure that all the write functionality happens
			Uint32 temp;
			memcpy(&temp, _val.iter->ImagePtr, _val.iter->bpp);
			iter->set(temp);
			#else
			// HACK: I know this replicates code, but it increase speed a lot.
			if(iter->type == READ_WRITE)
				memcpy(iter->ImagePtr, _val.iter->ImagePtr, _val.iter->bpp);
			#endif

			return *this;
		}

		template<class T>
		ALWAYS_INLINE reference& operator = (const T& _val) {
			iter->set(_val);
			return *this;
		}

		ALWAYS_INLINE operator Uint32() const {
			Uint32 temp;
			memcpy(&temp, iter->ImagePtr, iter->bpp);
			return temp;
		}
	private:
		ImageIterator* iter;
	};
	friend class reference;

	///
	ALWAYS_INLINE reference operator * () {
		return reference(this);
	}
protected:
	Uint8* ImagePtr;
	unsigned int Width, Pitch, Height;
	Uint8 bpp;
	Type type;
	
	#ifdef KSD_DEBUG_IMAGE_ITERATOR
	unsigned int CurX, CurY;
	#endif
};
//---------------------------------------------------------------------------
}; // namespace ksd
#undef ALWAYS_INLINE
//---------------------------------------------------------------------------
#endif



//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//	
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		TTextBox
//	
//	Purpose:		Not a widget!! Used by all widgets that deal with text, like:
//					TLabel, TRichLabel, TEdit, TMemo, TRichMemo.
//	
//	Checked-in by:	$Author$
//					$Date$
//--------------------------------------------------------------------------- 
#ifndef _KSD_TextBoxH_
#define _KSD_TextBoxH_
//--------------------------------------------------------------------------- 
#include <sigc++/class_slot.h>
#include <string>
#include <list>
#include <vector>
#include <algorithm>
#include "Font.h"
#include "Canvas.h"
#include "Keyboard.h"
#include "Rect.h"
#include "export_ksd.h"
//---------------------------------------------------------------------------
#ifdef __GNUG__
#pragma interface
#endif
//---------------------------------------------------------------------------
namespace ksd {
//---------------------------------------------------------------------------
// TTextBox as of libksd 0.0.3:
//
// For the record: this is the hardest individual unit I have ever had to
// build.  While its functionality is greatly expanded, some of the things I
// did in the external interface aren't very elegant.  In all, I would say
// that TTextBox is "averagely o.k."
//---------------------------------------------------------------------------
// NOTES:  Positions passed to TTextBox include the ActiveText.  Positions used
// internally don't refer to the ActiveText, this includes:  Line::Pos, Line::Len,
// and TextInfo::Pos.
//---------------------------------------------------------------------------
// TODO: Add a visible lines variable or some kind of height because we don't
// have to update lines that aren't seen.  So we can run Recalc from the changed
// line to the last visible line.  When the line becomes visible run recalc on that
// one but not the ones below it.
//---------------------------------------------------------------------------
KSD_EXPORT_CLASS class TTextBox {
public:
	TTextBox();
	~TTextBox() {
		Clear();
	}

	struct TextAttr {
		TextAttr() { 
			Font = NULL;
		}

		TextAttr(TFont* _font, const TFontAttr& _attr) {
			Font = _font;
			FontAttr = _attr;
		}

		bool IsValid() const {
			return Font != NULL;
		}

		bool operator == (const TextAttr& attr) const {
			if(Font == attr.Font) {
				if(FontAttr == attr.FontAttr) {
					return true;
				}
			}
			return false;
		}

		bool operator != (const TextAttr& attr) const {
			if(Font == attr.Font) {
				if(FontAttr == attr.FontAttr) {
					return false;
				}
			}
			return true;
		}
	
		TFont* Font;
		TFontAttr FontAttr;
	};
	
	enum WrapType { 
		WordWrap, CharWrap, NoWrap
	};

	// OnChange callback
	SigC::Slot0<void> OnChange;

	// accesors 
	unsigned int	GetLineCount() const				{ return LineList.size(); }
	unsigned int	GetTextInfoCount() const			{ return TextInfoList.size(); }
	const char*		GetText()							{ return Text.c_str(); }
	Uint16			GetWidth() const					{ return Width; }
	unsigned int	GetHeight() const; // a hack to get the text box height, TODO: elga-fy
	unsigned int	GetLength() const					{ return Text.size(); }

	// attr and line gets
	std::string		GetLine(unsigned int index)					{ return Text.substr(LineList[index].Pos, LineList[index].Len); }
	int				GetLineAscent(unsigned int index) const		{ return LineList[index].Ascent; }
	int				GetLineDescent(unsigned int index) const 	{ return LineList[index].Descent; }
	int				GetLineSkip(unsigned int index) const		{ return LineList[index].LineSkip; }
	int				GetLineHeight(unsigned int index) const		{ return GetLineAscent(index) + (-GetLineDescent(index)); }
	unsigned int	GetLineStart(unsigned int index) const		{ return LineList[index].Pos; }
	unsigned int	GetLineLength(unsigned int index) const  	{ return LineList[index].Len; }
	unsigned int	GetLineEnd(unsigned int index) const		{ return GetLineStart(index) + GetLineLength(index); }
	bool			GetLineExplicit(unsigned int index) const 	{ return IsExplicitLine(index); }

	// attr/data sets/gets
	void					SetText(const std::string& new_text, const TextAttr& attr);
	void 					SetWidth(Uint16 _width);
	void 					SetWrapType(WrapType _wt);
	void 	 				SetTextAttr(const TextAttr& text_attr, unsigned int Pos, unsigned int Len);
	void					SetTextAttr(const TextAttr& text_attr, unsigned int Pos = 0);
	void					SetActiveAttr(const TextAttr& text_attr, unsigned int Pos);
	void					AbondonActiveAttr();
	void					RemoveTextAttr(unsigned int Pos);
	void					RemoveTextAttrs(unsigned int Start, unsigned int Len);
	bool					IsValid() const						{ return TextInfoList.size() > 0; }
	Uint16					GetWidth() 							{ return Width; }
	WrapType				GetWrapType() 						{ return wrap_type; }
	TextAttr& 				GetTextAttr(unsigned int CharPos);//	{ LineList[GetCurrentLine(CharPos)].Clear(); return (*GetCurrentInfo(CharPos)).Attr; }
	TextAttr*				GetUniqueTextAttr(unsigned int Pos);
	std::vector<TextAttr*>	GetUniqueTextAttrs(unsigned int Start, unsigned int Len);

	// position functions
	unsigned int	GetCharPos(int X, int Y); // returns the char pos
	unsigned int 	GetScreenPos(unsigned int Pos, int& X, int& Y); // returns the line number
	unsigned int 	GetCurrentLine(unsigned int Pos);

	// text funcs
	void			Clear();
	const char* 	GetText(int start, int len);
	void			InsertText(const std::string& _text, unsigned int Pos);
	void			RemoveText(unsigned int start, unsigned int len);
	void			InsertLine(unsigned int Pos); // Pos = character pos

	// drawing functions
	// TODO: totally change the draw API for speed, refactoring, and less reduncance
	void 			Draw(TCanvas* Canvas, int X, int Y, int start_line = 0);
	void 			DrawLine(TCanvas* Canvas, int X, int Y, unsigned int line);

	// used when you change some text attrs, to update line heights and text wrap
	void UpdateText(unsigned int Pos, unsigned int Len = 0);
protected:
	struct Line {
		Line() {
			Pos = Len = 0;
			Clear();
		}

		void Clear() {
			Ascent = Descent = LineSkip = 0;
		}

		void FactorAttr(const TextAttr& attr) {
			Ascent = std::max(attr.Font->GetAscent(attr.FontAttr), Ascent);
			Descent = std::max(attr.Font->GetDescent(attr.FontAttr), Descent);
			LineSkip = std::max(attr.Font->GetLineSkip(attr.FontAttr), LineSkip);
		}
	
		unsigned int Pos, Len;
		int Ascent, Descent, LineSkip;
		//Uint16 X; // matters for right-aligned and centered text
		//TAlignment Align;
	};

	struct TextInfo {
		TextInfo() {
			Pos = 0;
		}
	
		unsigned int Pos;
		TextAttr Attr;
	};
	typedef std::list<TextInfo>::iterator TextInfoIter;

	class TextChunk {
	public:
		TextChunk(TTextBox* _parent) { 
			Parent = _parent;
			Start = End = Line = 0;
		}

		TextChunk(TTextBox* _parent, unsigned int _line) {
			Parent = _parent;
			GetFirst(_line);
		}

		// for chunk positioning
		void 			GetFirst(unsigned int line);
		void 			GetNext();
		void 			GetPrevious();
		int  			GetWidth(unsigned int offset = 0);
		unsigned int 	GetLength() { return End - Start; }

		// get string
		std::string GetString(unsigned int offset = 0) { 
			std::string temp(Parent->Text.c_str() + Start, GetLength() - offset); 

			// TODO: just check the front and end characters ... (why not?)
			// eliminate new line characters
			for(unsigned int i = 0; i < temp.size(); i++) {
				if(temp[i] == '\n') {
					//temp.erase(i, 1);
					temp[i] = ' ';
					break;
				}
			}

			return temp;
		}

		// draw chunk
		void Draw(TCanvas* Canvas, int X, int Y);

		// shrink chunk size
		void Cut(unsigned int amount);

		// operators
		inline void operator ++ (int) { GetNext(); }
		inline void operator -- (int) { GetPrevious(); }
	
		unsigned int Start, End;
		TextInfoIter info_iter;
	private:
		unsigned int Line;
		TTextBox* Parent;
	}; 
	friend class TextChunk;
private:
	std::string Text;
	Uint16 Width;
	WrapType wrap_type;
	std::vector<Line> LineList;
	std::list<TextInfo> TextInfoList;
	unsigned int ActivePos;
	TextAttr ActiveAttr;
	bool UseActiveAttr;

	// show internal diagnostic information
	void PrintDiag();

	// TODO:  Find a way to do all lines without calling GetFirstChunk to get
	// info_iter at the begginng of every line.  Also allow us to specify a end line
	// so that we don't update invisible lines!!
	void Recalc(unsigned int line = 0);
	void RecalcAll(unsigned int start_line = 0, unsigned int end_line = 0);
	bool Update(unsigned int line);

	// line attr functions
	void UpdateLineAttr(unsigned int line); // used to update line height values
	void PullLine(unsigned int line); // pulls wrapped text onto main line
	void ResetLineData(unsigned int start_line = 0); // re-wraps and updates line attrs

	// find current info
	TextInfoIter GetCurrentInfo(unsigned int Pos);

	// performs actual wrapping operation
	unsigned int DoWrap(TextChunk& chunk);

	// --
	// little utility inline functions:
	// --

	// Checks to see if this is an explicit line, not a wrapped line
	inline bool IsExplicitLine(unsigned int line) const {
		return Text[LineList[line].Pos + LineList[line].Len - 1] == '\n';
	}

	// gets a text infos length
	inline unsigned int GetTextInfoLength(const TextInfoIter iter) {
		unsigned int next;
		TextInfoIter next_iter;

		// set next iter
		next_iter = iter;
		next_iter++;

		if(next_iter == TextInfoList.end()) {
			next = GetLength();
		} else {
			next = (*next_iter).Pos;
		}

		return next - (*iter).Pos;
	}
};
//---------------------------------------------------------------------------  
}; // namespace ksd
//---------------------------------------------------------------------------  
#endif


//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//	
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		TSystemTimerBase, TSystemTimer, TBlinkTimer, TDeltaTimer
//	
//	Purpose:		A system timer, blinking timer and a difference timer.  When
//					KSD_USE_FAST_TIMERS is defined, the hook to keep track of
//					the current interval is removed which should slightly improve
//					speed but we lose some functionality.
//	
//	Checked-in by:	$Author$
//					$Date$
//--------------------------------------------------------------------------- 
#ifndef _KSD_TimerH_
#define _KSD_TimerH_
//--------------------------------------------------------------------------- 
#include <SDL/SDL_timer.h>
#include <sigc++/sigc++.h>
#include "export_ksd.h"

#ifndef NULL
// I am including this file in order to define NULL.  If someone
// finds a platform where doing this is unportable, PLEASE let me know.
#include <stddef.h>
#ifndef NULL
#error We have no NULL!!  Please see the file "Timer.h" and e-mail me, ASAP!!
#endif
#endif
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma interface
#endif
//--------------------------------------------------------------------------- 
namespace ksd {
//---------------------------------------------------------------------------
KSD_EXPORT_CLASS class TSystemTimerBase {
public:
	typedef Uint32 (*TimerCallback)(Uint32, void*);

	TSystemTimerBase(TimerCallback _callback, Uint32 _val = 0);
	virtual ~TSystemTimerBase();

	// get/set timer interval
	void 	SetInterval(Uint32);
	Uint32 	GetInterval() { 
		#ifndef KSD_USE_FAST_TIMERS
		return Interval; 
		#else
		return 0;
		#endif
	}

	// start/stop/check-state
	// NOTE: normally when you re-start a timer, it starts at what ever the 
	// interval was last set to BUT when KSD_USE_FAST_TIMERS is set, the initial
	// interval is used..
	void Start();
	void Stop();
	bool IsRunning() { return id != NULL; }
private:
	SDL_TimerID id;
	TimerCallback DoTimer;
	Uint32 Interval;

	#ifndef KSD_USE_FAST_TIMERS
	// this is a timer hook that is used to update the timer interval
	static Uint32 CallbackHook(Uint32, void*);
	#endif
};
//---------------------------------------------------------------------------  
KSD_EXPORT_CLASS class TSystemTimer : public TSystemTimerBase {
public:
	TSystemTimer(Uint32 _val = 0, bool _fixed = false) 
				: TSystemTimerBase(&TSystemTimer::TimerEvent, _val) {
		FixedInterval = _fixed;
	}

	// timer event
	SigC::Signal1<Uint32, Uint32> OnTimer;

	bool IsFixed() { return FixedInterval; }

	static Uint32 GetTicks() { return SDL_GetTicks(); }
	static void Delay(Uint32 ms) { SDL_Delay(ms); }
private:
	bool FixedInterval;

	static Uint32 TimerEvent(Uint32 _val, void* tptr) {
		TSystemTimer* Timer = (TSystemTimer*)tptr;

		// specific for fixed intervals
		if(Timer->IsFixed()) {
			Timer->OnTimer(_val);
			return _val;
		} 

		return Timer->OnTimer(_val);
	}
};
//---------------------------------------------------------------------------  
KSD_EXPORT_CLASS class TBlinkTimer : public TSystemTimerBase {
public:
	TBlinkTimer(Uint32 _on_time, Uint32 _off_time) 
				: TSystemTimerBase(&TBlinkTimer::DoBlink, _on_time) {
		OnTime = _on_time;
		OffTime = _off_time;
		State = true;
	}

	// events for on/off (show/hide)
	SigC::Signal0<void> OnShow;
	SigC::Signal0<void> OnHide;
private:
	static Uint32 DoBlink(Uint32, void* tptr) {
		TBlinkTimer* Timer = (TBlinkTimer*)tptr;
	
		// toggle state
		Timer->State = !Timer->State;

		// do state specific stuff
		if(Timer->State) {
			Timer->OnShow();
			return Timer->OnTime;
		} else {
			Timer->OnHide();
			return Timer->OffTime;
		}
	}
private:
	Uint32 OnTime, OffTime;
	bool State;
};
//--------------------------------------------------------------------------- 
KSD_EXPORT_CLASS class TDeltaTimer {
public:
	TDeltaTimer() {
		Last = Current = Delta = 0;
		Scale = 1.0F;
	}

	Uint32 GetDelta() { return Delta; }
	float  GetScale() { return Scale; }

	void SetScale(float _scale) { Scale = _scale; }
	void Reset()  { Last = Current = SDL_GetTicks(); Delta = 0; }
	void Freeze() {
		Current = SDL_GetTicks();
		Delta = Uint32((Current - Last) * Scale);
		if(Delta != 0) {
			Last = Current;
		}
	}
private:
	Uint32 Last, Current, Delta;
	float Scale;
};
//---------------------------------------------------------------------------  
// Global timers -- used for cursor blinking and that kind of stuff.
// They are created on demand so that there are no useless threads!!
KSD_EXPORT TSystemTimer* GetTimer(unsigned int interval);
KSD_EXPORT TBlinkTimer*  GetBlinkTimer(unsigned int on, unsigned int off);
//--------------------------------------------------------------------------- 
}; // namespace ksd
//--------------------------------------------------------------------------- 
#endif 


//--------------------------------------------------------------------------- 
//	libksd -- KewL STuFf DirectMedium
//	Copyright 2000-2002 David Snopek
//	
//	This library is free software; you can redistribute it and/or
//	modify it under the terms of the GNU Library General Public
//	License as published by the Free Software Foundation; either
//	version 2 of the License, or (at your option) any later version.
//  
//	This library is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//	Library General Public License for more details.
//  
//	You should have received a copy of the GNU Library General Public
//	License along with this library; if not, write to the
//	Free Software Foundation, Inc., 59 Temple Place - Suite 330,
//	Boston, MA  02111-1307, SA.
//		
//--------------------------------------------------------------------------- 
//	File:			$RCSfile$
//	
//	Author:			David Snopek
//	Revised by:		
//	Version:		$Revision$
//	
//	Objects:		Line
//	
//	Purpose:		Defines an object representing a line.
//	
//	Checked-in by:	$Author$
//					$Date$
//---------------------------------------------------------------------------
#ifndef _KSD_LineH
#define _KSD_LineH
//---------------------------------------------------------------------------
#include "Shape2D.h"
#include "Point2D.h"
#include "fixed.h"
#include "export_ksd.h"
//--------------------------------------------------------------------------- 
#ifdef __GNUG__
#pragma interface
#endif
//---------------------------------------------------------------------------
namespace ksd {
//---------------------------------------------------------------------------
/** A line shape object.  Used to describe edges in screen space.  This
 ** particular line implementation is good for collision detection but
 ** not much more.  It is help in slope-intercept format (y=mx+b) with 
 ** two limiting values to describe the start and end of the line segment.
 ** This is makes it a very slow operation to move the line.
 */
KSD_EXPORT_CLASS class TLine : public TShape2D {
public:
    ///
	TLine();
	/// Create a line with the given attributes.
	TLine(ksd::fixed slope, ksd::fixed yintercept, int start, int stop);
    /// This is the verticle line constructor.
	TLine(int xintercept, int top, int bottom); // verticle line constructor
    /// Create a line that connects the two given points
	TLine(const TPoint2D& point1, const TPoint2D& point2) {
		SetEndPoints(point1, point2);
	}

	/// Returns true id the line is verticle.
    bool IsVerticle() const { return Verticle; }
	/// Returns true if the line is horizontal
    bool IsHorizontal() const { return Slope == 0; }
	/// Returns the slopr of the line
    ksd::fixed GetSlope() const { return Slope; }
	/** Returns the y-intercept of the line, unless this is a verticle line, in
	 ** which case it returns the x-intercept.
	 */
    ksd::fixed GetIntercept() const { return Intercept; }
	/// Returns the value that the segment starts on.
    int GetStart() const { return Start; }
	/// Returns the value that the segment stops on,
    int GetStop() const { return Stop; }

	/// Allows you to change this to or from a verticle line
    void SetVerticle(bool _verticle) { Verticle = _verticle; Slope = 0; }
	/// Sets the line slope
    void SetSlope(ksd::fixed _slope) { if(!Verticle) Slope = _slope; }
	/// Sets the line intercept
    void SetIntercept(ksd::fixed _intercept) { Intercept = _intercept; }
	/// Sets the segment start
    void SetStart(int _start) { Start = _start; }
	/// Sets the segment stop
    void SetStop(int _stop) { Stop = _stop; }

	/// Creates a line using the given sttributes
    void CreateLine(ksd::fixed slope, ksd::fixed yintercept, int start, int stop);
	/// Creates a horizontal line
    void CreateHLine(int y, int start, int stop);
	/// Creates a verticle line
    void CreateVLine(int x, int start, int stop);

	/** Returns a line perpendicular to this line at the gvien point.  Sets
	 ** the Touches variable to true if the new line infact touches this line.
	 */
    TLine PerpendicularLine(const TPoint2D& start, bool& Touches) const;
    /// Returns the length of the line at a given point
	ksd::fixed Length();
	/// Returns the y-value for a given x-value (y=mx+b).
    ksd::fixed Solve(int Value);

	/// Creates a line using two endpoitns.
    void SetEndPoints(const TPoint2D& point1, const TPoint2D& point2);
    /// Returns the start point (Is slower than you may think).
	TPoint2D GetStartPoint();
	/// Returns the end point (Is slower than you may think).
    TPoint2D GetEndPoint();

    bool IsInside(const TPoint2D& V) const;
    void Move(int X, int Y);
    void MoveTo(int X, int Y);
	void Visit(Visitor* V) { V->VisitLine(this); }
protected:
    bool Verticle;			// set to true if line is verticle
    ksd::fixed Slope;		// slope of line (the m in: y = mx + b)
    ksd::fixed Intercept;	// y-intercept unless verticle, then x-intercept
    int Start, Stop;		// Start and stop points of line
};
//---------------------------------------------------------------------------
}; // namespace ksd
//---------------------------------------------------------------------------
#endif
 

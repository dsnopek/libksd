//--------------------------------------------------------------------------- 
#include <stdlib.h>
#include <cmath>
#include <iostream>
#include "ksd.h"
#include "log.h"
#include "fixed.h"

#ifdef WIN32
	#include <time.h>
#endif

using namespace ksd;
using namespace std;
//--------------------------------------------------------------------------- 
#define PARTICLE_COUNT			10
#define SCREEN_WIDTH			320
#define SCREEN_HEIGHT			200
#define SCREEN_BPP				16
#define FULL_SCREEN				false
#define PARTICLE_VARIATION		2
#define PARTICLE_MAXSPEED		1.0
#define EXPLOSION_DAMPER		.1666
#define GRAVITY					.01
#define FADE_RATE				.01
#define FADE_THREAD				false
//--------------------------------------------------------------------------- 
class ParticleDemo : public TApplication {
public:
	void Init();
	void Shutdown();

	void FireExplosion(int X = -1, int Y = -1);
	
	struct Particle {
		Particle() {
			Intensity = 0;
		}

		ksd::fixed X, Y, VectX, VectY;
		int Intensity; // goes from 0 to 1024
	};
protected:
	void KeyDown(const TKey& key);
	void MouseDown(int X, int Y, TMouseButton Button);

	void MoveParticles();
	void DrawParticle(int X, int Y, int inten, TCanvas*);
	TColor GetParticleColor(int intesity);
	//void BlurScreen();

	bool Draw(TCanvas*, DrawType dt);
	void Cycle();

	TImage Buffer;
	TBrightnessFilter FadeFilter;
	TMutex ParticleMutex;
	TDeltaTimer FrameTimer, FadeTimer;
private:
	Particle List[PARTICLE_COUNT];
};

KSD_MAIN(ParticleDemo);
//--------------------------------------------------------------------------- 
void ParticleDemo::Init()
{
	SetLogOptions(logger::LOG_TO_EXPLICIT_PATH | logger::LOG_TO_STANDARD_OUTPUT
				  | logger::SHOW_NONE); // | log::SHOW_MODULE_NAME
	SetLogLevel(5); 
	
	try {
		CreateScreen(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_BPP, FULL_SCREEN ? TScreen::FULLSCREEN : TScreen::DEFAULT);
	} catch (ksd::exception e) {
		e.report();
		Quit(1);
	}

	srand(time(NULL));

	SetPaintType(FullUpdate);
	SetCaption("Particle");
	//SetDecoupled(true);

	// make the back buffer
	Buffer.Create(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_BPP);

	// we will fade using a darkness (inverse-brightness) filter
	FadeFilter.Brightness = -(10 * FADE_RATE);
	FrameTimer.SetScale(EXPLOSION_DAMPER); // purposely slow the explosion
	FadeTimer.SetScale(EXPLOSION_DAMPER); // slow fade as much as explosion

	cout << "\nWelcome to the libksd particle demo!!\n";
	cout << "Press 'k' to generate a randomly located explosion OR\n";
	cout << "Click on the location you wish the explosion to start\n";
	cout << "NOTE: for optimal performance, close all other processes\n" << flush;
}
//--------------------------------------------------------------------------- 
void ParticleDemo::Shutdown()
{
	//
}
//--------------------------------------------------------------------------- 
void ParticleDemo::FireExplosion(int _X, int _Y)
{
	ksd::fixed X, Y;
	float c;

	ParticleMutex.Lock();

	if(_X == -1 || _Y == -1) {
		// randomly pick start place
		X = (int)(SCREEN_WIDTH * (rand() / (float)RAND_MAX));
		Y = (int)(SCREEN_HEIGHT * (rand() / (float)RAND_MAX));
	} else {
		X = _X;
		Y = _Y;
	}

	for(int i = 0; i < PARTICLE_COUNT; i++) {
		List[i].X = X;
		List[i].Y = Y;
		List[i].Intensity = 1024;

		// randomly assign vectors
		List[i].VectX = ((PARTICLE_VARIATION * 2 * (rand() / (float)RAND_MAX)) - PARTICLE_VARIATION);
		List[i].VectY = ((PARTICLE_VARIATION * 2 * (rand() / (float)RAND_MAX)) - PARTICLE_VARIATION);
		c = sqrt((List[i].VectX * List[i].VectX).ToFloat() + (List[i].VectY * List[i].VectY).ToFloat());
		if(c != 0) {
			List[i].VectX /= c;
			List[i].VectY /= c;
		}

		// get particle speed
		c = PARTICLE_MAXSPEED * (rand() / (float)RAND_MAX);
		//c = PARTICLE_MAXSPEED;
		List[i].VectX *= c;
		List[i].VectY *= c;
	}

	FrameTimer.Reset();

	ParticleMutex.Unlock();
}
//--------------------------------------------------------------------------- 
bool ParticleDemo::Draw(TCanvas* Canvas, DrawType dt)
{
	TCanvas buffer_canvas(&Buffer);

	ParticleMutex.Lock();

	// draw particles
	for(int i = 0; i < PARTICLE_COUNT; i++) {
		if(List[i].Intensity == 0) continue;
		
		DrawParticle(List[i].X.RoundInt(), List[i].Y.RoundInt(), List[i].Intensity, &buffer_canvas);
	}

	ParticleMutex.Unlock();

	// find fade rate
	FadeTimer.Freeze();
	FadeFilter.Brightness = -(FadeTimer.GetDelta() * FADE_RATE);

	FadeFilter.Process(Buffer);

	Canvas->Blit(0, 0, Buffer);

	return true;
}
//--------------------------------------------------------------------------- 
void ParticleDemo::Cycle()
{
	// move particles for next frame
	MoveParticles();
}
//--------------------------------------------------------------------------- 
void ParticleDemo::KeyDown(const TKey& key)
{
	switch(key.GetKeyCode()) {
		case SDLK_k:
			FireExplosion();
			break;
		case SDLK_ESCAPE:
			Quit(0);
			break;
	}	
}
//--------------------------------------------------------------------------- 
void ParticleDemo::MouseDown(int X, int Y, TMouseButton Button) 
{
	FireExplosion(X, Y);
}
//--------------------------------------------------------------------------- 
void ParticleDemo::MoveParticles()
{
	ParticleMutex.Lock();

	FrameTimer.Freeze();
	if(FrameTimer.GetDelta() == 0) {
		ParticleMutex.Unlock();
		return;
	}
	
	for(int i = 0; i < PARTICLE_COUNT; i++) {
		if(List[i].Intensity == 0) continue; // don;t mess arround with non existant particles

		// move particles
		/*List[i].X += List[i].VectX * FrameTimer.GetDelta();
		List[i].Y +=  List[i].VectY * FrameTimer.GetDelta();

		// add gravity
		List[i].VectY += GRAVITY * FrameTimer.GetDelta();*/

		// is there a difference??
		for(unsigned int e = 0; e < FrameTimer.GetDelta(); e++) {
			List[i].X += List[i].VectX;
			List[i].Y +=  List[i].VectY;
			List[i].VectY += GRAVITY;
		}

		// decrease intensity
		if(List[i].Intensity > 0) List[i].Intensity -= FrameTimer.GetDelta();

		// collision detect =
		// 		on collision with boundaries, move to the boudary and then
		// 	reverse the movement vector...
		if(List[i].X < 0) {
			List[i].X = 0;
			List[i].VectX = -List[i].VectX;
		} else if(List[i].X > SCREEN_WIDTH) {
			List[i].X = SCREEN_WIDTH;
			List[i].VectX = -List[i].VectX;
		}
		if(List[i].Y < 0) { 
			List[i].Y = 0;
			List[i].VectY = -List[i].VectY;
		} else if(List[i].Y > SCREEN_HEIGHT) { // make stop on the bottom
			List[i].Y = SCREEN_HEIGHT;
			//List[i].VectY = -List[i].VectY;
			List[i].VectY = 0;
			List[i].VectX = 0;
		}
	}

	ParticleMutex.Unlock();
}
//--------------------------------------------------------------------------- 
void ParticleDemo::DrawParticle(int X, int Y, int inten, TCanvas* Canvas)
{
	TColor color = GetParticleColor(inten);

	Canvas->Pen.Color = color;
	Canvas->Brush.Color = color;

	// NOTE: I do not use FillCircle here because of the added precision
	// we can get over the particle's size when using an ellipse
	if(inten < 832 && inten > 768) {
		// small
		Canvas->FillEllipse(X, Y, 2, 2);
	} else if(inten < 896) {
		// medium
		Canvas->FillEllipse(X, Y, 3, 3);
	} else if(inten < 960) {
		// medium-large
		Canvas->FillEllipse(X, Y, 4, 4);
	} else if(inten < 1024) {
		// large
		Canvas->FillEllipse(X, Y, 5, 5);
	}
}
//--------------------------------------------------------------------------- 
TColor ParticleDemo::GetParticleColor(int intensity)
{
	TColor color;
	color.Red = min((intensity >> 2) + 16, 255); // purposely make brighter
	return color;
}
//--------------------------------------------------------------------------- 

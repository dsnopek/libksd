//--------------------------------------------------------------------------- 
#include <iostream>
#include "ksd.h"
#include "Plugin.h"
#include "gui/Panel.h"
#include "gui/Button.h"

using namespace ksd;
using namespace ksd_gui;
using namespace std;
//--------------------------------------------------------------------------- 
class OpenGLDemo : public TApplication {
public:
	OpenGLDemo() {
		Shaded = false;
	}

	void Init();
	void Shutdown();
protected:
	void OnButton1Press(TButton*);
	void OnButton2Press(TButton*);
	void OnButton3Press(TButton*);
	void OnButton4Press(TButton*);

	bool Draw(TCanvas* Canvas, DrawType dt);
private:
	TPanel* Panel;
	TButton** ButtonList;
	TFontList FontList;
	bool Shaded;
};

KSD_MAIN(OpenGLDemo);
//---------------------------------------------------------------------------  
static TColor ColorList[] = { 
	TColor::lightgrey,
	TColor::blue, 
	TColor::green, 
	TColor::red,
	//TColor::yellow // yellow doesn't work with current light/dark colors
};
static int ColorListSize = 4;
//---------------------------------------------------------------------------  
static char* BtnLabelList[] = {
	"Cube Shading",
	"Nothing",
	"Bevel",
	"Color"
};
static int ButtonCount = ColorListSize;
//---------------------------------------------------------------------------  
static float CubeColor[8][3] = {{ 1.0,  1.0,  0.0}, 
						    { 1.0,  0.0,  0.0},
						    { 0.0,  0.0,  0.0},
						    { 0.0,  1.0,  0.0},
						    { 0.0,  1.0,  1.0},
						    { 1.0,  1.0,  1.0},
						    { 1.0,  0.0,  1.0},
						    { 0.0,  0.0,  1.0}};
static float Cube[8][3] = {{ 0.5,  0.5, -0.5}, 
						   { 0.5, -0.5, -0.5},
						   {-0.5, -0.5, -0.5},
						   {-0.5,  0.5, -0.5},
						   {-0.5,  0.5,  0.5},
						   { 0.5,  0.5,  0.5},
						   { 0.5, -0.5,  0.5},
						   {-0.5, -0.5,  0.5}};
//--------------------------------------------------------------------------- 
void OpenGLDemo::Init()
{
	SetCaption("opengl");
	SetLogLevel(5);

	if(!IsBackendSupported("opengl")) {
		cout << "ERROR: libksd doesn't have OpenGL support compiled in.\n" << flush;
		Quit(1);
	}

	CreateScreen("opengl", 320, 200);

	try {
		FontList.LoadList("opengl.fonts.conf");
	} catch(EUnsupported& e) {
		// We don't care if there are no fonts! If nothing else, we'll 
		// just see a spinning cube and colored buttons. -- DRS
	}

	SetColor(TColor::white);
	SetFont(FontList.GetFont("Arial"));

	Panel = new TPanel(this);
	Panel->SetPosition(200, 10);
	Panel->Resize(110, 180);
	Panel->SetBevelType(TBevel::Raised);
	Panel->SetBackgroundColor(TColor::red);

	// TODO: Add an open gl view widget, at (5, 5) dim (155x120)

	// create button list
	typedef TButton* ButtonPtr;
	ButtonList = new ButtonPtr[ButtonCount];
	
	// add buttons to the screen
	int btn_Y = 5;
	for(int i = 0; i < ButtonCount; i++) {
		ButtonList[i] = new TButton(Panel);
		ButtonList[i]->SetPosition(10, btn_Y);
		ButtonList[i]->Resize(85, 25);
		ButtonList[i]->SetBackgroundColor(ColorList[i]);
		ButtonList[i]->SetCaption(BtnLabelList[i]);
		ButtonList[i]->SetTextSize(11);
		ButtonList[i]->SetBorderless(i == 3);
		
		btn_Y += 30;
	}

	// set individual button handlers
	ButtonList[0]->OnPress.connect(SigC::slot(*this, &OpenGLDemo::OnButton1Press));
	ButtonList[1]->OnPress.connect(SigC::slot(*this, &OpenGLDemo::OnButton2Press));
	ButtonList[2]->OnPress.connect(SigC::slot(*this, &OpenGLDemo::OnButton3Press));
	ButtonList[3]->OnPress.connect(SigC::slot(*this, &OpenGLDemo::OnButton4Press));

	#ifdef KSD_ENABLE_OPENGL
	// setup open gl stuff
	// 
	// lifted from testgl.c from SDL by Sam Latinga
	// 
	
	glViewport( 5, 5, 200, 200 );
	glMatrixMode( GL_PROJECTION );
	glLoadIdentity( );

	glOrtho( -2.0, 2.0, -2.0, 2.0, -20.0, 20.0 );

	glMatrixMode( GL_MODELVIEW );
	glLoadIdentity( );

	glEnable(GL_DEPTH_TEST);

	glDepthFunc(GL_LESS);

	glShadeModel(GL_SMOOTH);
	#endif
}
//--------------------------------------------------------------------------- 
void OpenGLDemo::Shutdown()
{
	FontList.Clear();
}
//---------------------------------------------------------------------------  
void OpenGLDemo::OnButton1Press(TButton*)
{
	// change sube shading
	Shaded = !Shaded;
}
//---------------------------------------------------------------------------  
void OpenGLDemo::OnButton2Press(TButton*)
{
	// try to really screw up the widgets !!
	//Panel->SetDoubleBuffered(!Panel->GetDoubleBuffered());
}
//---------------------------------------------------------------------------  
void OpenGLDemo::OnButton3Press(TButton*)
{
	// change bevel type
	switch(Panel->GetBevelType()) {
		// NOTE: in the default theme thick is the default
		case TBevel::Default:
		case TBevel::Thick:
			Panel->SetBevelType(TBevel::Thin);
			break;
		case TBevel::Thin:
			Panel->SetBevelType(TBevel::Lowered);
			break;
		case TBevel::Lowered:
			Panel->SetBevelType(TBevel::Raised);
			break;
		case TBevel::Raised:
			Panel->SetBevelType(TBevel::None);
			break;
		case TBevel::None:
			Panel->SetBevelType(TBevel::Thick);
			break;
	}
}
//---------------------------------------------------------------------------  
void OpenGLDemo::OnButton4Press(TButton* Button)
{
	static int CurrentColor = ColorListSize;

	// get next color
	CurrentColor++;
	if(CurrentColor >= ColorListSize)
		CurrentColor = 0;
		
	Button->SetBackgroundColor(ColorList[CurrentColor]);
}
//---------------------------------------------------------------------------  
bool OpenGLDemo::Draw(TCanvas* Canvas, DrawType dt)
{
	#ifdef KSD_ENABLE_OPENGL
	
	// Draw a spinning cube
	// 
	// Lifted from testgl.c from SDL by Sam Latinga
	// 

	GLenum gl_error;

	/* Do our drawing, too. */
	glClearColor( 0.0, 0.0, 0.0, 1.0 );
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glBegin( GL_QUADS );

	if(Shaded) {
		glColor3fv(CubeColor[0]);
		glVertex3fv(Cube[0]);
		glColor3fv(CubeColor[1]);
		glVertex3fv(Cube[1]);
		glColor3fv(CubeColor[2]);
		glVertex3fv(Cube[2]);
		glColor3fv(CubeColor[3]);
		glVertex3fv(Cube[3]);
		
		glColor3fv(CubeColor[3]);
		glVertex3fv(Cube[3]);
		glColor3fv(CubeColor[4]);
		glVertex3fv(Cube[4]);
		glColor3fv(CubeColor[7]);
		glVertex3fv(Cube[7]);
		glColor3fv(CubeColor[2]);
		glVertex3fv(Cube[2]);
		
		glColor3fv(CubeColor[0]);
		glVertex3fv(Cube[0]);
		glColor3fv(CubeColor[5]);
		glVertex3fv(Cube[5]);
		glColor3fv(CubeColor[6]);
		glVertex3fv(Cube[6]);
		glColor3fv(CubeColor[1]);
		glVertex3fv(Cube[1]);
		
		glColor3fv(CubeColor[5]);
		glVertex3fv(Cube[5]);
		glColor3fv(CubeColor[4]);
		glVertex3fv(Cube[4]);
		glColor3fv(CubeColor[7]);
		glVertex3fv(Cube[7]);
		glColor3fv(CubeColor[6]);
		glVertex3fv(Cube[6]);

		glColor3fv(CubeColor[5]);
		glVertex3fv(Cube[5]);
		glColor3fv(CubeColor[0]);
		glVertex3fv(Cube[0]);
		glColor3fv(CubeColor[3]);
		glVertex3fv(Cube[3]);
		glColor3fv(CubeColor[4]);
		glVertex3fv(Cube[4]);

		glColor3fv(CubeColor[6]);
		glVertex3fv(Cube[6]);
		glColor3fv(CubeColor[1]);
		glVertex3fv(Cube[1]);
		glColor3fv(CubeColor[2]);
		glVertex3fv(Cube[2]);
		glColor3fv(CubeColor[7]);
		glVertex3fv(Cube[7]);
	} else {
		// same cube, no shading
		glColor3f(1.0, 0.0, 0.0);
		glVertex3fv(Cube[0]);
		glVertex3fv(Cube[1]);
		glVertex3fv(Cube[2]);
		glVertex3fv(Cube[3]);
		
		glColor3f(0.0, 1.0, 0.0);
		glVertex3fv(Cube[3]);
		glVertex3fv(Cube[4]);
		glVertex3fv(Cube[7]);
		glVertex3fv(Cube[2]);
		
		glColor3f(0.0, 0.0, 1.0);
		glVertex3fv(Cube[0]);
		glVertex3fv(Cube[5]);
		glVertex3fv(Cube[6]);
		glVertex3fv(Cube[1]);
		
		glColor3f(0.0, 1.0, 1.0);
		glVertex3fv(Cube[5]);
		glVertex3fv(Cube[4]);
		glVertex3fv(Cube[7]);
		glVertex3fv(Cube[6]);

		glColor3f(1.0, 1.0, 0.0);
		glVertex3fv(Cube[5]);
		glVertex3fv(Cube[0]);
		glVertex3fv(Cube[3]);
		glVertex3fv(Cube[4]);

		glColor3f(1.0, 0.0, 1.0);
		glVertex3fv(Cube[6]);
		glVertex3fv(Cube[1]);
		glVertex3fv(Cube[2]);
		glVertex3fv(Cube[7]);
	}

	glEnd( );
		
	glMatrixMode(GL_MODELVIEW);
	glRotatef(5.0, 1.0, 1.0, 1.0);

	/* Check for error conditions. */
	gl_error = glGetError( );

	if( gl_error != GL_NO_ERROR ) {
		LOG(1, "opengl: OpenGL error: %d\n", gl_error );
	}

	return true;

	#else
	return false;
	#endif
}
//--------------------------------------------------------------------------- 

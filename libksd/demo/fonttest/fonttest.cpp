//--------------------------------------------------------------------------- 
#include <iostream>
#include "ksd.h"
#include "Plugin.h"
#include "TextBox.h"

using namespace ksd;
using namespace std;
//---------------------------------------------------------------------------  
#if defined(KSD_USE_SFONT) && defined(KSD_USE_TTF)
// if we have both font types, then show off the bitmap fonts
static char* FontNames[] = { "Arial", "Arial_BM", "Special", "Special_BM" };
static const int FontNamesCount = 4;
#else
// otherwise use what is available
static char* FontNames[] = { "Arial", "Special" };
static const int FontNamesCount = 2;
#endif

static Uint8 FontSizes[] = { 12, 18, 24, 36, 50 };
static const int FontSizesCount = 5;
//--------------------------------------------------------------------------- 
class FonttestDemo : public TApplication {
public:
	void Init();
	void Shutdown();
protected:
	TTextBox TextBox;
	int CurFont, CurSize;
	TColor CurColor;
	TFontList FontList;

	bool Draw(TCanvas* Canvas, DrawType dt);
	void KeyDown(const TKey& key);
	void OnTextBoxChange();
};

KSD_MAIN(FonttestDemo);
//--------------------------------------------------------------------------- 
void FonttestDemo::Init()
{
	SetCaption("fonttest");
	//SetLogLevel(9);

	#ifndef KSD_NO_PLUGINS
	LoadPlugin("libimage");
	#endif

	CreateScreen(320, 200);
	SetPaintType(FullUpdate);
	SetBackgroundColor(TColor::white);

	FontList.LoadList("fonttest.fonts.conf");

	TextBox.OnChange = SigC::slot(*this, &FonttestDemo::OnTextBoxChange);
	TextBox.SetWidth(320);
	TextBox.SetWrapType(TTextBox::CharWrap);

	// insert initial text info
	TTextBox::TextAttr text_attr;
	text_attr.Font = FontList.GetFont(FontNames[0]);
	TextBox.SetTextAttr(text_attr);

	// set cur attr variables to the default
	CurFont = 0;
	CurSize = 0;
	CurColor = TColor::black;

	Keyboard.EnableUnicode(true);
	//GetSurface().GetCanvas()->ClipRect.Set(50, 20, 225, 175);

	// display help message
	cout << "This is the fonttest demo!!\n";
	cout << "Here are the controls:\n\n";
	cout << "UP and DOWN arrows = change size\n";
	cout << "LEFT and RIGHT arrows = change font\n";
	cout << "F1 through F9 = change the color\n\n";
	cout << flush;
}
//--------------------------------------------------------------------------- 
void FonttestDemo::Shutdown()
{
	FontList.Clear();
}
//--------------------------------------------------------------------------- 
bool FonttestDemo::Draw(TCanvas* Canvas, DrawType dt)
{
	if(dt == REPAINT) {
		TextBox.Draw(Canvas, 0, 0);
	}

	return false;
}
//---------------------------------------------------------------------------  
void FonttestDemo::KeyDown(const TKey& key)
{	
	if(key.GetKeyCode() == SDLK_ESCAPE) {
		// exit app on escape
		Quit(0);
	}
	else if(key.GetKeyCode() == SDLK_LEFT) {
		if(CurFont == 0) CurFont = FontNamesCount - 1;
		else CurFont--;
	} 
	else if(key.GetKeyCode() == SDLK_RIGHT) {
		if(CurFont == FontNamesCount - 1)
			CurFont = 0;
		else CurFont++;
	} 
	else if(key.GetKeyCode() == SDLK_UP) {
		if(CurSize != FontSizesCount - 1)
			CurSize++;
	} 
	else if(key.GetKeyCode() == SDLK_DOWN) {
		if(CurSize != 0)
			CurSize--;
	} 
	else if(key.GetKeyCode() == SDLK_F1) CurColor = TColor::black;
	else if(key.GetKeyCode() == SDLK_F2) CurColor = TColor::red;
	else if(key.GetKeyCode() == SDLK_F3) CurColor = TColor::green;
	else if(key.GetKeyCode() == SDLK_F4) CurColor = TColor::blue;
	else if(key.GetKeyCode() == SDLK_F5) CurColor = TColor::purple;
	else if(key.GetKeyCode() == SDLK_F6) CurColor = TColor::brown;
	else if(key.GetKeyCode() == SDLK_F7) CurColor = TColor::yellow;
	else if(key.GetKeyCode() == SDLK_F8) CurColor = TColor::grey;
	else if(key.GetKeyCode() == SDLK_F9) CurColor = TColor::orange;
	else {
		if(key.GetKeyCode() == SDLK_BACKSPACE) {
			if(TextBox.GetLength() > 0) {
				TextBox.RemoveText(TextBox.GetLength() - 1, 1);
			}
		} else if(key.GetKeyCode() == SDLK_RETURN) {
			TextBox.InsertLine(TextBox.GetLength());
		} else if(key.GetUnicode() != 0) {
			TextBox.InsertText(string(1, (char)key.GetUnicode()), TextBox.GetLength());
		}
		
		return;
	}

	TTextBox::TextAttr text_attr;
	text_attr.Font = FontList.GetFont(FontNames[CurFont]);
	text_attr.FontAttr.Size = FontSizes[CurSize];
	text_attr.FontAttr.Color = CurColor;
	TextBox.SetActiveAttr(text_attr, TextBox.GetLength());

	cout << "\nTextInfo change has been inserted at " << TextBox.GetLength()
		 << ": \n";
	cout << "Size = " << (unsigned int)FontSizes[CurSize] << endl;
	cout << "Font = " << FontNames[CurFont] << endl;
	cout << "Color = (" << (unsigned int)CurColor.Red << "," 
						<< (unsigned int)CurColor.Green << ","
						<< (unsigned int)CurColor.Blue << ")\n\n";
	cout << flush;
}
//---------------------------------------------------------------------------  
void FonttestDemo::OnTextBoxChange()
{
	NeedsRepaint();
}
//--------------------------------------------------------------------------- 

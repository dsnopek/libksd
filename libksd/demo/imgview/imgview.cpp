//--------------------------------------------------------------------------- 
#include <iostream>
#include <iomanip>
#include "ksd.h"
#include "Plugin.h"
//#include "CommandLine.h"

using namespace ksd;
using namespace std;
//--------------------------------------------------------------------------- 
class ImageViewDemo : public TApplication {
public:
	void Init();
protected:
	TImage img;

	bool Draw(TCanvas* Canvas, DrawType dt);
};

KSD_MAIN(ImageViewDemo);
//--------------------------------------------------------------------------- 
void ImageViewDemo::Init()
{
	SetCaption("imgview");
	SetLogLevel(8);
	
	#ifndef KSD_NO_PLUGINS
	LoadPlugin("libimage");
	#endif

	CreateScreen(320, 200);
	SetPaintType(FullUpdate);
	
	// set-up command line parser
	/*CommandLine.AddOption("H", 	"hide-infobar");
	CommandLine.AddOption("h",	"help");
	CommandLine.AddOption("", 	"version");
	CommandLine.Parse();

	// check for output-then-quit options
	if(CommandLine.GetOption("help")->IsSet()) {
		cout << "USAGE:\n" 
		     << "./imgview <files-to-open>\n\n"
		     << "OPTIONS:\n";
		cout << "      " << "-H" << "         " << "--hide-infobar\n";
		cout << "      " << "  " << "         " << "--version\n";
		Exit(0); return;
	} 
	if(CommandLine.GetOption("version")->IsSet()) {
		cout << "imgview version " << VERSION << endl;
		Exit(0); return;
	}

	if(CommandLine.GetArgumentCount() == 0) {
		cerr << "You must specify an image to load!!\n";
		Exit(1); 
	} else {
		// TODO: use widgets to deal with multiple images
		error e;
		if(!(e = img.Load(CommandLine.GetArgument().c_str())).Success()) {
			// TODO: use error commandline show
			cerr << e.GetMessage() << endl;
			Exit(1); 
		} else {
			SetCaption("imgview - " + CommandLine.GetArgument()); 
		}
	}*/
	img.Load("../resrcs/joytest.png");
}
//--------------------------------------------------------------------------- 
bool ImageViewDemo::Draw(TCanvas* Canvas, DrawType dt)
{
	if(dt == REPAINT) {
		Canvas->Blit(0, 0, img);
		return true;
	}

	return false;
}
//--------------------------------------------------------------------------- 

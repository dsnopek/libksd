/*
 * This is a demo program from the libksd distribution.
 *
 * Redistriute as you like. This file is under the GNU GPL.
 *
 * Copyright (c) 2001 Arthur Peters
 *
 * $Header$
 */

#include <iostream>
#include <ksdconfig.h>

#ifndef KSD_ENABLE_AUDIO
extern "C" int main(int argc, char* argv[]) {
	std::cout << "Audio support was not compiled into libksd\n";
	return 1;
}
#else

#include <sstream>
#include <Application.h>
#include <gui/Button.h>
#include <gui/Panel.h>
#include <Mixer.h>
#include <Song.h>
#include <Sample.h>

#include <sigc++/slot.h>
#include <sigc++/bind.h>

using namespace std;
using namespace ksd;
using namespace ksd_gui;
using SigC::slot;
using SigC::bind;

class AudioMonitor : public TCustomWidget {
public:
	AudioMonitor(TWidget* _parent) : TCustomWidget(_parent) {
		_state = None;

		SetDrawOnDemand(true);
		SetAcceptInput(false);
		SetBackgroundColor(TColor::grey50);
	}		

	enum State {
		None, Playing, Paused, Stopped
	};

	void SetState(State _s) { 
		if(_state != _s) {
			_state = _s; 
			NeedsRepaint(); 
		}
	}
protected:
	bool Draw(TCanvas* Canvas, DrawType dt) {
		if(dt == REPAINT) {
			Canvas->Font = GetFont();
			Canvas->Pen.Color = TColor::snow;

			if(_state != None) {
				Canvas->DrawString(
					5, 5, string("Playing: ") + (_state == Playing ? "true" : "false"));
				Canvas->DrawString(
					5, 20, string("Paused: ") + (_state == Paused ? "true" : "false"));
				Canvas->DrawString(
					5, 35, string("Stopped: ") + (_state == Stopped ? "true" : "false"));
			} else {
				Canvas->DrawString(5, 5, "No object, yet.");
			}

			return true;
		}

		return false;
	}

	void Cycle() { }
private:
	State _state;
};


class SamplePanel : public TPanel
{
protected:
	TSample sample;
	auto_ptr<TSampleInstance> instance;

	int loops, volume, fade_time, stop_in;
	
	bool isWorking;

	AudioMonitor* monitor;
	
public:
	SamplePanel( TWidget* _parent, const std::string& _sample_file )
		: instance( new TSampleInstance ),
		  isWorking( true ),
		  loops(1), volume(128), fade_time(0), stop_in(-1),
		  TPanel( _parent ) {
		
		try {
			sample.Load( _sample_file.c_str() );
		} catch(ksd::exception e) {
			isWorking = false;	
			throw e;
		};
		
		SetBackgroundColor( TColor::blue );
		SetBevelType( TBevel::Lowered );
		SetCaption( _sample_file );
		
		Resize( 500, 60 );
		
		// setup controls
		TButton* button = new TButton(this);
		button->SetCaption("Play");
		button->SetPosition(0, 0);
		button->Resize(50, 25);
		button->SetBackgroundColor(TColor::green);
		button->SetTextSize(11);
		button->OnPress.connect( slot(this, &SamplePanel::Play) );
		
		button = new TButton(this);
		button->SetCaption("Stop");
		button->SetPosition(0, 30);
		button->Resize(50, 25);
		button->SetBackgroundColor(TColor::green);
		button->SetTextSize(11);
		button->OnPress.connect( slot(this, &SamplePanel::Stop) );

	 
		button = new TButton(this);
		button->SetCaption("Pause");
		button->SetPosition(55, 0);
		button->Resize(50, 25);
		button->SetBackgroundColor(TColor::green);
		button->SetTextSize(11);
		button->OnPress.connect( slot(this, &SamplePanel::Pause) );
	 
		button = new TButton(this);
		button->SetCaption("Resume");
		button->SetPosition(55, 30);
		button->Resize(50, 25);
		button->SetBackgroundColor(TColor::green);
		button->SetTextSize(11);
		button->OnPress.connect( slot(this, &SamplePanel::Resume) );

		// Params
		TButton* plus_button, * minus_button;
	 
		plus_button = new TButton(this);
		plus_button->SetCaption("+ Fade Time: 0");
		plus_button->SetPosition(110, 0);
		plus_button->Resize(90, 25);
		plus_button->SetBackgroundColor(TColor::green);
		plus_button->SetTextSize(11);
		plus_button->OnPress.connect( bind( bind(slot(this, &SamplePanel::AddTo), plus_button),
											&fade_time, 100 ) );
		minus_button = new TButton(this);
		minus_button->SetCaption("- Fade Time");
		minus_button->SetPosition(110, 30);
		minus_button->Resize(90, 25);
		minus_button->SetBackgroundColor(TColor::green);
		minus_button->SetTextSize(11);
		minus_button->OnPress.connect(
			bind( bind(slot(this, &SamplePanel::AddTo), plus_button),
				  &fade_time, -100 ) );
	 
		plus_button = new TButton(this);
		plus_button->SetCaption("+ Loops: 1");
		plus_button->SetPosition(205, 0);
		plus_button->Resize(70, 25);
		plus_button->SetBackgroundColor(TColor::green);
		plus_button->SetTextSize(11);
		plus_button->OnPress.connect( bind( bind(slot(this, &SamplePanel::AddTo), plus_button),
											&loops, 1 ) );
		minus_button = new TButton(this);
		minus_button->SetCaption("- Loops");
		minus_button->SetPosition(205, 30);
		minus_button->Resize(70, 25);
		minus_button->SetBackgroundColor(TColor::green);
		minus_button->SetTextSize(11);
		minus_button->OnPress.connect(
			bind( bind(slot(this, &SamplePanel::AddTo), plus_button),
				  &loops, -1 ) );
	 
		plus_button = new TButton(this);
		plus_button->SetCaption("+ Volume: 128");
		plus_button->SetPosition(280, 0);
		plus_button->Resize(80, 25);
		plus_button->SetBackgroundColor(TColor::green);
		plus_button->SetTextSize(11);
		plus_button->OnPress.connect( bind( bind(slot(this, &SamplePanel::AddTo), plus_button),
											&volume, 8 ) );
		minus_button = new TButton(this);
		minus_button->SetCaption("- Volume");
		minus_button->SetPosition(280, 30);
		minus_button->Resize(80, 25);
		minus_button->SetBackgroundColor(TColor::green);
		minus_button->SetTextSize(11);
		minus_button->OnPress.connect(
			bind( bind(slot(this, &SamplePanel::AddTo), plus_button),
				  &volume, -8 ) );

		// TODO: Add: instance volume changing, stop in.
		
		monitor = new AudioMonitor( this );
		monitor->SetPosition( 370, 0 );
		monitor->Resize( 90, 60 );
	}

protected:
	void Play( TButton* ) {
		if( isWorking ) {
			// Not very clean, but GCC 3.0 out soon.
			instance.reset(
				Mixer.StartSample( sample, loops, volume, fade_time, stop_in ).release() );
		}
	}
      
	void Stop( TButton* ) {
		if( stop_in < 0 ) {
			instance->Stop( fade_time );
		} else {
			instance->StopIn( stop_in );
		}
	}

	void Pause( TButton* ) {
		instance->Pause();
	}

	void Resume( TButton* ) {
		instance->Resume();
	}

	void ChangeVolume( TButton*, int change ) {
		instance->SetVolume(instance->GetVolume() + change);
	}

	void AddTo( TButton*, int* value, int change, TButton* caption_button ) {
		*value += change;

		std::string caption = caption_button->GetCaption();

		if( caption.rfind( ':' ) != string::npos )
			{
				std::string base_caption( caption, 0, caption.rfind( ':' ) );

				std::ostringstream build;
				build << base_caption << ": " << *value;

				caption_button->SetCaption( build.str() );
			}
	}

	void Cycle() {
		if(instance.get() == NULL) {
			monitor->SetState(AudioMonitor::None);
		} else {
			if(instance->IsPlaying()) {
				monitor->SetState(AudioMonitor::Playing);
			} else if(instance->IsPaused()) {
				monitor->SetState(AudioMonitor::Paused);
			} else if(instance->IsStopped()) {
				monitor->SetState(AudioMonitor::Stopped);
			}
		}
	}
};

class Audio : public TApplication
{
public:

	Audio::Audio() {
	}
      
	void Init() {
		SetLogOptions( logger::LOG_TO_EXPLICIT_PATH | logger::LOG_TO_STANDARD_OUTPUT
					   | logger::SHOW_LINE_NUMBER );
		SetLogLevel( 7 );
	 
		try {
			CreateScreen( 640, 480 );
			Mixer.Init( 44100, AUDIO_S16, 2, 4096 );
			song.Load( "../resrcs/song.mod" );
		} catch(ksd::exception e) {
			e.report();
			Quit(130);
		};
	 
		SetCaption("Audio Test");	

		FontList.LoadList("audio.fonts.conf");
		SetFont(FontList.GetFont("Arial"));

		// ---- Music Buttons
		TButton* button;
		TPanel* music_panel = new TPanel(this);

		music_panel->SetPosition( 5, 5 );
		music_panel->SetBackgroundColor( TColor::red );
		music_panel->SetBevelType( TBevel::Lowered );
		music_panel->Resize( 600, 60 );
	 
		button = new TButton(music_panel);
		button->SetCaption("Play Song");
		button->SetPosition(0, 0);
		button->Resize(85, 25);
		button->SetBackgroundColor(TColor::green);
		button->SetTextSize(11);
		button->OnPress.connect(bind( slot(this, &Audio::PlaySong), 0 ));
	 
		button = new TButton(music_panel);
		button->SetCaption("Fadein Song");
		button->SetPosition(0, 30);
		button->Resize(85, 25);
		button->SetBackgroundColor(TColor::green);
		button->SetTextSize(11);
		button->OnPress.connect(bind( slot(this, &Audio::PlaySong), 2000 ));

		button = new TButton(music_panel);
		button->SetCaption("Pause Song");
		button->SetPosition(90, 0);
		button->Resize(85, 25);
		button->SetBackgroundColor(TColor::green);
		button->SetTextSize(11);
		button->OnPress.connect( slot(this, &Audio::PauseSong) );

		button = new TButton(music_panel);
		button->SetCaption("Rewind Song");
		button->SetPosition(90, 30);
		button->Resize(85, 25);
		button->SetBackgroundColor(TColor::green);
		button->SetTextSize(11);
		button->OnPress.connect( slot(this, &Audio::RewindSong) );

		button = new TButton(music_panel);
		button->SetCaption("Resume Song");
		button->SetPosition(180, 0);
		button->Resize(85, 25);
		button->SetBackgroundColor(TColor::green);
		button->SetTextSize(11);
		button->OnPress.connect( slot(this, &Audio::ResumeSong) );

		button = new TButton(music_panel);
		button->SetCaption("Fadeout Song");
		button->SetPosition(270, 0);
		button->Resize(85, 25);
		button->SetBackgroundColor(TColor::green);
		button->SetTextSize(11);
		button->OnPress.connect(bind( slot(this, &Audio::StopSong), 2000 ));
	 
		button = new TButton(music_panel);
		button->SetCaption("Stop Song");
		button->SetPosition(270, 30);
		button->Resize(85, 25);
		button->SetBackgroundColor(TColor::green);
		button->SetTextSize(11);
		button->OnPress.connect(bind( slot(this, &Audio::StopSong), 0));
	 
		button = new TButton(music_panel);
		button->SetCaption("Song Volume Up");
		button->SetPosition(360, 0);
		button->Resize(85, 25);
		button->SetBackgroundColor(TColor::green);
		button->SetTextSize(11);
		button->OnPress.connect(bind( slot(this, &Audio::VolSong), 10 ));

		button = new TButton(music_panel);
		button->SetCaption("Song Volume Down");
		button->SetPosition(360, 30);
		button->Resize(85, 25);
		button->SetBackgroundColor(TColor::green);
		button->SetTextSize(11);
		button->OnPress.connect(bind( slot(this, &Audio::VolSong), -10 ));

		music_monitor = new AudioMonitor(music_panel);
		music_monitor->SetPosition( 450, 0 );
		music_monitor->Resize( 90, 60 );
		music_monitor->SetState(AudioMonitor::Stopped);

		// ---- Other Buttons
		button = new TButton(this);
		button->SetCaption("Quit");
		button->SetPosition(550, 400);
		button->Resize(85, 25);
		button->SetBackgroundColor(TColor::green);
		button->SetTextSize(11);
		button->OnPress.connect( slot(this, &Audio::ExitButton) );

		try {
			SamplePanel* samplepanel;
	    
			samplepanel = new SamplePanel( this, "../resrcs/sample1.wav" );
			samplepanel->SetPosition( 5, 110 );
	    
			samplepanel = new SamplePanel( this, "../resrcs/sample2.wav" );
			samplepanel->SetPosition( 5, 190 );
	    
			samplepanel = new SamplePanel( this, "../resrcs/sample3.wav" );
			samplepanel->SetPosition( 5, 270 );
		} catch(ksd::exception e) {
			e.report();
			Quit(130);
		}
	 
		LOG( 0, "Done with Init" );
	}

	void Shutdown() {
		FontList.Clear();
	}

	void KeyDown(const TKey& key) {
		switch(key.GetKeyCode()) {
			case SDLK_ESCAPE:
			case SDLK_q:
				Mixer.Close();
				Quit(0);
				break;
		}
	}

	void Cycle() {
	 	if(Mixer.IsMusicPlaying()) {
	 		music_monitor->SetState(AudioMonitor::Playing);
	 	} else if(Mixer.IsMusicPaused()) {
	 		music_monitor->SetState(AudioMonitor::Paused);
	 	} else if(Mixer.IsMusicStopped()) {
	 		music_monitor->SetState(AudioMonitor::Stopped);
	 	}
	}
	
	void PlaySong( TButton*, int fade ) {
		LOG_ENTER_FUNC(__FUNCTION__);
		Mixer.StartMusic( song, fade );
		LOG_EXIT_FUNC(__FUNCTION__);
	}
      
	void StopSong( TButton*, int fade ) {
		LOG_ENTER_FUNC(__FUNCTION__);
		Mixer.StopMusic( fade );
		LOG_EXIT_FUNC(__FUNCTION__);
	}

	void PauseSong( TButton* ) {
		Mixer.PauseMusic();
	}
	void RewindSong( TButton* ) {
		Mixer.RewindMusic();
	}
	void ResumeSong( TButton* ) {
		Mixer.ResumeMusic();
	}
	void VolSong( TButton*, int change ) {
		Mixer.SetMusicVolume( Mixer.GetMusicVolume() + change );
	}

	void ExitButton( TButton* ) {
		Quit(0);
	}

	TSong song;
	TFontList FontList;
	AudioMonitor* music_monitor;	
};

KSD_MAIN(Audio);

#endif // ENABLE_AUDIO



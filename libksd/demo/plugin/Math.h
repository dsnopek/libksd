//--------------------------------------------------------------------------- 
#ifndef MathH
#define MathH
//--------------------------------------------------------------------------- 
#include "PluginSys.h"

using namespace ksd;
//--------------------------------------------------------------------------- 
class Math {
public:
	virtual int Add(int, int) = 0;
	virtual int Sub(int, int) = 0;
};

char MathTypeName[] = "Math";
typedef TPluginSystem<Math, MathTypeName> MathPluginSystem;
//--------------------------------------------------------------------------- 
#endif

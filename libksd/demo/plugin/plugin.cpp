//--------------------------------------------------------------------------- 
#include <Application.h>
#include <iostream>
#include "Math.h"
#include "PluginSys.h"
#include "Plugin.h"

using namespace ksd;
using namespace std;
//--------------------------------------------------------------------------- 
static MathPluginSystem MathPlugins;
//---------------------------------------------------------------------------  
class PluginDemo : public TApplication {
public:
	void Init();
};
//---------------------------------------------------------------------------  
KSD_MAIN(PluginDemo);
//--------------------------------------------------------------------------- 
void PluginDemo::Init()
{
	Math* plugin = NULL;
	int X, Y, R;

	SetLogLevel(9);

	// load the plugin
	MathPlugins.LoadPluginLib("math");

	// get specific math module
	if(GetArgCount() >= 2) {
		plugin = MathPlugins.GetPlugin(std::string(GetArgList()[1]));

		if(!plugin) {
			cerr << "Unable to load requested plugin!!\n";
cerr << "Try setting the LD_LIBRARY_PATH variable to include '.' as a search path.\n";
			cerr << "In bash:\n\texport LD_LIBRARY_PATH=$LD_LIBRARY_PATH:.\n";
			
			Quit(1);
		}
	} else {
		cerr << "Must specify a plugin to load!!\n";
		cerr << "options: good_math or bad_math\n";
		cerr << "example: ./plugin good_math\n";
		
		Quit(1);
	}

	// perform mathimatical operations
	cout << "Addition --\nEnter first number: ";
	cin >> X;
	cout << "Enter second number: ";
	cin >> Y;

	R = plugin->Add(X, Y);
	cout << "Result = " << R << endl;

	cout << "Subtraction --\nEnter first number: ";
	cin >> X;
	cout << "Enter second number: ";
	cin >> Y;

	R = plugin->Sub(X, Y);
	cout << "Result = " << R << endl;
	
	Quit(0);
}
//--------------------------------------------------------------------------- 

//--------------------------------------------------------------------------- 
#define BUILD_LIB
#include "Math.h"
#include "Export.h"
//--------------------------------------------------------------------------- 
class GoodMath : public Math {
public:
	int Add(int X, int Y) {
		return X + Y;
	}

	int Sub(int X, int Y) {
		return X - Y;
	}
};
//--------------------------------------------------------------------------- 
class BadMath : public Math {
public:
	int Add(int X, int Y) {
		return ((X + Y) / 2) - 1;
	}

	int Sub(int X, int Y) {
		return ((X * Y) / 2) - 1;
	}
};  
//--------------------------------------------------------------------------- 
DO_EXPORT_PLUGIN_FUNC void PluginMath(MathPluginSystem* sys)
{
	sys->Register(new GoodMath, "good_math");
	sys->Register(new BadMath, "bad_math");
}
//--------------------------------------------------------------------------- 

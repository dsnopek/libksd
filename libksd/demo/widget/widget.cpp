//--------------------------------------------------------------------------- 
#include <iostream>
#include "ksd.h"
#include "Plugin.h"
#include "gui/Memo.h"
#include "gui/Panel.h"
#include "gui/Button.h"
#include "gui/RichLabel.h"
#include "gui/Dialog.h"
//#include "Edit.h"

using namespace ksd;
using namespace ksd_gui;
using namespace std;
//--------------------------------------------------------------------------- 
class WidgetDemo : public TApplication {
public:
	void Init();
	void Shutdown();
protected:
	void OnButton1Press(TButton*);
	void OnButton2Press(TButton*);
	void OnButton3Press(TButton*);
	void OnButton4Press(TButton*);
private:
	TMemo* Memo;
	TPanel* Panel;
	TRichLabel* Label;
	TDialog* AboutDialog;
	TButton** ButtonList;
	TImage MemoBackground, BtnImg;
	TFontList FontList;
};

KSD_MAIN(WidgetDemo);
//---------------------------------------------------------------------------  
static TColor ColorList[] = { 
	TColor::lightgrey,
	TColor::blue, 
	TColor::green, 
	TColor::red,
	//TColor::yellow // yellow doesn't work with current light/dark colors
};
static int ColorListSize = 4;
//---------------------------------------------------------------------------  
static char* BtnLabelList[] = {
	"Scroll Type",
	"Buffering",
	"Bevel",
	"Color"
};
static int ButtonCount = ColorListSize;
//--------------------------------------------------------------------------- 
void WidgetDemo::Init()
{
	SetCaption("widget");
	SetLogLevel(5);

	#ifndef KSD_NO_PLUGINS
	LoadPlugin("libimage");
	#endif

	CreateScreen(320, 200);
	FontList.LoadList("widget.fonts.conf");
	MemoBackground.Load("../resrcs/background.png");
	BtnImg.Load("../resrcs/check.gif");

	Keyboard.EnableUnicode(true);
	Keyboard.EnableKeyRepeat(true);
	SetBackgroundColor(TColor::blue);
	SetColor(TColor::white);
	SetFont(FontList.GetFont("Arial"));

#if 0
	// build the about dialog
	AboutDialog = new TDialog;
	AboutDialog->SetPosition(50, 50);
	AboutDialog->Resize(100, 100);
	AboutDialog->SetBackgroundColor(TColor::green);
#endif

	// build the panel
	Panel = new TPanel(this);
	Panel->SetPosition(25, 17);
	Panel->Resize(270, 165);
	Panel->SetBackgroundColor(TColor::red);
	Panel->SetScrollBoxColor(TColor::lightgrey);
	Panel->SetScrollBoxBackgroundColor(TColor::grey);

	// set-up scrolling parameters
	Panel->SetHorzScrollRange(200);
	Panel->SetHorzScrollIncrement(5);
	Panel->SetVertScrollRange(100);
	Panel->SetVertScrollIncrement(5);

	Memo = new TMemo(Panel);	
	Memo->SetPosition(5, 5);
	Memo->Resize(5, 5);
	Memo->Resize(155, 120);
	Memo->SetDrawBackgroundImage(true);
	Memo->SetBackgroundColor(TColor::black);
	Memo->SetBackgroundImage(&MemoBackground);

	Label = new TRichLabel(Panel);
	Label->SetPosition(300, 50);
	Label->Resize(100, 50);
	Label->SetText("Super secret hidden label!!");
	Label->SetTextAttr(TRichLabel::TextAttr(GetFont(), TFontAttr(TFont::Normal, 14, TColor::blue)), 6, 6);

	// create button list
	typedef TButton* ButtonPtr;
	ButtonList = new ButtonPtr[ButtonCount];
	
	// add buttons to the screen
	int btn_Y = 5;
	for(int i = 0; i < ButtonCount; i++) {
		ButtonList[i] = new TButton(Panel);
		ButtonList[i]->SetPosition(170, btn_Y);
		ButtonList[i]->Resize(85, 25);
		ButtonList[i]->SetBackgroundColor(ColorList[i]);
		ButtonList[i]->SetCaption(BtnLabelList[i]);
		ButtonList[i]->SetTextSize(11);
		ButtonList[i]->SetBorderless(i == 3);
		ButtonList[i]->SetImage(&BtnImg);

		btn_Y += 30;
	}

	// set individual button handlers
	ButtonList[0]->OnPress.connect(SigC::slot(*this, &WidgetDemo::OnButton1Press));
	ButtonList[1]->OnPress.connect(SigC::slot(*this, &WidgetDemo::OnButton2Press));
	ButtonList[2]->OnPress.connect(SigC::slot(*this, &WidgetDemo::OnButton3Press));
	ButtonList[3]->OnPress.connect(SigC::slot(*this, &WidgetDemo::OnButton4Press));

	// shows the user the cursor -- removes dummy error
	SetKeyboardFocus(Memo);
}
//--------------------------------------------------------------------------- 
void WidgetDemo::Shutdown()
{
	FontList.Clear();
}
//---------------------------------------------------------------------------  
void WidgetDemo::OnButton1Press(TButton*)
{
	// change scroll type
	switch(Panel->GetScrollType()) {
		case TScrollBox::NoScrollControls:
			Panel->SetScrollType(TScrollBox::UseScrollers);
			break;
		case TScrollBox::UseScrollers:
			Panel->SetScrollType(TScrollBox::UseScrollBars);
			break;
		case TScrollBox::UseScrollBars:
			Panel->SetScrollType(TScrollBox::NoScrollControls);
			break;
	}
}
//---------------------------------------------------------------------------  
void WidgetDemo::OnButton2Press(TButton*)
{
	// try to really screw up the widgets !!
	Panel->SetDoubleBuffered(!Panel->GetDoubleBuffered()); // temp
}
//---------------------------------------------------------------------------  
void WidgetDemo::OnButton3Press(TButton*)
{
	// change bevel type
	switch(Panel->GetBevelType()) {
		// NOTE: in the default theme thick is the default
		case TBevel::Default:
		case TBevel::Thick:
			Panel->SetBevelType(TBevel::Thin);
			break;
		case TBevel::Thin:
			Panel->SetBevelType(TBevel::Lowered);
			break;
		case TBevel::Lowered:
			Panel->SetBevelType(TBevel::Raised);
			break;
		case TBevel::Raised:
			Panel->SetBevelType(TBevel::None);
			break;
		case TBevel::None:
			Panel->SetBevelType(TBevel::Thick);
			break;
	}
}
//---------------------------------------------------------------------------  
void WidgetDemo::OnButton4Press(TButton* Button)
{
	static int CurrentColor = ColorListSize;

	// get next color
	CurrentColor++;
	if(CurrentColor >= ColorListSize)
		CurrentColor = 0;
		
	Button->SetBackgroundColor(ColorList[CurrentColor]);

	//AboutDialog->Show();
}
//--------------------------------------------------------------------------- 

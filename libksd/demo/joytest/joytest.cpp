//--------------------------------------------------------------------------- 
#include <iostream>
#include "ksd.h"
#include "log.h"
#include "Image.h"
#include "Rect.h"
//#include "Clo.hh"

// VERY, VERY broken
#if 0

using namespace ksd;
using namespace std;
//--------------------------------------------------------------------------- 
#define JOY_INCREMENT			1
#define POINTER_START_X			75
#define POINTER_START_Y			70
//--------------------------------------------------------------------------- 
const TPoint2D BtnPos[] = { TPoint2D(35, 20), TPoint2D(35, 70), TPoint2D(35, 120),
							TPoint2D(35, 170) };
//--------------------------------------------------------------------------- 
class JoytestDemo : public TApplication {
public:
	void Init();
	void Shutdown();
protected:
	int X, Y, old_X, old_Y;
	TColor Color;

	void CreateBtns(int id);
	void CheckCollisions();
	void DrawSketchPad(TCanvas* Canvas);

	void JoyAxisMove(Uint8 joy, Uint8 axis, Sint16 value);
	void JoyDown(Uint8 joy, Uint8 button);
	void JoyUp(Uint8 joy, Uint8 button);
	void KeyDown(const TKey& key);
	bool Draw(TCanvas* Canvas, DrawType dt);
private:
	TImage Image, BackBuffer;
	TShape2D* Pointer, *RedBtn, *GreenBtn, *BlueBtn, *ShapeBtn;
	bool JoyPressed;
};
//--------------------------------------------------------------------------- 
KSD_MAIN(JoytestDemo);
//--------------------------------------------------------------------------- 
void JoytestDemo::Init()
{
	SetLogOptions(logger::LOG_TO_EXPLICIT_PATH | logger::LOG_TO_STANDARD_OUTPUT 
				  | logger::SHOW_MODULE_NAME | logger::SHOW_NONE); 
	SetLogLevel(5); 

	// parse command line
	Clo::Parser clo;
	try {
		clo.parse(GetArgCount(), GetArgValue());
	} catch (Clo::Exception e) {
		// deal with help
		if(e.autohelp) {
			cout << "Usage: " << GetArgValue()[0] << " [options]" << endl;
			cout << e.fancy();
			Exit(0); return; // TODO: fix exit
		} else if(e.autoversion) {
			cout << "The joytest demo included with libksd version " << KSD_VERSION << endl;
			Exit(0); return; // TODO: fix exit
		} else {
			cerr << e.fancy();
			Exit(1); return; // TODO: fix exit
		}
	}

	JoyPressed = false;
	old_X = POINTER_START_X;
	old_Y = POINTER_START_Y;

	CreateScreen(320, 200, clo.get_int_option("bpp"), 
		clo.get_flag_option("fullscreen") ? TScreen::FULLSCREEN : TScreen::DEFAULT);
	
	SetPaintType(FullUpdate);
	SetCaption("JoyTest");
	SetDrawOnDemand(true);
	SetBackgroundColor(TColor::grey);

	BackBuffer.Create(320, 200);
	BackBuffer->GetCanvas()->Pen.Color = TColor::red;
	BackBuffer->GetCanvas()->Brush.Color = TColor::red;
	
	Image.Create(200, 160);
	Image.Fill(TColor::blue);
	Image.GetCanvas()->Brush.Color = TColor::green;
	
	Pointer = RedBtn = GreenBtn = BlueBtn = ShapeBtn = NULL;
	CreateBtns(SHAPE_CIRCLE_ID);
	Color = TColor::green;
}
//--------------------------------------------------------------------------- 
void JoytestDemo::Shutdown()
{
	if(Pointer) {
		delete Pointer;
	}
	if(ShapeBtn) {
		delete ShapeBtn;
	}
	if(RedBtn) {
		delete RedBtn;
	}
	if(GreenBtn) {
		delete GreenBtn;
	}
	if(BlueBtn) {
		delete BlueBtn;
	}
}
//--------------------------------------------------------------------------- 
void JoytestDemo::CreateBtns(int id)
{
	// destroy current shapes
	if(Pointer) {
		delete Pointer;
	}
	if(ShapeBtn) {
		delete ShapeBtn;
	}
	if(RedBtn) {
		delete RedBtn;
	}
	if(GreenBtn) {
		delete GreenBtn;
	}
	if(BlueBtn) {
		delete BlueBtn;
	}

	// reset X, Y so you don't accidentally togle between types
	X = POINTER_START_X;
	Y = POINTER_START_Y;

	// re-create in proper shapes
	if(id == SHAPE_RECT_ID) {
		Pointer = new TRect(-15, -15, 15, 15);
		ShapeBtn = new TCircle(0, 0, 15);
		RedBtn = new TRect(-15, -15, 15, 15);
		GreenBtn = new TRect(-15, -15, 15, 15);
		BlueBtn = new TRect(-15, -15, 15, 15);
	} else if(id == SHAPE_CIRCLE_ID) {
		Pointer = new TCircle(0, 0, 15);
		ShapeBtn = new TRect(-15, -15, 15, 15);
		RedBtn = new TCircle(0, 0, 15);
		GreenBtn = new TCircle(0, 0, 15);
		BlueBtn = new TCircle(0, 0, 15);
	}

	// move to proper locations
	Pointer->Move(X, Y);
	ShapeBtn->Move(BtnPos[0].X, BtnPos[0].Y);
	RedBtn->Move(BtnPos[1].X, BtnPos[1].Y);
	GreenBtn->Move(BtnPos[2].X, BtnPos[2].Y);
	BlueBtn->Move(BtnPos[3].X, BtnPos[3].Y);
}
//--------------------------------------------------------------------------- 
void JoytestDemo::CheckCollisions()
{
	if(Pointer->Collision(ShapeBtn)) {
		CreateBtns(ShapeBtn->GetClassID());
		NeedsRepaint();
	}
	else {
		if(Pointer->Collision(RedBtn)) {
			Color = TColor::red;
			NeedsRepaint();
		}
		if(Pointer->Collision(GreenBtn)) {
			Color = TColor::green;
			NeedsRepaint();
		}
		if(Pointer->Collision(BlueBtn)) {
			Color = TColor::blue;
			NeedsRepaint();
		}
		
		Image.GetCanvas()->Brush.Color = Color;
	}
}
//--------------------------------------------------------------------------- 
void JoytestDemo::DrawSketchPad(TCanvas* Canvas)
{
	// clip arround sketch area
	Canvas->ClipRect.Set(100, 20, 300, 180);

	// draw sketch area
	Canvas->Mode = OP_REPLACE;
	Canvas->Blit(100, 20, Image);
	Canvas->FrameRect(100, 20, 300, 180);	

	Canvas->Mode = OP_XOR;
	Pointer->MoveTo(old_X, old_Y);
	Pointer->Draw(Canvas, true);

	// kill clipp rect
	Canvas->ClipRect.Set(0, 0, 0, 0);
}
//--------------------------------------------------------------------------- 
void JoytestDemo::JoyDown(Uint8 joy, Uint8 button)
{
	Pointer->Move(-100, -20);
	Pointer->Draw(Image.GetCanvas(), true);
	Pointer->Move(100, 20);
	DrawSketchPad(BackBuffer.GetCanvas());

	JoyPressed = true;
}
//--------------------------------------------------------------------------- 
void JoytestDemo::JoyUp(Uint8 joy, Uint8 button)
{
	JoyPressed = false;
}
//--------------------------------------------------------------------------- 
void JoytestDemo::JoyAxisMove(Uint8 joy, Uint8 axis, Sint16 value)
{
	int temp;

	if(axis == 0) {
		temp = (value / 32767) * JOY_INCREMENT;
		Pointer->Move(temp, 0);
		X += temp;
	} else if(axis == 1) {
		temp = (value / 32767) * JOY_INCREMENT;
		Pointer->Move(0, temp);
		Y += temp;
	}

	NeedsUpdate();

	if(JoyPressed) {
		Pointer->Move(-100, -20);
		Pointer->Draw(Image.GetCanvas(), true);
		Pointer->Move(100, 20);
		DrawSketchPad(BackBuffer.GetCanvas());
	}

	CheckCollisions();
}
//--------------------------------------------------------------------------- 
void JoytestDemo::KeyDown(const TKey& key)
{
	if(key.GetKeyCode() == SDLK_ESCAPE) {
		Exit(0);
	}
}
//--------------------------------------------------------------------------- 
bool JoytestDemo::Draw(TCanvas* Canvas, DrawType dt)
{
	if(dt == REPAINT) {
		TCanvas* BufferCanvas = BackBuffer.GetCanvas();

		// make sure we aren't XOR'ing
		BufferCanvas->Mode = OP_REPLACE;
		
		// draw btns
		BufferCanvas->Brush.Color = Color;
		ShapeBtn->Draw(BufferCanvas, true);

		BufferCanvas->Brush.Color = TColor::red;
		RedBtn->Draw(BufferCanvas, true);

		BufferCanvas->Brush.Color = TColor::green;
		GreenBtn->Draw(BufferCanvas, true);

		BufferCanvas->Brush.Color = TColor::blue;
		BlueBtn->Draw(BufferCanvas, true);

		// draw sketch area
		BufferCanvas->Blit(100, 20, Image);
		BufferCanvas->FrameRect(100, 20, 300, 180);

		// copy back buffer to front
		Canvas->Blit(0, 0, BackBuffer);

		Canvas->Mode = OP_XOR;
		Pointer->MoveTo(old_X, old_Y);
		Pointer->Draw(Canvas, true);
	}

	// draw pointer
	//  -- trying to use exclusive or'ing, so only the part that needs drawing
	// is drawn
	Canvas->Mode = OP_XOR;
	Pointer->MoveTo(old_X, old_Y);
	Pointer->Draw(Canvas, true);
	Pointer->MoveTo(X, Y);
	Pointer->Draw(Canvas, true);
	old_X = X;
	old_Y = Y;
	Canvas->Mode = OP_REPLACE;

	return true;
}
//--------------------------------------------------------------------------- 
//
// temporary main!
// 
#else
using namespace std;
extern "C" int main(int argc, char* argv[])
{
	cout << "A lot of the techniques used by this demo are no longer valid due to "
	     << "new APIs.  The functionality can definitely be re-implemented, but"
	     << "as of now, has not." << endl;
	return 0;
}
#endif



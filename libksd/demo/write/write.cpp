//--------------------------------------------------------------------------- 
#include <iostream>
#include <algorithm>
#include "ksd.h"
#include "Plugin.h"
#include "gui/RichMemo.h"
#include "gui/Panel.h"
#include "gui/Button.h"

using namespace ksd;
using namespace ksd_gui;
using namespace std;
//--------------------------------------------------------------------------- 
class WriteDemo : public TApplication {
public:
	void Init();
	void Shutdown();
protected:
	static const Uint8 		SizeList[];
	static const int 		SizeCount;
	static const TColor 	ColorList[];
	static const int 		ColorCount;
	static const char* 		FontNameList[];
	static const int 		FontCount;
	static const int 		ButtonCount;

	// event handlers
	void OnSizeIncreasePress(TButton* btn);
	void OnSizeDecreasePress(TButton* btn);
	void OnColorChangePress(TButton* btn);
	void OnFontChangePress(TButton* btn);

	// text modify functions
	void IncreaseTextSize(TRichMemo::TextAttr& attr) { 
		const Uint8* i = find(&SizeList[0], &SizeList[SizeCount - 1], attr.FontAttr.Size);
		if(i < &SizeList[SizeCount - 1]) {
			attr.FontAttr.Size = *(++i); 
		}
	}
	void DecreaseTextSize(TRichMemo::TextAttr& attr) { 
		const Uint8* i = find(&SizeList[0], &SizeList[SizeCount - 1], attr.FontAttr.Size);
		if(i > &SizeList[0] && i <= &SizeList[SizeCount - 1]) {
			attr.FontAttr.Size = *(--i); 
		}
	}
	void ChangeTextColor(TRichMemo::TextAttr& attr) {
		const TColor* i = find(&ColorList[0], &ColorList[ColorCount - 1], attr.FontAttr.Color);
		if(i >= &ColorList[ColorCount - 1]) attr.FontAttr.Color = ColorList[0];
		else {
			attr.FontAttr.Color = *(++i);
		}
	}
	void ChangeTextFont(TRichMemo::TextAttr& attr) {
		string FontName = FontList.GetFontName(attr.Font);
		int cur_index = -1;

		// find current font
		for(int i = 0; i < FontCount; i++) {
			if(string(FontNameList[i]) == FontName) {
				cur_index = i;
				break;
			}
		}

		// set new font
		if(cur_index != -1) {
			cur_index++;
			if(cur_index >= FontCount) cur_index = 0;
			attr.Font = FontList.GetFont(FontNameList[cur_index]);
		}
	}
private:
	TRichMemo* Memo;
	TPanel* Panel;
	TButton** ButtonList;
	TFontList FontList;
	TImage OpenImage, SaveImage, IncSizeImage, DecSizeImage, ColorImage, FontImage;
};

KSD_MAIN(WriteDemo);
//---------------------------------------------------------------------------  
const Uint8 	WriteDemo::SizeList[] = { 8, 10, 12, 14, 18, 24, 36, 48, 72 };
const int 		WriteDemo::SizeCount = sizeof(SizeList);
const TColor	WriteDemo::ColorList[] = { 
	TColor::black,
	TColor::red,
	TColor::green,
	TColor::blue,
	TColor::purple,
	TColor::brown,
	TColor::yellow,
	TColor::grey,
	TColor::orange
};
const int		WriteDemo::ColorCount = sizeof(ColorList);
#if defined(KSD_USE_SFONT) && defined(KSD_USE_TTF)
const char*		WriteDemo::FontNameList[] = { "Arial", "Arial_BM", "Special", "Special_BM" };
#else
const char*		WriteDemo::FontNameList[] = { "Arial", "Special" };
#endif
const int		WriteDemo::FontCount = sizeof(FontNameList);
const int 		WriteDemo::ButtonCount = 6;
//--------------------------------------------------------------------------- 
void WriteDemo::Init()
{
	SetCaption("write");
	//SetLogLevel(9);

	#ifndef KSD_NO_PLUGINS
	LoadPlugin("libimage");
	#endif

	CreateScreen(320, 200);
	FontList.LoadList("write.fonts.conf");
	OpenImage.Load("../resrcs/open.png");
	SaveImage.Load("../resrcs/save.png");
	IncSizeImage.Load("../resrcs/increase_size.png");
	DecSizeImage.Load("../resrcs/decrease_size.png");
	ColorImage.Load("../resrcs/color_change.png");
	FontImage.Load("../resrcs/font_change.png");

	Keyboard.EnableUnicode(true);
	Keyboard.EnableKeyRepeat(true);
	SetBackgroundColor(TColor::white);
	SetColor(TColor::black);
	SetFont(FontList.GetFont("Arial"));

	Panel = new TPanel(this);
	Panel->SetPosition(0, 0);
	Panel->Resize(320, 35);
	Panel->SetBackgroundColor(TColor::grey70);
	Panel->SetBevelType(TBevel::Raised);

	Memo = new TRichMemo(this);	
	Memo->SetPosition(0, 35);
	Memo->Resize(320, 165);
	Memo->SetBevelType(TBevel::None);
	
	// create button list
	typedef TButton* ButtonPtr;
	ButtonList = new ButtonPtr[ButtonCount];
	
	// add buttons to the screen
	int btn_X = 5;
	for(int i = 0; i < ButtonCount; i++) {
		// make button groupings
		if(i == 2) btn_X += 5;
	
		ButtonList[i] = new TButton(Panel);
		ButtonList[i]->SetPosition(btn_X, 3);
		ButtonList[i]->Resize(25, 25);

		btn_X += 25;
	}

	// set button images
	ButtonList[0]->SetImage(&OpenImage);
	ButtonList[1]->SetImage(&SaveImage);
	ButtonList[2]->SetImage(&IncSizeImage);
	ButtonList[3]->SetImage(&DecSizeImage);
	ButtonList[4]->SetImage(&ColorImage);
	ButtonList[5]->SetImage(&FontImage);

	// set individual button handlers
	//ButtonList[0]->OnPress.connect(SigC::slot(this, &WriteDemo::OnButton1Press));
	//ButtonList[1]->OnPress.connect(SigC::slot(this, &WriteDemo::OnButton2Press));
	ButtonList[2]->OnPress.connect(SigC::slot(this, &WriteDemo::OnSizeIncreasePress));
	ButtonList[3]->OnPress.connect(SigC::slot(this, &WriteDemo::OnSizeDecreasePress));
	ButtonList[4]->OnPress.connect(SigC::slot(this, &WriteDemo::OnColorChangePress));
	ButtonList[5]->OnPress.connect(SigC::slot(this, &WriteDemo::OnFontChangePress));

	// shows the user the cursor -- removes dummy error
	SetKeyboardFocus(Memo);
}
//--------------------------------------------------------------------------- 
void WriteDemo::Shutdown()
{
	if(ButtonList)
		delete[] ButtonList;
	
	FontList.Clear();
}
//---------------------------------------------------------------------------  
void WriteDemo::OnSizeIncreasePress(TButton* btn)
{
	Memo->ChangeTextAttr(slot(this, &WriteDemo::IncreaseTextSize));
	SetKeyboardFocus(Memo);
}
//---------------------------------------------------------------------------  
void WriteDemo::OnSizeDecreasePress(TButton* btn)
{
	Memo->ChangeTextAttr(slot(this, &WriteDemo::DecreaseTextSize));
	SetKeyboardFocus(Memo);
}
//---------------------------------------------------------------------------  
void WriteDemo::OnColorChangePress(TButton* btn)
{
	Memo->ChangeTextAttr(slot(this, &WriteDemo::ChangeTextColor));
	SetKeyboardFocus(Memo);
}
//---------------------------------------------------------------------------  
void WriteDemo::OnFontChangePress(TButton* btn)
{
	Memo->ChangeTextAttr(slot(this, &WriteDemo::ChangeTextFont));
	SetKeyboardFocus(Memo);
}
//---------------------------------------------------------------------------  

